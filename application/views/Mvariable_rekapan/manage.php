<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mvariable_rekapan" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mvariable_rekapan/save','class="form-horizontal push-10-t" id="form-work"') ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idtipe">Tipe</label>
			<div class="col-md-7">
				<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($idtipe == 1 ? 'selected' : '')?>>Pengeluaran</option>
					<option value="2" <?=($idtipe == 2 ? 'selected' : '')?>>Pengeluaran + Pendapatan</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idsub">Status</label>
			<div class="col-md-7">
				<select name="idsub" id="idsub" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="0" <?=($idsub == 0 ? 'selected' : '')?>>Non Sub</option>
					<option value="1" <?=($idsub == 1 ? 'selected' : '')?>>Sub</option>
				</select>
			</div>
		</div>
	</div>

	<div class="block-content">
		<div class="block">

			<div class="block-content" id="groupSub" style="<?=($idsub == 0 ? 'display: none;' :'')?> border-radius: 2px; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);">
				<h6>Sub Data</h6>
				<hr>
				<table class="table table-bordered table-striped" id="subDataList">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Aksi</th>
						</tr>
						<tr>
							<th>
								<input type="text" class="form-control" id="namasub" placeholder="Nama Sub" value="">
							</th>
							<th>
								<button id="addSub" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($list_sub as $row){ ?>
							<tr>
								<td><?=$row->nama?><td>
								<td>
									<div class="btn btn-group">
										<a href="#" class="btn btn-primary btn-sm editSub" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
										<a href="#" class="btn btn-danger btn-sm removeSub" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>
									</div>
								<td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>

			<br>

			<div class="block-content" id="groupPengeluaran" style="border-radius: 2px; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);">
				<h6>Pengeluaran</h6>
				<div class="row">
					<div class="col-md-6">
						<hr>
						<span class="badge badge-success">Debit</span>
						<hr>
						<div class="table-responsive">
						<table class="table table-bordered table-striped" id="pengeluaranDebitList">
							<thead>
								<tr>
									<th>No. Akun</th>
									<th>Aksi</th>
								</tr>
								<tr>
									<th>
										<select id="noakunPengeluaranDebit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
											<?php foreach(get_all('makun_nomor') as $row){ ?>
												<option value="<? echo $row->noakun ?>"><?php echo $row->noakun.' '.$row->namaakun; ?></option>
											<?php } ?>
										</select>
									</th>
									<th>
										<button id="addPengeluaranDebit" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($list_akun_pengeluaran_debit as $row){ ?>
									<tr>
										<td style="display:none"><?=$row->noakun?></td>
										<td><?=$row->noakun.' '.$row->namaakun?></td>
										<td>
											<div class="btn btn-group">
												<a href="#" class="btn btn-primary btn-sm editAkun" data-tipe="<?=$row->tipevariable.''.$row->tipeakun?>" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-sm removeAkun" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					</div>
					<div class="col-md-6">
						<hr>
						<span class="badge badge-danger">Kredit</span>
						<hr>
						<div class="table-responsive">
						<table class="table table-bordered table-striped" id="pengeluaranKreditList">
							<thead>
								<tr>
									<th>No. Akun</th>
									<th>Aksi</th>
								</tr>
								<tr>
									<th>
										<select id="noakunPengeluaranKredit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
											<?php foreach(get_all('makun_nomor') as $row){ ?>
												<option value="<? echo $row->noakun ?>"><?php echo $row->noakun.' '.$row->namaakun; ?></option>
											<?php } ?>
										</select>
									</th>
									<th>
										<button id="addPengeluaranKredit" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($list_akun_pengeluaran_kredit as $row){ ?>
									<tr>
										<td style="display:none"><?=$row->noakun?></td>
										<td><?=$row->noakun.' '.$row->namaakun?></td>
										<td>
											<div class="btn btn-group">
												<a href="#" class="btn btn-primary btn-sm editAkun" data-tipe="<?=$row->tipevariable.''.$row->tipeakun?>" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-sm removeAkun" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					</div>
				</div>
			</div>

			<br>

			<div class="block-content" id="groupPendapatan" style="<?=($idtipe == 1 ? 'display: none;' :'')?> border-radius: 2px; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);">
				<h6>Pendapatan</h6>
				<div class="row">
					<div class="col-md-6">
						<hr>
						<span class="badge badge-success">Debit</span>
						<hr>
						<div class="table-responsive">
						<table class="table table-bordered table-striped" id="pendapatanDebitList">
							<thead>
								<tr>
									<th>No. Akun</th>
									<th>Aksi</th>
								</tr>
								<tr>
									<th>
										<select id="noakunPendapatanDebit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
											<?php foreach(get_all('makun_nomor') as $row){ ?>
												<option value="<? echo $row->noakun ?>"><?php echo $row->noakun.' '.$row->namaakun; ?></option>
											<?php } ?>
										</select>
									</th>
									<th>
										<button id="addPendapatanDebit" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($list_akun_pendapatan_debit as $row){ ?>
									<tr>
										<td style="display:none"><?=$row->noakun?></td>
										<td><?=$row->noakun.' '.$row->namaakun?></td>
										<td>
											<div class="btn btn-group">
												<a href="#" class="btn btn-primary btn-sm editAkun" data-tipe="<?=$row->tipevariable.''.$row->tipeakun?>" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-sm removeAkun" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					</div>
					<div class="col-md-6">
						<hr>
						<span class="badge badge-danger">Kredit</span>
						<hr>
						<div class="table-responsive">
						<table class="table table-bordered table-striped" id="pendapatanKreditList">
							<thead>
								<tr>
									<th>No. Akun</th>
									<th>Aksi</th>
								</tr>
								<tr>
									<th>
										<select id="noakunPendapatanKredit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
											<?php foreach(get_all('makun_nomor') as $row){ ?>
												<option value="<? echo $row->noakun ?>"><?php echo $row->noakun.' '.$row->namaakun; ?></option>
											<?php } ?>
										</select>
									</th>
									<th>
										<button id="addPendapatanKredit" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($list_akun_pendapatan_kredit as $row){ ?>
									<tr>
										<td style="display:none"><?=$row->noakun?></td>
										<td><?=$row->noakun.' '.$row->namaakun?></td>
										<td>
											<div class="btn btn-group">
												<a href="#" class="btn btn-primary btn-sm editAkun" data-tipe="<?=$row->tipevariable.''.$row->tipeakun?>" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-sm removeAkun" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row" style="margin-top:-25px">
		<div class="block-content block-content-narrow">
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mvariable_rekapan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="rowindex" value="">
	<input type="hidden" id="subDataListValue" name="subdatalist_value" value="">
	<input type="hidden" id="pengeluaranDebitListValue" name="pengeluarandebit_list_value" value="">
	<input type="hidden" id="pengeluaranKreditListValue" name="pengeluarankredit_list_value" value="">
	<input type="hidden" id="pendapatanDebitListValue" name="pendapatandebit_list_value" value="">
	<input type="hidden" id="pendapatanKreditListValue" name="pendapatankredit_list_value" value="">

	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$(document).on("change", "#idtipe", function() {
			if($(this).val() == 1){
				$("#groupPendapatan").hide();
			}else{
				$("#groupPendapatan").show();
			}
		});

		$(document).on("change", "#idsub", function() {
			if($(this).val() == 0){
				$("#groupSub").hide();
			}else{
				$("#groupSub").show();
			}
		});

		// Sub Content Group
		$(document).on("click", "#addSub", function() {
			var rowIndex;
			var duplicate = false;

			if($("#rowindex").val() != ''){
				var content = "";
				rowIndex = $("#rowindex").val();
			}else{
				var content = "<tr>";
				$('#subDataList tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(0).text() === $("#namasub").val()){
						sweetAlert("Maaf...", "Nama Sub " + $("#namasub").val() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}
				});
			}

			if(duplicate == false){
				content += "<td>" + $("#namasub").val() + "</td>";
				content += "<td>";
				content += "<div class='btn btn-group'>";
				content += "<a href='#' class='btn btn-primary btn-sm editSub' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
				content += "<a href='#' class='btn btn-danger btn-sm removeSub' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
				content += "</div>";

				if($("#rowindex").val() != ''){
					$('#subDataList tbody tr:eq(' + rowIndex + ')').html(content);
				}else{
					content += "</tr>";
					$('#subDataList tbody').append(content);
				}

				$("#rowindex").val(null);
				$("#namasub").val(null);
			}
		});

		$(document).on("click", ".editSub", function() {
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#namasub").val($(this).closest('tr').find("td:eq(0)").html());

			return false;
		});

		$(document).on("click", ".removeSub", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$(document).on("click", "#addPengeluaranDebit", function() {
			addAkun('1', 'D');
		});

		$(document).on("click", "#addPengeluaranKredit", function() {
			addAkun('1', 'K');
		});

		$(document).on("click", "#addPendapatanDebit", function() {
			addAkun('2', 'D');
		});

		$(document).on("click", "#addPendapatanKredit", function() {
			addAkun('2', 'K');
		});

		$(document).on("click", ".editAkun", function() {
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);

			tipe = $(this).data('tipe');
			value = $(this).closest('tr').find("td:eq(0)").html();

			if(tipe == '1D'){
				$("#noakunPengeluaranDebit").val(value).trigger('change');
			}else if(tipe == '1K'){
				$("#noakunPengeluaranKredit").val(value).trigger('change');
			}else if(tipe == '2D'){
				$("#noakunPendapatanDebit").val(value).trigger('change');
			}else if(tipe == '2K'){
				$("#noakunPendapatanKredit").val(value).trigger('change');
			}

			return false;
		});

		$(document).on("click", ".removeAkun", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$("#form-work").submit(function(e) {
			var form = this;

			var subDataListValue_tbl = $('table#subDataList tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			var pengeluaranDebitListValue_tbl = $('table#pengeluaranDebitList tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			var pengeluaranKreditListValue_tbl = $('table#pengeluaranKreditList tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			var pendapatanDebitListValue_tbl = $('table#pendapatanDebitList tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			var pendapatanKreditListValue_tbl = $('table#pendapatanKreditList tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			$("#subDataListValue").val(JSON.stringify(subDataListValue_tbl));
			$("#pengeluaranDebitListValue").val(JSON.stringify(pengeluaranDebitListValue_tbl));
			$("#pengeluaranKreditListValue").val(JSON.stringify(pengeluaranKreditListValue_tbl));
			$("#pendapatanDebitListValue").val(JSON.stringify(pendapatanDebitListValue_tbl));
			$("#pendapatanKreditListValue").val(JSON.stringify(pendapatanKreditListValue_tbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});

	function addAkun(idtipevariable, idtipeakun){
		var tipe;
		var table;
		var option;
		var contentAppend;
		var contentReplace;
		var rowIndex;
		var duplicate = false;

		if(idtipevariable == '1'){
			if(idtipeakun == 'D'){
				tipe = '1D';
				table = $('#pengeluaranDebitList tbody tr');
				option = $("#noakunPengeluaranDebit option:selected");
				contentAppend = $('#pengeluaranDebitList tbody');
				if($("#rowindex").val() != ''){
					rowIndex = $("#rowindex").val();
					contentReplace = $('#pengeluaranDebitList tbody tr:eq(' + rowIndex + ')');
				}
			}else if(idtipeakun == 'K'){
				tipe = '1K';
				table = $('#pengeluaranKreditList tbody tr');
				option = $("#noakunPengeluaranKredit option:selected");
				contentAppend = $('#pengeluaranKreditList tbody');
				if($("#rowindex").val() != ''){
					rowIndex = $("#rowindex").val();
					contentReplace = $('#pengeluaranKreditList tbody tr:eq(' + rowIndex + ')');
				}
			}
		}else if(idtipevariable == '2'){
			if(idtipeakun == 'D'){
				tipe = '2D';
				table = $('#pendapatanDebitList tbody tr');
				option = $("#noakunPendapatanDebit option:selected");
				contentAppend = $('#pendapatanDebitList tbody');
				if($("#rowindex").val() != ''){
					rowIndex = $("#rowindex").val();
					contentReplace = $('#pendapatanDebitList tbody tr:eq(' + rowIndex + ')');
				}
			}else if(idtipeakun == 'K'){
				tipe = '2K';
				table = $('#pendapatanKreditList tbody tr');
				option = $("#noakunPendapatanKredit option:selected");
				contentAppend = $('#pendapatanKreditList tbody');
				if($("#rowindex").val() != ''){
					rowIndex = $("#rowindex").val();
					contentReplace = $('#pendapatanKreditList tbody tr:eq(' + rowIndex + ')');
				}
			}
		}

		if($("#rowindex").val() != ''){
			var content = "";
			rowIndex = $("#rowindex").val();
		}else{
			var content = "<tr>";

			table.filter(function (){
				var $cells = $(this).children('td');
				if($cells.eq(0).text() === option.val()){
					sweetAlert("Maaf...", "Nama Akun " + option.text() + " sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if(duplicate == false){
			content += "<td style='display:none'>" + option.val() + "</td>";
			content += "<td>" + option.text() + "</td>";
			content += "<td>";
			content += "<div class='btn btn-group'>";
			content += "<a href='#' class='btn btn-primary btn-sm editAkun' data-tipe='" + tipe + "' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
			content += "<a href='#' class='btn btn-danger btn-sm removeAkun' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "</div>";

			if($("#rowindex").val() != ''){
				contentReplace.html(content);
			}else{
				content += "</tr>";
				contentAppend.append(content);
			}

			$("#rowindex").val(null);
			$("#noakunPendapatanDebit").val(null).trigger('change');
			$("#noakunPendapatanKredit").val(null).trigger('change');
			$("#noakunPengeluaranDebit").val(null).trigger('change');
			$("#noakunPengeluaranKredit").val(null).trigger('change');
		}
	}
</script>
