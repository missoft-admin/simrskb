<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>
<div class="block push-10">
	<div class="block-content bg-primary">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<input type="hidden" id="st_login" name="st_login" value="{st_login}">
			<div class="row pull-10">
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="mppa_id">Nama</label>
						<div class="col-xs-12">
							<select id="mppa_id" disabled name="mppa_id"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($mppa_id=='#'?'selected':'')?>>- Semua -</option>
								<?foreach($list_ppa as $r){?>
								<option value="<?=$r->id?>" <?=($mppa_id==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
				</div>
				
				<div class="col-md-1 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="st_ranap">Asal Pasien</label>
						<div class="col-xs-12">
							<select id="st_ranap" name="st_ranap"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($st_ranap=='#'?'selected':'')?>>- Semua -</option>
								<option value="1" <?=($st_ranap=='1'?'selected':'')?>>Rawat Inap</option>
								<option value="0" <?=($st_ranap=='0'?'selected':'')?>>Poliklinik</option>
								
							</select>
						</div>
					</div>
				</div>
				
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="st_ranap">Tanggal Pendaftaran</label>
						<div class="col-xs-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_1" placeholder="From" value="{tanggal_1}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_2" placeholder="To" value="{tanggal_2}">
                            </div>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="st_ranap">Tanggal Permintaan</label>
						<div class="col-xs-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_permintaan_1" placeholder="From" value="{tanggal_permintaan_1}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_permintaan_2" placeholder="To" value="{tanggal_permintaan_2}">
                            </div>
						</div>
					</div>
				</div>
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="st_ranap">Search</label>
						<div class="col-xs-12">
							<input type="text" class="form-control" id="pencarian" placeholder="No. Medrec | Nama Pasien" value="">
						</div>
					</div>
				</div>
				
				<div class="col-md-1 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="st_ranap">&nbsp;&nbsp;</label>
						<div class="col-xs-12">
							<span class="input-group-btn ">
								<button class="btn btn-warning btn-block " id="btn_cari" type="button" title="Login"><i class="fa fa-search pull-left"></i> Cari</button>&nbsp;
							</span>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
	</div>
</div>

<div class="block">
	<ul class="nav nav-pills">
		<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Menunggu Verifikasi</a>
		</li>
		<li  id="div_2" class="<?=($tab=='2'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-check"></i> Telah Diverifikasi </a>
		</li>
		
	</ul>
	<div class="block-content">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table" id="index_all">
							<thead>
								<tr>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		
	
	</div>
</div>
<div class="modal in" id="modal_pelaksanaan_lihat" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PELAKSANAAN S B A R</h3>
				</div>
				<div class="block-content">
					<input type="hidden" id="assesmen_id_pelaksanaan" value="" >		
					<div class="row">
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="recomendation">User Pelaksanaan</label>
									<div class="input-group date">
										<input type="text" class="form-control" disabled id="user_pelaksanaan_lihat" value="">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="recomendation">Waktu Pelaksanaan</label>
									<div class="input-group date">
										<input type="text" class="form-control" disabled data-date-format="dd/mm/yyyy" id="waktu_pelaksanaan_lihat" placeholder="HH/BB/TTTT" value="">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="form-group push-10-t">
							<div class="col-md-12 ">
								<div class="col-md-12 ">
									<label for="ket_pelaksanaan">Deskripsi</label>
									<textarea class="form-control" disabled id="ket_pelaksanaan_lihat"  rows="4" placeholder="Deskripsi"></textarea>
								</div>
								
							</div>
						</div>
						<div class="form-group push-10-t">
							<div class="col-md-12 ">
									<label for="ket_pelaksanaan"></label>
								</div>
						</div>
						
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<? $this->load->view('Tpendaftaran_poli_ttv/modal_alergi')?>
<script type="text/javascript">
var table;
var tab;
var st_login;

$(document).ready(function(){	
	tab=1;
	load_index_all();
	
});
function lihat_pelaksanaan(id){
	$("#modal_pelaksanaan_lihat").modal('show');
	$.ajax({
		url: '{site_url}tkomunikasi/lihat_pelaksanaan', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:id,
			   },
		success: function(data) {
			$("#waktu_pelaksanaan_lihat").val(data.waktu_pelaksanaan);
			$("#user_pelaksanaan_lihat").val(data.nama);
			$("#ket_pelaksanaan_lihat").val(data.ket_pelaksanaan);
			$("#cover-spin").hide();
			// $('#index_history_kajian').DataTable().ajax.reload( null, false );
		}
	});
}
$(document).on("change","#st_ranap",function(){
	load_index_all();
});
$(document).on("click","#btn_cari",function(){
	load_index_all();
});
function verifikasi_assesmen(id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Verifikasi Assesmen ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkomunikasi/verifikasi_komunikasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				// $('#index_history_kajian').DataTable().ajax.reload( null, false );
				load_index_all();
			}
		});
	});

}
function set_tab($tab){
	tab=$tab;
	// alert(tab);
	// $("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	document.getElementById("div_2").classList.remove("active");
	// document.getElementById("div_3").classList.remove("active");
	if (tab=='1'){
		document.getElementById("div_1").classList.add("active");
	}
	if (tab=='2'){
		document.getElementById("div_2").classList.add("active");
	}
	// if (tab=='3'){
		// document.getElementById("div_3").classList.add("active");
	// }
	
	load_index_all();
}


function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let st_ranap=$("#st_ranap").val();
	let mppa_id=$("#mppa_id").val();
	let tanggal_permintaan_1=$("#tanggal_permintaan_1").val();
	let tanggal_permintaan_2=$("#tanggal_permintaan_2").val();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let pencarian=$("#pencarian").val();
	
	// alert(ruangan_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					// { "width": "5%", "targets": 0},
					{ "width": "25%", "targets": 0},
					{ "width": "60%", "targets": 1},
					{ "width": "15%", "targets": 2},
					
				],
            ajax: { 
                url: '{site_url}tkomunikasi/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						st_ranap:st_ranap,
						mppa_id:mppa_id,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						pencarian:pencarian,
						tab:tab,
						tanggal_permintaan_1:tanggal_permintaan_1,
						tanggal_permintaan_2:tanggal_permintaan_2,
						
					   }
            },
			"drawCallback": function( settings ) {
				 // $("#index_all thead").remove();
			 }  
        });
	$("#cover-spin").hide();
}
function startWorker() {
	if(typeof(Worker) !== "undefined") {
		if(typeof(w) == "undefined") {
			w = new Worker("{site_url}assets/js/worker.js");
	
		}
		w.postMessage({'cmd': 'start' ,'msg': 5000});
		w.onmessage = function(event) {
			$('#index_all').DataTable().ajax.reload( null, false );
		};
		w.onerror = function(event) {
			window.location.reload();
		};
	} else {
		alert("Sorry, your browser does not support Autosave Mode");
		$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
	}
 }

</script>