<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
 #cover-spin {
  position: fixed;
  width: 100%;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: rgba(255, 255, 255, 0.7);
  z-index: 9999;
  display: none;
}

@-webkit-keyframes spin {
  from {
	-webkit-transform: rotate(0deg);
  }

  to {
	-webkit-transform: rotate(360deg);
  }
}

@keyframes spin {
  from {
	transform: rotate(0deg);
  }

  to {
	transform: rotate(360deg);
  }
}
video {
  /* override other styles to make responsive */
  width: 100%    !important;
  height: auto   !important;
}
#cover-spin::after {
  content: '';
  display: block;
  position: absolute;
  left: 48%;
  top: 40%;
  width: 40px;
  height: 40px;
  border-style: solid;
  border-color: black;
  border-top-color: transparent;
  border-width: 4px;
  border-radius: 50%;
  -webkit-animation: spin .8s linear infinite;
  animation: spin .8s linear infinite;
}
#main-container {
    background-color: <?=$bg_color?>;
}
.block {
    background-color: <?=$bg_color?>;
}
.font-s25 {
  font-size: 25px !important;
}
.font-s55 {
  font-size: 55px !important;
}
.font-s20 {
  font-size: 20px !important;
}
.font-s120 {
  font-size: 120px !important;
}
.ms-container {
    height: 400px;
    overflow: hidden;
    position:relative
}
.flexslider {
    position:static;
}
.metaslider .slides img {
    width: auto;
    min-width: 100vw!important;
    min-height: 400px;
    position: absolute;
}
 @media screen {
	 table {
	   font-size: 13px !important;
	   border-collapse: collapse !important;
	   width: 100% !important;
	   
	 }
	 td {
	   padding: 5px;
	   border: 0px solid #fff !important;
	 }
	 .content td {
	   padding: 0px;
	 }
	 .border-full {
	   border: 1px solid #000 !important;
	 }
	 .border-bottom {
	   border-bottom:1px solid #000 !important;
	 }
	 .border-right {
	   border-right:1px solid #000 !important;
	 }
	 .border-left {
	   border-left:1px solid #000 !important;
	 }
	 .border-top {
	   border-top:1px solid #000 !important;
	 }
	 .border-thick-top{
	   border-top:2px solid #000 !important;
	 }
	 .border-thick-bottom{
	   border-bottom:2px solid #000 !important;
	 }
	 .text-center{
	   text-align: center !important;
	 }
	 .text-right{
	   text-align: right !important;
	 }
	 .text-semi-bold {
	   font-size: 15px !important;
	   font-weight: 700 !important;
	 }
	 .text-bold {
	   font-size: 15px !important;
	   font-weight: 700 !important;
	 }
	 .text-footer {
	   font-size: 12px !important;
	 }
	 .text-address {
	   color: #3abad8 !important;
	 }
  }
  .videoWrapper {
	  position: relative;
	  padding-bottom: 56.25%; /* 16:9 */
	  height: 0;
	}
	.videoWrapper iframe {
	  position: absolute;
	  top: 0;
	  left: 0;
	  width: 100%;
	  height: 100%;
	}
</style>
<div class="block">
<div id="cover-spin"></div>
<div class="content content-narrow">
	<div class="row pull-t">
		<div class="col-sm-8 col-lg-8">
			<div class="block-content block-content-full">
					<table class="" style="width:100%">
						<tbody>
							<tr>
								<td style="width: 15%;" rowspan="2" class="text-center">
									<img class="img-avatar img-avatar96" src="{upload_path}antrian/<?=$header_logo?>" alt="">
								</td>
								<td class="" style="width: 85%;">
									<h3 class="text-white  text-left">{judul_header}</h3>
								</td>
							</tr>
							<tr>
								<td >
									<address class="text-white">
									<strong>{alamat}</strong><br>
									{telepone}<br>{email} | {website}
									</address>
								</td>
							</tr>
							
						</tbody>
					</table>
					
			</div>
		</div>
		
		<div class="col-sm-4 col-lg-4">
			<div class="block-content block-content-full clearfix">
				<table class="block-table text-center">
					<tbody>
						<tr>
							<td style="width: 80%;">
								<div class="text-right font-w700 text-white"><span class="font-s25" id="tanggal"><?=GetDayIndonesia(date('Y-m-d'),true,'text')?></span></div>
								<div class="text-right text-white"><h2 id="current-time-now" data-start="<?php echo time() ?>"></h2></div>
							</td>
							<td class="" style="width: 20%;">
								<a class="push-5-l" onclick="tes_play()" title="Cetak Ulang"  href="javascript:void(0)">
									<i class="fa fa-volume-up fa-3x text-white text-left"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
				
				
			</div>
		</div>
		
	</div>

	<div class="row pull-t">
		<div class="col-sm-6" >
			<div class="block-content block-content-full bg-gray-lighter text-center " style="height: 500px">
				<a class="block block-rounomded block-link-hover3 text-center" href="javascript:void(0)">
					<div class="block-content block-content-full block-content-mini  bg-danger h1 text-white font-s55" style="border-bottom: 1px solid white;">
						NOMOR ANTRIAN
					</div>
					<div class="block-content block-content-full bg-danger">
						<div class="font-s120 font-w600 text-white push-5-t" id="no_antrian_atas">-</div>
					</div>
					<div class="block-content block-content-full block-content-mini bg-gray h1 font-w400 font-s55">
						<i class="si si-screen-desktop"></i>&nbsp;&nbsp; <span id="nama_counter_atas"></span>
					</div>
					<div class="block-content block-content-full bg-danger">
						<div class=" font-s25 text-white push-10" id="nama_dokter_atas" style="padding:0;height: 20px"> </div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="block block-rounded block-opt-refresh-icon8">
				<div class="block-content block-content-full bg-gray-lighter text-center">
					<div style="height: 460px">
						<div id="elvideo"></div>
						<!--<iframe width="100%" height="100%" src="<?=$upload_path?>/video/vid-1.mp4" frameborder="100" allowfullscreen controls autoplay mutted></iframe>-->
					</div>
				</div>
				
			</div>
			
		</div>
		
		
		
	</div>
	<div class="row pull-t">
		<div class="block" id="div_list_antrian">
			<div class="col-sm-2">
					<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
						<div class="block-content block-content-full bg-primary-dark" style="border: 1px solid white;">
							<div class="h3 font-w400 text-white" >RUANG PRAKTEK 1</div>
							<div class="font-s13 text-white" style="border-bottom: 1px solid white;padding:0;height: 40px">dr. Renaldi Prasetia Hermawan Nagar Rasyid, Sp.OT</div>
							<div class="h1 font-w700 text-white push-5-t">A2323</div>
						</div>
						
					</a>
			</div>
			<div class="col-sm-2">
					<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
						<div class="block-content block-content-full bg-primary-dark" style="border: 1px solid white;">
							<div class="h3 font-w400 text-white" >RUANG PRAKTEK 2</div>
							<div class="font-s13 text-white" style="border-bottom: 1px solid white;padding:0;height: 40px">dr. Giri Marsela, Sp.OT</div>
							<div class="h1 font-w700 text-white push-5-t">A2323</div>
						</div>
						
					</a>
			</div>
		</div>
	</div>
	<input type="hidden" class="form-control" id="display_id" placeholder="" name="display_id" value="{id}">
	<audio id="notification" src="{site_url}assets/upload/in.wav" muted></audio>
	
</div>
</div>
 <div class="modal fade" id="modal_test" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					
					<h3 class="block-title">Test Sound</h3>
				</div>
				<div class="block-content text-center">
					<p>Silahkan Klik Untuk Memastikan Sound Berjalan</p>
					<button class="btn btn-danger" onclick="tes_play()" title="Test Sound" type="button">
						<i class="fa fa-volume-up fa-3x text-white text-left"></i>
					</button>
					<p></p>
				</div>
				<div class="modal-footer">
				</div>
			</div>
		
		</div>
	</div>
</div>
<script src="{plugins_path}sparkline/jquery.sparkline.min.js"></script>
<script src="{plugins_path}slick/slick.min.js"></script>
<script src="{js_path}pages/base_ui_widgets.js"></script>
<script>

var x = document.getElementById("myAudio"); 
var no_antrian_panggil='';
var st_interaction=false;
var st_call=false;
startWorker();
var index_vide=0;
var display_id;
var file_video;
var antrian_id = 0;
var freshTime = new Date(parseInt($("#current-time-now").attr("data-start"))*1000);
//loop to tick clock every second
var func = function myFunc() {
	//set text of clock to show current time
	$("#current-time-now").text((new Date()).toLocaleTimeString());
	//add a second to freshtime var
	freshTime.setSeconds(freshTime.getSeconds() + 1);
	//wait for 1 second and go again
	setTimeout(myFunc, 1000);
};
func();
$(document).ready(function(){	

	$('#modal_test').modal({backdrop: 'static', keyboard: false})  
	$("#cover-spin").show();
	display_id=$("#display_id").val();
	$.ajax({
		url: '{site_url}antrian_poli_tv/get_video',
		type: 'POST',
		dataType: "json",
		data: {
			display_id:display_id
			
		},
		success: function(data) {
			file_video=data;
			if (data){
				load_video();
			}
			// $("#content_button").html(data);
			$("#cover-spin").hide();
		}
	});
	tes_play();
})	
function tes_play(){
	$('#modal_test').modal('hide');
	get_antrian_awal();
	let path_sound='{site_url}assets/upload/sound_antrian/';
	let file_sound = new Audio(path_sound+'in.wav');
	
	file_sound.play();
}
function play_sound_arr(str_file,id){
	st_call=true;
	var myArray = str_file.split(":");
	// console.log(myArray[0]);
	queue_sounds(myArray,id);
}
function queue_sounds(sounds,id){
	// $config['upload_path'] = './assets/upload/sound_antrian/';
	let path_sound='{site_url}assets/upload/sound_antrian/';
	
	var index = 0;
	function recursive_play()
	{
		let file_sound = new Audio(path_sound+sounds[index]);
	  if(index+1 === sounds.length)
	  {
		play(file_sound,update_antrian(id));
		
	  }
	  else
	  {
		play(file_sound,function(){
			index++; recursive_play();
			});
	  }
	}

recursive_play();   
}
 function play(audio, callback) {

	audio.play();
	if(callback)
	{
		audio.onended = callback;
	}
}
function update_antrian(id){
	$.ajax({
		url: '{site_url}antrian_poli_tv/call_update_panggil',
		type: 'POST',
		dataType: "json",
		data: {
			id:id
			
		},
		success: function(data) {
			st_call=false;
		}
	});
}

function load_video(){
	var str = file_video;
	
	var path='{site_url}assets/upload/video/'
	var n = str.includes(","); 
	console.log(str);
	if (str) {
		var str_loop='';
		console.log(n);
		if (n==false){
			var nArr=[];
			nArr[0]=str;
			str_loop=' loop ';
		}else{
			var nArr = str.split(',');
			
		}
			document.getElementById('elvideo').innerHTML ="<video id='videoElement' width=100% controls controlsList='nodownload' autoplay muted playsinline "+str_loop+"><p>Tu navegador no funciona, actualizalo</p></video>";
		var videoPlayer = document.getElementById('videoElement');
		videoPlayer.src = path+nArr[0];
		console.log(path+nArr[0]);
		i = 1;
		console.log(nArr.length);
		var batas_akhir=nArr.length-1;
		videoPlayer.onended = function(){
			if (i < nArr.length) {
				videoPlayer.src = path+nArr[i]
				if (i==batas_akhir){
					i=0;
					// console.log(i);
				}else{
					i++
				}
			}
		  }
	}
	
}

function get_antrian(){
	if (st_interaction==true){
	if (st_call==false){
		let display_id=$("#display_id").val();
		$.ajax({
			url: '{site_url}antrian_poli_tv/get_antrian',
			type: 'POST',
			dataType: "json",
			data: {
				display_id:display_id
				
			},
			success: function(data) {
				// console.log(data.antrian_atas);
				if (data.antrian_atas != null){
					$("#no_antrian_atas").html(data.antrian_atas.kode_antrian);
					$("#nama_counter_atas").html(data.antrian_atas.nama_counter);
					if (data.antrian_atas.st_panggil_suara=='0'){
						if (data.antrian_atas.sound_play){
							play_sound_arr(data.antrian_atas.sound_play,data.antrian_atas.id);
							
						}else{
							update_antrian(data.antrian_atas.id);
						}
						// proses_antrian(data.antrian_atas);
						// call_update_panggil(data.antrian_atas.id);
					}
				}
				
				$("#div_list_antrian").html(data.list_antrian);
				
			}
		});
	}
	}
}
function get_antrian_awal(){
		let display_id=$("#display_id").val();
		$.ajax({
			url: '{site_url}antrian_poli_tv/get_antrian_awal',
			type: 'POST',
			dataType: "json",
			data: {
				display_id:display_id
				
			},
			success: function(data) {
				// console.log(data.antrian_atas);
				if (data.antrian_atas != null){
					$("#no_antrian_atas").html(data.antrian_atas.kode_antrian);
					$("#nama_counter_atas").html(data.antrian_atas.nama_tujuan);
					$("#nama_dokter_atas").html(data.antrian_atas.nama_dokter);
					
				}
				st_interaction=true;
				$("#div_list_antrian").html(data.list_antrian);
				
			}
		});
}

function proses_antrian(data){
    antrian_id = data.id
    var txt_antrian = data.kodeantrian;
    var no_antrian = parseInt(txt_antrian.substr(1, 3));
    terbilang(no_antrian);
    sound_call.push(new Audio(ke));
    sound_call.push(new Audio(service_sound[parseInt(data.jenis_panggilan)]));
    sound_call.push(new Audio(number_sound[parseInt(data.ruang_id)]));
    queue_sounds(sound_call);
}
function startWorker() {
	if(typeof(Worker) !== "undefined") {
		if(typeof(w) == "undefined") {
			w = new Worker("{site_url}assets/js/worker.js");
	
		}
		w.postMessage({'cmd': 'start' ,'msg': 2000});
		w.onmessage = function(event) {
			console.log(st_call);
			get_antrian();
		};
		w.onerror = function(event) {
			window.location.reload();
		};
	} else {
		alert("Sorry, your browser does not support Autosave Mode");
		$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
	}
 }


</script>