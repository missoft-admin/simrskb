<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>

<style>
#cover-spin {
    position: fixed;
    width: 100%;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-color: rgba(255, 255, 255, 0.7);
    z-index: 9999;
    display: none;
}

@-webkit-keyframes spin {
    from {
        -webkit-transform: rotate(0deg);
    }

    to {
        -webkit-transform: rotate(360deg);
    }
}

@keyframes spin {
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
}

#cover-spin::after {
    content: '';
    display: block;
    position: absolute;
    left: 48%;
    top: 40%;
    width: 40px;
    height: 40px;
    border-style: solid;
    border-color: black;
    border-top-color: transparent;
    border-width: 4px;
    border-radius: 50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}

#main-container {
    background-color: <?=$bg_color?>;
}

.block {
    background-color: <?=$bg_color?>;
}

.font-s25 {
    font-size: 25px !important;
}
.font-s50 {
    font-size: 35px !important;
}
javascript:void(0)
.font-s55 {
    font-size: 55px !important;
}

.font-s20 {
    font-size: 20px !important;
}

.font-s120 {
    font-size: 100px !important;
}

@media screen {
    table {
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;

    }

    td {
        padding: 5px;
        border: 0px solid #fff !important;
    }

    .content td {
        padding: 0px;
    }

    .border-full {
        border: 1px solid #000 !important;
    }

    .border-bottom {
        border-bottom: 1px solid #000 !important;
    }

    .border-right {
        border-right: 1px solid #000 !important;
    }

    .border-left {
        border-left: 1px solid #000 !important;
    }

    .border-top {
        border-top: 1px solid #000 !important;
    }

    .border-thick-top {
        border-top: 2px solid #000 !important;
    }

    .border-thick-bottom {
        border-bottom: 2px solid #000 !important;
    }

    .text-center {
        text-align: center !important;
    }

    .text-right {
        text-align: right !important;
    }

    .text-semi-bold {
        font-size: 15px !important;
        font-weight: 700 !important;
    }

    .text-bold {
        font-size: 15px !important;
        font-weight: 700 !important;
    }

    .text-footer {
        font-size: 12px !important;
    }

    .text-address {
        color: #3abad8 !important;
    }
}
</style>
<div class="block">
    <div id="cover-spin"></div>
    <div class="content content-narrow">
        <div class="row pull-t">
            <div class="col-sm-8 col-lg-8">
                <div class="block-content block-content-full">
                    <table class="" style="width:100%">
                        <tbody>
                            <tr>
                                <td style="width: 15%;" rowspan="2" class="text-center">
                                    <img class="img-avatar img-avatar96" src="{upload_path}antrian/<?=$header_logo?>" alt="">
																</td>
                                <td class="" style="width: 85%;">
                                    <h3 class="text-white  text-left">{judul_header}</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <address class="text-white">
                                        <strong>{alamat}</strong><br>
                                        {telepone}<br>{email} | {website}
                                    </address>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-4 col-lg-4">
                <div class="block-content block-content-full clearfix">
                    <table class="block-table text-center">
                        <tbody>
                            <tr>
                                <td style="width: 80%;">
                                    <div class="text-right font-w700 text-white"><span class="font-s25" id="tanggal"><?=GetDayIndonesia(date('Y-m-d'),true,'text')?></span></div>
                                    <div class="text-right text-white"><h2 id="current-time-now" data-start="<?php echo time() ?>"></h2>
                                    </div>
                                </td>
                                <td class="" style="width: 20%;">
                                    <a class="push-5-l" onclick="testPlay()" title="Cetak Ulang"  href="javascript:void(0)">
																			<i class="fa fa-volume-up fa-3x text-white text-left"></i>
																		</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="block-content block-content-full bg-gray-lighter text-center " style="height: 440px">
                    <a class="block block-rounomded block-link-hover3 text-center" href="javascript:void(0)">
                        <div class="block-content block-content-full block-content-mini bg-danger text-white font-s25" style="border-bottom: 1px solid white;">
                            NOMOR ANTRIAN
                        </div>
                        <div class="block-content block-content-full bg-danger">
                            <div class="font-s120 font-w600 text-white push-5-t" id="no_antrian_atas">-</div>
                        </div>
                        <div class="block-content block-content-full block-content-mini bg-gray h1 font-w400 font-s25">
                            <i class="si si-screen-desktop"></i>&nbsp;&nbsp; <span id="nama_counter_atas"></span>
                        </div>
                        <div class="block-content block-content-full bg-danger">
                            <div class=" font-s50 text-white" id="nama_tujuan_atas" style="padding:0;height: 40px">FARMASI </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-9" id="list-antrian">
            </div>
        </div>

        <input type="hidden" class="form-control" id="display_id" placeholder="" name="display_id" value="{id}">
        <audio id="notification" src="{site_url}assets/upload/in.wav" muted></audio>
    </div>
</div>

<div class="modal fade" id="modal_test" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Test Sound</h3>
                </div>
                <div class="block-content text-center">
                    <p>Silahkan Klik Untuk Memastikan Sound Berjalan</p>
                    <button class="btn btn-danger" onclick="testPlay()" title="Test Sound" type="button">
                        <i class="fa fa-volume-up fa-3x text-white text-left"></i>
                    </button>
                    <p></p>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{plugins_path}sparkline/jquery.sparkline.min.js"></script>
<script src="{plugins_path}slick/slick.min.js"></script>
<script src="{js_path}pages/base_ui_widgets.js"></script>
<script type="text/javascript">
var st_interaction = false;
var st_call = false;

$(document).ready(function() {    
    // $("#cover-spin").show();

    $('#modal_test').modal({
        backdrop: 'static',
        keyboard: false
    })

    testPlay();
    startWorker();
})

function testPlay() {
    $('#modal_test').modal('hide');

    getAntrianAwal();

    let path_sound = '{site_url}assets/upload/sound_antrian/';
    let file_sound = new Audio(path_sound + 'in.wav');

    console.log(file_sound);

    file_sound.play();
}

function playSounds(str_file, id) {
    st_call = true;

    var myArray = str_file.split(":");
    queueSounds(myArray, id);
}

function queueSounds(sounds, id) {
    var index = 0;
    let path_sound = '{site_url}assets/upload/sound_antrian/';

    function recursivePlay() {
        let file_sound = new Audio(path_sound + sounds[index]);
        if (index + 1 === sounds.length) {
            playAudio(file_sound, updateAntrian(id));
        } else {
            playAudio(file_sound, function() {
                index++;
                recursivePlay();
            });
        }
    }

    recursivePlay();
}

function playAudio(audio, callback) {
    audio.play();

    if (callback) {
        audio.onended = callback;
    }
}

function updateAntrian(id) {
    $.ajax({
        url: '{site_url}antrian_poli_tv/call_update_panggil_khusus',
        type: 'POST',
        dataType: "json",
        data: {
            id: id
        },
        success: function(data) {
            st_call = false;
        }
    });
}

function getAntrian() {
    if (st_interaction == true) {
        if (st_call == false) {
            let display_id = $("#display_id").val();
            $.ajax({
                url: '{site_url}antrian_poli_tv/get_antrian_khusus',
                type: 'POST',
                dataType: "json",
                data: {
                    display_id: display_id

                },
                success: function(data) {
                    if (data.antrian_atas != null) {
                        $("#no_antrian_atas").html(data.antrian_atas.kode_antrian);
                        $("#nama_tujuan_atas").html(data.antrian_atas.nama_tujuan);
                        if (data.antrian_atas.st_panggil_suara == '0') {
                            if (data.antrian_atas.sound_play) {
                                playSounds(data.antrian_atas.sound_play, data.antrian_atas.id);

                            } else {
                                updateAntrian(data.antrian_atas.id);
                            }
                        }
                    }

                    $("#list-antrian").html(data.list_antrian);
                }
            });
        }
    }
}

function getAntrianAwal() {
    let display_id = $("#display_id").val();
    $.ajax({
        url: '{site_url}antrian_poli_tv/get_antrian_awal_khusus',
        type: 'POST',
        dataType: "json",
        data: {
            display_id: display_id
        },
        success: function(data) {
            st_interaction = true;

            $("#list-antrian").html(data.list_antrian);
            if (data.antrian_atas != null) {
                $("#no_antrian_atas").html(data.antrian_atas.kode_antrian);
                $("#nama_counter_atas").html(data.antrian_atas.nama_tujuan);
                $("#nama_dokter_atas").html(data.antrian_atas.nama_dokter);
            }
        }
    });
}

function startWorker() {
    if (typeof(Worker) !== "undefined") {
        if (typeof(w) == "undefined") {
            w = new Worker("{site_url}assets/js/worker.js");
        }

        w.postMessage({
            'cmd': 'start',
            'msg': 2000
        });

        w.onmessage = function(event) {
            getAntrian();
        };

        w.onerror = function(event) {
            window.location.reload();
        };
    } else {
        alert("Sorry, your browser does not support Autosave Mode");
    }
}

var freshTime = new Date(parseInt($("#current-time-now").attr("data-start"))*1000);
var getCurrentTime = function getCurrentTime() {
	$("#current-time-now").text((new Date()).toLocaleTimeString());
	freshTime.setSeconds(freshTime.getSeconds() + 1);
	setTimeout(getCurrentTime, 1000);
};
getCurrentTime();

</script>
