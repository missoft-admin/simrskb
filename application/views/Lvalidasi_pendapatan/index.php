<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('lvalidasi_pendapatan/export','class="form-horizontal" id="form-work" target="_blank"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Periode</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Bukti</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nojurnal" placeholder="No. Bukti" name="nojurnal" value="{nojurnal}">
                    </div>
                </div>
                
				
				                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="status">Kode Akun</label>
                    <div class="col-md-8">
                        <select id="idakun" name="idakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Akun -</option>
							<?foreach($list_akun as $row){?>
								<option value="<?=$row->id?>"><?=$row->noakun.' - '.$row->namaakun?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success" type="button" name="btn_filter" id="btn_filter" ><i class="fa fa-filter"></i> Filter</button>
                        <button class="btn btn-primary" type="submit" name="btn_export" id="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
                        <button class="btn btn-danger" type="submit" name="btn_export" id="btn_export" value="2" ><i class="fa fa-file-pdf-o"></i> Export PDF</button>
                        <button class="btn btn-success" type="submit" name="btn_export" id="btn_export"  value="3"><i class="fa fa-print"></i> Cetak</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="2%">ID LAPORAN</th>
                    <th hidden width="2%">ID VALIDASI</th>
                    <th width="10%">Tanggal Transaksi</th>
                    <th width="10%">No Bukti</th>
                    <th width="10%">Kode Bantu</th>
                    <th width="10%">No AKun</th>
                    <th width="10%">Nama Akun</th>
                    <th width="10%">Debit</th>
                    <th width="10%">Kredit</th>
                    <th width="10%">Keterangan</th>
                    <th width="15%">Action</th>
                   
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
var rNobukti='';
var rBaris='0';
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var idakun=$("#idakun").val();
	var nojurnal=$("#nojurnal").val();
	var tanggal_trx1=$("#tanggal_trx1").val();
	var tanggal_trx2=$("#tanggal_trx2").val();
	// alert(tanggalterima1);
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			
			columnDefs: [{ "targets": [0,1], "visible": false },
							{ "width": "4%", "targets": [4] },
							{ "width": "8%", "targets": [2,3,5] },
							{ "width": "15%", "targets": [6,9,10] },
							{ "width": "10%", "targets": [7,8,9] },
							{"targets": [7,8], className: "text-right" },
							{"targets": [2,3,4,5], className: "text-center" }
							
						 ],
            ajax: { 
                url: '{site_url}lvalidasi_pendapatan/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idakun:idakun,nojurnal:nojurnal,tanggal_trx1:tanggal_trx1,tanggal_trx2:tanggal_trx2
					   }
            },
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					if (rNobukti!=aData[1]){
						rNobukti=aData[1];
						if (rBaris=='0'){
							rBaris='1';
						}else{
							rBaris='0';
						}
					}
					// console.log(rBaris);
					if (rBaris=='0'){
						$('td', nRow).css('background-color', '#ffffff');
						rBaris=='1';
					}else{
						$('td', nRow).css('background-color', '#e5eeff');
						rBaris=='0';
					}
            },
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});


</script>