<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mbiaya_operasional" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mbiaya_operasional/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" id="div_3">
				<label class="col-md-3 control-label" for="nama">Biaya Operasional</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama Biaya Operasional" name="nama" value="{nama}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="estimasi">Estimasi Harga</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="estimasi" placeholder="Estimasi Harga" name="estimasi" value="{estimasi}">
				</div>
			</div>
			
			
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mbiaya_operasional" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');

	})	
	
	
	function validate_final(){
		
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Biaya Operasional", "error");
			return false;
		}
		if ($("#estimasi").val()=='' || $("#estimasi").val()=='0'){
			sweetAlert("Maaf...", "Tentukan Estimasi", "error");
			return false;
		}
		return true;

		
	}
</script>