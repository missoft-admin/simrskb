<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}SatuSehat/organization" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('SatuSehat/organization/save','class="form-horizontal push-10-t"') ?>
        <div class="form-group">
            <label class="col-md-2" for="organization_id">Organization ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="organization_id" placeholder="Organization ID" name="organization_id" value="{organization_id}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="organization_name">Organization Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="organization_name" placeholder="Organization Name" name="organization_name" value="{organization_name}">
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label class="col-md-2" for="type_code">Type Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="type_code" placeholder="Type Code" name="type_code" value="{type_code}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="type_name">Type Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="type_name" placeholder="Type Name" name="type_name" value="{type_name}" readonly>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label class="col-md-2" for="telecom_phone">Telecom Phone</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="telecom_phone" placeholder="Telecom Phone" name="telecom_phone" value="{telecom_phone}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="telecom_email">Telecom Email</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="telecom_email" placeholder="Telecom Email" name="telecom_email" value="{telecom_email}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="telecom_url">Telecom URL</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="telecom_url" placeholder="Telecom URL" name="telecom_url" value="{telecom_url}">
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label class="col-md-2" for="address_line">Address Line</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_line" placeholder="Address Line" name="address_line" value="{address_line}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_city">Address City</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_city" placeholder="Address City" name="address_city" value="{address_city}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_postal_code">Address Postal Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_postal_code" placeholder="Address Postal Code" name="address_postal_code" value="{address_postal_code}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_country">Address Country</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_country" placeholder="Address Country" name="address_country" value="{address_country}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_province_id">Province ID</label>
            <div class="col-md-8">
                <select name="address_province_id" id="province_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mfwilayah', array('jenis' => '1')) as $row) {?>
                        <option value="<?= $row->id; ?>" <?= $address_province_id === $row->id ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_city_id">City ID</label>
            <div class="col-md-8">
                <select name="address_city_id" id="city_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mfwilayah', array('parent_id' => $address_province_id)) as $row) {?>
                        <option value="<?= $row->id; ?>" <?= $address_city_id == $row->id ? 'selected' : ''; ?>><?= $row->nama; ?></<option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_district_id">District ID</label>
            <div class="col-md-8">
                <select name="address_district_id" id="district_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mfwilayah',array('parent_id' => $address_city_id)) as $row){?>
                        <option value="<?= $row->id; ?>"<?= $address_district_id == $row->id ? 'selected':''; ?>><?= $row->nama; ?></<option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_village_id">Village ID</label>
            <div class="col-md-8">
                <select name="address_village_id" id="village_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mfwilayah', array('parent_id' => $address_district_id)) as $row):?>
                        <option value="<?= $row->id; ?>" <?= $address_village_id == $row->id ? 'selected' : ''; ?>><?= $row->nama; ?></<option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}SatuSehat/organization" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <?php echo form_close() ?>
    </div>
</div>

<style>
    #json-content {
    background-color: #000;
    color: #fff;
    padding: 10px;
    border-radius: 5px;
    overflow-x: auto;
}
</style>

<div id="json-debug" style="display: none;">
    <h6>RESPONSE:</h6>
    <pre id="json-content" class="json"></pre>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
    displayJson('<?= $satu_sehat_response; ?>');

    function displayJson(json) {
        const formattedJson = JSON.stringify(JSON.parse(json), null, 4);
        document.getElementById('json-content').textContent = formattedJson;
        document.getElementById('json-debug').style.display = 'block';
    }

    $(document).ready(function() {
		$('.select2').select2();

		$("#province_id").change(function() {
			const province_code = $("#province_id").val();

			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKabupaten',
				method: "POST",
				dataType: "json",
				data: {
					"kodeprovinsi": province_code
				},
				success: function(data) {
					$("#city_id").empty();
					$("#city_id").append("<option value=''>Pilih Opsi</option>");
                    data.map(({id, nama}) => {
                        $("#city_id").append("<option value='" + id + "'>" + nama + "</option>");
                    });
					$("#city_id").selectpicker("refresh");
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#city_id").change(function() {
			const city_code = $("#city_id").val();

			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKecamatan',
				method: "POST",
				dataType: "json",
				data: {
					"kodekab": city_code
				},
				success: function(data) {
					$("#district_id").empty();
					$("#district_id").append("<option value=''>Pilih Opsi</option>");
                    data.map(({id, nama}) => {
                        $("#district_id").append("<option value='" + id + "'>" + nama + "</option>");
                    });
					$("#district_id").selectpicker("refresh");
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#district_id").change(function() {
			const district_code = $("#district_id").val();
            
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKelurahan',
				method: "POST",
				dataType: "json",
				data: {
					"kodekec": district_code
				},
				success: function(data) {
					$("#village_id").empty();
					$("#village_id").append("<option value=''>Pilih Opsi</option>");
                    data.map(({id, nama}) => {
                        $("#village_id").append("<option value='" + id + "'>" + nama + "</option>");
                    });
					$("#village_id").selectpicker("refresh");
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#village_id").change(function() {
			const village_code = $("#district_id").val();
            
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKodepos',
				method: "POST",
				dataType: "json",
				data: {
					"kodekel": village_code
				},
				success: function(data) {
                    if (data[0]) {
                        $("#address_postal_code").empty();
                        $("#address_postal_code").val(data[0].kode);
                    }
				}
			});
		});
    });
</script>


