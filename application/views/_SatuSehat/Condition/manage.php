<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}SatuSehat/condition" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('SatuSehat/condition/save','class="form-horizontal push-10-t"') ?>
        <div class="form-group">
            <label class="col-md-2" for="encounter_id">Encounter ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="encounter_id" name="encounter_id" value="{encounter_id}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="category_code">Category Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="category_code" name="category_code" value="{category_code}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="category_name">Category Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="category_name" name="category_name" value="{category_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="condition_code">Condition Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="condition_code" name="condition_code" value="{condition_code}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="condition_name">Condition Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="condition_name" name="condition_name" value="{condition_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Patient ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="patient_id" name="patient_id" value="{patient_id}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Patient Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="patient_name" name="patient_name" value="{patient_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="encounter_display">Encounter Display</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="encounter_display" name="encounter_display" value="{encounter_display}">
            </div>
        </div>
        
        <hr>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}SatuSehat/condition" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_hidden('condition_id', $condition_id); ?>
        <?php echo form_hidden('transaction_id', $transaction_id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<style>
    #json-content {
    background-color: #000;
    color: #fff;
    padding: 10px;
    border-radius: 5px;
    overflow-x: auto;
}
</style>

<div id="json-debug" style="display: none;">
    <h6>RESPONSE:</h6>
    <pre id="json-content" class="json"></pre>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
    <?php if ($satu_sehat_response) { ?>
    displayJson('<?php echo $satu_sehat_response; ?>');

    function displayJson(json) {
        const formattedJson = JSON.stringify(JSON.parse(json), null, 4);
        document.getElementById('json-content').textContent = formattedJson;
        document.getElementById('json-debug').style.display = 'block';
    }
    <?php } ?>
</script>


