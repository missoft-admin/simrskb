<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="table-responsive">
            <div class="row">
				<?php echo form_open('SatuSehat/Condition/filter','class="form-horizontal" id="form-work"') ?>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No. Registration</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="registration_number" name="registration_number" value="{registration_number}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Medical Record</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="medical_record_number" name="medical_record_number" value="{medical_record_number}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Patient Name</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="patient_name" name="patient_name" value="{patient_name}">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="example-daterange1">Date</label>
                        <div class="col-md-8">
                            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="start_date" name="start_date" placeholder="From" value="{start_date}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="end_date" name="end_date" placeholder="To" value="{end_date}">
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Type Transaction</label>
						<div class="col-md-8">
							<select id="patient_origin" name="patient_origin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="" <?=($patient_origin == '' ? 'selected':'')?>>All</option>
								<option value="1" <?=($patient_origin == '1' ? 'selected' : '')?>>Poliklinik</option>
								<option value="2" <?=($patient_origin == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Polyclicnic</label>
						<div class="col-md-8">
							<select id="polyclinic_id" name="polyclinic_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" <?=($polyclinic_id == '' ? 'selected':'')?>>All</option>
                                <?php foreach (get_all('mpoliklinik') as $row){?>
                                <option value="<?=$row->id ?>" <?=($polyclinic_id == $row->id ? 'selected':'')?>><?=$row->nama ?></option>
                                <?php } ?>
                            </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Practitioner</label>
						<div class="col-md-8">
							<select id="doctor_id" name="doctor_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="" <?=($doctor_id == '' ? 'selected':'')?>>All</option>
                                <?php foreach (get_all('mdokter') as $row){?>
                                <option value="<?=$row->id ?>" <?=($doctor_id == $row->id ? 'selected':'')?>><?=$row->nama ?></option>
                                <?php } ?>
                            </select>
						</div>
					</div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="">Status</label>
                        <div class="col-md-8">
                            <select id="status_condition" name="status_condition" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="-">All</option>
                                <option value="1">Connected</option>
                                <option value="0">Not Connect</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">
							<button class="btn btn-success text-uppercase" type="button" onclick="loadIndex()" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
				</div>
				<?php echo form_close() ?>
			</div>

			<hr style="margin-top:10px">

            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Encounter ID</th>
                        <th>Patient Name</th>
                        <th>Practitioner Name</th>
                        <th>Location Name</th>
                        <th>Diagnosis Name</th>
                        <th>Transaction Info</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}SatuSehat/Condition/getIndex',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "10%",
                "targets": 1,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "15%",
                "targets": 2,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "15%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "15%",
                "targets": 4,
                "orderable": true,
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "20%",
                "targets": 6,
                "orderable": true,
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 8,
                "orderable": true,
                "className": "text-center"
            },
        ]
    });
});

function loadIndex() {
    var registration_number = $('#registration_number').val();
    var medical_record_number = $('#medical_record_number').val();
    var patient_name = $('#patient_name').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var patient_origin = $('#patient_origin').val();
    var polyclinic_id = $('#polyclinic_id').val();
    var doctor_id = $('#doctor_id').val();
    var status_condition = $('#status_condition').val();

    $('#datatable-simrs').DataTable().destroy();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '{site_url}SatuSehat/Condition/getIndex/',
            type: "POST",
            dataType: 'json',
                data: {
                    registration_number: registration_number,
                    medical_record_number: medical_record_number,
                    patient_name: patient_name,
                    start_date: start_date,
                    end_date: end_date,
                    patient_origin: patient_origin,
                    polyclinic_id: polyclinic_id,
                    doctor_id: doctor_id,
                    status_condition: status_condition,
                }
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "10%",
                "targets": 1,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "15%",
                "targets": 2,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "15%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "15%",
                "targets": 4,
                "orderable": true,
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "20%",
                "targets": 6,
                "orderable": true,
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 8,
                "orderable": true,
                "className": "text-center"
            },
        ]
    });
}
</script>
