<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}SatuSehat/encounter" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('SatuSehat/encounter/save','class="form-horizontal push-10-t"') ?>

        <?php if ($encounter_id) { ?>
        <div class="form-group">
            <label class="col-md-2" for="encounter_id">Encounter ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="encounter_id" placeholder="Encounter ID" name="encounter_id" value="{encounter_id}" readonly>
            </div>
        </div>
        <?php } ?>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Resource Type</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="resource_type" name="resource_type" value="{resource_type}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Status Encounter</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="status_encounter" name="status_encounter" value="{status_encounter}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Class Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="class_code" name="class_code" value="{class_code}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Class Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="class_name" name="class_name" value="{class_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Patient ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="patient_id" name="patient_id" value="{patient_id}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Patient Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="patient_name" name="patient_name" value="{patient_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Participant Type Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="participant_type_code" name="participant_type_code" value="{participant_type_code}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Participant Type Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="participant_type_name" name="participant_type_name" value="{participant_type_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Practitioner ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="practitioner_id" name="practitioner_id" value="{practitioner_id}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Practitioner Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="practitioner_name" name="practitioner_name" value="{practitioner_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2">Arrived Date</label>
            <div class="col-md-8">
                <div class="input-group" data-date-format="dd/mm/yyyy">
                    <input class="form-control" readonly type="text" id="arrived_start_date" name="arrived_start_date" placeholder="Start Date" value="{arrived_start_date}">
                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                    <input class="form-control" readonly type="text" id="arrived_end_date" name="arrived_end_date" placeholder="End Date" value="{arrived_end_date}">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2">In-Progress Date</label>
            <div class="col-md-8">
                <div class="input-group" data-date-format="dd/mm/yyyy">
                    <input class="form-control" readonly type="text" id="inprogress_start_date" name="inprogress_start_date" placeholder="Start Date" value="{inprogress_start_date}">
                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                    <input class="form-control" readonly type="text" id="inprogress_end_date" name="inprogress_end_date" placeholder="End Date" value="{inprogress_end_date}">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2">Finished Date</label>
            <div class="col-md-8">
                <div class="input-group" data-date-format="dd/mm/yyyy">
                    <input class="form-control" readonly type="text" id="finished_start_date" name="finished_start_date" placeholder="Start Date" value="{finished_start_date}">
                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                    <input class="form-control" readonly type="text" id="finished_end_date" name="finished_end_date" placeholder="End Date" value="{finished_end_date}">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Location ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="location_id" name="location_id" value="{location_id}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Location Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="location_name" name="location_name" value="{location_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Diagnosis Condition ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="diagnosis_condition_id" name="diagnosis_condition_id" value="{diagnosis_condition_id}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Diagnosis Condition Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="diagnosis_condition_name" name="diagnosis_condition_name" value="{diagnosis_condition_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Diagnosis Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="diagnosis_code" name="diagnosis_code" value="{diagnosis_code}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Diagnosis Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="diagnosis_name" name="diagnosis_name" value="{diagnosis_name}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2" for="resource_type">Organization ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" readonly id="organization_id" name="organization_id" value="{organization_id}">
            </div>
        </div>
        
        <hr>

        <div class="alert alert-danger alert-dismissable">
            <p><?= $error_messages; ?></p>
        </div>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button class="btn btn-success" type="submit" <?= $is_error ? 'disabled' : ''; ?>>Simpan</button>
                <a href="{base_url}SatuSehat/encounter" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_hidden('transaction_id', $transaction_id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<style>
    #json-content {
    background-color: #000;
    color: #fff;
    padding: 10px;
    border-radius: 5px;
    overflow-x: auto;
}
</style>

<div id="json-debug" style="display: none;">
    <h6>RESPONSE:</h6>
    <pre id="json-content" class="json"></pre>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
    <?php if ($satu_sehat_response) { ?>
    displayJson('<?php echo $satu_sehat_response; ?>');

    function displayJson(json) {
        const formattedJson = JSON.stringify(JSON.parse(json), null, 4);
        document.getElementById('json-content').textContent = formattedJson;
        document.getElementById('json-debug').style.display = 'block';
    }
    <?php } ?>
</script>


