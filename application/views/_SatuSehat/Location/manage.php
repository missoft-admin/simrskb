<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}SatuSehat/location" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('SatuSehat/location/save','class="form-horizontal push-10-t"') ?>
        <div class="form-group">
            <label class="col-md-2" for="reference_location_id">Reference Location</label>
            <div class="col-md-8">
                <select name="reference_location_id" id="reference_location_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mlokasi_ruangan', array('status' => 1)) as $row):?>
                        <option value="<?= $row->id; ?>" <?= $reference_location_id == $row->id ? 'selected' : ''; ?>
                            data-identifier="<?= $row->kode; ?>" data-name="<?= $row->nama; ?>" data-description="<?= $row->deskripsi; ?>"><?= $row->deskripsi; ?></<option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

        <hr>

        <div class="form-group">
            <label class="col-md-2" for="organization_id">Organization ID</label>
            <div class="col-md-8">
                <select name="organization_id" id="organization_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('satu_sehat_organization', []) as $row):?>
                        <option value="<?= $row->organization_id; ?>" <?= $organization_id == $row->organization_id ? 'selected' : ''; ?>><?= $row->organization_name; ?></<option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
        <?php if ($location_id) { ?>
        <div class="form-group">
            <label class="col-md-2" for="location_id">Location ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="location_id" placeholder="Location ID" name="location_id" value="{location_id}" readonly>
            </div>
        </div>
        <?php } ?>
        <div class="form-group">
            <label class="col-md-2" for="location_status">Location Status</label>
            <div class="col-md-8">
                <select name="location_status" class="select2 form-control">
                    <option value="active">Active</option>
                    <option value="inactive">Inactive</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="location_identifier">Location Identifier</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="location_identifier" placeholder="Location Identifier" name="location_identifier" value="{location_identifier}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="location_name">Location Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="location_name" placeholder="Location Name" name="location_name" value="{location_name}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="location_description">Location Description</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="location_description" placeholder="Location Description" name="location_description" value="{location_description}">
            </div>
        </div>

        <hr>

        <div class="form-group">
            <label class="col-md-2" for="telecom_phone">Phone</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="telecom_phone" placeholder="Phone" name="telecom_phone" value="{telecom_phone}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="telecom_fax">Fax</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="telecom_fax" placeholder="Fax" name="telecom_fax" value="{telecom_fax}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="telecom_email">Email</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="telecom_email" placeholder="Email" name="telecom_email" value="{telecom_email}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="telecom_url">URL</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="telecom_url" placeholder="URL" name="telecom_url" value="{telecom_url}">
            </div>
        </div>

        <hr>

        <div class="form-group">
            <label class="col-md-2" for="address_line">Address Line</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_line" placeholder="Address Line" name="address_line" value="{address_line}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_city">Address City</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_city" placeholder="Address City" name="address_city" value="{address_city}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_postal_code">Address Postal Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_postal_code" placeholder="Address Postal Code" name="address_postal_code" value="{address_postal_code}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_country">Address Country</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_country" placeholder="Address Country" name="address_country" value="{address_country}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_province_id">Province ID</label>
            <div class="col-md-8">
                <select name="address_province_id" id="province_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mfwilayah', array('jenis' => '1')) as $row) {?>
                        <option value="<?= $row->id; ?>" <?= $address_province_id === $row->id ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_city_id">City ID</label>
            <div class="col-md-8">
                <select name="address_city_id" id="city_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mfwilayah', array('parent_id' => $address_province_id)) as $row) {?>
                        <option value="<?= $row->id; ?>" <?= $address_city_id == $row->id ? 'selected' : ''; ?>><?= $row->nama; ?></<option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_district_id">District ID</label>
            <div class="col-md-8">
                <select name="address_district_id" id="district_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mfwilayah',array('parent_id' => $address_city_id)) as $row){?>
                        <option value="<?= $row->id; ?>"<?= $address_district_id == $row->id ? 'selected':''; ?>><?= $row->nama; ?></<option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_village_id">Village ID</label>
            <div class="col-md-8">
                <select name="address_village_id" id="village_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <?php foreach(get_all('mfwilayah', array('parent_id' => $address_district_id)) as $row):?>
                        <option value="<?= $row->id; ?>" <?= $address_village_id == $row->id ? 'selected' : ''; ?>><?= $row->nama; ?></<option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_rt">Address RT</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_rt" placeholder="Address RT" name="address_rt" value="{address_rt}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="address_rw">Address RW</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="address_rw" placeholder="Address RW" name="address_rw" value="{address_rw}">
            </div>
        </div>

        <hr>

        <div class="form-group">
            <label class="col-md-2" for="physical_type">Physical Type</label>
            <div class="col-md-8">
                <select name="physical_type" id="physical_type" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
                    <option value="">Select Option</option>
                    <option value="bu" <?= $physical_type_code == 'bu' ? 'selected' : ''; ?>>Building</option>
                    <option value="wi" <?= $physical_type_code == 'wi' ? 'selected' : ''; ?>>Wing</option>
                    <option value="wa" <?= $physical_type_code == 'wa' ? 'selected' : ''; ?>>Ward</option>
                    <option value="lvl" <?= $physical_type_code == 'lvl' ? 'selected' : ''; ?>>Level</option>
                    <option value="co" <?= $physical_type_code == 'co' ? 'selected' : ''; ?>>Corridor</option>
                    <option value="ro" <?= $physical_type_code == 'ro' ? 'selected' : ''; ?>>Room</option>
                    <option value="bd" <?= $physical_type_code == 'bd' ? 'selected' : ''; ?>>Bed</option>
                    <option value="ve" <?= $physical_type_code == 've' ? 'selected' : ''; ?>>Vehicle</option>
                    <option value="ho" <?= $physical_type_code == 'ho' ? 'selected' : ''; ?>>House</option>
                    <option value="ca" <?= $physical_type_code == 'ca' ? 'selected' : ''; ?>>Cabinet</option>
                    <option value="rd" <?= $physical_type_code == 'rd' ? 'selected' : ''; ?>>road</option>
                    <option value="area" <?= $physical_type_code == 'area' ? 'selected' : ''; ?>>Area</option>
                    <option value="jdn" <?= $physical_type_code == 'jdn' ? 'selected' : ''; ?>>Jurisdiction</option>
                    <option value="vir" <?= $physical_type_code == 'vir' ? 'selected' : ''; ?>>Virtual</option>
                </select>
            </div>
        </div>
        <div class="form-group" hidden>
            <label class="col-md-2" for="physical_type_code">Physical Type Code</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="physical_type_code" placeholder="Physical Type Code" name="physical_type_code" value="{physical_type_code}" readonly>
            </div>
        </div>
        <div class="form-group" hidden>
            <label class="col-md-2" for="physical_type_name">Physical Type</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="physical_type_name" placeholder="Physical Type Name" name="physical_type_name" value="{physical_type_name}" readonly>
            </div>
        </div>

        <hr>

        <div class="form-group">
            <label class="col-md-2" for="position_longitude">Position Longitude</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="position_longitude" placeholder="Position Longitude" name="position_longitude" value="{position_longitude}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="position_latitude">Position Latitude</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="position_latitude" placeholder="Position Latitude" name="position_latitude" value="{position_latitude}">
            </div>
        </div>
        
        <hr>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}SatuSehat/location" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<style>
    #json-content {
    background-color: #000;
    color: #fff;
    padding: 10px;
    border-radius: 5px;
    overflow-x: auto;
}
</style>

<div id="json-debug" style="display: none;">
    <h6>RESPONSE:</h6>
    <pre id="json-content" class="json"></pre>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
    <?php if ($satu_sehat_response) { ?>
    displayJson('<?php echo $satu_sehat_response; ?>');

    function displayJson(json) {
        const formattedJson = JSON.stringify(JSON.parse(json), null, 4);
        document.getElementById('json-content').textContent = formattedJson;
        document.getElementById('json-debug').style.display = 'block';
    }
    <?php } ?>

    $(document).ready(function() {
		$('.select2').select2();

		$("#reference_location_id").change(function() {
			const data = $("#reference_location_id option:selected").data();

			$("#location_identifier").val(data.identifier);
            $("#location_name").val(data.name);
            $("#location_description").val(data.description);
		});

		$("#organization_id").change(function() {
			const organizationId = $("#organization_id option:selected").val();
            $("#province_id").select2('destroy');

            $.ajax({
				url: '{site_url}SatuSehat/location/getOrganization/' + organizationId,
				dataType: "json",
				success: function(response) {
					$("#telecom_phone").val(response.data.telecom_phone);
                    $("#telecom_fax").val(response.data.telecom_fax);
                    $("#telecom_email").val(response.data.telecom_email);
                    $("#telecom_url").val(response.data.telecom_url);
                    $("#address_line").val(response.data.address_line);
                    $("#address_city").val(response.data.address_city);
                    $("#address_postal_code").val(response.data.address_postal_code);
                    $("#address_country").val(response.data.address_country);
                    $("#province_id").val(response.data.address_province_id);
                    $("#city_id").val(response.data.address_city_id);
                    $("#district_id").val(response.data.address_district_id);
                    $("#village_id").val(response.data.address_village_id);
                    
                    getCity(response.data.address_province_id, response.data.address_city_id);
                    getDistrict(response.data.address_city_id, response.data.address_district_id);
                    getVillage(response.data.address_district_id, response.data.address_village_id);
                    getPostalCode(response.data.address_village_id);

                    $("#province_id").select2();
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#province_id").change(function() {
			const province_code = $("#province_id").val();
            getCity(province_code);
		});

		$("#city_id").change(function() {
			const city_code = $("#city_id").val();
            getDistrict(city_code);
		});

		$("#district_id").change(function() {
			const district_code = $("#district_id").val();
            getVillage(district_code);
		});

		$("#village_id").change(function() {
			const village_code = $("#district_id").val();
            getPostalCode(village_code);
		});

		$("#physical_type").change(function() {
			const physicalType = $("#physical_type option:selected");
            $("#physical_type_code").val(physicalType.val());
            $("#physical_type_name").val(physicalType.text());
		});
    });

    function getCity(province_code, city_id = '') {
        $.ajax({
            url: '{site_url}tpoliklinik_pendaftaran/getKabupaten',
            method: "POST",
            dataType: "json",
            data: {
                "kodeprovinsi": province_code
            },
            success: function(data) {
                $("#city_id").empty();
                $("#city_id").append("<option value=''>Pilih Opsi</option>");
                data.map(({id, nama}) => {
                    $("#city_id").append("<option value='" + id + "' " + (city_id == id ? 'selected' : '') + ">" + nama + "</option>");
                });
                $("#city_id").selectpicker("refresh");
            },
            error: function(data) {
                alert(data);
            }
        });
    }
    
    function getDistrict(city_code, district_id = '') {
        $.ajax({
            url: '{site_url}tpoliklinik_pendaftaran/getKecamatan',
            method: "POST",
            dataType: "json",
            data: {
                "kodekab": city_code
            },
            success: function(data) {
                $("#district_id").empty();
                $("#district_id").append("<option value=''>Pilih Opsi</option>");
                data.map(({id, nama}) => {
                    $("#district_id").append("<option value='" + id + "' " + (district_id == id ? 'selected' : '') + ">" + nama + "</option>");
                });
                $("#district_id").selectpicker("refresh");
            },
            error: function(data) {
                alert(data);
            }
        });
    }

    function getVillage(district_code, village_id = '') {
        $.ajax({
            url: '{site_url}tpoliklinik_pendaftaran/getKelurahan',
            method: "POST",
            dataType: "json",
            data: {
                "kodekec": district_code
            },
            success: function(data) {
                $("#village_id").empty();
                $("#village_id").append("<option value=''>Pilih Opsi</option>");
                data.map(({id, nama}) => {
                    $("#village_id").append("<option value='" + id + "' " + (village_id == id ? 'selected' : '') + ">" + nama + "</option>");
                });
                $("#village_id").selectpicker("refresh");
            },
            error: function(data) {
                alert(data);
            }
        });
    }
    
    function getPostalCode(village_code) {
        $.ajax({
            url: '{site_url}tpoliklinik_pendaftaran/getKodepos',
            method: "POST",
            dataType: "json",
            data: {
                "kodekel": village_code
            },
            success: function(data) {
                if (data[0]) {
                    $("#address_postal_code").empty();
                    $("#address_postal_code").val(data[0].kode);
                }
            }
        });
    }
</script>


