<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}SatuSehat/patient" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('SatuSehat/patient/save','class="form-horizontal push-10-t"') ?>
        <div class="form-group">
            <label class="col-md-2" for="patient_nik">Patient NIK</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="patient_nik" placeholder="NIK" name="patient_nik" value="{patient_nik}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="patient_nama">Patient Nama</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="patient_nama" placeholder="Nama" name="patient_nama" value="{patient_nama}" readonly>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label class="col-md-2" for="patient_id">Patient ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="patient_id" placeholder="Patient ID" name="patient_id" value="{patient_id}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="patient_name">Patient Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="patient_name" placeholder="Patient Name" name="patient_name" value="{patient_name}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="patient_gender">Patient Gender</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="patient_gender" placeholder="Patient Gender" name="patient_gender" value="{patient_gender}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="patient_birthdate">Patient Birthdate</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="patient_birthdate" placeholder="Patient Birthdate" name="patient_birthdate" value="{patient_birthdate}" readonly>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}SatuSehat/patient" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<style>
    #json-content {
    background-color: #000;
    color: #fff;
    padding: 10px;
    border-radius: 5px;
    overflow-x: auto;
}
</style>

<div id="json-debug" style="display: none;">
    <h6>RESPONSE:</h6>
    <pre id="json-content" class="json"></pre>
</div>

<script type="text/javascript">
    displayJson('<?= $satu_sehat_response; ?>');

    function displayJson(json) {
        const formattedJson = JSON.stringify(JSON.parse(json), null, 4);
        document.getElementById('json-content').textContent = formattedJson;
        document.getElementById('json-debug').style.display = 'block';
    }
</script>


