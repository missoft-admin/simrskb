<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('SatuSehat/Encounter/filter','class="form-horizontal" id="form-work"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">No. Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="medical_record_number" name="medical_record_number" value="{medical_record_number}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="patient_name" name="patient_name" value="{patient_name}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">IHS Number</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="ihs_number" name="ihs_number" value="{ihs_number}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Status Integrasi</label>
                    <div class="col-md-8">
                        <select id="status_integration" name="status_integration" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="-">Semua</option>
                            <option value="0">Not Connect</option>
                            <option value="1">Connect</option>
                            <option value="9">Failed</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Filter Tanggal</label>
                    <div class="col-md-8">
                        <select id="filter_date" name="filter_date" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Tidak Ditentukan</option>
                            <option value="1">Ditentukan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">Tanggal Kunjungan</label>
                    <div class="col-md-8">
                        <div class="input-group date">
                            <input class="form-control js-datepicker" type="text" id="arrived_date_start" name="arrived_date_start" placeholder="Tanggal Dari" value="{arrived_date_start}" data-date-format="dd/mm/yyyy">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control js-datepicker" type="text" id="arrived_date_end" name="arrived_date_end" placeholder="Tanggal Sampai" value="{arrived_date_end}" data-date-format="dd/mm/yyyy">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px; float: right;">
                    <div class="col-md-12">
                        <button class="btn btn-success text-uppercase" type="button" onclick="loadIndex()" name="button" style="font-size:13px;"><i class="fa fa-filter"></i> Filter</button>
                        <button class="btn btn-primary text-uppercase" type="button" onclick="processAllByNik()" name="button" style="font-size:13px;"><i class="fa fa-pencil"></i> BY NIK ALL</button>
                        <button class="btn btn-danger text-uppercase" type="button" onclick="processAllByKTP()" name="button" style="font-size:13px;"><i class="fa fa-pencil"></i> BY KTP ALL</button>
                        <button class="btn btn-warning text-uppercase" type="button" onclick="processAllByData()" name="button" style="font-size:13px;"><i class="fa fa-pencil"></i> BY DATA ALL</button>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>

        <hr style="margin-top:10px">
        
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No. Medrec</th>
                        <th>Nama Pasien</th>
                        <th>Tanggal Lahir</th>
                        <th>NIK</th>
                        <th>No. KTP</th>
                        <th>IHS Number</th>
                        <th>Status Integrasi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal History Integrasi -->
<div class="modal in" id="HistoryIntegrasiModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout" style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<h3 class="block-title">History</h3>
				</div>
                <div class="block-content">
                    <div class="col-md-12">
                        <table id="historyIntegrasi" class="table table-bordered table-striped" style="margin-top: 10px;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Tipe</th>
                                    <th>Nomor NIK / KTP</th>
                                    <th>Status</th>
                                    <th>User</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update Data -->
<div class="modal in" id="UpdateDataModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout" style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<h3 class="block-title">Update Data</h3>
				</div>
                <div class="block-content">
					<div class="row">
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="no-medrec">No. Medrec</label>
                                    <input type="text" class="form-control" id="no-medrec" data-patient-id="" value="" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal-lahir">Tanggal Lahir</label>
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggal-lahir" value="">
                                </div>
                                <div class="form-group">
                                    <label for="no-ktp">Nomor KTP</label>
                                    <input type="text" class="form-control" id="no-ktp" value="">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="nama-pasien">Nama Pasien</label>
                                    <input type="text" class="form-control" id="nama-pasien" value="" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="nik">Nomor Induk Kependudukan (NIK)</label>
                                    <input type="text" class="form-control" id="nik" value="">
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" style="width: 20%;" id="btn-submit-update"><i class="fa fa-save"></i> Update</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 100,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}SatuSehat/patient/getIndex',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "10%",
                "targets": 1,
            },
            {
                "width": "10%",
                "targets": 2,
            },
            {
                "width": "5%",
                "targets": 3,
                "className": "text-center"
            },
            {
                "width": "10%",
                "targets": 4,
                "className": "text-center"
            },
            {
                "width": "10%",
                "targets": 5,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 6,
                "className": "text-center"
            },
            {
                "width": "15%",
                "targets": 7,
                "className": "text-center"
            },
            {
                "width": "40%",
                "targets": 8,
                "className": "text-center"
            },
        ]
    });
});

function loadIndex() {
    var medical_record_number = $('#medical_record_number').val();
    var patient_name = $('#patient_name').val();
    var ihs_number = $('#ihs_number').val();
    var status_integration = $('#status_integration option:selected').val();
    var filter_date = $('#filter_date option:selected').val();
    var arrived_date_start = $('#arrived_date_start').val();
    var arrived_date_end = $('#arrived_date_end').val();

    $('#datatable-simrs').DataTable().destroy();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '{site_url}SatuSehat/patient/getIndex/',
            type: "POST",
            dataType: 'json',
                data: {
                    medical_record_number: medical_record_number,
                    patient_name: patient_name,
                    ihs_number: ihs_number,
                    status_integration: status_integration,
                    filter_date: filter_date,
                    arrived_date_start: arrived_date_start,
                    arrived_date_end: arrived_date_end,
                }
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 3,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 4,
                "orderable": true,
            },
            {
                "width": "5%",
                "targets": 5,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 6,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "20%",
                "targets": 8,
                "orderable": true,
                "className": "text-center"
            },
        ]
    });
}

function loadHistory(id) {
    var tbody = $('#historyIntegrasi tbody');
    tbody.empty();

    $.ajax({
        url: '{site_url}SatuSehat/patient/getHistoryIntegration',
        type: 'POST',
        data: { id: id },
        dataType: 'json',
        success: function(response) {
            if (response.data.length > 0) {
                var no = 1;
                var label = '';
                response.data.forEach(function(item) {
                    if (item.type == 'BY NIK') {
                        label = 'label-warning';
                    } else if (item.type == 'BY KTP') {
                        label = 'label-danger';
                    } else if (item.type == 'BY DATA') {
                        label = 'label-success';
                    }
                    
                    var row = '<tr>' +
                        '<td>' + no + '</td>' +
                        '<td>' + item.date + '</td>' +
                        '<td><span class="label ' + label + '">' + item.type + '</span></td>' +
                        '<td>' + item.identifier + '</td>' +
                        '<td>' + item.status + '</td>' +
                        '<td>' + item.user_name + '</td>' +
                    '</tr>';
                    tbody.append(row);
                    no++;
                });
            } else {
                tbody.append('<tr><td colspan="6" class="text-center">Tidak ada data history</td></tr>');
            }
        },
        error: function(xhr, status, error) {
            console.error(xhr.responseText);
        }
    });
}

$('#UpdateDataModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var patientId = button.data('id');

    $.ajax({
        url: '{site_url}SatuSehat/Patient/getPatientById',
        type: 'POST',
        data: {patient_id: patientId},
        dataType: 'json',
        success: function(data) {
            $('#no-medrec').data('patient-id', patientId);
            $('#no-medrec').val(data.no_medrec);
            $('#nama-pasien').val(data.nama);
            $('#tanggal-lahir').val(data.tanggal_lahir);
            $('#no-ktp').val(data.ktp);
            $('#nik').val(data.nik);
        }
    });
});

$('#btn-submit-update').click(function () {
    var patientId = $('#no-medrec').data('patient-id');
    var tanggalLahir = $('#tanggal-lahir').val();
    var noKtp = $('#no-ktp').val();
    var nik = $('#nik').val();

    $.ajax({
        url: '{site_url}SatuSehat/Patient/updatePatientData',
        type: 'POST',
        dataType: 'json',
        data: {
            patient_id: patientId,
            tanggal_lahir: tanggalLahir,
            ktp: noKtp,
            nik: nik
        },
        success: function(response) {
            console.log(response)
            if(response.success) {
                swal("Berhasil!", "Data berhasil diperbarui", "success");
                location.reload();
            } else {
                swal("Error!", "Gagal memperbarui data", "success");
            }
        }
    });
});

function processAllByNik() {
    const startDate = $('#arrived_date_start').val();
    const endDate = $('#arrived_date_end').val();

    if (!startDate || !endDate) {
        alert('Silakan masukkan rentang tanggal yang valid.');
        return;
    }

    $('.loading-full').show();

    $.ajax({
        url: '{site_url}SatuSehat/Patient/process_all_by_nik',
        type: 'POST',
        data: {
            start_date: startDate,
            end_date: endDate
        },
        success: function(response) {
            alert('Proses berdasarkan NIK selesai!');
            loadIndex(); // Memuat ulang data setelah proses
            $('.loading-full').hide();
        },
        error: function(error) {
            console.error('Error processing by NIK:', error);
        }
    });
}

function processAllByKTP() {
    const startDate = $('#arrived_date_start').val();
    const endDate = $('#arrived_date_end').val();

    if (!startDate || !endDate) {
        alert('Silakan masukkan rentang tanggal yang valid.');
        return;
    }

    $('.loading-full').show();

    $.ajax({
        url: '{site_url}SatuSehat/Patient/process_all_by_ktp',
        type: 'POST',
        data: {
            start_date: startDate,
            end_date: endDate
        },
        success: function(response) {
            alert('Proses berdasarkan KTP selesai!');
            loadIndex(); // Memuat ulang data setelah proses
            $('.loading-full').hide();
        },
        error: function(error) {
            console.error('Error processing by KTP:', error);
        }
    });
}

function processAllByData() {
    const startDate = $('#arrived_date_start').val();
    const endDate = $('#arrived_date_end').val();

    if (!startDate || !endDate) {
        alert('Silakan masukkan rentang tanggal yang valid.');
        return;
    }

    $('.loading-full').show();

    $.ajax({
        url: '{site_url}SatuSehat/Patient/process_all_by_data',
        type: 'POST',
        data: {
            start_date: startDate,
            end_date: endDate
        },
        success: function(response) {
            alert('Proses berdasarkan data selesai!');
            loadIndex(); // Memuat ulang data setelah proses
            $('.loading-full').hide();
        },
        error: function(error) {
            console.error('Error processing by data:', error);
        }
    });
}

</script>
