<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}SatuSehat/practitioner" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('SatuSehat/practitioner/save','class="form-horizontal push-10-t"') ?>
        <div class="form-group">
            <label class="col-md-2" for="ppa_nik">PPA NIK</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="ppa_nik" placeholder="NIK" name="ppa_nik" value="{ppa_nik}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="ppa_nama">PPA Nama</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="ppa_nama" placeholder="Nama" name="ppa_nama" value="{ppa_nama}" readonly>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label class="col-md-2" for="practitioner_id">Practitioner ID</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="practitioner_id" placeholder="Practitioner ID" name="practitioner_id" value="{practitioner_id}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="practitioner_name">Practitioner Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="practitioner_name" placeholder="Practitioner Name" name="practitioner_name" value="{practitioner_name}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="practitioner_gender">Practitioner Gender</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="practitioner_gender" placeholder="Practitioner Gender" name="practitioner_gender" value="{practitioner_gender}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="practitioner_birthdate">Practitioner Birthdate</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="practitioner_birthdate" placeholder="Practitioner Birthdate" name="practitioner_birthdate" value="{practitioner_birthdate}" readonly>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}SatuSehat/practitioner" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<style>
    #json-content {
    background-color: #000;
    color: #fff;
    padding: 10px;
    border-radius: 5px;
    overflow-x: auto;
}
</style>

<div id="json-debug" style="display: none;">
    <h6>RESPONSE:</h6>
    <pre id="json-content" class="json"></pre>
</div>

<script type="text/javascript">
    displayJson('<?= $satu_sehat_response; ?>');

    function displayJson(json) {
        const formattedJson = JSON.stringify(JSON.parse(json), null, 4);
        document.getElementById('json-content').textContent = formattedJson;
        document.getElementById('json-debug').style.display = 'block';
    }
</script>


