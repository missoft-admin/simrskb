<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="table-responsive">
            <div class="row">
				<?php echo form_open('SatuSehat/Practitioner/filter','class="form-horizontal" id="form-work"') ?>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="">Status</label>
                        <div class="col-md-8">
                            <select id="status_practitioner" name="status_practitioner" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="-">All</option>
                                <option value="1">Connected</option>
                                <option value="0">Not Connect</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-8">
                            <button class="btn btn-success text-uppercase" type="button" onclick="loadIndex()" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                        </div>
                    </div>
                </div>
				<div class="col-md-6">
                </div>
				<?php echo form_close() ?>
			</div>

			<hr style="margin-top:10px">

            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Practitioner ID</th>
                        <th>Information</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}SatuSehat/practitioner/getIndex',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "10%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "15%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 4,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 5,
                "orderable": true,
                "className": "text-center"
            },
        ]
    });
});

function loadIndex() {
    var status_practitioner = $('#status_practitioner').val();

    $('#datatable-simrs').DataTable().destroy();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}SatuSehat/practitioner/getIndex',
            type: "POST",
            dataType: 'json',
            data: {
                status_practitioner: status_practitioner,
            }
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "10%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "15%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 4,
                "orderable": true,
                "className": "text-center"
            },
            {
                "width": "5%",
                "targets": 5,
                "orderable": true,
                "className": "text-center"
            },
        ]
    });
}

</script>
