<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Distributor</label>
                    <div class="col-md-8">
                        <select id="iddistributor" name="iddistributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Distributor -</option>
							<?foreach($list_distributor as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th width="10%">#</th>
                    <th width="10%">KODE</th>
                    <th width="10%">DISTRIBUTOR</th>
                    <th width="10%">ALAMAT</th>
                    <th width="10%">TELEPON</th>
                    <th width="15%" class="text-center">AKSI</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var iddistributor=$("#iddistributor").val();
	
	// alert(tanggalterima1);
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [
					// { "targets": [0,1], "visible": false },
							{ "width": "5%", "targets": [0] },
							{ "width": "10%", "targets": [1,2,3,4,5] },
							// { "width": "15%", "targets": [14] },
						 {"targets": [2,3,4], className: "text-left" },
						 {"targets": [1,5], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tkontrabon_his/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						iddistributor:iddistributor,
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

</script>