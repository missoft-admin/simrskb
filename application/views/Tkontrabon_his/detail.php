<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
       
        <div class="row">
            <div class="col-sm-6 col-lg-3">
				<div class="block block-rounded block-bordered">
					<div class="block-header bg-gray-lighter">
						<ul class="block-options">
							<li>
								<button type="button"><i class="fa fa-calendar"></i></button>
							</li>
						</ul>
						<h3 class="block-title">History Tagihan</h3>
					</div>
					<div class="block-content">
						<div class="pull-r-l pull-t push">
							<table class="block-table text-center bg-primary border-b">
								<tbody>
									<tr>
										<td class="border-r bg-success" style="width: 20%;">
											<div class="push-30 push-30-t">
												<i class="fa fa-building fa-3x text-white-op"></i>
											</div>
										</td>
										<td>
											<div class="h5 text-left text-white  font-w500"><?=$nama?></div>
											<div class="text-left text-white  font-w200">Alamat :<?=$alamat?></div>
											<div class="text-left text-white  font-w200">Telepon :<?=$telepon?></div>
											<input class="form-control" type="hidden" name="iddistributor" id="iddistributor" value="{iddistributor}" >
										</td>
									</tr>
									<tr>
										<td colspan="2" class="border-r bg-danger">
											<select id="tahun" name="tahun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#">- Semua Tahun -</option>
												<?for($x =2020; $x <=  date('Y'); $x++){?>
													<option value="<?=$x?>" <?=($x==date('Y')?'selected':'')?>><?=$x?></option>									
												<?}?>
											
											</select>
										</td>
									<tr>
								</tbody>
							</table>
							
						</div>
						
					</div>
					<div class="block-content">
						
						<div class="pull-r-l pull-t push">
							
							<table class="table block-table text-center" id="index_left">
								<tbody>
												
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-lg-9">
				<div class="block block-rounded block-bordered">
					<div class="block-header bg-gray-lighter">
						<ul class="block-options">
							<li>
								<button type="button"><i class="fa fa-history"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Detail Tagihan</h3>
					</div>
					<div class="block-content">
						<div class="row">
							<input type="hidden" class="form-control detail" id="ym" name="ym" value="{ym}">
							<div class="col-md-6">
								<h3 id="text_header"></h3>
							</div>
							<div class="col-md-6">
								<button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;float:right;"><i class="fa fa-print"></i> Cetak History</button>
							</div>
							<hr>
							<div class="col-md-12">
								<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_list">
									<thead>
										<tr>
											<th width="10%">#</th>
											<th width="10%">#</th>
											<th width="10%">NO REG.</th>
											<th width="10%">CARA BAYAR</th>
											<th width="10%">NOMINAL</th>
											<th width="10%">USER VERIFIKASI</th>
											<th width="10%">USER VALIDASI</th>
											<th width="10%">USER USER KONFIRMASI</th>
											<th width="10%">PENYERAHAN</th>
											<th width="10%">AKSI</th>
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
	
</div>
<div class="modal fade" id="modal_serahkan" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Bukti Penyerahan</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal </label>
							<div class="col-md-8">
							<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
								<input class="js-datepicker form-control" type="text" id="tgl_serahkan" name="tgl_serahkan" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 
						<hr>
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Nama Penerima </label>
							<div class="col-md-8">
							<input type="text" class="form-control" id="penerima" placeholder="Nama Penerima" name="penerima" value="">
							<input type="hidden" class="form-control" id="st_edit" placeholder="" name="st_edit" value="">
							</div>
						</div> 
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="block" id="box_file2">	
								<div class="table-responsive">
								 <table class="table table-bordered" id="list_file">
									<thead>
										<tr>
											<th width="50%" class="text-center">File</th>
											<th width="15%" class="text-center">Size</th>
											<th width="35%" class="text-center">User</th>
											<th width="35%" class="text-center">X</th>
											
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							</div>
							
						</div> 
						
						
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="id_penyerahan" placeholder="No. PO" name="id_penyerahan" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_serahkan" id="btn_serahkan" style="display:none"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){	
   load_tahun();
   clear_data();
});
$("#tahun").change(function() {
	clear_data();
	load_tahun();
});
function load_index(){
	var iddistributor=$("#iddistributor").val();
	var ym=$("#ym").val();
	
	// alert(tanggalterima1);
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [
							 { targets: [0], visible: false},
							 {  className: "text-center", targets:[2,3,5,6,7,8,9] },
							 {  className: "text-right", targets:[1,4] },
							 { "width": "3%", "targets": [1] },
							 { "width": "8%", "targets": [3] },
						 ],
            ajax: { 
                url: '{site_url}tkontrabon_his/get_index_detail', 
                type: "POST" ,
                dataType: 'json',
				data : {
						iddistributor:iddistributor,ym:ym,
					   }
            }
        });
}
function clear_data(){
	$("#text_header").html('');
		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		var table = $('#index_list').DataTable( {
			"searching": false,
			columnDefs: [
				 { targets: [0], visible: false},
				 {  className: "text-center", targets:[2,3,5,6,7,8,9] },
				 {  className: "text-right", targets:[1,4] },
				 { "width": "3%", "targets": [1] },
				 { "width": "8%", "targets": [3] },
				 // { "width": "10%", "targets": [11] }
			]
		} );
	}
function load_tahun(){
	var tahun=$("#tahun").val();
	var iddistributor=$("#iddistributor").val();
	$('#index_left tbody').empty();
	$.ajax({
		url: '{site_url}tkontrabon_his/load_left',
		type: 'POST',
		dataType: "json",
		data: {
			iddistributor: iddistributor,
			tahun: tahun,
			
		},
		success: function(data) {
			var content='';
			$.each(data, function (i,row) {
				content='';
				content +='<tr>';										
				content +='<td><button class="btn btn-block btn-primary btn_bulan" type="button" value="'+row.ym+'"><i class="fa fa-calendar pull-left"></i>'+get_nama_bulan(row.m)+' '+row.y+'</button></td>';
				content +='</tr>';
				$('#index_left tbody').append(content);
			console.log(row.ym);
					
			});
		}
	});

	// alert(tahun);
}
$(document).on("click",".btn_bulan",function(){	
	var ym=$(this).val();
	$("#ym").val(ym);
	generate_bulan(ym);
	load_index();
});
function generate_bulan($ym){
	var str=$ym;
	$bulan=get_nama_bulan(str.substring(str.length - 2, str.length));
	$tahun=str.substring(0,4);
	$("#text_header").html('<span class="label label-default">'+$bulan+' '+$tahun+'</span>');
	
}
$(document).on("click",".serahkan",function(){	
	var table = $('#index_list').DataTable();
	tr = table.row( $(this).parents('tr') ).index()
	var id=table.cell(tr,0).data();
	// alert(id);
	load_file_serahkan(id);		
});
function load_file_serahkan($id){
		var id=$id;
		 
		$('#modal_serahkan').modal('show');
		$('#list_file tbody').empty();
		$.ajax({
			url: '{site_url}tkontrabon/get_data_serahkan/'+id,
			dataType: "json",
			success: function(data) {
				$("#penerima").val(data.header.nama_penerima);
				$("#tgl_serahkan").val(data.header.tanggal_penyerahan);
				if (data.detail!=''){
					$("#list_file tbody").append(data.detail);
					$("#box_file2").attr("hidden",false);
				}else{
					$("#box_file2").attr("hidden",true);
					
				}
				// console.log(data);
				
			}
		});
	}
function get_nama_bulan($bulan){
	var text;
	// var bulan = "0";
	switch ($bulan) {
	  case '01':
		text = "JANUARI";
		break;
	  case '02':
		text = "FEBRUARI";
		break;
	  case '03':
		text = "MARET";
		break;
	  case '04':
		text = "APRIL";
		break;
	  case '05':
		text = "MEI";
		break;
	  case '06':
		text = "JUNI";
		break;
	   case '07':
		text = "JULI";
		break;
	  case '08':
		text = "AGUSTUS";
		break;
	  case '09':
		text = "SEPTEMBER";
		break;
	  case '10':
		text = "OKTOBER";
		break;
	  case '11':
		text = "NOVEMBER";
		break;
	  case '12':
		text = "DESEMBER";
		break;
	  default:
		text = " ";
	}
	return text;
}
</script>