<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tvalidasi_jurnal_umum" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tvalidasi_jurnal_umum/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			
			<input type="hidden" style="width: 100%"  class="form-control" id="id" placeholder="id" name="id" value="{id}">										
			<input type="hidden" style="width: 100%"  class="form-control" id="disabel" placeholder="disabel" name="disabel" value="{disabel}">										
			
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-2 control-label" >No Jurnal</label>				
				<div class="col-md-9">
					<input type="text" readonly style="width: 100%"  class="form-control" id="nojurnal" placeholder="No. Jurnal" name="nojurnal" value="{nojurnal}">		
				</div>				
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-2 control-label" >Tanggal</label>				
				<div class="col-md-2">
					<div class="input-group date">
					<input  class="js-datepicker form-control input" <?=$disabel?>  type="text" id="tanggal_transaksi" name="tanggal_transaksi" value="<?=HumanDateShort($tanggal_transaksi)?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>	
				</div>				
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-2 control-label" >Tipe Transaksi</label>				
				<div class="col-md-9">
					<select id="tipe" name="tipe" <?=$disabel?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#">- Pilih Tipe -</option>
						<?foreach($list_tipe as $row){?>
							<option value="<?=$row->id?>" <?=$row->id==$tipe?'selected':''?>><?=$row->nama?></option>									
						<?}?>
						
					</select>
				</div>
				
			</div>
			<div class="form-group" id="div_jenis"  style="margin-bottom: 10px;" <?=($tipe !='3'?'hidden':'')?>>
				<label class="col-md-2 control-label" >Jenis Transaksi</label>				
				<div class="col-md-9">
					<select id="jenis" name="jenis" <?=$disabel?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#">- Pilih Jenis -</option>
						<?foreach($list_jenis as $row){?>
							<option value="<?=$row->id?>" <?=$row->id==$jenis?'selected':''?>><?=$row->nama?></option>									
						<?}?>
						
					</select>
				</div>
				
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-2 control-label" >Keterangan</label>				
				<div class="col-md-9">
					<textarea class="form-control js-summernote" name="keterangan" id="keterangan"><?=$keterangan?></textarea>
				</div>
				
			</div>
			<?if ($id==''){?>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-2 control-label" ></label>				
				<div class="col-md-9">
					<button type="submit" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Create"><i class="fa fa-plus"></i> Create Jurnal</button>
				</div>				
			</div>
			<?}?>
			<hr>
			
			<?if ($id!=''){?>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-1 control-label"><h4><?=text_primary('Jurnal Detail')?></h4></label>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-12">
					<table id="tabel_jurnal" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 5%;">#</th>															
								<th style="width: 30%;">No Akun</th>															
								<th style="width: 10%;">Debit</th>
								<th style="width: 10%;">Kredit</th>
								<th style="width: 25%;">Keterangan</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>								
								<td>
									<select id="idakun" <?=$disabel?> name="idakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih No Akun -</option>
										<?foreach($list_akun as $row){?>
											<option value="<?=$row->id?>"><?=$row->noakun.' - '.$row->namaakun?></option>									
										<?}?>
										
									</select>									
								</td>								
								
								<td>
									<input type="text" <?=$disabel?> style="width: 100%"  class="form-control number" id="debet" placeholder="Debit" name="debet" value="">
								</td>
								<td>
									<input type="text" <?=$disabel?> style="width: 100%"  class="form-control number" id="kredit" placeholder="Kredit" name="kredit" value="">
								</td>
								<td>
									<input type="text" <?=$disabel?> style="width: 100%"  class="form-control" id="ket" placeholder="Keterangan" name="ket" value="">
								</td>	
								
								<td>									
									<button type="button" <?=$disabel?> class="btn btn-sm btn-primary" tabindex="8" id="simpan_jurnal" title="Masukan Item"><i class="fa fa-plus"></i></button>
									<button type="button" <?=$disabel?> class="btn btn-sm btn-warning" tabindex="8" id="clear_jurnal" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
									<input type="hidden" style="width: 100%" readonly class="form-control number" id="iddet" placeholder="Batas Waktu Batal" name="iddet" value="">
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0px;">
				<label class="col-md-1 control-label"><button type="button" class="btn btn-sm btn-danger" tabindex="8" id="btn_upload" title="Create"><i class="fa fa-plus"></i> Upload Dokumen</button></label>
			</div>
			<div class="block-content ">		
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<div class="table-responsive">
							 <table class="table table-bordered table-striped table-responsive" id="listFile">
								<thead>
									<tr>
										<th width="1%">No</th>
										<th width="20%">File</th>
										<th width="15%">Upload By</th>
										<th width="5%">Size</th>
										<th width="5%">Aksi</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
			<?if ($disabel==''){?>
			<div class="block-content">
				 <hr style="margin-top:10px">
				 <div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-xs-12 text-right">
								<a href="{base_url}tvalidasi_jurnal_umum" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Batal</a>
								<button class="btn btn-success btn_simpan" name="btn_simpan" type="submit" value="2"><i class="si si-arrow-right"></i> Simpan & Posting</button>
								<button class="btn btn-primary btn_simpan" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Simpan Jurnal</button>
							</div>
						</div>
						
					</div>
				</div>
				<hr style="margin-bottom:10px">		
				
			</div>
			<?}?>
			<?}?>
			<?php echo form_close() ?>
	</div>
	
	
</div>
<div class="modal fade" id="modal_upload" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Upload Document</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<form class="dropzone" action="{base_url}tvalidasi_jurnal_umum/upload_files" method="post" enctype="multipart/form-data">
									<input name="idvalidasi" type="hidden" value="{id}">
								  </form>
							</div>
						</div>
					</div>
					
				</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
var myDropzone 
	$(document).ready(function() {
		// $('.number').number(true, 2);
		$(".number").number(true,2,'.',',');
		$('#keterangan').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

		// load_unit();
		if ($('#id').val()){
			// load_jurnal();
			clear_input_jurnal();
			refresh_image();
			Dropzone.autoDiscover = false;
			myDropzone = new Dropzone(".dropzone", { 
			   autoProcessQueue: true,
			   maxFilesize: 30,
			});
			myDropzone.on("complete", function(file) {
			  
			  refresh_image();
			  myDropzone.removeFile(file);
			  
			});
			
		}

	});	
	function refresh_image(){
		var id=$("#id").val();
		$('#listFile tbody').empty();
		$.ajax({
			url: '{site_url}tvalidasi_jurnal_umum/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('#listFile tbody').empty();
					$("#listFile tbody").append(data.detail);
				// }
				// console.log();
				
			}
		});
	}
	function removeFile($id){
		 var id=$id;
        swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}tvalidasi_jurnal_umum/removeFile',
					type: 'POST',
					data: {id: id},
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
						refresh_image();
						// $("#cover-spin").hide();
						// filter_form();
					}
				});
			});
	}
	$(document).on("change","#tipe",function(){
		if ($(this).val()=='3'){
			$("#div_jenis").show();
		}else{
			$("#div_jenis").hide();
		}
	});
	function validate_update()
	{
		
		if ($("#batas_batal_dep").val()=='' || $("#batas_batal_dep").val()=='0'){
			sweetAlert("Maaf...", "Batas Waktu Depresiasi Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal_pen").val()=='' || $("#batas_batal_pen").val()=='0'){
			sweetAlert("Maaf...", "Batas Waktu Batal Penyesesuaian Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	
	function validate_final()
	{		
		if ($("#tipe").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#tipe").val()=='3' && $("#jenis").val()=='#'){
			sweetAlert("Maaf...", "Jenis Harus diisi", "error");
			return false;
		}
		if ($("#tanggal_transaksi").val()==''){
			sweetAlert("Maaf...", "Tanggal Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function validate_add_jurnal()
	{
		
		var debet=($("#debet").val());
		var kredit=($("#kredit").val());
		if ($("#idakun").val()=='#'){
			sweetAlert("Maaf...", "No. Akun Harus diisi", "error");
			return false;
		}
		if (debet=='' && kredit==''){
			sweetAlert("Maaf...", "Debit / Kredit Harus diisi", "error");
			return false;
		}
		if (debet=='0' && kredit=='0'){
			sweetAlert("Maaf...", "Debit / Kredit Harus diisi", "error");
			return false;
		}
		if (debet > 0 && kredit>0){
			sweetAlert("Maaf...", "Salah satu Debit / Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_jurnal(){
		$("#idakun").val('#').trigger('change');		
		$("#debet").val('0');		
		$("#kredit").val('0');		
		$("#ket").val('');		
		$("#iddet").val('');	
		load_jurnal();
	}
	$(document).on("click","#btn_upload",function(){
		$("#modal_upload").modal('show');
	});
	$(document).on("click","#simpan_jurnal",function(){
		if (validate_add_jurnal()==false)return false;
		var id=$("#id").val();
		var iddet=$("#iddet").val();
		var debet=$("#debet").val();
		var kredit=$("#kredit").val();
		var ket=$("#ket").val();
		var idakun=$("#idakun").val();
		
		$.ajax({
			url: '{site_url}tvalidasi_jurnal_umum/simpan_jurnal',
			type: 'POST',
			data: {
				iddet:iddet,debet:debet,kredit: kredit,
				idakun:idakun,ket:ket,id:id
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					clear_input_jurnal();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	$(document).on("click",".edit",function(){
		var iddet=$(this).closest('tr').find(".idtrx").val();
		$.ajax({
			url: '{site_url}tvalidasi_jurnal_umum/get_edit/',
			dataType: "json",
			type: "POST",
			data: {id: iddet},
			success: function(data) {
				$("#debet").val(data.debet);
				$("#kredit").val(data.kredit);
				$("#idakun").val(data.idakun).trigger('change');
				$("#ket").val(data.ket);
				$("#iddet").val(data.id);
				// alert(data)
			}
		});

	});
	function hapus_jurnal($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tvalidasi_jurnal_umum/hapus_jurnal/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						clear_input_jurnal();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_jurnal",function(){
		
		clear_input_jurnal();
	});
	
	function load_jurnal(){
		$("#cover-spin").show();
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		$('#tabel_jurnal tbody').empty();
		$.ajax({
			url: '{site_url}tvalidasi_jurnal_umum/load_jurnal/',
			dataType: "json",
			type: "POST",
			data: {id: id,disabel: disabel},
			success: function(data) {
				// console.log(data.tabel);
				$('#tabel_jurnal').append(data.tabel);
				$("#cover-spin").hide();
				// alert($("#total_debet").val());
				cek_validasi();
			}
		});
	}
	function cek_validasi(){
		
		if ($("#total_debet").val()=='0' || $("#total_kredit").val()=='0'){
			$(".btn_simpan").attr('disabled',true);
		}else{
			if ($("#total_debet").val() != $("#total_kredit").val()){
				$(".btn_simpan").attr('disabled',true);
			}else{
				$(".btn_simpan").attr('disabled',false);
			}
		}
	}
	
</script>
