<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
		@page {
            margin-top: 1,8em;
            margin-left: 2,3em;
            margin-right: 2em;
            margin-bottom: 2.5em;
        }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
	  
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-normal{
		font-size: 16px !important;
      }
	  .text-white{
		color: #fff;
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
        font-weight: bold;
      }
	  .text-judul-rs{
        font-size: 24px  !important;
        font-weight: bold;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
		}
		legend {
			background:#fff;
		}
    }

footer {
	position: fixed;
	bottom: 0px;
	height: 50px;
	text-align: center;
	line-height: 35px;
}
  </style>
</head>

<body>
	<? $this->load->view('Tsurat/pdf_header')?>
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><?=$judul_ina?><br><i><?=$judul_eng?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-judul"><?=$nopermintaan?> <br></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-judul"></td>
		</tr>
		
	</table>
	<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><?=strip_tags($paragraf_1_ina)?>&nbsp;&nbsp;&nbsp; : <?=$nama_dokter?><br> <i><?=strip_tags($paragraf_1_eng)?></i></td>
		</tr>
		<tr>
			<td class="text-left text-normal"><?=strip_tags($paragraf_2_ina)?><br> <i><?=strip_tags($paragraf_2_eng)?></i></td>
		</tr>
	</table>	
	<table class="content">
		
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong><?=strip_tags($nama_pasien_ina)?></strong><br><i><?=strip_tags($nama_pasien_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$nama_pasien?> </strong></td>
		</tr>
		
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong><?=strip_tags($alamat_pasien_ina)?></strong><br><i><?=strip_tags($alamat_pasien_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$alamat_pasien?> </strong></td>
		</tr>
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong>Pekerjaan</strong><br><i>Job</i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$nama_pekerjaan?> </strong></td>
		</tr>
		
	</table>
	<table class="content">
		
		<tr>
			<td width="24%" class="text-left text-normal" ><strong><?=strip_tags($pelayanan_ina)?></strong><br><i><?=strip_tags($pelayanan_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><strong><?=($st_ranap=='1'?'RAWAT INAP':'RAWAT JALAN')?> </strong></td>
		</tr>
		<?
		$tanggal_rawat='';
		if ($st_ranap=='1'){//RANAP
			$tanggal_rawat=tanggal_indo_DMY($tanggal_pelayanan_awal).($tanggal_pelayanan_akhir?' s.d '.tanggal_indo_DMY($tanggal_pelayanan_akhir):'');
		}else{
			$tanggal_rawat=tanggal_indo_DMY($tanggal_pelayanan_awal);
		}
		?>
		<tr>
			<td width="24%" class="text-left text-normal" ><strong><?=strip_tags($tanggal_tindakan_ina)?></strong><br><i><?=strip_tags($tanggal_tindakan_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><strong><?=($tanggal_rawat)?> </strong></td>
		</tr>
		
		
	</table>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><?=strip_tags($paragraf_3_ina)?><br> <i><?=strip_tags($paragraf_3_eng)?></i></td>
		</tr>
	</table>	
	<table class="content">
		
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong><?=strip_tags($riwayat_ina)?></strong><br><i><?=strip_tags($riwayat_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$riwayat_penyakit?> </strong></td>
		</tr>
		
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong><?=strip_tags($penunjang_ina)?></strong><br><i><?=strip_tags($penunjang_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$penunjang?> </strong></td>
		</tr>
		
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong><?=strip_tags($diagnosa_akhir_ina)?></strong><br><i><?=strip_tags($diagnosa_akhir_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$diagnosa_akhir?> </strong></td>
		</tr>
		
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong><?=strip_tags($terapi_ina)?></strong><br><i><?=strip_tags($terapi_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$terapi?> </strong></td>
		</tr>
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong><?=strip_tags($tindakan_ina)?></strong><br><i><?=strip_tags($tindakan_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$tindakan?> </strong></td>
		</tr>
		<tr>
			<td width="4%">&nbsp;</td>
			<td width="20%" class="text-left text-normal" ><strong><?=strip_tags($keadaan_ina)?></strong><br><i><?=strip_tags($keadaan_eng)?></i></td>
			<td width="1%"><strong>:</strong></td>
			<td width="75%" class="text-left text-normal"><?=$keadaan?> </strong></td>
		</tr>
		
		
	</table>
	<table class="content">
		
		<tr>
			<td colspan="3" class="text-left text-normal"><?=strip_tags($paragraf_4_ina)?><br><i><?=strip_tags($paragraf_4_eng)?></i></td>
		</tr>
		<tr>
			<td colspan="3" class="text-left text-normal">&nbsp;</i></td>
		</tr>
		<tr>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:40%" class="text-center text-normal"><strong><?=$dokter_ina?></strong><br><i class="text-muted"><?=$dokter_eng?></i></td>
		</tr>
		<tr>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:40%" class="text-center text-normal">
			<img class="" style="width:100px;height:100px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ttd_dokter/<?=$dpjp?>" alt="" title=""></td>
		</tr>
		<tr>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:40%" class="text-center text-normal">
			
			<?=get_nama_dokter_ttd($dpjp)?> 
			</td>
		</tr>
		
		<tr>
			<td colspan="3" class="text-left text-normal"><?=strip_tags($footer_ina)?><br>
			Created By : <?=get_nama_ppa($created_ppa)?>   <?=HumanDateLong($tanggal_input)?>
			<?if ($printed_by){?>
			 | Printed By :  <?=get_nama_ppa($printed_by)?>   <?=HumanDateLong($printed_date)?> | Printed Number : <?=$jumlah_cetak?> <br></td>
			<?}else{
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				?>
			 | Printed By :  <?=get_nama_ppa($login_ppa_id)?>   <?=HumanDateLong(date('Y-m-d H:i:s'))?> | Printed Number : 1 <br></td>
			<?}?>
		</tr>
		
	</table>
	</main>
</body>

</html>
