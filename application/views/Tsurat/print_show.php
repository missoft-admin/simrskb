<!DOCTYPE html>
<html>
  <head>
	  <link rel="stylesheet" href="<?=$css_path?>bootstrap.min.css">
  <link rel="stylesheet" href="<?=$css_path?>fonts.googleapis.com.css">
  <link rel="stylesheet" id="css-main" href="<?=$css_path?>oneui.css">
  <link rel="stylesheet" href="<?=$css_path?>ionicons.min.css">

	<script type="text/javascript" src="<?=$js_path?>jmin.js"></script>
    <meta charset="utf-8">
    <title>PRINT</title>
  </head>
  <body>
   
	<input type="hidden" name="assesmen_id" id="assesmen_id" value="<?=$assesmen_id?>">
	<input type="hidden" name="jenis_surat" id="jenis_surat" value="<?=$jenis_surat?>">
	<input type="hidden" name="base_url" id="base_url" value="<?=$base_url?>">
	<input type="hidden" name="st_cetak" id="st_cetak" value="<?=$st_cetak?>">
	<input type="hidden" name="ukuran" id="ukuran" value="<?=$ukuran?>">
        <div class="content">
			<div class="block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<div class="form-group">
						<div class="col-md-12">
							<button id="print" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</button><br>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<iframe id="nota" name="nota" allowFullscreen="true" frameborder="0" height="1500" width="100%" scrolling="no" src="<?= site_url("tsurat/$function_surat/$assesmen_id/#toolbar=0&navpanes=0") ?>"></iframe>
						</div>
					</div>
				</form>
				
			</div>
	</div>
  
   <script type="text/javascript">  
      printFrame = document.getElementById('nota'); 
	  base_url=$('#base_url').val(); 
	  assesmen_id=$('#assesmen_id').val(); 
	  jenis_surat=$('#jenis_surat').val(); 
	  ukuran=$('#ukuran').val(); 
      btnprint = document.getElementById('print'); 
      btnprint.onclick = function(){ 
	  // var base_url=$("#base_url").val();
				// // // printFrame.close();
				var win = window.open(base_url+'tsurat/show_cetak/'+assesmen_id+'/'+jenis_surat+'/1','_self');
				if (win) {
					//Browser has allowed it to be opened
					win.focus();
				} else {
					//Browser has blocked it
					alert('Please allow popups for this website');
				}
					
				$("#st_cetak").val('1');
				// alert(base_url);
				// printFrame.contentWindow.print();
			   }
			   
	$(document).ready(function() {
		// alert('SNI MASR BROO');
		if ($("#st_cetak").val()=='1'){
			printFrame.contentWindow.print();
		}
	});
  </script>
  </body>
</html>
