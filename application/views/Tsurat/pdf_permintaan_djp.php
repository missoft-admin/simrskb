<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>PERMINTAAN DOKTER PENANGGUNG JAWAB</title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
		@page {
            margin-top: 1,8em;
            margin-left: 2,3em;
            margin-right: 2em;
            margin-bottom: 2.5em;
        }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
	  
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-normal{
		font-size: 16px !important;
      }
	  .text-white{
		color: #fff;
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
        font-weight: bold;
      }
	  .text-judul-italic{
        font-size: 16px  !important;
      }
	  .text-judul-rs{
        font-size: 24px  !important;
        font-weight: bold;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
		}
		legend {
			background:#fff;
		}
    }

footer {
	position: fixed;
	bottom: 0px;
	height: 50px;
	text-align: center;
	line-height: 35px;
}
  </style>
</head>

<body>
	<? $this->load->view('Tsurat/pdf_header')?>
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><?=$judul_header?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-judul-italic"><i><?=$judul_header_eng?></i></td>
		</tr>
		
		<tr>
			<td width="100%" class="text-center text-judul"></td>
		</tr>
		
	</table>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?=$paragraf_1_ina?></strong><br><i><?=$paragraf_1_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal "><strong><?=$nama_ttd_ina?></strong><br><i><?=$nama_ttd_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$nama_ttd?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$ttl_ttd_ina?></strong><br><i><?=$ttl_ttd_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=($ttl_ttd?HumanDateShort($ttl_ttd):'')?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$umur_ttd_ina?></strong><br><i><?=$umur_ttd_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="24%" class="text-left text-normal"><?=$umur_ttd?></td>
		</tr>
		
	</table>
	<table class="content">
		
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$alamat_ttd_ina?></strong><br><i><?=$alamat_ttd_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="51%" class="text-left text-normal"><?=$alamat_ttd?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$hubungan_ina?></strong><br><i><?=$hubungan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=get_nama_ref($hubungan_ttd,9)?></td>
		</tr>
		
	</table>
	<br>
		<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?=$paragraf_2_ina?></strong><br><i><?=$paragraf_2_eng?></i></td>
		</tr>
	</table>
		
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$noreg_ina?></strong><br><i><?=$noreg_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$nomedrec_ina?></strong><br><i><?=$nomedrec_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$namapasien?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$ttl_pasien_ina?></strong><br><i><?=$ttl_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= HumanDateShort($tanggal_lahir)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$umur_pasien_ina?></strong><br><i><?=$umur_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$umurtahun?> Tahun <?=$umurbulan?> Bulan <?=$umurhari?> Hari</td>
			<td width="12%" class="text-left text-normal"><strong><?=$jk_ina?></strong><br><i><?=$jk_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=get_nama_ref($jenis_kelamin,1)?></td>
		</tr>
		
		
	</table>
	
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?=$paragraf_3_ina?></strong><br><i><?=$paragraf_3_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;&nbsp;</td>
			<td width="20%" class="text-left text-normal"><strong><?=$jadi_dokter_ina?></strong><br><i><?=$jadi_dokter_eng?></i></td>
			<td width="1%" class="text-left text-normal">:</td>
			<td width="76%" class="text-left text-normal"><?=$nama_dokter?></td>
		</tr>
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;&nbsp;</td>
			<td width="20%" class="text-left text-normal"><strong><?=$alasan_ina?></strong><br><i><?=$alasan_eng?></i></td>
			<td width="1%" class="text-left text-normal">:</td>
			<td width="76%" class="text-left text-normal"><?=$alasan?></td>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?=$paragraf_4_ina?></strong><br><i><?=$paragraf_4_eng?></i></td>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td style="width:30%" class="text-left text-normal"><strong><?=$yg_pernyataan_ina?></strong><br><i><?=$yg_pernyataan_eng?></i></td>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:40%" class="text-center text-normal"><strong><?=$saksi_rs_ina?></strong><br><i><?=$saksi_rs_eng?></i></td>
		</tr>
		<tr>
			<td style="width:30%" class="text-left text-normal"><img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_pernyataan?>" alt="" title=""></td>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:40%" class="text-center text-normal">
			<img class="" style="width:120px;height:120px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_id?$petugas_id:$login_ppa_id)?>" alt="" title=""></td>
		</tr>
		<?
			$arr_petugas=get_nama_ppa_array($petugas_id);
		?>
		<tr>
			<td style="width:30%" class="text-left text-normal"><strong>( <?=$nama_ttd?> )</strong></td>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:40%" class="text-center text-normal"><i><?=$arr_petugas['nama']?><br><?=$arr_petugas['nik']?></i></td>
		</tr>
		<tr>
			<td colspan="3" class="text-left text-normal"><?=strip_tags($footer_ina)?><br>
			Created By : <?=get_nama_ppa($created_ppa)?>   <?=HumanDateLong($tanggal_input)?>
			<?if ($printed_by){?>
			 | Printed By :  <?=get_nama_ppa($printed_by)?>   <?=HumanDateLong($printed_date)?> | Printed Number : <?=$jumlah_cetak?> <br></td>
			<?}else{
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				?>
			 | Printed By :  <?=get_nama_ppa($login_ppa_id)?>   <?=HumanDateLong(date('Y-m-d H:i:s'))?> | Printed Number : 1 <br></td>
			<?}?>
		</tr>
		
		
	</table>
	<br>
	</main>
</body>

</html>
