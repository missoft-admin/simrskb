<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrka_kegiatan_setting" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka_kegiatan_setting/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Kegiatan</label>
				<div class="col-md-10">
					<input type="hidden" class="form-control" id="idrka_kegiatan" placeholder="idrka_kegiatan" name="idrka_kegiatan" value="{idrka_kegiatan}" required="" aria-required="true">
					<input type="text" class="form-control" disabled <?=$disabel?> <?=$disabel?> placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
					<input type="hidden" class="form-control" id="disabel" placeholder="DISABEL" name="disabel" value="{disabel}">
					<input type="hidden" class="form-control" id="idkegiatan" placeholder="idkegiatan" name="idkegiatan" value="{idkegiatan}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="bulan" placeholder="bulan" name="bulan" value="{bulan}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="idrka" placeholder="idrka" name="idrka" value="{idrka}" required="" aria-required="true">
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Bulan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="form-control input" readonly <?=$disabel?> type="text" id="bulan_nama" name="bulan_nama" value="<?=$bulan_nama?>"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama"></label>
				<label class="col-md-2 control-label" for="nama">Total Anggaran</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="form-control input number" readonly <?=$disabel?> type="text" id="nominal" name="nominal" value="<?=$nominal?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-money"></span>
					</span>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Vendor</label>
				<div class="col-md-4">
					<div class="input-group">
						<select id="idvendor" name="idvendor" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idvendor=='#'?'selected':'')?>>- Pilih Vendor -</option>
							<option value="0" <?=($idvendor=='0'?'selected':'')?>>Tentukan Vendor Nanti</option>
							<? foreach($list_vendor as $row){ ?>
								<option value="<?=$row->id?>" <?=($idvendor==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" <?=$disabel?> type="button" id="btn_refresh_vendor" title="Refresh"><i class="fa fa-refresh"></i></button>
							<a href="{base_url}mvendor" target="_blank" class="btn btn-primary" <?=$disabel?> type="button" id="btn_add_vendor" title="Tambah Vendor"><i class="fa fa-plus"></i></a>
						</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama">Unit</label>
				<div class="col-md-4">
					<select id="idunit" disabled name="idunit" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit==$row->idunit?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Dibutuhkan</label>
				<div class="col-md-4">
					<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                           
							<input type="text"  class="form-control" id="tanggal_bayar"  placeholder="Tanggal Pembayaran" name="tanggal_bayar" value="{tgl_bayar}">

							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
							
					</div>
				</div>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<h5 style="margin-bottom: 10px;">DETAIL PENGAJUAN </h5>
			
			<div class="form-group">
				<table id="tabel_add" class="table table-striped table-bordered" style="margin-bottom: 0;">
					<thead>
						<tr>
							<th style="width: 15%;">Nama Barang</th>
							<th style="width: 15%;">Merk Barang</th>
							<th style="width: 5%;">QTY</th>
							<th style="width: 5%;">Satuan</th>
							<th style="width: 10%;">Harga</th>
							<th style="width: 10%;">Total</th>
							<th style="width: 15%;">Keterangan</th>
							<th style="width: 8%;">Actions</th>
						</tr>
						<tr>							
							<td><input class="form-control input-sm" tabindex="2" type="text" id="nama_barang" /></td>
							<td><input class="form-control input-sm" tabindex="3" type="text" id="merk_barang" /></td>
							<td><input class="form-control input-sm number" tabindex="4" type="text" id="kuantitas" /></td>
							<td><input class="form-control input-sm" tabindex="5" type="text" id="satuan" /></td>
							<td><input class="form-control input-sm number" tabindex="6" type="text" id="harga_satuan" /></td>
							<td><input class="form-control input-sm number" readonly  type="text" id="total_harga" /></td>
							<td><input class="form-control input-sm" tabindex="7" type="text" id="keterangan" /></td>
							<td>
								<button type="button" <?=$disabel?> class="btn btn-sm btn-primary kunci" tabindex="8" id="addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
								<button type='button' <?=$disabel?> class='btn btn-sm btn-danger kunci' tabindex="9" id="canceljual" title="Refresh"><i class='fa fa-times'></i></button>
							</td>
						</tr>
					</thead>
					<tbody>
						<? foreach($list_detail as $row){ ?>
							<tr>
								<td><?=$row->nama_barang?></td>
								<td><?=$row->merk_barang?></td>
								<td><?=$row->kuantitas?></td>
								<td><?=$row->satuan?></td>
								<td><?=number_format($row->harga_satuan)?></td>
								<td><?=number_format($row->total_harga)?></td>
								<td><?=$row->keterangan?></td>
								<td><? echo "<div class='btn-group'><button type='button' ".$disabel." class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' ".$disabel." class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div>"?></td>
								<td style='display:none'><input type='text' name='xiddet[]' value="<?=$row->id?>"></td>
								<td style='display:none'><input type='text' name='xnama_barang[]' value="<?=$row->nama_barang?>"></td>
								<td style='display:none'><input type='text' name='xmerk_barang[]' value="<?=$row->merk_barang?>"></td>
								<td style='display:none'><input type='text' name='xkuantitas[]' value="<?=$row->kuantitas?>"></td>
								<td style='display:none'><input type='text' name='xsatuan[]' value="<?=$row->satuan?>"></td>
								<td style='display:none'><input type='text' name='xharga_satuan[]' value="<?=$row->harga_satuan?>"></td>
								<td style='display:none'><input type='text' name='xtotal_harga[]' value="<?=$row->total_harga?>"></td>
								<td style='display:none'><input type='text' name='xketerangan[]' value="<?=$row->keterangan?>"></td>
								<td style='display:none'><input type='text' name='xstatus[]' value="<?=$row->status?>"></td>
							</tr>
						<?}?>
					</tbody>
					<tfoot id="foot-total-nonracikan">
						<tr>
							<th colspan="5" class="hidden-phone">

								<span class="pull-right"><b>TOTAL</b></span></th>
							<input type="hidden" id="rowindex" name="rowindex" value="">
							<input type="hidden" id="iddet" name="iddet" value="">
							<input type="hidden" id="tempgrandtotal" name="tempgrandtotal" value="{total_harga}">
							<th><input class="form-control input-sm number" readonly type="text" id="grand_total" name="grand_total" value="{grand_total}"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_item" name="total_item" value="{total_item}"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_jenis_barang" name="total_jenis_barang" value="{total_jenis_barang}"/></th>
							
						</tr>
					</tfoot>
				</table>
				
			</div>
			
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<? if ($disabel==''){ ?>
			<div class="form-group">
				<div class="text-right bg-light lter">
					<button class="btn btn-info" type="submit" id="btn_simpan" name="btn_simpan">Simpan<img style="display: none;" id="loading-button-ajax" src="http://localhost/simrskb/assets/ajax/img/ajax-loader.gif"></button>
					<a href="{base_url}mrka_pengajuan" class="btn btn-default" type="button">Batal</a>
				</div>
			</div>
			<?}?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		clear_input();
		show_hide_pembayaran();
	})	
	function show_hide_pembayaran(){
		$('#div_cicilan').hide();
		$('#div_satu').hide();
		$('#div_kbo').hide();
		$('#div_termin').hide();
		$('#div_tf').hide();
		if ($("#cara_pembayaran").val()=='1'){//Satu Kali			
			$('#div_satu').show();
			if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
				$('#div_tf').show();
			}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
				$('#div_kbo').show();
			}
		}else if ($("#cara_pembayaran").val()=='2'){//Cicilan
			$('#div_cicilan').show();
			hitung_total_cicilan();
		}else if ($("#cara_pembayaran").val()=='3'){//Fix
			$('#div_termin').show();
			hitung_total_termin();
		}
	}
	function clear_input(){
		$("#nama_barang").val('')
		$("#merk_barang").val('')
		$("#kuantitas").val('0')
		$("#satuan").val('')
		$("#harga_satuan").val('0')
		$("#total_harga").val('')
		$("#keterangan").val('')
		$("#rowindex").val('')
		if ($("#disabel").val==''){
			$(".edit").attr('disabled', false);
			$(".hapus").attr('disabled', false);
		}
		// hitung_total();
	}
	$(document).on("change","#jenis_pembayaran,#cara_pembayaran",function(){
		// console.log('sini');
		show_hide_pembayaran();
		
	});
	$(document).on("keyup","#kuantitas,#harga_satuan",function(){
		// console.log('sini');
		perkalian();
		
	});
	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	function perkalian(){
		var total=0;
		total=parseFloat($("#kuantitas").val()) * parseFloat($("#harga_satuan").val());
		$("#total_harga").val(total)
	}
	$("#addjual").click(function() {
			var content = "";
			if (!validate_detail()) return false;
			if ($("#rowindex").val() == '') {
				content += "<tr>";
			}
			content += "<td>" + $("#nama_barang").val() + "</td>"; //0 No Urut
			content += "<td>" + $("#merk_barang").val() + "</td>"; //1 
			content += "<td>" + $("#kuantitas").val() + "</td>"; //2 
			content += "<td>" + $("#satuan").val() + "</td>"; //3 
			content += "<td>" + formatNumber($("#harga_satuan").val()) + "</td>"; //4 
			content += "<td>" + formatNumber($("#total_harga").val()) + "</td>"; //5 
			content += "<td>" + $("#keterangan").val() + "</td>"; //6			
			content +="<td ><div class='btn-group'><button type='button' class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div></td>";
			content += "<td style='display:none'><input type='text' name='xiddet[]' value='"+$("#iddet").val()+"'></td>"; //8 nomor
			content += "<td style='display:none'><input type='text' name='xnama_barang[]' value='"+$("#nama_barang").val()+"'></td>"; //9 
			content += "<td style='display:none'><input type='text' name='xmerk_barang[]' value='"+$("#merk_barang").val()+"'></td>"; //10
			content += "<td style='display:none'><input type='text' name='xkuantitas[]' value='"+$("#kuantitas").val()+"'></td>"; //11
			content += "<td style='display:none'><input type='text' name='xsatuan[]' value='"+$("#satuan").val()+"'></td>"; //12
			content += "<td style='display:none'><input type='text' name='xharga_satuan[]' value='"+$("#harga_satuan").val()+"'></td>"; //13
			content += "<td style='display:none'><input type='text' name='xtotal_harga[]' value='"+$("#total_harga").val()+"'></td>"; //14
			content += "<td style='display:none'><input type='text' name='xketerangan[]' value='"+$("#keterangan").val()+"'></td>"; //15
			content += "<td style='display:none'><input type='text' name='xstatus[]' value='1'></td>"; //16
			
			if ($("#rowindex").val() != '') {
				$('#tabel_add tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
			} else {
				content += "</tr>";
				$('#tabel_add tbody').append(content);
			}
			 hitung_total();
			clear_input();
	});
	function validate_detail() {
		if ($("#nama_barang").val() == "") {
			sweetAlert("Maaf...", "Nama barang Harus Diisi!", "error");
			$("#nama_barang").focus();
			return false;
		}
		if ($("#merk_barang").val() == "") {
			sweetAlert("Maaf...", "Merk barang Harus Diisi!", "error");
			$("#merk_barang").focus();
			return false;
		}	
		
		if ($("#kuantitas").val() < 1) {
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#kuantitas").focus();
			return false;
		}
		if ($("#satuan").val() == "") {
			sweetAlert("Maaf...", "Satuan barang Harus Diisi!", "error");
			$("#satuan").focus();
			return false;
		}
		if ($("#harga_satuan").val() < 1) {
			sweetAlert("Maaf...", "Harga Harus Diisi!", "error");
			$("#harga_satuan").focus();
			return false;
		}
		if ($("#total_harga").val() < 1) {
			sweetAlert("Maaf...", "Total Harga Harus Diisi!", "error");
			$("#total_harga").focus();
			return false;
		}
		return true;
	}
	function validate_final() {
		var rowCount = $('#tabel_add tbody tr').length;
		if (rowCount < 1){
			sweetAlert("Maaf...", "Tidak ada barang yang diajukan!", "error");
			return false;
		}
		if ($("#idunit").val() == "#") {
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit").focus();
			return false;
		}
		
		if ($("#nama_kegiatan").val() == "") {
			sweetAlert("Maaf...", "Kegiatan Harus Diisi!", "error");
			$("#merk_barang").focus();
			return false;
		}	
		if ($("#idvendor").val() == "#") {
			sweetAlert("Maaf...", "Teruntukan Vendor!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($("#tanggal_bayar").val() == "") {
			sweetAlert("Maaf...", "Teruntukan Tanggal Dibutuhkan!", "error");
			$("#tanggal_bayar").focus();
			return false;
		}
		
		$("#cover-spin").show();
		$("*[disabled]").not(true).removeAttr("disabled");
		return true;
	}
	$(document).on("click", ".edit", function() {
			$(".edit").attr('disabled', true);
			$(".hapus").attr('disabled', true);
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);			
			$("#nama_barang").val($(this).closest('tr').find("td:eq(9) input").val());
			$("#merk_barang").val($(this).closest('tr').find("td:eq(10) input").val());
			$("#kuantitas").val($(this).closest('tr').find("td:eq(11) input").val());
			$("#satuan").val($(this).closest('tr').find("td:eq(12) input").val());
			$("#harga_satuan").val($(this).closest('tr').find("td:eq(13) input").val());
			$("#total_harga").val($(this).closest('tr').find("td:eq(14) input").val());
			$("#keterangan").val($(this).closest('tr').find("td:eq(15) input").val());
			$("#kuantitas").focus();
	});
	$(document).on("click", ".hapus", function() {
		var baris=$(this).closest('td').parent();
		
			if ($("#id").val()==''){
				// alert($("#id").val());
				baris.remove();
				hitung_total();
			}else{
				$(this).closest('tr').find("td:eq(16) input").val(0)
				$(this).closest('tr').find("td:eq(6)").html('<span class="label label-danger">DELETED</span>')
				$(this).closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
			}
		
		 hitung_total();
	});
	
	function hitung_total(){
		var total_grand = 0;
		var total_item = 0;
		var total_jenis_barang = 0;
		$('#tabel_add tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(16) input").val()=='1'){
				total_jenis_barang +=1;
				total_grand += parseFloat($(this).find('td:eq(14) input').val());
				total_item += parseFloat($(this).find('td:eq(11) input').val());
			}
		});
		$("#grand_total").val(total_grand);
		$("#total_item").val(total_item);
		$("#total_jenis_barang").val(total_jenis_barang);
	}
	$(document).on("click", "#canceljual", function() {
		clear_input();
	});
	$(document).on("change", "#idrka", function() {
		refresh_program();
	});
	$(document).on("click", "#btn_refresh_vendor", function() {
		refresh_vendor();
	});
	
	function refresh_vendor(){
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_vendor/',
			dataType: "json",
			success: function(data) {
				$("#idvendor").empty();
				$('#idvendor').append('<option value="#">- Pilih Vendor -</option><option value="0">Tentukan Vendor Nanti</option>');
				$('#idvendor').append(data.detail);
			}
		});
	}
	function refresh_program(){
		var idrka=$("#idrka").val();
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_program/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#idprogram").empty();
				$('#idprogram').append('<option value="#">- Pilih Program -</option>');
				$('#idprogram').append(data.detail);
			}
		});
	}
	//Cicilan
	$("#add_dp_cicilan").click(function() {
			var content = "";
			
			if (!validate_detail_cicilan()) return false;
			if ($("#rowindex_dp_cicilan").val() == '') {
				content += "<tr>";
			}
			content+='<td><input class="form-control input-sm number" readonly type="text" name="nominal_cicilan[]" value="'+$("#xnominal_cicilan").val()+'"/></td>';
			content+='						<td>';
			content+='							<div class="input-group date">';
			content+='								<input readonly class="js-datepicker form-control input-sm"  type="text" name="tanggal_cicilan[]" value="'+$("#xtanggal_cicilan").val()+'" data-date-format="dd-mm-yyyy"/>';
			content+='								<span class="input-group-addon">';
			content+='									<span class="fa fa-calendar"></span>';
			content+='								</span>';
			content+='							</div>';
			content+='						</td>';
			content+='						<td><input readonly class="form-control input-sm" type="text" value="'+$("#xketerangan_cicilan").val()+'" name="keterangan_cicilan[]" /></td>';
			content+='						<td>';
			content+='							<button type="button" <?=$disabel?> class="btn btn-sm btn-danger hapus_dp_cicilan" tabindex="9" title="Refresh"><i class="fa fa-times"></i></button>';
			content+='						</td>';
			content+='<td style="display:none"><input readonly class="form-control input-sm number" type="text" name="jenis_cicilan[]" value="1"/></td>';
			
			if ($("#rowindex").val() != '') {
				$('#tabel_dp_cicilan tbody tr:eq(' + $("#rowindex_dp_cicilan").val() + ')').html(content);
			} else {
				content += "</tr>";
				$('#tabel_dp_cicilan tbody').append(content);
			}
			clear_input_cicilan();
			hitung_total_cicilan();
	});
	function clear_input_cicilan(){
		$("#xnominal_cicilan").val('0')
		$("#xtanggal_cicilan").val('')
		$("#xketerangan_cicilan").val('')
		$("#rowindex_dp_cicilan").val('')
		$('.number').number(true, 0, '.', ',');
	}
	function validate_detail_cicilan() {
		// alert('sini');
		if ($("#xnominal_cicilan").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#xnominal_cicilan").focus();
			return false;
		}
		if ($("#xtanggal_cicilan").val() == "") {
			sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			$("#xnominal_cicilan").focus();
			return false;
		}
		var sisa=0;
		sisa=parseFloat($("#grand_total").val()) - (parseFloat($("#xnominal_cicilan").val()) + parseFloat($("#total_dp_cicilan").val()));
		if (sisa < 0){
			sweetAlert("Maaf...", "Pembayaran Melebihi dari tagihan!", "error");
			return false;
		}
		
		
		return true;
	}
	function hitung_total_cicilan(){
		var total_dp = 0;
		$('#tabel_dp_cicilan tbody tr').each(function() {
				console.log($(this).find('td:eq(0) input').val());
				total_dp += parseFloat($(this).find('td:eq(0) input').val());
		});
		$("#total_dp_cicilan").val(total_dp);
		$("#sisa_cicilan").val(parseFloat($("#grand_total").val()) - total_dp);
		
	}
	$(document).on("click", "#generate_cicilan", function() {
		generate_cicilan();
	});
	$(document).on("click", "#cancel_dp_cicilan", function() {
		clear_input_cicilan();
	});
	$(document).on("keyup", "#jml_kali_cicilan", function() {
		var perbulan=0;
		if ($("#jml_kali_cicilan").val()==''){
			$("#jml_kali_cicilan").val(0);
		}
		if ($("#sisa_cicilan").val()=='0' || $("#sisa_cicilan").val()=='0'){
			return false;
		}
		
		perbulan=parseFloat($("#sisa_cicilan").val()) / parseFloat($("#jml_kali_cicilan").val());
		$("#per_bulan_cicilan").val(perbulan)
	});
	function generate_cicilan(){
		if ($("#sisa_cicilan").val()=='0' || $("#sisa_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Sisa cicilan harus ada!", "error");
			return false;
		}
		if ($("#xtanggal_cicilan_pertama").val()==null || $("#xtanggal_cicilan_pertama").val()==''){
			sweetAlert("Maaf...", "Tanggal pertama harus ada!", "error");
			return false;
		}
		if ($("#jml_kali_cicilan").val()=='0' || $("#jml_kali_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Jumlah cicilan harus ada!", "error");
			return false;
		}
		if ($("#per_bulan_cicilan").val()=='0' || $("#per_bulan_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Jumlah cicilan per bulan harus ada!", "error");
			return false;
		}
		$("tabel_cicilan tbody").empty();
		$.ajax({
			url: '{site_url}mrka_pengajuan/generate_cicilan/',
			type: "POST",
			dataType: "json",
			data : {
					xtanggal_cicilan:$("#xtanggal_cicilan_pertama").val(),
					jml_kali_cicilan:$("#jml_kali_cicilan").val(),
					per_bulan_cicilan:$("#per_bulan_cicilan").val(),					
					
				   },
			success: function(data) {
				$("#tabel_cicilan tbody").empty();
				$('#tabel_cicilan tbody').append(data.detail);
				$('.number').number(true, 0, '.', ',');
			}
		});
	}
	function reset_cicilan(){
		$("#tabel_cicilan tbody").empty();
		
	}
	$(document).on("click", ".hapus_dp_cicilan", function() {
		var baris=$(this).closest('td').parent();		
		baris.remove();
		hitung_total_cicilan();
		reset_cicilan();
	});
	//END CICILAN
	//Termin
	$("#add_dp_termin").click(function() {
			var content = "";
			
			if (!validate_detail_termin()) return false;
			if ($("#rowindex_dp_cicilan").val() == '') {
				content += "<tr>";
			}
		content+='<td><input class="form-control input-sm" readonly type="text" value="'+$("#xjenis_termin option:selected").text()+'"/></td>';	
		content+='<td><input class="form-control input-sm" readonly type="text" name="judul_termin[]" value="'+$("#xjudul_termin").val()+'"</td>';
		content+='<td><input class="form-control input-sm" readonly type="text" name="deskripsi_termin[]" value="'+$("#xdeskripsi_termin").val()+'"/></td>';
		content+='<td><input class="form-control input-sm number" readonly type="text" name="nominal_termin[]" value="'+$("#xnominal_termin").val()+'"/></td>';
		content+='<td>';
			content+='<div class="input-group date">';
				content+='<input class="js-datepicker form-control input-sm"  disabled type="text" name="tanggal_termin[]" value="'+$("#xtanggal_termin").val()+'" data-date-format="dd-mm-yyyy"/>';
				content+='<span class="input-group-addon">';
					content+='<span class="fa fa-calendar"></span>';
				content+='</span>';
			content+='</div>';
		content+='</td>';
		content+='<td><button type="button" class="btn btn-sm btn-danger hapus_termin" tabindex="9" id="cancel_dp_termin" title="Refresh"><i class="fa fa-times"></i></button></td>';
		content+='<td style="display:none"><input class="form-control input-sm" type="text" name="jenis_termin[]" value="'+$("#xjenis_termin").val()+'"/></td>';
		content+='<td style="display:none"><input class="form-control input-sm" type="text" name="status_dibayar_termin[]" value="0"/></td>';
			
			if ($("#rowindex").val() != '') {
				$('#tabel_dp_termin tbody tr:eq(' + $("#rowindex_dp_cicilan").val() + ')').html(content);
			} else {
				content += "</tr>";
				$('#tabel_dp_termin tbody').append(content);
			}
			hitung_total_termin();
			clear_input_termin();
	});
	function validate_detail_termin() {
		// alert('sini');
		if ($("#xjenis_termin").val() == "#") {
			sweetAlert("Maaf...", "Tipe Transaksi Harus Diisi!", "error");
			$("#xjenis_termin").focus();
			return false;
		}
		if ($("#xjudul_termin").val() == "") {
			sweetAlert("Maaf...", "Judul Harus Diisi!", "error");
			$("#xjudul_termin").focus();
			return false;
		}
		if ($("#xdeskripsi_termin").val() == "") {
			sweetAlert("Maaf...", "Deskripsi Harus Diisi!", "error");
			$("#xdeskripsi_termin").focus();
			return false;
		}
		if ($("#xnominal_termin").val() == "" || $("#xnominal_termin").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#xnominal_termin").focus();
			return false;
		}
		if ($("#xtanggal_termin").val() == "") {
			sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			$("#xtanggal_termin").focus();
			return false;
		}
		var sisa=0;
		sisa=parseFloat($("#grand_total").val()) - (parseFloat($("#xnominal_termin").val()) + parseFloat($("#total_dp_termin").val()));
		if (sisa < 0){
			sweetAlert("Maaf...", "Pembayaran Melebihi dari tagihan!", "error");
			return false;
		}
		
		
		return true;
	}
	$(document).on("click", ".hapus_termin", function() {
		var baris=$(this).closest('td').parent();		
		baris.remove();
		hitung_total_termin();
		clear_input_termin();
	});
	
	
	function hitung_total_termin(){
		var total_dp = 0;
		$('#tabel_dp_termin tbody tr').each(function() {
				console.log($(this).find('td:eq(3) input').val());
				total_dp += parseFloat($(this).find('td:eq(3) input').val());
		});
		$("#total_dp_termin").val(total_dp);
		$("#sisa_termin").val(parseFloat($("#grand_total").val()) - total_dp);
		
	}
	function clear_input_termin(){
		$("#xjenis_termin").val('#').trigger('change')
		$("#xjudul_termin").val('')
		$("#xdeskripsi_termin").val('')
		$("#xnominal_termin").val('0')
		$("#xtanggal_termin").val('')
		$('.number').number(true, 0, '.', ',');
	}
	
	// END TERMIN
	$(document).on("click", "#btn_open_modal_kegiatan", function() {
		$("#modal_kegiatan").modal('show');
		
	});
	function load_kegiatan(){
		var idrka=$("#idrka").val();
		var idprespektif=$("#idprespektif").val();
		var idprogram=$("#idprogram").val();
		
		$('#tabel_kegiatan').DataTable().destroy();
		table=$('#tabel_kegiatan').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_pengajuan/load_kegiatan',
					type: "POST",
					dataType: 'json',
					data : {
						idrka:idrka,
						idprespektif:idprespektif,
						idprogram:idprogram,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,7], "visible": false },
					 {  className: "text-left", targets:[1,2,3,4,5] },
					 {  className: "text-center", targets:[6] },
					
				]
			});
	}
	$(document).on("click",".pilih",function(){
		var table = $('#tabel_kegiatan').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var nama = table.cell(tr,4).data()
		var idunit = table.cell(tr,7).data()
		$("#nama_kegiatan").val(nama);
		$("#idrka_kegiatan").val(id);
		$("#idunit_pengaju").val(idunit).trigger('change');
		$("#modal_kegiatan").modal('hide');
		
	});
	$(document).on("click","#btn_filter_rka",function(){
		load_kegiatan();
		
	});
</script>