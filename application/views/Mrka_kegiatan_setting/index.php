<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1391'))){ ?>
<div class="block">
	<div class="block-header">
	
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				
				<div class="form-group" style="margin-bottom: 5px;">
                     <label class="col-md-4 control-label" for="status">RKA</label>
                    <div class="col-md-8">
						<select id="idrka" name="idrka" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua RKA -</option>
							<?foreach(get_all('mrka',array('status'=>3),'id') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
						</select>                       
                    </div>
                </div>	
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Prespektif</label>
                    <div class="col-md-8">
						<select id="idprespektif" name="idprespektif" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Prespektif -</option>
							<?foreach(get_all('mprespektif_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
						</select>                       
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Program</label>
                    <div class="col-md-8">
						<select id="idprogram" name="idprogram" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Program -</option>
							<?foreach(get_all('mprogram_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>                     
                    </div>
                </div>	
                                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <div class="col-md-3">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th hidden>idrka_kegiatan</th>
					<th hidden>idrka</th>
					<th hidden>idpresprekti</th>
					<th hidden>idprogram</th>
					<th hidden>idkegiatan</th>
					<th>#</th>
					<th>RKA</th>
					<th>Kegiatan</th>
					<th>Program</th>
					<th>Prespektif</th>
					<th>Target</th>
					<th>Status</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<div class="modal fade" id="modal_setting" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Setting Transaksi</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Nama Kegiatan</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="nama_kegiatan"  placeholder="Kegiatan" name="nama_kegiatan" value="">
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Periode</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="periode"  placeholder="Periode" name="periode" value="">
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Anggaran</label>
								<div class="col-md-8">
									<div class="input-group date">
											<input type="text" readonly class="form-control number" id="nominal_anggaran"  placeholder="Nominal" name="nominal_anggaran" value="0">

											<span class="input-group-addon">
												<span class="fa fa-money"></span>
											</span>
											
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Klasifikasi</label>
								<div class="col-md-8">
									<select id="idklasifikasi" name="idklasifikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(get_all('mklasifikasi',array('status'=>1),'id') as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>									
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Jenis Nominal Transaksi</label>
								<div class="col-md-8">
									<select id="jenis_muncul" name="jenis_muncul" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0">- Pilih Jenis -</option>
										<option value="1">Tampilkan Fix</option>
										<option value="2">Tampilkan Sesuai Tagihan</option>
										
									</select>
								</div>
							</div>				
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Jumlah Nominal</label>
								<div class="col-md-8">
									<div class="input-group date">
											<input type="text" class="form-control number" id="nominal_muncul"  placeholder="Nominal" name="nominal_muncul" value="0">

											<span class="input-group-addon">
												<span class="fa fa-money"></span>
											</span>
											
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Jenis Pembayaran</label>
								<div class="col-md-8">
									<select id="tipe_pembayaran" name="tipe_pembayaran" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0">- Pilih Jenis -</option>
										<option value="1">Bayar 1 kali</option>
										<option value="2">Pecah Menjadi Beberapa Kali</option>
										
									</select>
								</div>
							</div>				
						</div>		
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Tanggal Pembayaran</label>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                           
											<input type="text"  class="form-control" id="tanggal_bayar"  placeholder="Tanggal Pembayaran" name="tanggal_bayar" value="">

											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
											
									</div>
								</div>
							</div>
						</div>
						<div id="div_beberapa" style="display:none">
							<div class="col-md-12">						
								<div class="form-group" style="margin-top: 5px;">
									<label class="col-md-4 control-label" for="status">Dimunculkan setiap</label>
									<div class="col-md-4">
										<div class="input-group date">
												<input type="text" class="form-control number" id="hari_muncul"  placeholder="Hari" name="hari_muncul" value="">

												<span class="input-group-addon">
													Hari
												</span>
												
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">						
								<div class="form-group" style="margin-top: 5px;">
									<label class="col-md-4 control-label" for="status">Nominal Permbayaran</label>
									<div class="col-md-8">
										<div class="input-group date">
												<input type="text" class="form-control number" id="nominal_bayar"  placeholder="Nominal Per Permbayaran" name="nominal_bayar" value="">

												<span class="input-group-addon">
													<span class="fa fa-money"></span>
												</span>
												
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">						
								<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
									<label class="col-md-4 control-label" for="status">Tanggal</label>
									<div class="col-md-8">
										<select id="xtgl_pilih" disabled name="xtgl_pilih[]" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											
										</select>
									</div>
								</div>				
							</div>			
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xidrka_kegiatan" placeholder="No. PO" name="xidrka_kegiatan" value="">
								<input type="hidden" class="form-control" id="xidkegiatan" placeholder="No. PO" name="xidkegiatan" value="">
								<input type="hidden" class="form-control" id="xidrka" placeholder="No. PO" name="xidrka" value="">
								<input type="hidden" class="form-control" id="bulan" placeholder="No. PO" name="bulan" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		$('.number').number(true, 0);
	})	
	function load_index(){
		var idrka=$("#idrka").val();
		var idprespektif=$("#idprespektif").val();
		var idprogram=$("#idprogram").val();
		// alert(periode);
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_kegiatan_setting/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						idrka:idrka,
						idprespektif:idprespektif,
						idprogram:idprogram,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,1,2,3,4], "visible": false },
					// { "width": "5%", "targets": 0, "orderable": true },
					// { "width": "20%", "targets": 1, "orderable": true },
					// { "width": "10%", "targets": 2, "orderable": true },
					// { "width": "10%", "targets": 3, "orderable": true },
					// { "width": "10%", "targets": 4, "orderable": true },
					// { "width": "20%", "targets": 5, "orderable": true },
					// { "width": "20%", "targets": 6, "orderable": true },
				]
			});
	}
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	$(document).on("click", ".setting", function() {
		var table = $('#table_index').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		var bulan=table.cell(tr,1).data()
		$('#modal_setting').modal('show');
		$.ajax({
			url: '{site_url}mrka_kegiatan_setting/get_rka/'+id+'/'+bulan,
			dataType: "json",
			success: function(data) {
				console.log(data.idkegiatan);
				$("#nama_kegiatan").val(data.nama_kegiatan);
				$("#xidrka_kegiatan").val(data.id);
				$("#xidrka").val(data.idrka);
				$("#xidkegiatan").val(data.idkegiatan);
				$("#jenis_muncul").val(data.jenis_muncul).trigger('change');
				$("#idklasifikasi").val(data.idklasifikasi).trigger('change');
				$("#tipe_pembayaran").val(data.tipe_pembayaran).trigger('change');
				$("#periode").val(data.periode);
				$("#bulan").val(data.bulan);
				$("#tanggal_bayar").val(data.tanggal_bayar);
				$("#nominal_bayar").val(data.nominal_bayar);
				if (data.nominal_muncul=='0'){
					$("#nominal_anggaran").val(data.nominal);
					$("#nominal_muncul").val(data.nominal);
				}else{
					$("#nominal_anggaran").val(data.nominal);
					$("#nominal_muncul").val(data.nominal);
					
				}
				$("#hari_muncul").val(data.hari_muncul);
				load_list_tanggal();
			}
		});


	});
	$(document).on("change", "#tipe_pembayaran", function() {
		// alert($("#tipe_pembayaran").val());
		if ($("#tipe_pembayaran").val()=='2'){
			$("#div_beberapa").show();
		}else{
			$("#div_beberapa").hide();
		}

	});
	$(document).on("click", "#btn_ubah", function() {
		if ($('#jenis_muncul').val()=='0'){
			sweetAlert("Maaf...", "Tentukan Jenis Nominal Transaksi", "error");
			return false;

		}
		
		if ($('#nominal_muncul').val()=='0' || $('#nominal_muncul').val()==''){
			sweetAlert("Maaf...", "Tentukan Nominal", "error");
			return false;

		}
		if ($('#idklasifikasi').val()=='0' || $('#idklasifikasi').val()==null){
			sweetAlert("Maaf...", "Tentukan Klasifikasi", "error");
			return false;

		}
		if ($('#tipe_pembayaran').val()=='0'){
			sweetAlert("Maaf...", "Tentukan Tipe Pembayaran", "error");
			return false;

		}
		if ($('#tanggal_bayar').val()==''){
			sweetAlert("Maaf...", "Tentukan Tanggal Pembayaran", "error");
			return false;

		}
		if ($('#tipe_pembayaran').val()=='2'){
			if ($('#hari_muncul').val()=='0' || $('#hari_muncul').val()==''){
				sweetAlert("Maaf...", "Tentukan Hari Muncul", "error");
				return false;

			}
			if ($('#nominal_bayar').val()=='0' || $('#nominal_bayar').val()==''){
				sweetAlert("Maaf...", "Tentukan Nominal Per Pembayaran", "error");
				return false;

			}
			
		}
		
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Update Setting?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			update_setting();
		});
	});
	function update_setting(){
		var idrka=$("#xidrka").val();
		// alert(idrka);
		var idkegiatan=$("#xidkegiatan").val();
		var idrka_kegiatan=$("#xidrka_kegiatan").val();
		var hari_muncul=$("#hari_muncul").val();
		var nominal_muncul=$("#nominal_muncul").val();
		var idklasifikasi=$("#idklasifikasi").val();
		var jenis_muncul=$("#jenis_muncul").val();
		var bulan=$("#bulan").val();
		var tipe_pembayaran=$("#tipe_pembayaran").val();
		var tanggal_bayar=$("#tanggal_bayar").val();
		// alert(tanggal_bayar);
		var tgl_pilih=$("#xtgl_pilih").val();
		var nominal_bayar=$("#nominal_bayar").val();
		var nama_kegiatan=$("#nama_kegiatan").val();
		$.ajax({
			url: '{site_url}mrka_kegiatan_setting/update_setting/',
			type: 'POST',
			dataType: 'json',
			data : {
				idrka_kegiatan:idrka_kegiatan,
				hari_muncul:hari_muncul,
				nominal_muncul:nominal_muncul,
				idklasifikasi:idklasifikasi,
				jenis_muncul:jenis_muncul,
				tgl_pilih:tgl_pilih,
				tipe_pembayaran:tipe_pembayaran,
				tanggal_bayar:tanggal_bayar,
				nominal_bayar:nominal_bayar,
				bulan:bulan,
				idrka:idrka,
				idkegiatan:idkegiatan,
				nama_kegiatan:nama_kegiatan,
				
			   },
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Setting Berhasil disimpan'});
				$('#table_index').DataTable().ajax.reload( null, false );
				$('#modal_setting').modal('hide');
			}
		});
		
	}
	$(document).on("change", "#tanggal_bayar", function() {
		load_list_tanggal();
	});
	$(document).on("keyup", "#hari_muncul", function() {
		load_list_tanggal();
	});
	function load_list_tanggal(){
		var tanggal_bayar=$("#tanggal_bayar").val();
		var hari_muncul=$("#hari_muncul").val();
		var tipe_pembayaran=$("#tipe_pembayaran").val();
		if (hari_muncul!='0' && tanggal_bayar && tipe_pembayaran=='2'){
			$.ajax({
				url: '{site_url}mrka_kegiatan_setting/list_tanggal/',
				type: "POST",
				dataType: 'json',
				data: {
					tanggal_bayar:tanggal_bayar,
					hari_muncul:hari_muncul,
					
					},
				success: function(data) {
					$("#xtgl_pilih").empty();
					$('#xtgl_pilih').append(data.detail);
				}
			});
		}
	}
</script>
