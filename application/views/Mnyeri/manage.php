<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mnyeri" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mnyeri/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="nyeri_id" value="{id}" required="" aria-required="true">
					
				</div>
			</div>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="singkatan">Singkatan</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="singkatan" placeholder="Singkatan" name="singkatan" value="{singkatan}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="nyeri_id" placeholder="nyeri_id" name="nyeri_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom:5px">
				<label class="col-md-2 control-label" for="isi_header">Header</label>
				<div class="col-md-10">
					<textarea class="form-control js-summernote" name="isi_header" id="isi_header" required><?=$isi_header?></textarea>
				</div>
			</div>
			<div class="form-group" style="margin-bottom:5px">
				<label class="col-md-2 control-label" for="isi_footer">Footer</label>
				<div class="col-md-10">
					<textarea class="form-control js-summernote" name="isi_footer" id="isi_footer" required><?=$isi_footer?></textarea>
				</div>
			</div>
			
			<input type="hidden" class="form-control" id="st_locked" placeholder="st_locked" name="st_locked" value="{st_locked}" aria-required="true">
			<input type="hidden" class="form-control" id="nrs_id" placeholder="nrs_id" value="" >
			<input type="hidden" class="form-control" id="wbf_id" placeholder="wbf_id" value="" >
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mnyeri" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
			<?if($id){?>
			<?php echo form_open('mnyeri/save','class="form-horizontal push-10-t"') ?>
				<?if($st_locked==1){?>
					<div class="form-group" style="margin-bottom:5px">
						<label class="col-md-2 text-danger"></label>
						<label class="col-md-10 text-danger"> Penilaian Numeric Rating Scale</label>
						
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for=""></label>
						<div class="col-md-10">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_nrs" style=" vertical-align: baseline;">
									<thead>
										<tr>
											<th width="15%">No Urut</th>
											<th width="40%">Deskripsi</th>
											<th width="15%">Nilai</th>
											<th width="15%">Warna</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th  style=" vertical-align: top;">
												<select tabindex="8" id="nourut" class="js-select2 form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>Urutan</option>
													<?for($i=0;$i<=10;$i++){?>
													<option value="<?=$i?>"><?=$i?></option>
													<?}?>
													
												</select>
											</th>
											<th  style=" vertical-align: top;">
												<input type="text" class="form-control" id="keterangan" style="width: 100%;" placeholder="Deskripsi" required="" aria-required="true">
											</th>
											<th  style=" vertical-align: top;">
												<select tabindex="8" id="nilai" class="js-select2 form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>Nilai</option>
													<?for($i=0;$i<=10;$i++){?>
													<option value="<?=$i?>"><?=$i?></option>
													<?}?>
													
												</select>
											</th>
											<th  style=" vertical-align: top;">
												<div class="js-colorpicker input-group colorpicker-element">
													<input class="form-control" type="text" id="warna" name="warna" value="#0000">
													<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
												</div>
											</th>
											
											<th style=" vertical-align: top;">
												<div class="btn-group">
												<button class="btn btn-primary btn-sm" type="button" id="btn_add_nrs" onclick="simpan_nrs()"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm" type="button" onclick="clear_nrs()"><i class="fa fa-refresh"></i></button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<label class="col-md-2 text-danger"></label>
						<label class="col-md-10 text-danger"><i class="fa fa-smile-o"></i> Penilaian Wong Baker Face</label>
						
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for=""></label>
						<div class="col-md-10">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_wbf" style=" vertical-align: baseline;">
									<thead>
										<tr>
											<th width="15%">No Urut</th>
											<th width="40%">Deskripsi</th>
											<th width="15%">Nilai</th>
											<th width="15%" class="text-center">Gambar</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th  style=" vertical-align: top;">
												<select tabindex="8" id="nourut_wbf" class="js-select2 form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>Urutan</option>
													<?for($i=0;$i<=10;$i++){?>
													<option value="<?=$i?>"><?=$i?></option>
													<?}?>
													
												</select>
											</th>
											<th  style=" vertical-align: top;">
												<input type="text" class="form-control" id="keterangan_wbf" style="width: 100%;" placeholder="Deskripsi" required="" aria-required="true">
											</th>
											<th  style=" vertical-align: top;">
												<select tabindex="8" id="nilai_wbf" class="js-select2 form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
													<option value="#" selected>Nilai</option>
													<?for($i=0;$i<=10;$i++){?>
													<option value="<?=$i?>"><?=$i?></option>
													<?}?>
													
												</select>
											</th>
											<th  style=" vertical-align: top;">
												<label class="text-danger"><i class="fa fa-file-picture-o"></i> After Save</label>
											</th>
											
											<th style=" vertical-align: top;">
												<div class="btn-group">
												<button class="btn btn-primary btn-sm" type="button" id="btn_add_wbf" onclick="simpan_wbf()"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm" type="button" onclick="clear_wbf()"><i class="fa fa-refresh"></i></button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				<?}else{ ?>
					<div class="form-group">
						<label class="col-md-2 control-label" for="">Penilaian</label>
						<div class="col-md-10">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_parameter" style=" vertical-align: baseline;">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="80%">Pengkajian</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th  style=" vertical-align: top;">#<input class="form-control" type="hidden" id="parameter_id" name="parameter_id" value=""></th>
											<th  style=" vertical-align: top;">
												<textarea class="form-control js-summernote" name="parameter_nama" id="parameter_nama" ></textarea>
											</th>
											
											<th style=" vertical-align: top;">
												<div class="btn-group">
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_parameter" name="btn_tambah_parameter"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_parameter()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				<?}?>
				
				<?php echo form_close() ?>
			<?}?>
			
	</div>
</div>
<div class="modal in" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tambah value</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-horizontal">
									<label for="parameter_add">Parameter :</label><br>
									<label id="parameter_add"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_jawaban" style=" vertical-align: baseline;">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Skor</th>
												<th width="70%">Pengkajian</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th  style=" vertical-align: top;">#
													<input class="form-control" type="hidden" id="parameter_id2" name="parameter_id2" value="">
													<input class="form-control" type="hidden" id="skor_id" name="skor_id" value="">
												</th>
												<th  style=" vertical-align: top;">
													<input type="text" class="form-control number" style="width:100%" id="skor" placeholder="Value Skor" name="skor" value="" >
												</th>
												<th  style=" vertical-align: top;">
													<input type="text" class="form-control" id="deskripsi_nama"  style="width:100%"  placeholder="Deskripsi Parameter" name="deskripsi_nama" value="" >
												</th>
												
												<th style=" vertical-align: top;">
													<div class="btn-group">
													<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_jawaban" name="btn_tambah_jawaban"><i class="fa fa-save"></i> Simpan</button>
													<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_jawaban()"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>	
								
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_gambar" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Face</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-horizontal">
									<form class="dropzone" action="{base_url}mnyeri/upload_files" method="post" enctype="multipart/form-data">
										<input name="gambar_id" id="gambar_id" type="hidden" value="">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var myDropzone 
$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let st_locked=$("#st_locked").val();
	if (st_locked=='1'){
		load_nrs();
		load_wbf();
		
	}else{
		// alert('sini');
		load_parameter();
	}
	Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  $("#modal_gambar").modal('hide');
		  load_wbf();
		  myDropzone.removeFile(file);
		  
		});
})	
function simpan_nrs(){
	let nyeri_id=$("#nyeri_id").val();
	let nrs_id=$("#nrs_id").val();
	let nourut=$("#nourut").val();
	let keterangan=$("#keterangan").val();
	let nilai=$("#nilai").val();
	let warna=$("#warna").val();
	
	if (nourut=='#'){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if (keterangan==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
		return false;
	}
	if (nilai=='#'){
		sweetAlert("Maaf...", "Tentukan Nilai", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mnyeri/simpan_nrs', 
		dataType: "JSON",
		method: "POST",
		data : {
				nrs_id:nrs_id,
				nyeri_id:nyeri_id,
				nourut:nourut,
				keterangan:keterangan,
				nilai:nilai,
				warna:warna,
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data.st_save=='0'){
				if (data.st_duplikat=='1'){
					$.toaster({priority : 'danger', title : 'Error!', message : ' Duplicate No Urut'});
				}else{
					$.toaster({priority : 'danger', title : 'Error!', message : ' Gatal Simpan'});
				}
			}else{
				
				clear_nrs();
				$('#index_nrs').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		}
	});
}
function hapus_nrs(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mnyeri/hapus_nrs',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_nrs').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function hapus_wbf(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mnyeri/hapus_wbf',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_wbf').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function edit_nrs(nrs_id){
	$("#nrs_id").val(nrs_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mnyeri/find_nrs',
		type: 'POST',
		dataType: "JSON",
		data: {id: nrs_id},
		success: function(data) {
			$("#btn_tambah_nrs").html('<i class="fa fa-save"></i> Edit');
			$("#keterangan").val(data.keterangan);
			$("#nilai").val(data.nilai).trigger('change');
			$("#nourut").val(data.nourut).trigger('change');
			$("#warna").val(data.warna).trigger('change');
			$('#btn_add_nrs').html('<i class="glyphicon glyphicon-floppy-saved"></i>');
			$("#cover-spin").hide();
			
		}
	});
}
function load_nrs(){
	let nyeri_id=$("#nyeri_id").val();
	$('#index_nrs').DataTable().destroy();	
	table = $('#index_nrs').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 25,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mnyeri/load_nrs', 
                type: "POST" ,
                dataType: 'json',
				data : {
						nyeri_id:nyeri_id
					   }
            }
        });
}
function clear_nrs(){
	$("#nrs_id").val('');
	$('#warna').val('#FFF').trigger('change');
	$('#keterangan').val('');
	$('#nilai').val('#').trigger('change');
	$('#nourut').val('#').trigger('change');
	$('#btn_add_nrs').html('<i class="fa fa-save"></i>');
	
}
//WONG BAKER Face
function simpan_wbf(){
	let nyeri_id=$("#nyeri_id").val();
	let wbf_id=$("#wbf_id").val();
	let nourut=$("#nourut_wbf").val();
	let keterangan=$("#keterangan_wbf").val();
	let nilai=$("#nilai_wbf").val();
	
	
	if (nourut=='#'){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if (keterangan==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
		return false;
	}
	if (nilai=='#'){
		sweetAlert("Maaf...", "Tentukan Nilai", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mnyeri/simpan_wbf', 
		dataType: "JSON",
		method: "POST",
		data : {
				wbf_id:wbf_id,
				nyeri_id:nyeri_id,
				nourut:nourut,
				keterangan:keterangan,
				nilai:nilai,
				
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data.st_save=='0'){
				if (data.st_duplikat=='1'){
					$.toaster({priority : 'danger', title : 'Error!', message : ' Duplicate No Urut'});
				}else{
					$.toaster({priority : 'danger', title : 'Error!', message : ' Gatal Simpan'});
				}
			}else{
				
				clear_wbf();
				$('#index_wbf').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		}
	});
}
function edit_wbf(wbf_id){
	$("#wbf_id").val(wbf_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mnyeri/find_wbf',
		type: 'POST',
		dataType: "JSON",
		data: {id: wbf_id},
		success: function(data) {
			
			$("#keterangan_wbf").val(data.keterangan);
			$("#nilai_wbf").val(data.nilai).trigger('change');
			$("#nourut_wbf").val(data.nourut).trigger('change');
			$('#btn_add_wbf').html('<i class="glyphicon glyphicon-floppy-saved"></i>');
			$("#cover-spin").hide();
			
		}
	});
}
function load_wbf(){
	let nyeri_id=$("#nyeri_id").val();
	$('#index_wbf').DataTable().destroy();	
	table = $('#index_wbf').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 25,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					{ "targets": 3,  className: "text-center" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}mnyeri/load_wbf', 
                type: "POST" ,
                dataType: 'json',
				data : {
						nyeri_id:nyeri_id
					   }
            }
        });
}
function clear_wbf(){
	$("#wbf_id").val('');
	
	$('#keterangan_wbf').val('');
	$('#nilai_wbf').val('#').trigger('change');
	$('#nourut_wbf').val('#').trigger('change');
	$('#btn_add_wbf').html('<i class="fa fa-save"></i>');
	
}
//
function clear_parameter(){
	$("#parameter_id").val('');
	$('#parameter_nama').summernote('code','');
	
	$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Save');
	
}
function ganti_wbf(id){
	$("#gambar_id").val(id);
	$("#modal_gambar").modal('show');
}
function load_parameter(){
	let nyeri_id=$("#nyeri_id").val();
	$('#index_parameter').DataTable().destroy();	
	table = $('#index_parameter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mnyeri/load_parameter', 
                type: "POST" ,
                dataType: 'json',
				data : {
							nyeri_id:nyeri_id
					   }
            }
        });
}		
function edit_parameter(parameter_id){
	$("#parameter_id").val(parameter_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mnyeri/find_parameter',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Edit');
			$("#parameter_id").val(data.id);
			$('#parameter_nama').summernote('code',data.parameter_nama);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_parameter(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Parameter?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mnyeri/hapus_parameter',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_parameter').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}


$("#btn_tambah_parameter").click(function() {
	let nyeri_id=$("#nyeri_id").val();
	let parameter_id=$("#parameter_id").val();
	let parameter_nama=$("#parameter_nama").val();
	
	if (parameter_nama==''){
		sweetAlert("Maaf...", "Tentukan Range Parameter", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mnyeri/simpan_parameter', 
		dataType: "JSON",
		method: "POST",
		data : {
				nyeri_id:nyeri_id,
				parameter_id:parameter_id,
				parameter_nama:parameter_nama,
				
			},
		complete: function(data) {
			clear_parameter();
			$('#index_parameter').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
$("#btn_tambah_jawaban").click(function() {
	let parameter_id=$("#parameter_id2").val();
	let skor=$("#skor").val();
	let skor_id=$("#skor_id").val();
	let deskripsi_nama=$("#deskripsi_nama").val();
	
	if (skor==''){
		sweetAlert("Maaf...", "Tentukan Skor", "error");
		return false;
	}
	if (deskripsi_nama==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mnyeri/simpan_jawaban', 
		dataType: "JSON",
		method: "POST",
		data : {
				parameter_id:parameter_id,
				skor_id:skor_id,
				deskripsi_nama:deskripsi_nama,
				skor:skor,
				
			},
		complete: function(data) {
			clear_jawaban();
			load_parameter(); 
			$('#index_jawaban').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function add_jawaban(parameter_id){
	$("#parameter_id2").val(parameter_id);
	$("#modal_tambah").modal('show');
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mnyeri/find_parameter',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			// $("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Edit');
			// $("#parameter_id").val(data.id);
			$("#parameter_add").html(data.parameter_nama);
			// $('#parameter_add').summernote('code',data.parameter_nama);
			$("#cover-spin").hide();
			load_jawaban();
		}
	});
}

function clear_jawaban(){
	$("#skor_id").val('');
	$('#deskripsi_nama').val('');
	$('#skor').val('');
	
	$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Save');
	
}
function load_jawaban(){
	let parameter_id=$("#parameter_id2").val();
	$('#index_jawaban').DataTable().destroy();	
	table = $('#index_jawaban').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mnyeri/load_jawaban', 
                type: "POST" ,
                dataType: 'json',
				data : {
							parameter_id:parameter_id
					   }
            }
        });
}	
function edit_jawaban(skor_id){
	$("#skor_id").val(skor_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mnyeri/find_jawaban',
		type: 'POST',
		dataType: "JSON",
		data: {id: skor_id},
		success: function(data) {
			$("#modal_tambah").modal('show');
			$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Edit');
			$("#skor").val(data.skor);
			$("#skor_id").val(data.id);
			$("#deskripsi_nama").val(data.deskripsi_nama);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_jawaban(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Skor?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mnyeri/hapus_jawaban',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_jawaban').DataTable().ajax.reload( null, false ); 
					load_parameter();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function set_default(param_id,id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Set Default?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mnyeri/set_default_jawaban',
				type: 'POST',
				data: {
					param_id: param_id,
					id: id,
					},
				complete: function() {
					$('#index_jawaban').DataTable().ajax.reload( null, false ); 
					load_parameter();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>