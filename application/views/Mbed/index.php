<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('191'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('192'))){ ?>
		<ul class="block-options">
			<li>
				<a href="{base_url}mbed/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	<?php if (UserAccesForm($user_acces_form,array('253'))){ ?>
		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('mbed/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idruangan">Ruangan</label>
					<div class="col-md-8">
						<select name="idruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<option value="#" selected>Semua Ruangan</option>
							<?php foreach  ($list_ruangan as $row) { ?>
								<option value="<?=$row->id?>" <?=($idruangan == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idtipe">Kelas</label>
					<div class="col-md-8">
						<select name="idkelas" id="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<option value="#" <?=($idkelas == '#' ? 'selected' : '')?>>Semua Kelas</option>
							<option value="0" <?=($idkelas == '0' ? 'selected' : '')?>>NON KELAS</option>
							<option value="1" <?=($idkelas == '1' ? 'selected' : '')?>>III</option>
							<option value="2" <?=($idkelas == '2' ? 'selected' : '')?>>II</option>
							<option value="3" <?=($idkelas == '3' ? 'selected' : '')?>>I</option>
							<option value="4" <?=($idkelas == '4' ? 'selected' : '')?>>UTAMA</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr>
		<hr>
	<?}?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Ruangan</th>
					<th>Kelas</th>
					<th>Nama Bed</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function() {
		BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
			"bSort": false,
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mbed/getIndex/' + "<?=$this->uri->segment(2)?>",
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
					"width": "10%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "20%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "20%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "20%",
					"targets": 3,
					"orderable": true
				},
				{
					"width": "20%",
					"targets": 4,
					"orderable": true
				}
			]
		});
	});
</script>
