<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
			<button class="btn" onclick="goBack()"> <i class="fa fa-reply"></i></button>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tkasbon/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idtipe">Tanggal</label>
				<div class="col-md-7">
					<input class="js-datepicker form-control" type="text" id="tanggal" name="tanggal" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?=DMYFormat($tanggal);?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idtipe">Tipe</label>
				<div class="col-md-7">
					<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="" disabled selected>Pilih Opsi</option>
						<option value="1" <?=($idtipe == 1) ? "selected" : "" ?>>Dokter</option>
						<option value="2" <?=($idtipe == 2) ? "selected" : "" ?>>Pegawai</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idpegawai">Nama</label>
				<div class="col-md-7">
					<select name="idpegawai" id="idpegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="" disabled selected>Pilih Opsi</option>
						<?php if($this->uri->segment(2) == 'update'){ ?>
							<?php foreach  ($list_pegawai as $row) { ?>
								<option value="<?=$row->id?>" <?=($row->id == $idpegawai ? 'selected' : '')?>><?=$row->nama?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="catatan">Catatan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="catatan" placeholder="Catatan" name="catatan" value="{catatan}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nominal">Nominal</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="nominal" placeholder="Nominal" name="nominal" value="{nominal}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idtipe">Alokasi Dana</label>
				<div class="col-md-7">
					<select name="alokasidana" id="alokasidana" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="" disabled selected>Pilih Opsi</option>
						<option value="1" <?=($idtipe == 1) ? "selected" : "" ?>>Kasir Rawat Jalan</option>
						<option value="2" <?=($idtipe == 2) ? "selected" : "" ?>>Kasir Rawat Inap</option>
						<option value="3" <?=($idtipe == 3) ? "selected" : "" ?>>Bendahara</option>
					</select>
				</div>
			</div>
			<?if ($disabel==''){?>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}tkasbon" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?}?>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$("#idtipe").change(function() {
			$.ajax({
				url: '{site_url}tkasbon/getPegawai',
				method: "POST",
				dataType: "json",
				data: {
					"idtipe": $(this).val()
				},
				success: function(data) {
					$("#idpegawai").empty();
					$("#idpegawai").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.length; i++) {
						$("#idpegawai").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}
					$("#idpegawai").selectpicker("refresh");
				},
				error: function(data) {
					alert(data);
				}
			});
		});
	});
	function goBack() {
	  window.history.back();
	}

</script>
