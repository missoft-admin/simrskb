<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tkasbon/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('tkasbon/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idtipe">Tipe</label>
					<div class="col-md-8">
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<option value="#" <?=($idtipe == '#' ? 'selected' : '')?>>- All Tipe -</option>
							<option value="1" <?=($idtipe == 1 ? 'selected' : '')?>>Dokter</option>
							<option value="2" <?=($idtipe == 2 ? 'selected' : '')?>>Pegawai</option>
						</select>
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="iduser">Nama</label>
					<div class="col-md-8">
						<select name="iduser" id="idpegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<?php if ($idtipe == 1) { ?>
							<option value="#" selected>Semua Dokter</option>
							<?php }else{ ?>
							<option value="#" selected>Semua Pegawai</option>
							<?php } ?>
							<?php foreach  ($list_user as $row) { ?>
								<option value="<?=$row->id?>" <?=($iduser == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tanggaldaftar">Tanggal</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
            </div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<hr style="margin-top:10px">

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal</th>
					<th>Tipe</th>
					<th>Nama</th>
					<th>Catatan</th>
					<th>Nominal</th>
					<th>Alokasi Dana</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tkasbon/getIndex/' + '<?=$this->uri->segment(2)?>',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true },
					{ "width": "10%", "targets": 8, "orderable": true }
				]
			});
	});
</script>

<script type="text/javascript">
  $(document).ready(function() {
		$("#idtipe").change(function() {

			$("#idpegawai").empty();
			if ($(this).val() == 1) {
				$("#idpegawai").append("<option value='#' selected>Semua Dokter</option>");
			} else {
				$("#idpegawai").append("<option value='#' selected>Semua Pegawai</option>");
			}

			$.ajax({
				url: '{site_url}tkasbon/getPegawai',
				method: "POST",
				dataType: "json",
				data: {
					"idtipe": $(this).val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#idpegawai").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}
					$("#idpegawai").selectpicker("refresh");
				},
				error: function(data) {
					alert(data);
				}
			});
		});
	});
</script>
