<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bukti Kasbon</title>
    <style>
    body{
        font-family: "Courier", Arial,sans-serif;
    }
    

    @media screen {
      table {
        font-size: 24px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .header {
		  font-size: 30px !important;
        font-weight: bold;
        text-align: center;
      }
	  .bawah {
		  font-size: 18px !important;
        text-align: center;
      }
      .subheader {
        font-size: 32px !important;
        text-align: center;
		 padding: 5px;
      }
      .content td {
        padding: 0px;
		border: 0px solid #000 !important;
		white-space: normal !important; 
		word-wrap: break-word;  
      }
      .content-2 td {
		   border: 0px solid #000 !important;
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
	<table class="content-2">
      <tr  height="300px">
        <td rowspan="4" width="20%" class="text-center"><img src="{logo1_rs}" alt="" width="100" height="100"></td>
        <td width="60%" colspan="2" class="text-right header"><b>BUKTI KASBON</b> &nbsp; &nbsp; &nbsp; </td>
		<td width="10%"></td>
      </tr>
	  <tr>
			<td width="20%" class="text-left">TANGGAL KASBON</td>
			<td width="90%" colspan="2" >:  {tanggal}</td>
	  </tr>
	  <tr>
			<td class="text-left">NO KASBON</td>
			<td colspan="2">:  {notransaksi}</td>
	  </tr>
	  <tr>
			<td class="text-left">NAMA</td>
			<td colspan="2">:  {namapegawai}</td>
	  </tr>
	  
  </table>
  
    <br>
    <table class="content-2">
     
      <tr>
        <td style="width:20%">Rp.</td>
        <td style="width:5%">:</td>
        <td class="border-full" style="width:80%">{nominal}</td>
      </tr>
	  <tr>
		<td></td>
		<td></td>
		<td></td>
	  </tr>
      <tr>
        <td>TERBILANG</td>
		<td style="width:5%">:</td>
        <td class="border-full">{terbilang}</td>
      </tr>
	   <tr>
		<td></td>
		<td></td>
		<td></td>
	  </tr>
      <tr>
        <td >UNTUK KEPERLUAN</td>   
		<td style="width:5%">:</td>
        <td class="border-full">{deskripsi}</td>
      </tr>
    </table>
    <br>
    <table>
      <tr>
        <td>
          <table>
            <tr>
              <td style="text-align:center; width: 33%;" class="border-full"><b>KEPALA KEUANGAN</b></td>
              <td style="text-align:center; width: 33%;" class="border-full"><b>BENDAHARA</b></td>
              <td style="text-align:center; width: 33%;" class="border-full"><b>KASIR</b></td>
            </tr>
			<tr>
			<td></td>
			<td></td>
			<td></td>
		  </tr>
            <tr>
              <td class="border-full"><br><br><br><br><br></td>
              <td class="border-full"><br><br><br><br><br></td>
              <td class="border-full"><br><br><br><br><br></td>
            </tr>
          </table>
        </td>
        <td style="text-align:center; vertical-align: top;">
          <b>Bandung, <?=HumanDate($tanggal)?></b><br>Yang Menerima
          <br><br><br><br><br><br>
          ___________________________
		  
        </td>
      </tr>
	  
	  <tr>
		<td></td>
		<td class="text-center bawah" ><p style="font-size: 15px;"><i>Tanggal cetak : <?=date('d-m-Y H:i:s')?> | <?=$this->session->userdata('user_name')?></i></p></td>
		
	  </tr>
    </table>
  </body>
</html>
