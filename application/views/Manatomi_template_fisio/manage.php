<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}manatomi_template_fisio" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('manatomi_template_fisio/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama_template_lokasi">Nama Template</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="nama_template_lokasi" placeholder="Nama Template" name="nama_template_lokasi" value="{nama_template_lokasi}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="example-hf-email"></label>
						<div class="col-md-1">
							<img class=""  id="output_img" width="550" height="400" src="{upload_path}anatomi/<?=($gambar_tubuh!='' ? $gambar_tubuh : 'no_image.png')?>" />
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="example-hf-email">Logo Login (100 x 100)</label>
						
						<div class="col-md-10">
							<div class="box">
								<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="gambar_tubuh" value="{gambar_tubuh}" />
								<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-md-12 col-md-offset-2">
							<button class="btn btn-success" name="btn_simpan" type="submit" value="1">Simpan</button>
							<button class="btn btn-danger" name="btn_simpan"  type="submit" value="2">Simpan & Terapkan</button>
							<a href="{base_url}manatomi_template_fisio" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
						</div>
					</div>
					
				
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<input type="hidden" id="id" value="{id}">

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
	
	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	
	function validate_final(){
		
		
		if ($("#nama_template_lokasi").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Template", "error");
			return false;
		}
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#cover-spin").show();
		return true;

		
	}
	
	
</script>