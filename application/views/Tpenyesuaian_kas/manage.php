<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">

		<h3 class="block-title">TRANSAKSI PENYESUAIAN KAS</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<?php echo form_open('tpenyesuaian_kas/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
		<div class="row">
			<div class="col-md-12">
				
				<div class="form-group">
					<label class="col-md-2  control-label" for="dari">Jenis Kas</label>
					<div class="col-md-4">
						<select <?=$disabel?> disabled name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Jenis Kas -</option>
							<? foreach($list_jenis_kas as $row){?>
								<option value="<?=$row->id?>" <?=($jenis_kas_id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
					
				</div>
				<div class="form-group">
					
					<label class="col-md-2  control-label" for="ke">Sumber Kas</label>
					<div class="col-md-4">
						<select name="sumber_kas_id" disabled id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Sumber Kas -</option>
							<? foreach($list_sumber_kas as $row){?>
								<option value="<?=$row->id?>" <?=($sumber_kas_id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
					<label class="col-md-2  control-label" for="ke">Saldo Awal</label>
					<div class="col-md-4">
						<input  type="text" readonly class="form-control decimal" id="saldo_awal" placeholder="Saldo" name="saldo_awal" value="{saldo_awal}">
					</div>
				</div>
				<div class="form-group">
					
					<label class="col-md-2  control-label" for="ke">Bank</label>
					<div class="col-md-4">
						<select name="bank_id" disabled id="bank_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Bank-</option>
							<? foreach($list_bank as $row){?>
								<option value="<?=$row->id?>" <?=($bank_id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
					<label class="col-md-2  control-label" for="ke">No Akun</label>
					<div class="col-md-4">
						<input  type="text" readonly class="form-control" id="noakun" placeholder="Saldo" name="noakun" value="{noakun}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2  control-label" for="ke">Atur Saldo</label>
					<div class="col-md-4">
						<input  type="text" class="form-control decimal" <?=$disabel?> id="saldo_akhir" placeholder="saldo_akhir" name="saldo_akhir" value="{saldo_akhir}">
					</div>
					<label class="col-md-2 control-label" for="nama">Atur Tanggal</label>
					<div class="col-md-4">
						<div class="input-group date">
						<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_trx" name="tanggal_trx" value="<?=$tanggal_trx?>" data-date-format="dd-mm-yyyy"/>
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
						</div>
					</div>
				</div>
								
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama">Alasan Penyesuaian </label>
					<div class="col-md-10">
						<textarea class="form-control js-summernote" name="deskripsi" id="deskripsi"><?=$deskripsi?></textarea>
					</div>
				</div>
				<?if ($disabel==''){?>
				<div class="form-group hiden btn_hide">
					<div class="text-right bg-light lter">
						
						<button class="btn btn-success" type="submit" id="btn_simpan" name="btn_simpan" value="2">Simpan & Verifikasi</button>
						<!--
						<button class="btn btn-primary" type="submit" id="btn_simpan" name="btn_simpan" value="1" hidden>Simpan</button>
							-->
						<a href="{base_url}tpenyesuaian_kas" class="btn btn-default" type="button">Kembali</a>
					</div>
				</div>
				<?}?>
			</div>
		</div>
		<input  type="hidden" class="form-control" id="id" placeholder="id" name="id" >
		
		<?php echo form_close() ?>
		
		
</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	
	$(document).ready(function(){
		
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		$('#deskripsi').summernote({
		  height: 70,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});

	})
	
	function validate_final(){
		// alert($("#deskripsi").summernote('code'));return false;
		if ($("#deskripsi").summernote('code') == "<p><br></p>" || $("#deskripsi").summernote('code') == "") {
			sweetAlert("Maaf...", "Alasan harus diisi!", "error");
			$("#deskripsi").focus();
			return false;
		}
		$("*[disabled]").not(true).removeAttr("disabled");
	}
	function goBack() {
	  window.history.back();
	}
	
</script>