<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('129'))){ ?>
<div class="block">
	<div class="block-header">
	
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('Tpenyesuaian_kas/export','class="form-horizontal" id="form-work" target="_blank"') ?>
            <div class="col-md-6">
				<input  type="hidden" readonly class="form-control"name="xjenis_kas_id" value="{jenis_kas_id}">
				<input  type="hidden" readonly class="form-control"name="xsumber_kas_id" value="{id}">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Jenis Kas</label>
                    <div class="col-md-8">
                      <select <?=$disabel?> disabled name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Jenis Kas -</option>
							<? foreach($list_jenis_kas as $row){?>
								<option value="<?=$row->id?>" <?=($jenis_kas_id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Sumber Kas</label>
                    <div class="col-md-8">
                       <select name="sumber_kas_id" disabled id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Sumber Kas -</option>
							<? foreach($list_sumber_kas as $row){?>
								<option value="<?=$row->id?>" <?=($id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="status">No. Akun</label>
                    <div class="col-md-8">
                       <input  type="text" readonly class="form-control" id="noakun" placeholder="Saldo" name="noakun" value="{noakun}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status"></label>
                    <div class="col-md-8">
						<button  class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
						<button  class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
						<button  class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
                    </div>
                </div>
				
				
				
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Bank</label>
                    <div class="col-md-8">
                       <select name="bank_id" disabled id="bank_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Bank-</option>
							<? foreach($list_bank as $row){?>
								<option value="<?=$row->id?>" <?=($bank_id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Saldo</label>
                    <div class="col-md-8">
                       <input  type="text" readonly class="form-control number" id="saldo_awal" placeholder="Saldo" name="saldo_awal" value="{saldo}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 15px;">
					<label class="col-md-4 control-label">Tanggal</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="tanggal1" name="tanggal1" placeholder="From" value="<?=HumanDateShort($tanggal1)?>">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="tanggal2" name="tanggal2" placeholder="To" value="<?=HumanDateShort($tanggal2)?>">
						</div>
						
					</div>
				</div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						
				  </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>#</th>
					<th>Tanggal</th>					
					<th>Sumber Kas</th>
					<th>Tipe Transaksi</th>
					<th>Keterangan</th>
					<th>Debet</th>
					<th>Kredit</th>
					<th>Saldo</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>


<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		$(".number").number(true,2,'.',',');
		load_index();
		
	})	
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	
	
	function load_index(){
		var tanggal1=$("#tanggal1").val();
		var tanggal2=$("#tanggal2").val();
		var sumber_kas_id=$("#sumber_kas_id").val();		
		
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tpenyesuaian_kas/getJurnal',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal1:tanggal1,
						tanggal2:tanggal2,
						sumber_kas_id:sumber_kas_id,
					   }
				},
				"columnDefs": [
					{ "width": "3%", "targets": 0, "visible": true },
					{ "width": "8%", "targets": 1, "visible": true,"class":"text-center" },
					{ "width": "8%", "targets": 2, "visible": true,"class":"text-center" },
					{ "width": "10%", "targets": 3, "visible": true,"class":"text-center" },
					{ "width": "30%", "targets": 4, "visible": true,"class":"text-left" },
					{ "width": "10%", "targets": 5, "visible": true,"class":"text-right" },
					{ "width": "10%", "targets": 6, "visible": true ,"class":"text-right"},
					{ "width": "10%", "targets": 7, "visible": true ,"class":"text-right"},
					{ "width": "10%", "targets": 8, "visible": true ,"class":"text-left"},
				]
			});
	}
	
</script>
