<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('129'))){ ?>
<div class="block">
	<div class="block-header">
	
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Jenis Kas</label>
                    <div class="col-md-8">
                       <select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Jenis Kas -</option>
							<? foreach($list_jenis_kas as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Sumber Kas</label>
                    <div class="col-md-8">
                       <select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Sumber Kas -</option>
							<? foreach($list_sumber_kas as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				
				
				
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Bank</label>
                    <div class="col-md-8">
                       <select name="bank_id" id="bank_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Bank -</option>
							<? foreach($list_bank as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_cetak" id="btn_cetak" style="font-size:13px;width:100%;float:right;"><i class="fa fa-print"></i> Cetak Seluruh</button>
                    </div>
					<div class="col-md-4">
                        <a href="{base_url}tpenyesuaian_kas/log" class="btn btn-primary text-uppercase" type="button" name="btn_log" id="btn_log" style="font-size:13px;width:100%;float:right;"><i class="si si-doc"></i> Log Penyesuaian</a>
                    </div>
                </div>
                
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>#</th>
					<th>No</th>					
					<th>Jenis Kas</th>
					<th>Sumber Kas</th>
					<th>Bank</th>
					<th>Saldo</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>


<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		
	})	
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	
	$(document).on("click",".verif",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Verifikasi Mutas Kas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tmutasi_kas/verif/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Mutas diselesaikan diverifikasi'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".hapus",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Hapus Mutas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tmutasi_kas/hapus/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Mutas diselesaikan diverifikasi'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	function load_index(){
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id").val();		
		var bank_id=$("#bank_id").val();
		
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tpenyesuaian_kas/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						jenis_kas_id:jenis_kas_id,
						sumber_kas_id:sumber_kas_id,
						bank_id:bank_id,
					   }
				},
				"columnDefs": [
					{ "width": "0%", "targets": 0, "visible": false },
					{ "width": "3%", "targets": 1, "visible": true,"class":"text-right" },
					{ "width": "8%", "targets": 2, "visible": true,"class":"text-left" },
					{ "width": "10%", "targets": 3, "visible": true,"class":"text-left" },
					{ "width": "10%", "targets": 4, "visible": true,"class":"text-left" },
					{ "width": "10%", "targets": 5, "visible": true,"class":"text-right" },
					{ "width": "10%", "targets": 6, "visible": true ,"class":"text-left"},
				]
			});
	}
	
</script>
