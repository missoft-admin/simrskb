<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mpengaturan_laporan_gudang" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open_multipart('mpengaturan_laporan_gudang/save','class="form-horizontal" id="form-work"') ?>

        <b><span class="label label-primary" style="font-size:12px">Setting Label Jenis Gudang</span></b><br /><br />
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="jenis-gudang-list">
                <thead>
                    <tr>
                        <th style="width:5%" class="text-center">No</th>
                        <th style="width:50%" class="text-center">Jenis Gudang</th>
                        <th style="width:50%" class="text-center">Label Name</th>
                        <th style="width:5%" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle;">X</th>
                        <th>
                            <select id="jenis_gudang" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="1">NON LOGISTIK</option>
                                <option value="2">LOGISTIK</option>
                            </select>
                        </th>
                        <th class="text-center">
                            <input type="text" class="form-control" id="label_name" placeholder="Label Name" name="label_name" value="">
                        </th>
                        <th class="text-center">
                            <button type="button" id="jenis-gudang-add" class="btn btn-success">Tambahkan</button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list_jenis_gudang as $index => $row) { ?>
                    <tr>
                        <td class="text-center"><?= $index + 1; ?></td>
                        <td data-id="<?= $row->jenis_gudang; ?>"><?= ($row->jenis_gudang == '1' ? 'NON LOGISTIK' : 'LOGISTIK'); ?></td>
                        <td><?= $row->label_name; ?></td>
                        <td>
                            <button type="button" class="btn btn-danger jenis-gudang-remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <b><span class="label label-primary" style="font-size:12px">Pengaturan Email</span></b><br /><br />
        <div class="form-group">
            <div class="col-md-12">
                <input type="text" name="email" class="js-tags-input form-control" value="<?= $list_email; ?>">
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12" style="float: right;">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}mpengaturan_laporan_gudang" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>

        <br><br>

        <input type="hidden" id="jenisGudangData" name="jenis_gudang_data">

        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<link href="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css">
<link href="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
$(document).ready(function() {
    // Add Jenis Gudang entry
    $('#jenis-gudang-add').on('click', function () {
        // Get values from input fields
        var jenisGudangId = $('#jenis_gudang').val();
        var jenisGudangText = $('#jenis_gudang option:selected').text();
        var labelName = $('#label_name').val();

        // Validate input
        if (!jenisGudangId) {
            swal('Error', 'Silakan pilih jenis gudang.', 'error');
            return;
        }

        // Add a new row to the table
        var counter = $('#jenis-gudang-list tbody tr').length + 1;
        var newRow = `<tr>
            <td class="text-center">${counter}</td>
            <td data-id="${jenisGudangId}">${jenisGudangText}</td>
            <td>${labelName}</td>
            <td>
                <button type="button" class="btn btn-danger jenis-gudang-remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></button>
            </td>
        </tr>`;
        $('#jenis-gudang-list tbody').append(newRow);

        // Clear input fields
        $('#jenis_gudang').val('1').trigger('change');
        $('#label_name').val('');

        // Keep the label visible
        $('#jenis_gudang').siblings('label').addClass('active');
    });

    // Remove Jenis Gudang entry
    $(document).on('click', '.jenis-gudang-remove', function () {
        var row = $(this).closest('tr');

        // Remove the current row
        row.remove();

        // Update the "No" column after deleting a row
        $('#jenis-gudang-list tbody tr').each(function (index) {
            $(this).find('td:first').text(index + 1);
        });
    });


    $('#form-work').submit(function (e) {
        // Add JSON data for tables
        var jenisGudangData = getJenisGudangData();

        $("#jenisGudangData").val(JSON.stringify(jenisGudangData));
    });

    function getJenisGudangData() {
        var jenisGudangData = [];
        $('#jenis-gudang-list tbody tr').each(function () {
            var idJenisGudang = $(this).find('td:nth-child(2)').attr('data-id');
            var labelName = $(this).find('td:nth-child(3)').html();

            jenisGudangData.push({
                jenis_gudang: idJenisGudang,
                label_name: labelName
            });
        });

        return jenisGudangData;
    }
});
</script>