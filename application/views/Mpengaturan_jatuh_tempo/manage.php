<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mpengaturan_jatuh_tempo/index/1" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>

    <?php echo form_open('mpengaturan_jatuh_tempo/save', '') ?>
    <div class="block-content">
        <div class="form-group">
            <label class="col-md-2" for="nama">Nama Setting</label>
            <div class="col-md-10">
                <input type="text" class="form-control" placeholder="Nama Setting" name="nama" value="{nama}">
            </div>
        </div>
		
		<br><br>
		<hr>
		<b><span class="label label-success" style="font-size:12px">SETTING JATUH TEMPO</span></b>
		<br><br>
		<div class="table-responsive">
        <table class="table table-bordered table-striped" id="jatuhtempoTable">
            <thead>
                <tr>
                    <th width="25%">Tanggal</th>
                    <th width="25%">Jatuh Tempo Bayar</th>
                    <th width="25%">User Info</th>
                    <th width="25%">Aksi</th>
                </tr>
                <tr>
                    <th>
                        <select class="form-control js-select2" id="tanggal" value="" style="width: 100%;">
                            <?php for ($i=1; $i <= 31; $i++) { ?>
                            <option value="<?= $i; ?>"><?= $i; ?></option>
                            <? } ?>
                        </select>
                    </th>
                    <th>
                        <input type="text" class="form-control number" id="jatuhtempo" value="">
                    </th>
                    <th>
                        <input type="text" class="form-control" readonly value="<?=$user_name;?> - <?=date("Y-m-d H:i:s");?>">
                    </th>
                    <th>
                        <a id="jatuhtempoAdd" class="btn btn-primary">Tambahkan</a>
                    </th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
		</div>
		<hr><br>

		<b><span class="label label-success" style="font-size:12px">SETTING RUJUKAN</span></b>
		<br><br>
		<div class="table-responsive">
        <table class="table table-bordered table-striped" id="rujukanTable">
            <thead>
                <tr>
                    <th width="25%">Tipe Rujukan</th>
                    <th width="25%">Nama Rujukan</th>
                    <th width="25%">Aksi</th>
                </tr>
                <tr>
                    <th>
                        <select class="form-control js-select2" id="tiperujukan" value="" style="width: 100%;">
                            <option value="0">Pilih Opsi</option>
                            <option value="1">Klinik</option>
                            <option value="2">Rumah Sakit</option>
                        </select>
                    </th>
                    <th>
                        <select class="form-control js-select2" id="idrujukan" value="" style="width: 100%;">
                            <option value="0">Pilih Opsi</option>
                        </select>
                    </th>
                    <th>
                        <a id="rujukanAdd" class="btn btn-primary">Tambahkan</a>
                    </th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
		</div>
		<hr>

        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-success" type="submit">Simpan</button>
				&nbsp;
                <a href="{base_url}mpengaturan_jatuh_tempo" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <br><br>
    </div>
    <?php echo form_hidden('id', $id); ?>
    <?php echo form_hidden('idtemp', $idtemp); ?>
    <?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	const idsetting = '{idtemp}';

	loadJatuhTempo(idsetting);
	loadRujukan(idsetting);

    $(".number").number(true, 0, '.', ',');
	
	$("#tiperujukan").change(function() {
		const tipeRujukan = $(this).val();

		if (tipeRujukan == 1) {
			getOpsiRujukan(1);
		} else if (tipeRujukan == 2) {
			getOpsiRujukan(2);
		} else {
			$('#idrujukan').html('<option value="0">Pilih Opsi</option>');
		}
	});

	$("#jatuhtempoAdd").click(function() {
		const tanggal = $("#tanggal option:selected").val();
		const jatuhtempo = $("#jatuhtempo").val();

		$.ajax({
			url: '{site_url}mpengaturan_jatuh_tempo/saveJatuhTempo',
			type: "POST",
			data: {
				idsetting: idsetting,
				tanggal: tanggal,
				jatuhtempo: jatuhtempo
			},
			success: function(data) {
				loadJatuhTempo(idsetting);
			}
		});
	});

	$(document).on("click", ".removeJatuhTempo", function() {
		const id = $(this).data('id');

		$.ajax({
			url: '{site_url}mpengaturan_jatuh_tempo/removeJatuhTempo/' + id,
			success: function(data) {
				loadJatuhTempo(idsetting);
			}
		});
	});

	$("#rujukanAdd").click(function() {
		const tiperujukan = $("#tiperujukan option:selected").val();
		const idrujukan = $("#idrujukan option:selected").val();
		const namarujukan = $("#idrujukan option:selected").text();

		$.ajax({
			url: '{site_url}mpengaturan_jatuh_tempo/saveRujukan',
			type: "POST",
			data: {
				idsetting: idsetting,
				tiperujukan: tiperujukan,
				idrujukan: idrujukan,
				namarujukan: namarujukan
			},
			success: function(data) {
				loadRujukan(idsetting);
			}
		});
	});

	$(document).on("click", ".removeRujukan", function() {
		const id = $(this).data('id');

		$.ajax({
			url: '{site_url}mpengaturan_jatuh_tempo/removeRujukan/' + id,
			success: function(data) {
				loadRujukan(idsetting);
			}
		});
	});
});

function loadJatuhTempo(idsetting) {
	$("#jatuhtempoTable tbody").empty();

	$.ajax({
		url: '{site_url}mpengaturan_jatuh_tempo/getJatuhTempo/' + idsetting,
		dataType: 'json',
		success: function(data) {
			data.map((item) => {
				$("#jatuhtempoTable tbody").append(`
					<tr>
						<td class="text-center">${item.tanggal}</td>
						<td class="text-center">${item.jatuhtempo}</td>
						<td class="text-center">${item.userinfo}</td>
						<td>
							<button class="btn btn-xs btn-danger removeJatuhTempo" data-id="${item.id}" type="button" title="Hapus">
								<i class="fa fa-trash-o"></i>
							</button>
						</td>
					</tr>
				`);
			});
		}
	});
}

function loadRujukan(idsetting) {
	$("#rujukanTable tbody").empty();

	$.ajax({
		url: '{site_url}mpengaturan_jatuh_tempo/getRujukan/' + idsetting,
		dataType: 'json',
		success: function(data) {
			data.map((item) => {
				$("#rujukanTable tbody").append(`
					<tr>
						<td class="text-center">${tipeRujukan(item.tiperujukan)}</td>
						<td class="text-center">${item.namarujukan}</td>
						<td>
							<button class="btn btn-xs btn-danger removeRujukan" data-id="${item.id}" type="button" title="Hapus">
								<i class="fa fa-trash-o"></i>
							</button>
						</td>
					</tr>
				`);
			});
		}
	});
}

function getOpsiRujukan(idtipe) {
	$('#idrujukan').html('<option value="0">Pilih Opsi</option>');

	$.ajax({
		url: '{site_url}mpengaturan_jatuh_tempo/getOpsiRujukan/' + idtipe,
		dataType: "json",
		success: function(data) {
			data.map((item) => {
				$('#idrujukan').append(`<option value="${item.id}">${item.nama}</option>`);
			});
			$('#idrujukan').selectpicker('refresh');
		}
	});
}

function tipeRujukan(id) {
	return (id == 1 ? 'Klinik' : 'Rumah Sakit');
}
</script>
