<?php
	$menuTransaksiPoliklinik = [
		'tpoliklinik_pendaftaran', 'tpoliklinik_tindakan'
	];

	$menuTransaksiRanap = [
		'trawatinap_pendaftaran', 'trawatinap_tindakan', 'trawatinap_tindakan_ods', 'trawatinap_nutrisi'
	];

	$menuTransaksiRanapTindakan = [
		'trawatinap_tindakan', 'trawatinap_tindakan_ods'
	];

	$menuTransaksiRujukan = [
		'trujukan_radiologi', 'trujukan_laboratorium', 'trujukan_fisioterapi'
	];

	$menuTransaksiGudang = [
		'tgudang_pemesanan', 'tgudang_penerimaan', 'tgudang_pengembalian', 'tunit_permintaan', 'tunit_pengembalian', 'tgudang_verifikasi', 'tgudang_approval'
	];

	$menuTransaksiGudangPusat = [
		'tgudang_approval', 'tgudang_pemesanan', 'tgudang_penerimaan', 'tgudang_pengembalian', 'tgudang_verifikasi'
	];

	$menuTransaksiGudangUnit = [
		'tunit_permintaan', 'tunit_pengembalian'
	];

	$menuTransaksiGudangSetting = [
		'mlogic_gudang', 'mlogic_gudang_kas'
	];

	$menuTransaksiJadwalDokter = [
		'mdokter_jadwal', 'mjadwal_dokter', 'mcuti_dokter'
	];

	$menuTransaksiGudang = array_merge($menuTransaksiGudang, $menuTransaksiGudangPusat, $menuTransaksiGudangUnit, $menuTransaksiGudangSetting);

	$menuTransaksiFarmasi = [
		'tpasien_penjualan', 'tpasien_pengembalian', 'tretur'
	];

	$menuTransaksiRM = [
		'trm_layanan_berkas', 'trm_pinjam_berkas', 'trm_berkas', 'trm_gabung', 'trm_gabung_history',
		'trm_gabung_transaksi', 'trm_gabung_transaksi_history', 'trm_analisa_hold', 'trm_pengelolaan_hold',
		'trm_pengajuan_informasi', 'trm_pengelolaan_informasi', 'trm_pengelolaan_dokter',
	];

	$menuTransaksi = array_merge(
		['tkasir', 'trm_pembayaran_informasi', 'tko', 'trefund', 'tstockopname'],
		$menuTransaksiPoliklinik,
		$menuTransaksiRanap,
		$menuTransaksiRanapTindakan,
		$menuTransaksiRujukan,
		$menuTransaksiGudang,
		$menuTransaksiFarmasi,
		$menuTransaksiRM,
        $menuTransaksiJadwalDokter
	);
?>

<li <?=(in_array($menu, $menuTransaksi)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-notebook"></i><span
			class="sidebar-mini-hide">Transaksi
			<?=(UserAccesForm($user_acces_form, ['260'])) ? $notif_keuangan : ''?></span></a>
    <ul>
        <?php if (UserAccesForm($user_acces_form, ['250'])) { ?>
            <li>
                <a <?=menuIsActive('tkasir')?> href="{base_url}tkasir"><i class="si si-list"></i><span class="sidebar-mini-hide">Kasir Rawat jalan</a>
            </li>
        <?php } ?>
        <?php if (UserAccesForm($user_acces_form, ['2621'])) { ?>
            <li>
                <a <?=menuIsActive('trm_pembayaran_informasi')?> href="{base_url}trm_pembayaran_informasi"><i class="si si-notebook"></i><span class="sidebar-mini-hide">P. Informasi Medis</a>
            </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['317', '330'])) { ?>
        <li <?=(in_array($menu, $menuTransaksiPoliklinik)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-book-open"></i><span
					class="sidebar-mini-hide">Poliklinik & IGD</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['317'])) { ?>
                <li>
                    <a <?=menuIsActive('tpoliklinik_pendaftaran')?> href="{base_url}tpoliklinik_pendaftaran/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pendaftaran</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['330'])) { ?>
                <li>
                    <a <?=menuIsActive('tpoliklinik_tindakan')?> href="{base_url}tpoliklinik_tindakan/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Tindakan</a>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['359', '369', '1200'])) { ?>
        <li <?=(in_array($menu, $menuTransaksiRanap)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-hospital-o"></i><span
					class="sidebar-mini-hide">Rawat Inap & ODS</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['359'])) { ?>
                <li>
                    <a <?=menuIsActive('trawatinap_pendaftaran')?> href="{base_url}trawatinap_pendaftaran/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pendaftaran</a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['1199', '369', '1200'])) { ?>
                <li <?=(in_array($menu, $menuTransaksiRanapTindakan)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
							class="glyphicon glyphicon-option-horizontal"></i><span
							class="sidebar-mini-hide">Tindakan</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['369', '1200'])) { ?>
                        <li>
                            <a <?=menuIsActive('trawatinap_tindakan')?> href="{base_url}trawatinap_tindakan/index"><i
									class="glyphicon glyphicon-option-horizontal"></i>Rawat Inap</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1199'])) { ?>
                        <li>
                            <a <?=menuIsActive('trawatinap_tindakan_ods')?>
								href="{base_url}trawatinap_tindakan_ods/index"><i
									class="glyphicon glyphicon-option-horizontal"></i>One Day Surgery</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (navigation_roles('tko')) { ?>
        <?php if (UserAccesForm($user_acces_form, ['282'])) { ?>
        <li>
            <a <?=menuIsActive('tko')?> href="{base_url}tko"><i class="fa fa-stethoscope"></i><span
					class="sidebar-mini-hide">Kamar Operasi</span></a>
        </li>
        <?php } ?>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['994', '1013', '1031'])) { ?>
        <li <?=(in_array($menu, $menuTransaksiRujukan)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-direction"></i><span
					class="sidebar-mini-hide">Rujukan</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['994'])) { ?>
                <li>
                    <a <?=menuIsActive('trujukan_radiologi')?> href="{base_url}trujukan_radiologi/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Radiologi</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1013'])) { ?>
                <li>
                    <a <?=menuIsActive('trujukan_laboratorium')?> href="{base_url}trujukan_laboratorium/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Laboratorium</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1031'])) { ?>
                <li>
                    <a <?=menuIsActive('trujukan_fisioterapi')?> href="{base_url}trujukan_fisioterapi/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Fisioterapi</a>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['1115', '1125', '1138', '1144', '1102', '1109'])) { ?>
        <li <?=(in_array($menu, $menuTransaksiGudang)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-home"></i><span
					class="sidebar-mini-hide">Gudang</span>
				<?=(UserAccesForm($user_acces_form, ['260'])) ? $notif_keuangan : ''?></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['1115', '1125', '1138', '1144'])) { ?>
                <li <?=(in_array($menu, $menuTransaksiGudangSetting)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
							class="glyphicon glyphicon-option-horizontal"></i><span
							class="sidebar-mini-hide">Setting</span>
					</a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1115'])) { ?>
                        <li>
                            <a <?=menuIsActive('mlogic_gudang')?> href="{base_url}mlogic_gudang"><i
									class="glyphicon glyphicon-option-horizontal"></i>Approval</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1115'])) { ?>
                        <li>
                            <a <?=menuIsActive('mlogic_gudang_kas')?> href="{base_url}mlogic_gudang_kas"><i
									class="glyphicon glyphicon-option-horizontal"></i>Kas Proses</a>
                        </li>
                        <?php } ?>

                    </ul>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1115', '1125', '1138', '1144'])) { ?>
                <li <?=(in_array($menu, $menuTransaksiGudangPusat)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
							class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pusat Ke
							Dist</span>
						<?=(UserAccesForm($user_acces_form, ['260'])) ? $notif_keuangan : ''?></a>
                    <ul>

                        <?php if (UserAccesForm($user_acces_form, ['1115'])) { ?>
                        <li>
                            <a <?=menuIsActive('tgudang_pemesanan')?> href="{base_url}tgudang_pemesanan"><i
									class="glyphicon glyphicon-option-horizontal"></i>Pemesanan</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1125'])) { ?>
                        <li>
                            <a <?=menuIsActive('tgudang_verifikasi')?> href="{base_url}tgudang_verifikasi"><i
									class="glyphicon glyphicon-option-horizontal"></i>Verifikasi</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1125'])) { ?>
                        <li>
                            <a <?=menuIsActive('tgudang_approval')?> href="{base_url}tgudang_approval"><i
									class="glyphicon glyphicon-option-horizontal"></i>Approval
								<?=(UserAccesForm($user_acces_form, ['260'])) ? $notif_keuangan : ''?></a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1138'])) { ?>
                        <li>
                            <a <?=menuIsActive('tgudang_penerimaan')?> href="{base_url}tgudang_penerimaan"><i
									class="glyphicon glyphicon-option-horizontal"></i>Penerimaan</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1144'])) { ?>
                        <li>
                            <a <?=menuIsActive('tgudang_pengembalian')?> href="{base_url}tgudang_pengembalian"><i
									class="glyphicon glyphicon-option-horizontal"></i>Pengembalian</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>


                <?php if (UserAccesForm($user_acces_form, ['1102', '1109'])) { ?>
                <li <?=(in_array($menu, $menuTransaksiGudangUnit)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
							class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Unit Ke
							Pusat</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1102'])) { ?>
                        <li>
                            <a <?=menuIsActive('tunit_permintaan')?> href="{base_url}tunit_permintaan"><i
									class="glyphicon glyphicon-option-horizontal"></i>Permintaan</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1109'])) { ?>
                        <li>
                            <a <?=menuIsActive('tunit_pengembalian')?> href="{base_url}tunit_pengembalian"><i
									class="glyphicon glyphicon-option-horizontal"></i>Pengembalian</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['1044', '1065', '1053'])) { ?>
        <li <?=(in_array($menu, $menuTransaksiFarmasi)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-support"></i><span
					class="sidebar-mini-hide">Farmasi</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['1044', '1053'])) { ?>
                <li>
                    <a <?=menuIsActive('tpasien_penjualan')?> href="{base_url}tpasien_penjualan/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Penjualan Obat</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1065'])) { ?>
                <li>
                    <a <?=menuIsActive('tpasien_pengembalian')?> href="{base_url}tpasien_pengembalian"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pengembalian Obat</a>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['1090'])) { ?>
        <li>
            <a <?=menuIsActive('trefund')?> href="{base_url}trefund/index"><i class="si si-notebook"></i><span
					class="sidebar-mini-hide">Refund</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['267'])) { ?>
        <li>
            <a <?=menuIsActive('tstockopname')?> href="{base_url}tstockopname/index"><i class="si si-notebook"></i><span
					class="sidebar-mini-hide">Stock Opname</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['1155', '1161', '1165', '1167', '1224', '1193', '1196', '1194'])) { ?>
        <li <?=(in_array($menu, $menuTransaksiRM)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
					class="glyphicon glyphicon-registration-mark"></i><span class="sidebar-mini-hide">Rekam Medis
				</span><?=$notif_berkas?> <?=$notif_pinjam?></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['1155', '1161'])) { ?>
                <li>
                    <a <?=menuIsActive('trm_layanan_berkas')?> href="{base_url}trm_layanan_berkas/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pelayanan Berkas </span><?=$notif_berkas?>
						<?=$notif_pinjam?></a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1165'])) { ?>
                <li>
                    <a <?=menuIsActive('trm_pinjam_berkas')?> href="{base_url}trm_pinjam_berkas/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pinjam Berkas</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1167'])) { ?>
                <li>
                    <a <?=menuIsActive('trm_berkas')?> href="{base_url}trm_berkas"><i
							class="glyphicon glyphicon-option-horizontal"></i>Berkas</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1193', '1224'])) { ?>
                <li>
                    <a <?=menuIsActive('trm_gabung')?> <?=menuIsActive('trm_gabung_history')?>
						href="{base_url}trm_gabung"><i class="glyphicon glyphicon-option-horizontal"></i>Gabung RM</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1196', '1194'])) { ?>
                <li>
                    <a <?=menuIsActive('trm_gabung_transaksi')?> <?=menuIsActive('trm_gabung_transaksi_history')?>
						href="{base_url}trm_gabung_transaksi"><i
							class="glyphicon glyphicon-option-horizontal"></i>Gabung Transaksi</a>
                </li>
                <?php } ?>
                <li>
                    <a <?=menuIsActive('trm_analisa_hold')?>
						href="{base_url}trm_analisa_hold"><i class="glyphicon glyphicon-option-horizontal"></i>Rincian
						Analisa Hold</a>
                </li>
                <li>
                    <a <?=menuIsActive('trm_pengelolaan_hold')?>
						href="{base_url}trm_pengelolaan_hold"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pengelolaan Hold</a>
                </li>
                <li>
                    <a <?=menuIsActive('trm_pengajuan_informasi')?>
						href="{base_url}trm_pengajuan_informasi"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pengajuan Informasi</a>
                </li>
                <li>
                    <a <?=menuIsActive('trm_pengelolaan_informasi')?>
						href="{base_url}trm_pengelolaan_informasi"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pengelolaan Informasi</a>
                </li>
                <li>
                    <a <?=menuIsActive('trm_pengelolaan_dokter')?>
						href="{base_url}trm_pengelolaan_dokter"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pengelolaan Dokter</a>
                </li>
            </ul>
        </li>
        <?php } ?>

<?php $menu2 = $this->uri->segment(2); ?>

        <li <?=(in_array($menu, $menuTransaksiJadwalDokter)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Jadwal Dokter</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['16'])) { ?>
                <li>
                    <a <?=menuIsActive('mdokter_jadwal')?> href="{base_url}mdokter_jadwal"><i class="glyphicon glyphicon-option-horizontal"></i>Jadwal Dokter IGD</a>
                </li>
                <?php } ?>
                <li>
                    <a <?=($menu=='mjadwal_dokter' && $menu2!='display'?'class="active"' : '')?> href="{base_url}mjadwal_dokter"><i class="glyphicon glyphicon-option-horizontal"></i>Jadwal Dokter Poliklinik</a>
                </li>
                <li>
                    <a <?=menuIsActive('mcuti_dokter')?> href="{base_url}mcuti_dokter"><i class="glyphicon glyphicon-option-horizontal"></i>Cuti Dokter</a>
                </li>
                <li>
                    <a <?=($menu=='mjadwal_dokter' && $menu2=='display'?'class="active"' : '')?>  href="{base_url}mjadwal_dokter/display"><i class="glyphicon glyphicon-option-horizontal"></i>Display Jadwal</a>
                </li>
            </ul>
        </li>

    </ul>
</li>
