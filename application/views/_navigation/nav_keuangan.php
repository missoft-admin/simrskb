<?php
  //Akuntansi
  $menuAkuntansiMaster = [
    'mnomorakuntansi', 'mkategori_akun', 'mgroup_pembayaran', 'mperiode_akun'
  ];
  $menuAkuntansiSettingKas = [
    'msetting_jurnal_kasbon', 'msetting_jurnal_refund', 'msetting_jurnal_fee', 'msetting_jurnal_penyesuaian', 'msetting_jurnal_bagi_hasil', 'msetting_jurnal_gaji', 'msetting_jurnal_kas_lain', 'msetting_jurnal_honor'
  ];
  $menuAkuntansiSettingPendapatan = [
    'msetting_jurnal_pendapatan_general', 'msetting_jurnal_pendapatan_farmasi', 'msetting_jurnal_pendapatan_jual_rajal', 'msetting_jurnal_pendapatan_jual_ranap', 'msetting_jurnal_pendapatan_jual_ko', 'msetting_jurnal_pendapatan_ko'
  ];
  $menuAkuntansiSetting = [
    'msetting_jurnal_pembelian', 'msetting_jurnal_hutang', 'msetting_jurnal_pengelolaan', 'msetting_jurnal_deposit', 'msetting_jurnal_pendapatan', 'msetting_jurnal_retur', 'msetting_jurnal_klaim', 'msetting_jurnal_setoran', 'msetting_jurnal_umum'
  ];
  $menuAkuntansiSetting = array_merge($menuAkuntansiSetting, $menuAkuntansiSettingKas, $menuAkuntansiSettingPendapatan);
  $menuAkuntansiTransaksiPendapatan = [
    'tvalidasi_pendapatan', 'tvalidasi_pendapatan_rajal', 'tvalidasi_pendapatan_ranap'
  ];
  $menuAkuntansiTransaksi = [
    'tvalidasi_gudang', 'tvalidasi_hutang','tvalidasi_pengelolaan', 'tvalidasi_deposit', 'tvalidasi_retur', 'tvalidasi_klaim', 'tvalidasi_setoran', 'tvalidasi_jurnal_umum', 'tvalidasi_kas'
  ];
  $menuAkuntansiTransaksi = array_merge($menuAkuntansiTransaksi, $menuAkuntansiTransaksiPendapatan);
  $menuAkuntansiLaporanPendapatan = ['lvalidasi_pendapatan', 'lvalidasi_pendapatan_rajal', 'lvalidasi_pendapatan_ranap'];
  $menuAkuntansiLaporan = [
    'lvalidasi_gudang', 'lvalidasi_hutang', 'lvalidasi_pengelolaan','lvalidasi_deposit', 'lvalidasi_retur', 'lvalidasi_klaim', 'lvalidasi_setoran', 'lvalidasi_jurnal_umum', 'lvalidasi_kas'
  ];
  $menuAkuntansiHutangPiutangMaster = ['mdata_hutang', 'mdata_piutang', 'mdata_pengelolaan'];
  $menuAkuntansiHutangPiutangTrx = ['tpengelolaan', 'tpengelolaan_approval'];
  $menuAkuntansiHutangPiutang = array_merge($menuAkuntansiHutangPiutangMaster, $menuAkuntansiHutangPiutangTrx);
  
  $menuAkuntansiLabaRugi = ['tlaba_rugi', 'tlaba_rugi_setting'];
  $menuAkuntansiNeraca = ['tneraca', 'tneraca_setting'];
  $menuAkuntansiBukuBesarPiutang = ['tbuku_besar_piutang_setting', 'tbuku_besar_piutang_detail', 'tbuku_besar_piutang_rekap'];
  $menuAkuntansiBukuBesarHutang = ['tbuku_besar_hutang_setting', 'tbuku_besar_hutang_detail', 'tbuku_besar_hutang_rekap'];
  $menuAkuntansiBukuBesarPersediaan = ['tbuku_besar_persediaan_setting', 'tbuku_besar_persediaan'];
  $menuAkuntansiBukuBesar = ['tbuku_besar'];
  $menuAkuntansiBukuBesar = array_merge($menuAkuntansiBukuBesar,$menuAkuntansiBukuBesarHutang, $menuAkuntansiBukuBesarPiutang, $menuAkuntansiBukuBesarPersediaan);
  
  $menuAkuntansiLaporan = array_merge($menuAkuntansiLaporan, $menuAkuntansiLaporanPendapatan);
  $menuAkuntansi = array_merge(['tneraca_lajur'],
    $menuAkuntansiMaster,
    $menuAkuntansiSetting,
    $menuAkuntansiTransaksi,
    $menuAkuntansiLaporan,
    $menuAkuntansiBukuBesar,$menuAkuntansiLabaRugi,$menuAkuntansiNeraca
  );
    
  // Verifikator
  $menuVerifikatorVerifikasi = [
    'tverifikasi_transaksi', 'tverifikasi_deposit', 'ttransport_dokter'
  ];
  $menuVerifikatorSetting = [
    'msetting_verifikasi_deposit'
  ];
  $menuVerifikator = array_merge(
    ['tkwitansi_manual'],
    $menuVerifikatorVerifikasi,
    $menuVerifikatorSetting
  );

  // Pencairan
  $menuPencairan = [
    'tpencairan',
  ];

  // Honor Dokter
  $menuHonorData = [
    'msetting_feerujukan_dokter', 'mpotongan_dokter', 'msetting_honor_dokter', 'mlogic_honor', 'msetting_honor_dokter_email',
  ];

  $menuHonorTransaksi = [
    'tpendapatan_nonpelayanan', 'tpotongan_dokter', 'thonor_dokter', 'thonor_approval', 'thonor_bayar',
  ];

  $menuHonor = array_merge($menuHonorData, $menuHonorTransaksi);

  // Kasbon
  $menuKasbonSetting = [
    'mlogic_kasbon',
  ];

  $menuKasbonTransaksi = [
    'tkasbon', 'tkasbon_kas',
  ];

  $menuKasbon = array_merge($menuKasbonSetting, $menuKasbonTransaksi);

  // Payroll
  $menuPayrollData = [
    'mjenis_gaji', 'mvariable', 'mrekapan'
  ];

  $menuPayrollTransaksi = [
    'tpenggajian', 'trekap',
  ];

  $menuPayroll = array_merge($menuPayrollData, $menuPayrollTransaksi);

  // Pendapatan Lain Lain
  $menuPendapatanLainLainData = [
    'mpendapatan', 'mdari'
  ];

  $menuPendapatanLainLainTransaksi = [
    'tpendapatan'
  ];

  $menuPendapatanLainLain = array_merge($menuPendapatanLainLainData, $menuPendapatanLainLainTransaksi);

  // Kontrabon
  $menuKontrabon = [
    'mkontrabon', 'tkontrabon_verifikasi', 'tkontrabon', 'tkontrabon_his',
  ];
  $menuPiutang = [
    'msetting_piutang', 'tpiutang_tt_verifikasi', 'tpiutang_tt_monitoring', 'tpiutang_tt_pembayaran', 'tpiutang_tt_reminder', 'tpiutang_tt_umur'
  ];

  // Penagihan
  $menuPenagihanProses = [
    'tklaim_verifikasi', 'tklaim_rincian', 'tklaim_monitoring', 'tklaim_umur',
  ];

  $menuPenagihanPiutang = [
    'mpiutang', 'tpiutang_verifikasi', 'tpiutang_rincian', 'tpiutang_history',
  ];

  $menuPenagihan = array_merge($menuPenagihanProses, $menuPenagihanPiutang);

  // Pengajuan
  $menuPengajuanData = [
    'mprespektif', 'mprogram', 'mklasifikasi', 'mkegiatan', 'mjenis_pengajuan', 'mlogic', 'mvendor',
  ];

  $menuPengajuanTransaksi = [
    'mrka', 'mrka_kegiatan', 'mrka_kegiatan_setting', 'mrka_reminder', 'mrka_pengajuan',
    'mrka_approval', 'mrka_approval_lanjutan','mrka_pengajuan_monitoring', 'mrka_bendahara', 'mrka_belanja',
  ];

  $menuPengajuan = array_merge($menuPengajuanData, $menuPengajuanTransaksi);

  // Master Kas
  $menuMasterKasData = [
    'mjenis_kas', 'msumber_kas', 'medc', 'mlogic_refund','mlogic_mutasi',
  ];

  $menuMasterKasTransaksi = [
    'tsetoran_kas', 'tmonitoring', 'tmutasi_kas','tmutasi_kas_approval',
    'tpenyesuaian_kas', 'trefund_kas', 'tklaim_validasi',
  ];

  $menuMasterKas = array_merge($menuMasterKasData, $menuMasterKasTransaksi);

  // Bagi Hasil
  $menuBagiHasilData = [
    'mpemilik_saham', 'mbiaya_operasional', 'mbagi_hasil_setting',
  ];

  $menuBagiHasilTransaksi = [
    'mbagi_hasil', 'tbagi_hasil', 'tbagi_hasil_bayar', 'tbagi_hasil_approval'
  ];

  $menuBagiHasil = array_merge($menuBagiHasilData, $menuBagiHasilTransaksi);

  $menuReturGudang = [
    'tretur_bayar', 'tretur_terima'
  ];

  $menuReferalManagement = [
    'msetting_approval_feerujukan', 'mpengaturan_jatuh_tempo', 'trujukan_rumahsakit_verifikasi', 'trujukan_rumahsakit_rincian', 'trujukan_rumahsakit_approval', 'trujukan_rumahsakit_pembayaran'
  ];

  $menuKeuangan = array_merge(
    $menuVerifikator,
    $menuPencairan,
    $menuHonor,
    $menuKontrabon,
    $menuPendapatanLainLain,
    $menuKasbon,
    $menuPayroll,
    $menuPiutang,
    $menuPenagihan,
    $menuPengajuan,
    $menuMasterKas,
    $menuBagiHasil,
    $menuReferalManagement,
    $menuReturGudang,
    $menuAkuntansi,
    $menuAkuntansiHutangPiutang
  );
?>

<li <?=(in_array($menu, $menuKeuangan)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Keuangan</span></a>
    <ul>
        <?php if (UserAccesForm($user_acces_form, ['252'])) { ?>
        <li <?=(in_array($menu, $menuAkuntansi)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-notebook"></i><span class="sidebar-mini-hide">Akuntansi</span></a>
            <ul>
                <li <?=(in_array($menu, $menuAkuntansiMaster)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Master Data</span></a>
                    <ul>
                        <li>
                            <a <?=menuIsActive('mkategori_akun')?> href="{base_url}mkategori_akun"><i class="glyphicon glyphicon-option-horizontal"></i>Kategori No. Akuntansi</a>
                        </li>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('mnomorakuntansi')?> href="{base_url}mnomorakuntansi"><i class="glyphicon glyphicon-option-horizontal"></i>No. Akuntansi</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('mperiode_akun')?> href="{base_url}mperiode_akun"><i class="glyphicon glyphicon-option-horizontal"></i>Periode Akuntansi</a>
                        </li>
                        <?php endif ?>
                        <li>
                            <a <?=menuIsActive('mgroup_pembayaran')?> href="{base_url}mgroup_pembayaran"><i class="glyphicon glyphicon-option-horizontal"></i>Group Pendapatan</a>
                        </li>
                    </ul>
                </li>
                <li <?=(in_array($menu, $menuAkuntansiSetting)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Setting Jurnal</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_pembelian')?> href="{base_url}msetting_jurnal_pembelian"><i class="glyphicon glyphicon-option-horizontal"></i>Pembelian</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_pengelolaan')?> href="{base_url}msetting_jurnal_pengelolaan"><i class="glyphicon glyphicon-option-horizontal"></i>Selisih Hutang & Piutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_hutang')?> href="{base_url}msetting_jurnal_hutang"><i class="glyphicon glyphicon-option-horizontal"></i>Hutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_deposit')?> href="{base_url}msetting_jurnal_deposit"><i class="glyphicon glyphicon-option-horizontal"></i>Deposit</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_pendapatan')?> href="{base_url}msetting_jurnal_pendapatan"><i class="glyphicon glyphicon-option-horizontal"></i>Pendapatan</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_retur')?> href="{base_url}msetting_jurnal_retur"><i class="glyphicon glyphicon-option-horizontal"></i>Retur</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_klaim')?> href="{base_url}msetting_jurnal_klaim"><i class="glyphicon glyphicon-option-horizontal"></i>Pelunasan Piutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_setoran')?> href="{base_url}msetting_jurnal_setoran"><i class="glyphicon glyphicon-option-horizontal"></i>Setoran Kas</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('msetting_jurnal_umum')?> href="{base_url}msetting_jurnal_umum"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Umum</a>
                        </li>
                        <?php endif ?>
                        <li <?=(in_array($menu, $menuAkuntansiSettingKas)) ? 'class="open"' : ''; ?>>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Kas</span></a>
                            <ul>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_kasbon')?> href="{base_url}msetting_jurnal_kasbon"><i class="glyphicon glyphicon-option-horizontal"></i>Kasbon</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_refund')?> href="{base_url}msetting_jurnal_refund"><i class="glyphicon glyphicon-option-horizontal"></i>Refund</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_fee')?> href="{base_url}msetting_jurnal_fee"><i class="glyphicon glyphicon-option-horizontal"></i>Fee Rujukan</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_penyesuaian')?> href="{base_url}msetting_jurnal_penyesuaian"><i class="glyphicon glyphicon-option-horizontal"></i>Penyesuaian Kas</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_bagi_hasil')?> href="{base_url}msetting_jurnal_bagi_hasil"><i class="glyphicon glyphicon-option-horizontal"></i>Bagi Hasil</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_honor')?> href="{base_url}msetting_jurnal_honor"><i class="glyphicon glyphicon-option-horizontal"></i>Honor Dokter</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_gaji')?> href="{base_url}msetting_jurnal_gaji"><i class="glyphicon glyphicon-option-horizontal"></i>Rekap Gaji</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_kas_lain')?> href="{base_url}msetting_jurnal_kas_lain"><i class="glyphicon glyphicon-option-horizontal"></i>Kas Lainnya</a></li>
                                <?php endif ?>
                            </ul>
                        </li>
                        <li <?=(in_array($menu, $menuAkuntansiSettingPendapatan)) ? 'class="open"' : ''; ?>>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Pendapatan</span></a>
                            <ul>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_pendapatan_general')?> href="{base_url}msetting_jurnal_pendapatan_general"><i class="glyphicon glyphicon-option-horizontal"></i>General</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_pendapatan_farmasi')?> href="{base_url}msetting_jurnal_pendapatan_farmasi"><i class="glyphicon glyphicon-option-horizontal"></i>Penjualan Farmasi</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_pendapatan_jual_rajal')?> href="{base_url}msetting_jurnal_pendapatan_jual_rajal"><i class="glyphicon glyphicon-option-horizontal"></i>Obat & Alkes Rajal</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_pendapatan_jual_ranap')?> href="{base_url}msetting_jurnal_pendapatan_jual_ranap"><i class="glyphicon glyphicon-option-horizontal"></i>Obat & Alkes Ranap</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_pendapatan_jual_ko')?> href="{base_url}msetting_jurnal_pendapatan_jual_ko"><i class="glyphicon glyphicon-option-horizontal"></i>Obat & Alkes KO</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('msetting_jurnal_pendapatan_ko')?> href="{base_url}msetting_jurnal_pendapatan_ko"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi KO</a></li>
                                <?php endif ?>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li <?=(in_array($menu, $menuAkuntansiTransaksi)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi Validasi</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_gudang')?> href="{base_url}tvalidasi_gudang"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Pembelian</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_pengelolaan')?> href="{base_url}tvalidasi_pengelolaan"><i class="glyphicon glyphicon-option-horizontal"></i>Selisih Hutang & Piutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_hutang')?> href="{base_url}tvalidasi_hutang"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Hutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_deposit')?> href="{base_url}tvalidasi_deposit"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Deposit</a>
                        </li>
                        <?php endif ?>

                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_retur')?> href="{base_url}tvalidasi_retur"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Retur</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_klaim')?> href="{base_url}tvalidasi_klaim"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Pelunasan Piutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_setoran')?> href="{base_url}tvalidasi_setoran"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Setoran Kas</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_jurnal_umum')?> href="{base_url}tvalidasi_jurnal_umum"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Umum</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('tvalidasi_kas')?> href="{base_url}tvalidasi_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Kas</a>
                        </li>
                        <?php endif ?>
                        <li <?=(in_array($menu, $menuAkuntansiTransaksiPendapatan)) ? 'class="open"' : ''; ?>>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Pendapatan</span></a>
                            <ul>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tvalidasi_pendapatan_rajal')?> href="{base_url}tvalidasi_pendapatan_rajal"><i class="glyphicon glyphicon-option-horizontal"></i>Rawat Jalan</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tvalidasi_pendapatan_ranap')?> href="{base_url}tvalidasi_pendapatan_ranap"><i class="glyphicon glyphicon-option-horizontal"></i>Rawat Inap</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tvalidasi_pendapatan')?> href="{base_url}tvalidasi_pendapatan"><i class="glyphicon glyphicon-option-horizontal"></i>Lainnya</a></li>
                                <?php endif ?>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li <?=(in_array($menu, $menuAkuntansiLaporan)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Laporan Jurnal</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('lvalidasi_gudang')?> href="{base_url}lvalidasi_gudang"><i class="glyphicon glyphicon-option-horizontal"></i>Pembelian</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('lvalidasi_pengelolaan')?> href="{base_url}lvalidasi_pengelolaan"><i class="glyphicon glyphicon-option-horizontal"></i>Selisih Hutang Piutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('lvalidasi_hutang')?> href="{base_url}lvalidasi_hutang"><i class="glyphicon glyphicon-option-horizontal"></i>Hutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('lvalidasi_deposit')?> href="{base_url}lvalidasi_deposit"><i class="glyphicon glyphicon-option-horizontal"></i>Deposit</a>
                        </li>
                        <?php endif ?>

                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('lvalidasi_retur')?> href="{base_url}lvalidasi_retur"><i class="glyphicon glyphicon-option-horizontal"></i>Retur</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('lvalidasi_klaim')?> href="{base_url}lvalidasi_klaim"><i class="glyphicon glyphicon-option-horizontal"></i>Pelunasan Piutang</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('lvalidasi_setoran')?> href="{base_url}lvalidasi_setoran"><i class="glyphicon glyphicon-option-horizontal"></i>Setoran Kas</a>
                        </li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=menuIsActive('lvalidasi_kas')?> href="{base_url}lvalidasi_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi Kas</a>
                        </li>
                        <?php endif ?>
                        <li <?=(in_array($menu, $menuAkuntansiLaporanPendapatan)) ? 'class="open"' : ''; ?>>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Pendapatan</span></a>
                            <ul>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('lvalidasi_pendapatan_rajal')?> href="{base_url}lvalidasi_pendapatan_rajal"><i class="glyphicon glyphicon-option-horizontal"></i>Rawat Jalan</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('lvalidasi_pendapatan_ranap')?> href="{base_url}lvalidasi_pendapatan_ranap"><i class="glyphicon glyphicon-option-horizontal"></i>Rawat Inap</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('lvalidasi_pendapatan')?> href="{base_url}lvalidasi_pendapatan"><i class="glyphicon glyphicon-option-horizontal"></i>Lainnya</a></li>
                                <?php endif ?>

                            </ul>
                        </li>

                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li>
                            <a <?=($this->uri->segment(1) == 'lvalidasi_jurnal_umum' && $this->uri->segment(3) == '1' ? 'class="active"' : '')?> href="{base_url}lvalidasi_jurnal_umum/index/1"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Depresiasi</a>
                        </li>
                        <li>
                            <a <?=($this->uri->segment(1) == 'lvalidasi_jurnal_umum' && $this->uri->segment(3) == '2' ? 'class="active"' : '')?> href="{base_url}lvalidasi_jurnal_umum/index/2"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Penyesuaian</a>
                        </li>
                        <li>
                            <a <?=($this->uri->segment(1) == 'lvalidasi_jurnal_umum' && $this->uri->segment(3) == '3' ? 'class="active"' : '')?> href="{base_url}lvalidasi_jurnal_umum/index/3"><i class="glyphicon glyphicon-option-horizontal"></i>Jurnal Rekonsiliasi</a>
                        </li>
                        <?php endif ?>
                    </ul>
                </li>
                <li <?=(in_array($menu, $menuAkuntansiBukuBesar)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-book"></i>Buku Besar</span></a>
                    <ul>
                        <a <?=menuIsActive('tbuku_besar')?> href="{base_url}tbuku_besar"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Buku Besar</a>

                        <li <?=(in_array($menu, $menuAkuntansiBukuBesarHutang)) ? 'class="open"' : ''; ?>>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Pembantu Hutang</span></a>
                            <ul>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tbuku_besar_hutang_setting')?> href="{base_url}tbuku_besar_hutang_setting"><i class="glyphicon glyphicon-option-horizontal"></i>Setting</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tbuku_besar_hutang_detail')?> href="{base_url}tbuku_besar_hutang_detail"><i class="glyphicon glyphicon-option-horizontal"></i>Detail Laporan</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tbuku_besar_hutang_rekap')?> href="{base_url}tbuku_besar_hutang_rekap"><i class="glyphicon glyphicon-option-horizontal"></i>Rekapitulasi</a></li>
                                <?php endif ?>


                            </ul>
                        </li>
                        <li <?=(in_array($menu, $menuAkuntansiBukuBesarPiutang)) ? 'class="open"' : ''; ?>>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Pembantu Piutang</span></a>
                            <ul>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tbuku_besar_piutang_setting')?> href="{base_url}tbuku_besar_piutang_setting"><i class="glyphicon glyphicon-option-horizontal"></i>Setting</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tbuku_besar_piutang_detail')?> href="{base_url}tbuku_besar_piutang_detail"><i class="glyphicon glyphicon-option-horizontal"></i>Detail Laporan</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tbuku_besar_piutang_rekap')?> href="{base_url}tbuku_besar_piutang_rekap"><i class="glyphicon glyphicon-option-horizontal"></i>Rekapitulasi</a></li>
                                <?php endif ?>

                            </ul>
                        </li>
                        <li <?=(in_array($menu, $menuAkuntansiBukuBesarPersediaan)) ? 'class="open"' : ''; ?>>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Persediaan</span></a>
                            <ul>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tbuku_besar_persediaan_setting')?> href="{base_url}tbuku_besar_persediaan_setting"><i class="glyphicon glyphicon-option-horizontal"></i>Setting</a></li>
                                <?php endif ?>
                                <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                                <li><a <?=menuIsActive('tbuku_besar_persediaan')?> href="{base_url}tbuku_besar_persediaan"><i class="glyphicon glyphicon-option-horizontal"></i>Detail Laporan</a></li>
                                <?php endif ?>


                            </ul>
                        </li>
                    </ul>
                </li>
                <a <?=menuIsActive('tneraca_lajur')?> href="{base_url}tneraca_lajur"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Neraca Lajur</a>
                <li <?=(in_array($menu, $menuAkuntansiLabaRugi)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Laba Rugi</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li><a <?=menuIsActive('tlaba_rugi_setting')?> href="{base_url}tlaba_rugi_setting"><i class="glyphicon glyphicon-option-horizontal"></i>Setting</a></li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li><a <?=menuIsActive('tlaba_rugi')?> href="{base_url}tlaba_rugi"><i class="glyphicon glyphicon-option-horizontal"></i>Detail Laporan</a></li>
                        <?php endif ?>


                    </ul>
                </li>
				<li <?=(in_array($menu, $menuAkuntansiNeraca)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Neraca</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li><a <?=menuIsActive('tneraca_setting')?> href="{base_url}tneraca_setting"><i class="glyphicon glyphicon-option-horizontal"></i>Setting</a></li>
                        <?php endif ?>
                        <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                        <li><a <?=menuIsActive('tneraca')?> href="{base_url}tneraca"><i class="glyphicon glyphicon-option-horizontal"></i>Detail Neraca</a></li>
                        <?php endif ?>


                    </ul>
                </li>
            </ul>
        </li>
        <?php } ?>
        
        <?php if (UserAccesForm($user_acces_form, ['1340'])) { ?>
            <li <?=(in_array($menu, $menuVerifikator)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-check"></i><span class="sidebar-mini-hide">Verifikator</span></a>
                <ul>

                    <?php if (UserAccesForm($user_acces_form, ['1340'])): ?>
                    <li>
                        <a <?=menuIsActive3('poliklinik')?> href="{base_url}tverifikasi_transaksi/index/poliklinik"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi Rawat Jalan</a>
                    </li>
                    <?php endif ?>

                    <?php if (UserAccesForm($user_acces_form, ['1340'])): ?>
                    <li>
                        <a <?=menuIsActive3('rawatinap')?> href="{base_url}tverifikasi_transaksi/index/rawatinap"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi Rawat Inap</a>
                    </li>
                    <?php endif ?>

                    <?php if (UserAccesForm($user_acces_form, ['1340'])): ?>
                    <li>
                        <a <?=menuIsActive2('index_deposit')?> href="{base_url}tverifikasi_transaksi/index_deposit"><i class="glyphicon glyphicon-option-horizontal"></i>Deposit Perawatan</a>
                    </li>
                    <?php endif ?>

                    <?php if (UserAccesForm($user_acces_form, ['1340'])): ?>
                    <li>
                        <a <?=menuIsActive2('index_refund')?> href="{base_url}tverifikasi_transaksi/index_refund"><i class="glyphicon glyphicon-option-horizontal"></i>Refund Tunai</a>
                    </li>
                    <?php endif ?>

                    <?php if (UserAccesForm($user_acces_form, ['1340'])): ?>
                    <li>
                        <a <?=menuIsActive('tverifikasi_deposit')?> href="{base_url}tverifikasi_deposit/index"><i class="glyphicon glyphicon-option-horizontal"></i>Pembatalan Deposit</a>
                    </li>
                    <?php endif ?>

                    <li>
                        <a <?=menuIsActive('ttransport_dokter')?> href="{base_url}ttransport_dokter"><i class="glyphicon glyphicon-option-horizontal"></i>Transport Dokter</a>
                    </li>

                    <?php if (UserAccesForm($user_acces_form, ['1347'])): ?>
                    <li>
                        <a <?=menuIsActive('tkwitansi_manual')?> href="{base_url}tkwitansi_manual"><i class="glyphicon glyphicon-option-horizontal"></i>Kwitansi Manual</a>
                    </li>
                    <?php endif ?>
                </ul>
            </li>
            <li <?=(in_array($menu, $menuAkuntansiHutangPiutang)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-link"></i>Hutang & Piutang</span></a>
                <ul>

                    <li <?=(in_array($menu, $menuAkuntansiHutangPiutangMaster)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Master</span></a>
                        <ul>
                            <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                            <li><a <?=menuIsActive('mdata_hutang')?> href="{base_url}mdata_hutang"><i class="glyphicon glyphicon-option-horizontal"></i>Hutang</a></li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                            <li><a <?=menuIsActive('mdata_piutang')?> href="{base_url}mdata_piutang"><i class="glyphicon glyphicon-option-horizontal"></i>Piutang</a></li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                            <li><a <?=menuIsActive('mdata_pengelolaan')?> href="{base_url}mdata_pengelolaan"><i class="glyphicon glyphicon-option-horizontal"></i>Pengelolaan</a></li>
                            <?php endif ?>

                        </ul>
                    </li>
                    <li <?=(in_array($menu, $menuAkuntansiHutangPiutangTrx)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi</span></a>
                        <ul>
                            <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                            <li><a <?=menuIsActive('tpengelolaan')?> href="{base_url}tpengelolaan"><i class="glyphicon glyphicon-option-horizontal"></i>Pengelolaan</a></li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['252'])): ?>
                            <li><a <?=menuIsActive('tpengelolaan_approval')?> href="{base_url}tpengelolaan_approval"><i class="glyphicon glyphicon-option-horizontal"></i>Approval</a></li>
                            <?php endif ?>


                        </ul>
                    </li>


                </ul>
            </li>
            <li>
                <a <?=menuIsActive('tpencairan')?> href="{base_url}tpencairan"><i class="si si-wallet"></i><span class="sidebar-mini-hide">Pencairan</a>
            </li>

            <?
                $index_honor_setting=array('1436','1435','1437','1438');
                $index_honor_trx=array('1439','1440','1443','1441','1442');
                $index_menu_hd=array_merge($index_honor_setting,$index_honor_trx);
            ?>

            <?php if (UserAccesForm($user_acces_form, $index_menu_hd)) { ?>
            <li <?=(in_array($menu, $menuHonor)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-calendar"></i><span class="sidebar-mini-hide">Honor Dokter</span></a>
                <ul>
                    <?php if (UserAccesForm($user_acces_form, $index_honor_setting)){ ?>
                    <li <?=(in_array($menu, $menuHonorData)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Setting & Master</span></a>
                        <ul>
                            <?php if (UserAccesForm($user_acces_form, ['1436'])): ?>
                            <li>
                                <a <?=menuIsActive('msetting_feerujukan_dokter')?> href="{base_url}msetting_feerujukan_dokter"><i class="glyphicon glyphicon-option-horizontal"></i>Fee Rujukan Dokter</a>
                            </li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['1435'])): ?>
                            <li>
                                <a <?=menuIsActive('mpotongan_dokter')?> href="{base_url}mpotongan_dokter"><i class="glyphicon glyphicon-option-horizontal"></i>Potongan Dokter</a>
                            </li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['1437'])): ?>
                            <li>
                                <a <?=menuIsActive('msetting_honor_dokter')?> href="{base_url}msetting_honor_dokter"><i class="glyphicon glyphicon-option-horizontal"></i>Honor Dokter</a>
                            </li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['1438'])): ?>
                            <li>
                                <a <?=menuIsActive('mlogic_honor')?> href="{base_url}mlogic_honor"><i class="glyphicon glyphicon-option-horizontal"></i>Approval Honor Dokter</a>
                            </li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['1437'])) { ?>
                            <li>
                                <a <?php echo menuIsActive('msetting_honor_dokter_email'); ?> href="{base_url}msetting_honor_dokter_email"><i class="glyphicon glyphicon-option-horizontal"></i>General Email</a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?}?>

                    <?php if (UserAccesForm($user_acces_form, $index_honor_trx)){ ?>
                    <li <?=(in_array($menu, $menuHonorTransaksi)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi</span></a>
                        <ul>
                            <?php if (UserAccesForm($user_acces_form, ['1439'])): ?>
                            <li>
                                <a <?=menuIsActive('tpendapatan_nonpelayanan')?> href="{base_url}tpendapatan_nonpelayanan"><i class="glyphicon glyphicon-option-horizontal"></i>Non Pelayanan</a>
                            </li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['1440'])): ?>
                            <li>
                                <a <?=menuIsActive('tpotongan_dokter')?> href="{base_url}tpotongan_dokter/index"><i class="glyphicon glyphicon-option-horizontal"></i>Potongan Dokter</a>
                            </li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['1443'])): ?>
                            <li>
                                <a <?=menuIsActive('thonor_dokter')?> href="{base_url}thonor_dokter"><i class="glyphicon glyphicon-option-horizontal"></i>Honor Dokter</a>
                            </li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['1441'])): ?>
                            <li>
                                <a <?=menuIsActive('thonor_approval')?> href="{base_url}thonor_approval"><i class="glyphicon glyphicon-option-horizontal"></i>HD Approval</a>
                            </li>
                            <?php endif ?>
                            <?php if (UserAccesForm($user_acces_form, ['1442'])): ?>
                            <li>
                                <a <?=menuIsActive('thonor_bayar')?> href="{base_url}thonor_bayar"><i class="glyphicon glyphicon-option-horizontal"></i>HD Pembayaran</a>
                            </li>
                            <?php endif ?>
                        </ul>
                    </li>
                    <?}?>
                </ul>
            </li>
            <? } ?>

            <li <?=(in_array($menu, $menuKasbon)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-briefcase"></i><span class="sidebar-mini-hide">Kasbon</span></a>
                <ul>
                    <li <?=(in_array($menu, $menuKasbonSetting)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Setting</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('mlogic_kasbon')?> href="{base_url}mlogic_kasbon"><i class="glyphicon glyphicon-option-horizontal"></i>Logic Kasbon</a>
                            </li>
                        </ul>
                    </li>
                    <li <?=(in_array($menu, $menuKasbonTransaksi)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi</span></a>
                        <ul>
                            <?php if (UserAccesForm($user_acces_form, ['1359'])): ?>
                            <li>
                                <a <?=menuIsActive('tkasbon')?> href="{base_url}tkasbon/index"><i class="glyphicon glyphicon-option-horizontal"></i>Create Kasbon</a>
                            </li>
                            <?php endif ?>
                            <li>
                                <a <?=menuIsActive('tkasbon_kas')?> href="{base_url}tkasbon_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Pembayaran Kasbon </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li <?=(in_array($menu, $menuPayroll)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-credit-card"></i><span class="sidebar-mini-hide">Payroll</span></a>
                <ul>
                    <li <?=(in_array($menu, $menuPayrollData)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Master Data</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('mjenis_gaji')?> href="{base_url}mjenis_gaji"><i class="glyphicon glyphicon-option-horizontal"></i>Jenis Gaji</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('mvariable')?> href="{base_url}mvariable"><i class="glyphicon glyphicon-option-horizontal"></i>Variable</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('mrekapan')?> href="{base_url}mrekapan"><i class="glyphicon glyphicon-option-horizontal"></i>Variable Rekap</a>
                            </li>
                        </ul>
                    </li>
                    <li <?=(in_array($menu, $menuPayrollTransaksi)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('tpenggajian')?> href="{base_url}tpenggajian"><i class="glyphicon glyphicon-option-horizontal"></i>Trx Gaji</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('trekap')?> href="{base_url}trekap"><i class="glyphicon glyphicon-option-horizontal"></i>Rekap Gaji</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li <?=(in_array($menu, $menuPendapatanLainLain)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-docs"></i><span class="sidebar-mini-hide">Pendapatan Lain Lain</span></a>
                <ul>
                    <li <?=(in_array($menu, $menuPendapatanLainLainData)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Master Data</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('mpendapatan')?> href="{base_url}mpendapatan"><i class="glyphicon glyphicon-option-horizontal"></i>Jenis Pendapatan</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('mdari')?> href="{base_url}mdari"><i class="glyphicon glyphicon-option-horizontal"></i>Asal Pendapatan</a>
                            </li>
                        </ul>
                    </li>
                    <li <?=(in_array($menu, $menuPendapatanLainLainTransaksi)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('tpendapatan')?> href="{base_url}tpendapatan"><i class="glyphicon glyphicon-option-horizontal"></i>Pendapatan Lain Lain</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li <?=(in_array($menu, $menuBagiHasil)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-share-alt"></i><span class="sidebar-mini-hide">Bagi Hasil</span></a>
                <ul>

                    <li <?=(in_array($menu, $menuBagiHasilData)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Master Data</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('mpemilik_saham')?> href="{base_url}mpemilik_saham"><i class="glyphicon glyphicon-option-horizontal"></i>Pemilik Saham</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('mbiaya_operasional')?> href="{base_url}mbiaya_operasional"><i class="glyphicon glyphicon-option-horizontal"></i>Biaya Operasional</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('mbagi_hasil_setting')?> href="{base_url}mbagi_hasil_setting"><i class="glyphicon glyphicon-option-horizontal"></i>Setting Approval</a>
                            </li>

                        </ul>
                    </li>

                    <li <?=(in_array($menu, $menuBagiHasilTransaksi)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('mbagi_hasil')?> href="{base_url}mbagi_hasil"><i class="glyphicon glyphicon-option-horizontal"></i>Create</a>
                            </li>

                            <li>
                                <a <?=menuIsActive('tbagi_hasil')?> href="{base_url}tbagi_hasil"><i class="glyphicon glyphicon-option-horizontal"></i>Trx Bagi Hasil</a>
                            </li>

                            <li>
                                <a <?=menuIsActive('tbagi_hasil_bayar')?> href="{base_url}tbagi_hasil_bayar"><i class="glyphicon glyphicon-option-horizontal"></i>Trx Pembayaran</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('tbagi_hasil_approval')?> href="{base_url}tbagi_hasil_approval"><i class="glyphicon glyphicon-option-horizontal"></i>Approval</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li <?=(in_array($menu, $menuKontrabon)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-credit-card"></i><span class="sidebar-mini-hide">Kontrabon</span></a>
                <ul>
                    <li>
                        <a <?=menuIsActive('mkontrabon')?> href="{base_url}mkontrabon"><i class="glyphicon glyphicon-option-horizontal"></i>Pengaturan</a>
                    </li>
                    <li>
                        <a <?=menuIsActive('tkontrabon_verifikasi')?> href="{base_url}tkontrabon_verifikasi"><i class="glyphicon glyphicon-option-horizontal"></i>Kontrabon Verifikasi</a>
                    </li>
                    <li>
                        <a <?=menuIsActive('tkontrabon')?> href="{base_url}tkontrabon"><i class="glyphicon glyphicon-option-horizontal"></i>Kontrabon</a>
                    </li>
                    <li>
                        <a <?=menuIsActive('tkontrabon_his')?> href="{base_url}tkontrabon_his"><i class="glyphicon glyphicon-option-horizontal"></i>Kontrabon History</a>
                    </li>
                </ul>
            </li>

            <li <?=(in_array($menu, $menuPiutang)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-product-hunt"></i><span class="sidebar-mini-hide">Piutang Tidak Tertagih</span></a>
                <ul>
                    <li>
                        <a <?=menuIsActive('msetting_piutang')?> href="{base_url}msetting_piutang"><i class="glyphicon glyphicon-option-horizontal"></i>Pengaturan</a>
                    </li>
                    <li>
                        <a <?=menuIsActive('tpiutang_tt_verifikasi')?> href="{base_url}tpiutang_tt_verifikasi"><i class="glyphicon glyphicon-option-horizontal"></i>Verifikasi</a>
                    </li>
                    <li>
                        <a <?=menuIsActive('tpiutang_tt_monitoring')?> href="{base_url}tpiutang_tt_monitoring"><i class="glyphicon glyphicon-option-horizontal"></i>Monitoring</a>
                    </li>
                    <li>
                        <a <?=menuIsActive('tpiutang_tt_pembayaran')?> href="{base_url}tpiutang_tt_pembayaran"><i class="glyphicon glyphicon-option-horizontal"></i>Pembayaran</a>
                    </li>
                    <li>
                        <a <?=menuIsActive('tpiutang_tt_reminder')?> href="{base_url}tpiutang_tt_reminder"><i class="glyphicon glyphicon-option-horizontal"></i>Penghapusan & Reminder</a>
                    </li>
                    <li>
                        <a <?=menuIsActive('tpiutang_tt_umur')?> href="{base_url}tpiutang_tt_umur"><i class="glyphicon glyphicon-option-horizontal"></i>Umur Piutang</a>
                    </li>
                </ul>
            </li>

            <li <?=(in_array($menu, $menuPenagihan)) ? 'class="open"' : ''; ?>>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-calculator"></i><span class="sidebar-mini-hide">Penagihan</span></a>
                <ul>

                    <li <?=(in_array($menu, $menuPenagihanProses)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Proses Klaim</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('tklaim_verifikasi')?> href="{base_url}tklaim_verifikasi"><i class="glyphicon glyphicon-option-horizontal"></i>Verifikasi Piutang</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('tklaim_rincian')?> href="{base_url}tklaim_rincian"><i class="glyphicon glyphicon-option-horizontal"></i>Rincian Penagihan</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('tklaim_monitoring')?> href="{base_url}tklaim_monitoring"><i class="glyphicon glyphicon-option-horizontal"></i>Monitoring Tagihan</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('tklaim_umur')?> href="{base_url}tklaim_umur"><i class="glyphicon glyphicon-option-horizontal"></i>Umur Piutang</a>
                            </li>
                        </ul>
                    </li>

                    <li <?=(in_array($menu, $menuPenagihanPiutang)) ? 'class="open"' : ''; ?>>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Piutang Karyawan</span></a>
                        <ul>
                            <li>
                                <a <?=menuIsActive('mpiutang')?> href="{base_url}mpiutang"><i class="glyphicon glyphicon-option-horizontal"></i>Pengaturan</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('tpiutang_verifikasi')?> href="{base_url}tpiutang_verifikasi"><i class="glyphicon glyphicon-option-horizontal"></i>Verifikasi</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('tpiutang_rincian')?> href="{base_url}tpiutang_rincian"><i class="glyphicon glyphicon-option-horizontal"></i>Rincian</a>
                            </li>
                            <li>
                                <a <?=menuIsActive('tpiutang_history')?> href="{base_url}tpiutang_history"><i class="glyphicon glyphicon-option-horizontal"></i>History</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        <? } ?>

        <?php if (UserAccesForm($user_acces_form, ['1376','1384','1391','1388','1395','1403','1404','1410','1430','1417','1422','1423','1424','1419','1420','1421'])) { ?>
        <li <?=(in_array($menu, $menuPengajuan)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-basket-loaded"></i><span class="sidebar-mini-hide">Pengajuan</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['1417','1422','1423','1424','1419','1420','1421'])) { ?>
                <li <?=(in_array($menu, $menuPengajuanData)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Master Data</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1417'])) { ?>
                        <li><a <?=menuIsActive('mprespektif')?> href="{base_url}mprespektif"><i class="glyphicon glyphicon-option-horizontal"></i>Presprektif</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1422'])) { ?>
                        <li><a <?=menuIsActive('mprogram')?> href="{base_url}mprogram"><i class="glyphicon glyphicon-option-horizontal"></i>Program</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1423'])) { ?>
                        <li><a <?=menuIsActive('mklasifikasi')?> href="{base_url}mklasifikasi"><i class="glyphicon glyphicon-option-horizontal"></i>Klasifikasi</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1424'])) { ?>
                        <li><a <?=menuIsActive('mkegiatan')?> href="{base_url}mkegiatan"><i class="glyphicon glyphicon-option-horizontal"></i>Kegiatan</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1419'])) { ?>
                        <li><a <?=menuIsActive('mjenis_pengajuan')?> href="{base_url}mjenis_pengajuan"><i class="glyphicon glyphicon-option-horizontal"></i>Jenis Pengajuan</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1420'])) { ?>
                        <li><a <?=menuIsActive('mlogic')?> href="{base_url}mlogic"><i class="glyphicon glyphicon-option-horizontal"></i>Logic Pengajuan</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1421'])) { ?>
                        <li><a <?=menuIsActive('mvendor')?> href="{base_url}mvendor"><i class="glyphicon glyphicon-option-horizontal"></i>Vendor</a></li>
                        <?}?>
                    </ul>
                </li>
                <?}?>
                <?php if (UserAccesForm($user_acces_form, ['1376','1384','1391','1388','1395','1403','1404','1410'])) { ?>
                <li <?=(in_array($menu, $menuPengajuanTransaksi)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1376'])) { ?>
                        <li><a <?=menuIsActive('mrka')?> href="{base_url}mrka"><i class="glyphicon glyphicon-option-horizontal"></i>Create RKA</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1384'])) { ?>
                        <li><a <?=menuIsActive('mrka_kegiatan')?> href="{base_url}mrka_kegiatan"><i class="glyphicon glyphicon-option-horizontal"></i>Input Kegiatan</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1391'])) { ?>
                        <li><a <?=menuIsActive('mrka_kegiatan_setting')?> href="{base_url}mrka_kegiatan_setting"><i class="glyphicon glyphicon-option-horizontal"></i>Setting RKA TRX</a></li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1388'])) { ?>
                        <li><a <?=menuIsActive('mrka_reminder')?> href="{base_url}mrka_reminder"><i class="glyphicon glyphicon-option-horizontal"></i>Reminder RKA</a> </li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1395'])) { ?>
                        <li><a <?=menuIsActive('mrka_pengajuan')?> href="{base_url}mrka_pengajuan"><i class="glyphicon glyphicon-option-horizontal"></i>Pengajuan</a> </li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1403'])) { ?>
                        <li><a <?=menuIsActive('mrka_approval')?> href="{base_url}mrka_approval"><i class="glyphicon glyphicon-option-horizontal"></i>Approval</a> </li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1403'])) { ?>
                        <li><a <?=menuIsActive('mrka_approval_lanjutan')?> href="{base_url}mrka_approval_lanjutan"><i class="glyphicon glyphicon-option-horizontal"></i>Approval Lanjutan</a> </li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1404'])) { ?>
                        <li><a <?=menuIsActive('mrka_bendahara')?> href="{base_url}mrka_bendahara"><i class="glyphicon glyphicon-option-horizontal"></i>Bendahara</a> </li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1410'])) { ?>
                        <li><a <?=menuIsActive('mrka_belanja')?> href="{base_url}mrka_belanja"><i class="glyphicon glyphicon-option-horizontal"></i>Belanja</a> </li>
                        <?}?>
                        <?php if (UserAccesForm($user_acces_form, ['1430'])) { ?>
                        <li><a <?=menuIsActive('mrka_pengajuan_monitoring')?> href="{base_url}mrka_pengajuan_monitoring"><i class="glyphicon glyphicon-option-horizontal"></i>Monitoring</a> </li>
                        <?}?>
                    </ul>
                </li>
                <?}?>
            </ul>
        </li>
        <? } ?>

        <?php if (UserAccesForm($user_acces_form, ['1340','1429'])) { ?>
        <li <?=(in_array($menu, $menuMasterKas)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-handbag"></i><span class="sidebar-mini-hide">Master Kas</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['1340'])){ ?>
                <li <?=(in_array($menu, $menuMasterKasData)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Master Data</span></a>
                    <ul>
                        <li>
                            <a <?=menuIsActive('mjenis_kas')?> href="{base_url}mjenis_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Jenis Kas</a>
                        </li>
                        <li>
                            <a <?=menuIsActive('msumber_kas')?> href="{base_url}msumber_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Sumber Kas</a>
                        </li>
                        <li>
                            <a <?=menuIsActive('medc')?> href="{base_url}medc"><i class="glyphicon glyphicon-option-horizontal"></i>EDC</a>
                        </li>
                        <li>
                            <a <?=menuIsActive('mlogic_refund')?> href="{base_url}mlogic_refund"><i class="glyphicon glyphicon-option-horizontal"></i>Logic Refund</a>
                        </li>
                        <li>
                            <a <?=menuIsActive('mlogic_mutasi')?> href="{base_url}mlogic_mutasi"><i class="glyphicon glyphicon-option-horizontal"></i>Logic Mutasi</a>
                        </li>
                    </ul>
                </li>
                <?}?>
                <li <?=(in_array($menu, $menuMasterKasTransaksi)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i>Transaksi</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1340'])){ ?>
                        <a <?=menuIsActive('tsetoran_kas')?> href="{base_url}tsetoran_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Setoran Kas</a>
						
							<li>
								<a <?=menuIsActive('tmonitoring')?> href="{base_url}tmonitoring"><i class="glyphicon glyphicon-option-horizontal"></i>Monitoring</a>
							</li>
							<?php if (UserAccesForm($user_acces_form, ['1431'])){ ?>
							<li>
								<a <?=menuIsActive('tmutasi_kas')?> href="{base_url}tmutasi_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Mutasi Kas </a>
							</li>
							<?}?>
							<?php if (UserAccesForm($user_acces_form, ['1432'])){ ?>
							<li>
								<a <?=menuIsActive('tmutasi_kas_approval')?> href="{base_url}tmutasi_kas_approval"><i class="glyphicon glyphicon-option-horizontal"></i>Mutasi Kas Approval</a>
							</li>
							<?}?>
							<li>
								<a <?=menuIsActive('tpenyesuaian_kas')?> href="{base_url}tpenyesuaian_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Penyesuaian</a>
							</li>
							<?}?>
							<?php if (UserAccesForm($user_acces_form, ['1429'])){ ?>
							<li>
								<a <?=menuIsActive('trefund_kas')?> href="{base_url}trefund_kas"><i class="glyphicon glyphicon-option-horizontal"></i>Refund</a>
							</li>
							<?}?>
							<?php if (UserAccesForm($user_acces_form, ['1340'])){ ?>
							<li>
								<a <?=menuIsActive('tklaim_validasi')?> href="{base_url}tklaim_validasi"><i class="glyphicon glyphicon-option-horizontal"></i>Validasi Asuransi</a>
							</li>
						<?}?>
					</ul>
				</li>
			</ul>
		</li>
			<? } ?>

	<?php if (UserAccesForm($user_acces_form, ['1340'])) { ?>
		<li <?=(in_array($menu, $menuReturGudang)) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-home"></i><span class="sidebar-mini-hide">Retur Gudang</span></a>
			<ul>
				<li>
					<a <?=menuIsActive('tretur_bayar')?> href="{base_url}tretur_bayar"><i class="glyphicon glyphicon-option-horizontal"></i>Pembayaran Retur</a>
				</li>
				<li>
					<a <?=menuIsActive('tretur_terima')?> href="{base_url}tretur_terima"><i class="glyphicon glyphicon-option-horizontal"></i>Penerimaan Retur</a>
				</li>
				
			</ul>
		</li>

		<li <?=(in_array($menu, $menuReferalManagement)) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-directions"></i><span class="sidebar-mini-hide">Referal Management</span></a>
			<ul>
				<li>
					<a <?=menuIsActive('msetting_approval_feerujukan')?> href="{base_url}msetting_approval_feerujukan"><i class="glyphicon glyphicon-option-horizontal"></i>Setting Approval</a>
				</li>
				<li>
					<a <?=menuIsActive('mpengaturan_jatuh_tempo')?> href="{base_url}mpengaturan_jatuh_tempo"><i class="glyphicon glyphicon-option-horizontal"></i>Pengaturan Jatuh Tempo</a>
				</li>
				<li>
					<a <?=menuIsActive('trujukan_rumahsakit_verifikasi')?> href="{base_url}trujukan_rumahsakit_verifikasi"><i class="glyphicon glyphicon-option-horizontal"></i>Verifikasi Data</a>
				</li>
				<li>
					<a <?=menuIsActive('trujukan_rumahsakit_rincian')?> href="{base_url}trujukan_rumahsakit_rincian"><i class="glyphicon glyphicon-option-horizontal"></i>Rincian Fee Rujukan</a>
				</li>
				<li>
					<a <?=menuIsActive('trujukan_rumahsakit_approval')?> href="{base_url}trujukan_rumahsakit_approval"><i class="glyphicon glyphicon-option-horizontal"></i>Approval Fee Rujukan</a>
				</li>
				<li>
					<a <?=menuIsActive('trujukan_rumahsakit_pembayaran')?> href="{base_url}trujukan_rumahsakit_pembayaran"><i class="glyphicon glyphicon-option-horizontal"></i>Pembayaran Fee Rujukan</a>
				</li>
			</ul>
		</li>
	<? } ?>

</ul>
</li>