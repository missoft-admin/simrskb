<?php
  $menuSettingUser = [
  	'mresources', 'mrole', 'musers'
  ];

  $menuSetting = array_merge(
  	$menuSettingUser
  );
?>

<li <?=(in_array($menu, $menuSetting)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span class="sidebar-mini-hide">Settings</span></a>
    <ul>
        <li <?=(in_array($menu, $menuSettingUser)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">User Management</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['215'])) { ?>
                <li>
                    <a <?=menuIsActive('mresources')?> href="{base_url}mresources"><i class="glyphicon glyphicon-option-horizontal"></i>Resource</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['244'])) { ?>
                <li>
                    <a <?=menuIsActive('mrole')?> href="{base_url}mrole"><i class="glyphicon glyphicon-option-horizontal"></i>Role</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['248'])) { ?>
                <li>
                    <a <?=menuIsActive('musers')?> href="{base_url}musers"><i class="glyphicon glyphicon-option-horizontal"></i>Manage User</a>
                </li>
                <?php } ?>
            </ul>
        </li>
    </ul>
</li>
