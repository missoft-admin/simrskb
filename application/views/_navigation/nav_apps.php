<?php
	$menuMasterPPA = [
		'mppa', 'mppa_poli'
	];

	$Menu_App_trx = [''];
	$Menu_App_Pengaturan_Slot2 = ['reservasi','reservasi_poli_jadwal','reservasi_jadwal_harian','reservasi_jadwal_harian_detail'];
	$Menu_App_Pengaturan_Slot = ['tujuan'];
	$Menu_App_Pengaturan_Slot= array_merge($Menu_App_Pengaturan_Slot,$Menu_App_Pengaturan_Slot2);
	$Menu_App_Pengaturan = ['app_setting','apm_setting'];
	$menuRefTTV=['msuhu','mnadi','mgds','mnafas','mtd','mberat','mgcs_eye','mgcs_voice','mgcs_motion','mpupil','mcahaya','mspo2','makral'];
	$menuRefAss=['mrisiko_jatuh','msurvey_kepuasan','mpasca_anestesi','mpews','mrisiko_jatuh_ranap','mskrining_nutrisi','medukasi','mtemplate','mtemplate_gizi','mdiagnosa','mdiagnosa_ranap','mdiagnosa_gizi'
	,'manatomi_template','manatomi_template_igd','manatomi_template_fisio','manatomi_template_o','manatomi_template_ri','manatomi'
	,'mjenis_info_isolasi','mjenis_info_dnr','mjenis_info_icu','mjenis_info','mtemplate_tindakan_dnr','mtemplate_tindakan_isolasi','mtemplate_tindakan_icu','mtemplate_tindakan_ic','minvasif','mtemplate_invasif','mtemplate_tindakan_darah','mtemplate_tindakan_risiko','mtemplate_tindakan_anestesi','mjenis_info_tindakan','mjenis_info_darah','mjenis_info_risiko','mjenis_persetujuan_dnr','mjenis_persetujuan_isolasi','mjenis_persetujuan_invasif','mjenis_persetujuan_icu','mjenis_persetujuan_ic','mjenis_persetujuan_darah','mjenis_persetujuan_risiko','mjenis_persetujuan_tindakan','mnyeri','mnyeri_ri','msyaraf','muji_otot','mtriage_pemeriksaan','mtriage'];
	$Menu_App_masterERM_Surat=['mheader_surat','msurat_template'];
	$Menu_App_masterERM = ['merm_general','merm_general_ranap','merm_general_ranap_hak','merm_skrining_pasien','merm_skrining_covid','merm_referensi','setting_ttv'
	,'setting_ttv_ri','setting_assesmen','setting_asmed','setting_asmed_igd','setting_assesmen_igd'
	,'setting_nyeri','setting_nyeri_ri','setting_triage','setting_fisio','setting_fisio_ri','minterval','mshift','erm_jenis_operasi','mpoliklinik_direct','mpoliklinik_emergency','mtelaah','mpoliklinik_konsul','munitpelayanan_tujuan'
	,'setting_cppt','setting_dnr','setting_isolasi','setting_icu','setting_ic','setting_tindakan_anestesi','setting_invasif','setting_invasif','setting_tf_darah','setting_risiko_tinggi','setting_sga','setting_gizi_anak','setting_mpp','setting_keselamatan','setting_assesmen_askep','setting_assesmen_pulang','setting_pra_sedasi','setting_ringkasan_pulang','setting_survey_kepuasan','setting_pra_bedah','setting_ews'
	,'setting_pews','setting_observasi','setting_info_ods','setting_aps','setting_rohani','setting_lap_bedah','setting_evaluasi_mpp','setting_imp_mpp','setting_gizi_lanjutan','setting_discarge','setting_rekon_obat','setting_timbang','setting_intra_hospital','setting_tf_hospital','setting_implementasi_kep','setting_lokasi_operasi'
	,'setting_permintaan_dpjp','setting_pindah_dpjp','setting_edukasi'];
	
	$menuSettingAsses=['setting_ttv','setting_ttv_ri','setting_assesmen','setting_asmed','setting_assesmen_igd','setting_assesmen_ri','setting_asses_ri','setting_lokasi_operasi'
	,'setting_risiko_jatuh','setting_dpjp','setting_asmed_igd','setting_nyeri','setting_nyeri_ri','setting_triage','setting_fisio','setting_dnr','setting_fisio_ri','setting_isolasi','setting_cppt','setting_icu','setting_ic','setting_invasif','setting_tf_darah','setting_risiko_tinggi'
	,'setting_tindakan_anestesi','setting_sga','setting_gizi_anak','setting_mpp','setting_keselamatan','setting_assesmen_askep','setting_assesmen_pulang','setting_pra_sedasi','setting_ringkasan_pulang','setting_pra_bedah','setting_ews'
	,'setting_pews','setting_aps','setting_opinion','setting_privasi','setting_rohani','setting_info_ods','setting_observasi','setting_survey_kepuasan','setting_lap_bedah','setting_evaluasi_mpp','setting_imp_mpp','setting_tf_hospital','setting_implementasi_kep','setting_intra_hospital','setting_rekon_obat'
	,'setting_timbang','setting_gizi_lanjutan','setting_discarge','setting_edukasi','setting_pindah_dpjp','setting_permintaan_dpjp'];
	$subERmDiskon=['mpengaturan_diskon_kelompok_pasien_laboratorium','mpengaturan_diskon_asuransi_laboratorium'];
	$subERmEResep=['setting_akses_barang','setting_margin_barang','setting_cover_barang','setting_eresep','setting_eresep_print','setting_eresep_copy','mtujuan_farmasi'];
	$subERmRM=['setting_rm'];
	$subERmTKO=['setting_tko'];
	$Menu_App_setting_Tindakan_Ranap = ['setting_deposit','setting_trx_ranap','ruangan_display','setting_rencana_ranap','msetting_tarif_jenis_operasi','setting_pelayanan_ri','setting_visite','setting_rencana_bedah','setting_rencana_biaya','setting_ranap_kunjungan'];
	$Menu_App_masterERM= array_merge($Menu_App_masterERM,$Menu_App_masterERM_Surat,$menuMasterPPA,$menuRefTTV,$menuRefAss,$menuSettingAsses,$subERmDiskon,$subERmEResep,$subERmRM,$subERmTKO,$Menu_App_setting_Tindakan_Ranap);
	$Menu_App_Pendaftaran = ['tpendaftaran_poli','tcheckin','antrian_caller','tpendaftaran_ranap','tpendaftaran_ranap_terima'];
	$Menu_App_Tindakan = ['tpendaftaran_poli_tindakan','tpendaftaran_poli_ttv','tpendaftaran_poli_pasien','tpendaftaran_poli_jasa','tpendaftaran_poli_perawat','tkonsul','tperubahan','tpoliklinik_trx','tpoliklinik_resep'];
	$Menu_App_Tindakan_IGD = ['tpendaftaran_poli_igd_ttv','tpendaftaran_poli_igd_perawat','tpendaftaran_poli_igd_jasa','tpendaftaran_poli_igd_pasien'];
	$Menu_App_Tindakan_Farmasi = ['tfarmasi_tindakan','tfarmasi_tindakan_ri'];
	$Menu_App_Tindakan_RM = ['tpoliklinik_rm_order','tpoliklinik_rm_rj','trawatinap_rm'];
	$Menu_App_Tindakan_Ranap = ['ttindakan_ranap','ttindakan_ranap_ods','ttindakan_ranap_jasa','ttindakan_ranap_jasa_ods','trekon_obat','tpindah_dpjp','tintra_hospital','ttindakan_ranap_pasien','ttindakan_ranap_admin'
	,'ttindakan_ranap_deposit','tdeposit_approval','tdeposit_batal','tmonitoring_bed','tpoliklinik_ranap','treservasi_bed','trekon_obat'];
	$Menu_App_Tindakan_Ranap= array_merge($Menu_App_Tindakan_Ranap);
	$Menu_App_Tindakan_KamarBedah = ['tkamar_bedah'];
	
	
	$Menu_App_setting = ['setting_reservasi_petugas','app_setting','setting_checkin_petugas','setting_daftar_petugas'];
	$Menu_App_Booking = ['tbooking','tbooking_rehab'];
	$Menu_App_Booking= array_merge($Menu_App_Booking,$Menu_App_setting);
	$menu2_informasi=array('add_info','update_info','info','update_event','add_event');
	$menu2_daftar=array('pendaftaran');
	$Menu_app= array_merge($Menu_App_Pengaturan);
?>

<?php $menu2 = $this->uri->segment(2); ?>
<? if (in_array('12', $user_acces_menu, false) == '1') {?>
<li <?=(in_array($menu, $Menu_App_masterERM)  && !in_array($menu2, $Menu_App_Pengaturan_Slot)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-list-alt"></i><span class="sidebar-mini-hide"> Master ERM</span></a>
    <ul>
		 <?php if (UserAccesForm($user_acces_form, ['1464'])) { ?>
			<li>
				<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi"><i class="glyphicon glyphicon-option-horizontal"></i> Data Referensi</a>
			</li>
			<?php } ?>
			
			<?php if (UserAccesForm($user_acces_form, ['2246'])) { ?>
				<li <?=(in_array($menu, $Menu_App_masterERM_Surat)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide">Setting Surat</span></a>
					<ul>
						<?php if (UserAccesForm($user_acces_form, ['2246'])) { ?>
						<li>
							<a <?=menuIsActive('mheader_surat')?> href="{base_url}mheader_surat"><i class="glyphicon glyphicon-option-horizontal"></i> Header Surat</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2247'])) { ?>
						<li>
							<a <?=menuIsActive('msurat_template')?> href="{base_url}msurat_template"><i class="glyphicon glyphicon-option-horizontal"></i> Template Surat</a>
						</li>
						<?php } ?>
						
						
					</ul>
				</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['1461','1924','1925', '1462','1463'])) { 
				$menuMasterSkrining=['merm_general','merm_skrining_pasien','merm_skrining_covid','merm_general_ranap','merm_general_ranap_hak']?>
				<li <?=(in_array($menu, $menuMasterSkrining)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide">Skrining</span></a>
					<ul>
						<?php if (UserAccesForm($user_acces_form, ['1461'])) { ?>
						<li>
							<a <?=menuIsActive('merm_general')?> href="{base_url}merm_general"><i class="glyphicon glyphicon-option-horizontal"></i> General Consent Rajal</a>
						</li>
						<?php } ?>
						
						
						<?php if (UserAccesForm($user_acces_form, ['1462'])) { ?>
						<li>
							<a <?=menuIsActive('merm_skrining_pasien')?> href="{base_url}merm_skrining_pasien"><i class="glyphicon glyphicon-option-horizontal"></i> Skrining Pasien Rajal</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['1463'])) { ?>
						<li>
							<a <?=menuIsActive('merm_skrining_covid')?> href="{base_url}merm_skrining_covid"><i class="glyphicon glyphicon-option-horizontal"></i> Skrining Covid Rajal</a>
						</li>
						<?php } ?>
					    <?php if (UserAccesForm($user_acces_form, ['1924'])) { ?>
						<li>
							<a <?=menuIsActive('merm_general_ranap')?> href="{base_url}merm_general_ranap"><i class="glyphicon glyphicon-option-horizontal"></i> General Consent Ranap</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1925'])) { ?>
						<li>
							<a <?=menuIsActive('merm_general_ranap_hak')?> href="{base_url}merm_general_ranap_hak"><i class="glyphicon glyphicon-option-horizontal"></i> Hak & Kewajiban Ranap</a>
						</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['1594', '1589'])) { ?>
				<li <?=(in_array($menu, $menuMasterPPA)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span
							class="sidebar-mini-hide">PPA</span></a>
					<ul>
						<?php if (UserAccesForm($user_acces_form, ['1589'])) { ?>
						<li>
							<a <?=menuIsActive('mppa')?> href="{base_url}mppa"><i
									class="glyphicon glyphicon-option-horizontal"></i>Data PPA</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1594'])) { ?>
						<li>
							<a <?=menuIsActive('mppa_poli')?> href="{base_url}mppa_poli"><i
									class="glyphicon glyphicon-option-horizontal"></i>PPA Poliklinik</a>
						</li>
						<?php } ?>
					   
					</ul>
				</li>
			<?php } ?>
			
			
			
			<?php if (UserAccesForm($user_acces_form, ['1652','1968', '1656', '1646', '1641', '1955', '1669','1679','1765','1777','1689','1960','1699','1704','1964'])) { ?>
				<li <?=(in_array($menu, $menuRefAss)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide">Referensi Assesmen</span></a>
					<ul>
						<?php if (UserAccesForm($user_acces_form, ['1652'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate')?> href="{base_url}mtemplate"><i class="glyphicon glyphicon-option-horizontal"></i> Rencana Asuhan</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['1656'])) { ?>
						<li>
							<a <?=menuIsActive('mdiagnosa')?> href="{base_url}mdiagnosa"><i class="glyphicon glyphicon-option-horizontal"></i> Diagnosa</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1646'])) { ?>
						<li>
							<a <?=menuIsActive('medukasi')?> href="{base_url}medukasi"><i class="glyphicon glyphicon-option-horizontal"></i> Edukasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1641'])) { ?>
						<li>
							<a <?=menuIsActive('mrisiko_jatuh')?> href="{base_url}mrisiko_jatuh"><i class="glyphicon glyphicon-option-horizontal"></i> Risiko Jatuh</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1976'])) { ?>
						<li>
							<a <?=menuIsActive('mrisiko_jatuh_ranap')?> href="{base_url}mrisiko_jatuh_ranap"><i class="glyphicon glyphicon-option-horizontal"></i> Risiko Jatuh Ranap</a>
						</li>
						<?php } ?>
						
						
						<?php if (UserAccesForm($user_acces_form, ['1669'])) { ?>
						<li>
							<a <?=menuIsActive('manatomi_template')?> href="{base_url}manatomi_template"><i class="glyphicon glyphicon-option-horizontal"></i> Template Anatomi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1669'])) { ?>
						<li>
							<a <?=menuIsActive('manatomi_template_igd')?> href="{base_url}manatomi_template_igd"><i class="glyphicon glyphicon-option-horizontal"></i> Template Anatomi IGD</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1869'])) { ?>
						<li>
							<a <?=menuIsActive('manatomi_template_fisio')?> href="{base_url}manatomi_template_fisio"><i class="glyphicon glyphicon-option-horizontal"></i> Template Anatomi Fisio</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1940'])) { ?>
						<li>
							<a <?=menuIsActive('manatomi_template_ri')?> href="{base_url}manatomi_template_ri"><i class="glyphicon glyphicon-option-horizontal"></i> Template Anatomi RI</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1993'])) { ?>
						<li>
							<a <?=menuIsActive('manatomi_template_o')?> href="{base_url}manatomi_template_o"><i class="glyphicon glyphicon-option-horizontal"></i> Template Anatomi Operasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1679'])) { ?>
						<li>
							<a <?=menuIsActive('manatomi')?> href="{base_url}manatomi"><i class="glyphicon glyphicon-option-horizontal"></i> Anatomi Tubuh</a>
						</li>
						<?php } ?>
					   <?php if (UserAccesForm($user_acces_form, ['1765'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_info')?> href="{base_url}mjenis_info"><i class="glyphicon glyphicon-option-horizontal"></i> Jenis Informasi Bedah</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2417'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_tindakan_ic')?> href="{base_url}mtemplate_tindakan_ic"><i class="glyphicon glyphicon-option-horizontal"></i> Template Tind. Bedah</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2067'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_info_tindakan')?> href="{base_url}mjenis_info_tindakan"><i class="glyphicon glyphicon-option-horizontal"></i> Jenis Informasi Tind.</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2413'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_tindakan_anestesi')?> href="{base_url}mtemplate_tindakan_anestesi"><i class="glyphicon glyphicon-option-horizontal"></i> Template Tind. Anestesi</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['2078'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_info_risiko')?> href="{base_url}mjenis_info_risiko"><i class="glyphicon glyphicon-option-horizontal"></i> Jenis Info Risiko</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['2421'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_tindakan_risiko')?> href="{base_url}mtemplate_tindakan_risiko"><i class="glyphicon glyphicon-option-horizontal"></i> Template Tind. Risiko</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['1777'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_persetujuan_ic')?> href="{base_url}mjenis_persetujuan_ic"><i class="glyphicon glyphicon-option-horizontal"></i> Jenis Persetujuan IC</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2072'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_persetujuan_tindakan')?> href="{base_url}mjenis_persetujuan_tindakan"><i class="glyphicon glyphicon-option-horizontal"></i> Persetujuan Tindakan</a>
						</li>
						<?php } ?>
						
					   <?php if (UserAccesForm($user_acces_form, ['2083'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_persetujuan_risiko')?> href="{base_url}mjenis_persetujuan_risiko"><i class="glyphicon glyphicon-option-horizontal"></i> Persetujuan Risiko Tinggi</a>
						</li>
						<?php } ?>
					   <?php if (UserAccesForm($user_acces_form, ['1689'])) { ?>
						<li>
							<a <?=menuIsActive('mnyeri')?> href="{base_url}mnyeri"><i class="glyphicon glyphicon-option-horizontal"></i> Nyeri</a>
						</li>
						<?php } ?>
						
						 <?php if (UserAccesForm($user_acces_form, ['2195'])) { ?>
						<li>
							<a <?=menuIsActive('mnyeri_ri')?> href="{base_url}mnyeri_ri"><i class="glyphicon glyphicon-option-horizontal"></i> Nyeri Ranap</a>
						</li>
						<?php } ?>
						
						
						<?php if (UserAccesForm($user_acces_form, ['1699'])) { ?>
						<li>
							<a <?=menuIsActive('mtriage_pemeriksaan')?> href="{base_url}mtriage_pemeriksaan"><i class="glyphicon glyphicon-option-horizontal"></i> Pemeriksaan Triage</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1704'])) { ?>
						<li>
							<a <?=menuIsActive('mtriage')?> href="{base_url}mtriage"><i class="glyphicon glyphicon-option-horizontal"></i> Triage</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1955'])) { ?>
						<li>
							<a <?=menuIsActive('mskrining_nutrisi')?> href="{base_url}mskrining_nutrisi"><i class="glyphicon glyphicon-option-horizontal"></i> Skrining Nutrisi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1960'])) { ?>
						<li>
							<a <?=menuIsActive('msyaraf')?> href="{base_url}msyaraf"><i class="glyphicon glyphicon-option-horizontal"></i> System Persyarafan</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1964'])) { ?>
						<li>
							<a <?=menuIsActive('muji_otot')?> href="{base_url}muji_otot"><i class="glyphicon glyphicon-option-horizontal"></i> Uji Kekuatan Otot</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1968'])) { ?>
						<li>
							<a <?=menuIsActive('mdiagnosa_ranap')?> href="{base_url}mdiagnosa_ranap"><i class="glyphicon glyphicon-option-horizontal"></i> Diagnosa Keperawatan</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2014'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_gizi')?> href="{base_url}mtemplate_gizi"><i class="glyphicon glyphicon-option-horizontal"></i> Rencana Asuhan Gizi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2010'])) { ?>
						<li>
							<a <?=menuIsActive('mdiagnosa_gizi')?> href="{base_url}mdiagnosa_gizi"><i class="glyphicon glyphicon-option-horizontal"></i> Diagnosa Gizi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2241'])) { ?>
						<li>
							<a <?=menuIsActive('mpasca_anestesi')?> href="{base_url}mpasca_anestesi"><i class="glyphicon glyphicon-option-horizontal"></i> Pemulihan Pasca Anestesi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2371'])) { ?>
						<li>
							<a <?=menuIsActive('msurvey_kepuasan')?> href="{base_url}msurvey_kepuasan"><i class="glyphicon glyphicon-option-horizontal"></i> Survey Kepuasan</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2384'])) { ?>
						<li>
							<a <?=menuIsActive('mpews')?> href="{base_url}mpews"><i class="glyphicon glyphicon-option-horizontal"></i> PEWS</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2425'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_info_darah')?> href="{base_url}mjenis_info_darah"><i class="glyphicon glyphicon-option-horizontal"></i> Info TF Darah</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2430'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_persetujuan_darah')?> href="{base_url}mjenis_persetujuan_darah"><i class="glyphicon glyphicon-option-horizontal"></i> Persetujuan TF Darah</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2435'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_tindakan_darah')?> href="{base_url}mtemplate_tindakan_darah"><i class="glyphicon glyphicon-option-horizontal"></i> Template Tind. Tf Darah</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2453'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_persetujuan_invasif')?> href="{base_url}mjenis_persetujuan_invasif"><i class="glyphicon glyphicon-option-horizontal"></i> Persetujuan Invasif</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2458'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_invasif')?> href="{base_url}mtemplate_invasif"><i class="glyphicon glyphicon-option-horizontal"></i> Template Invasif</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2462'])) { ?>
						<li>
							<a <?=menuIsActive('minvasif')?> href="{base_url}minvasif"><i class="glyphicon glyphicon-option-horizontal"></i> Master Invasif</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2475'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_info_icu')?> href="{base_url}mjenis_info_icu"><i class="glyphicon glyphicon-option-horizontal"></i> Master Jenis Info ICU</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['2480'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_tindakan_icu')?> href="{base_url}mtemplate_tindakan_icu"><i class="glyphicon glyphicon-option-horizontal"></i>  Template Tin. ICU</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2484'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_persetujuan_icu')?> href="{base_url}mjenis_persetujuan_icu"><i class="glyphicon glyphicon-option-horizontal"></i> Jenis Persetujuan ICU</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2498'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_info_isolasi')?> href="{base_url}mjenis_info_isolasi"><i class="glyphicon glyphicon-option-horizontal"></i> Master Jenis Info Isolasi</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['2503'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_tindakan_isolasi')?> href="{base_url}mtemplate_tindakan_isolasi"><i class="glyphicon glyphicon-option-horizontal"></i>  Template Tin. Isolasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2507'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_persetujuan_isolasi')?> href="{base_url}mjenis_persetujuan_isolasi"><i class="glyphicon glyphicon-option-horizontal"></i>  Jenis Persetujuan Isolasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2522'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_info_dnr')?> href="{base_url}mjenis_info_dnr"><i class="glyphicon glyphicon-option-horizontal"></i> Master Jenis Info DNR</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2527'])) { ?>
						<li>
							<a <?=menuIsActive('mtemplate_tindakan_dnr')?> href="{base_url}mtemplate_tindakan_dnr"><i class="glyphicon glyphicon-option-horizontal"></i>  Template Tin. DNR</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2531'])) { ?>
						<li>
							<a <?=menuIsActive('mjenis_persetujuan_dnr')?> href="{base_url}mjenis_persetujuan_dnr"><i class="glyphicon glyphicon-option-horizontal"></i>  Jenis Persetujuan DNR</a>
						</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
			
			<?php if (UserAccesForm($user_acces_form, ['1605', '1609', '1613', '1617', '1621','1743','1710','1714','1718','1722','1726','1730','1734'])) { ?>
				<li <?=(in_array($menu, $menuRefTTV)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide"> Referensi TTV</span></a>
					<ul>
						<?php if (UserAccesForm($user_acces_form, ['1605'])) { ?>
						<li>
							<a <?=menuIsActive('msuhu')?> href="{base_url}msuhu"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Suhu Tubuh</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1609'])) { ?>
						<li>
							<a <?=menuIsActive('mnadi')?> href="{base_url}mnadi"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Nadi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1743'])) { ?>
						<li>
							<a <?=menuIsActive('mgds')?> href="{base_url}mgds"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi GDS</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1613'])) { ?>
						<li>
							<a <?=menuIsActive('mnafas')?> href="{base_url}mnafas"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Nafas</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1617'])) { ?>
						<li>
							<a <?=menuIsActive('mtd')?> href="{base_url}mtd"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Tekanan Darah</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1621'])) { ?>
						<li>
							<a <?=menuIsActive('mberat')?> href="{base_url}mberat"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Berat Badan</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1710'])) { ?>
						<li>
							<a <?=menuIsActive('mgcs_eye')?> href="{base_url}mgcs_eye"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi MGCS Eye</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1714'])) { ?>
						<li>
							<a <?=menuIsActive('mgcs_voice')?> href="{base_url}mgcs_voice"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi MGCS Voice</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1718'])) { ?>
						<li>
							<a <?=menuIsActive('mgcs_motion')?> href="{base_url}mgcs_motion"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi MGCS Motion</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1722'])) { ?>
						<li>
							<a <?=menuIsActive('mpupil')?> href="{base_url}mpupil"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Pupil</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1726'])) { ?>
						<li>
							<a <?=menuIsActive('mcahaya')?> href="{base_url}mcahaya"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Reflex Cahaya</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1730'])) { ?>
						<li>
							<a <?=menuIsActive('mspo2')?> href="{base_url}mspo2"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi SPo2</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1734'])) { ?>
						<li>
							<a <?=menuIsActive('makral')?> href="{base_url}makral"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Akral</a>
						</li>
						<?php } ?>
					   
					</ul>
				</li>
			<?php } ?>
			<?
				
			?>
			<?php if (UserAccesForm($user_acces_form, ['1981','1600','1950','1636','1674','1747','1738','1684','1694','1696','1752','1754','1758','1760','1772','1774'])) { ?>
				<li <?=(in_array($menu, $menuSettingAsses)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide">Setting Assesmen</span></a>
					<ul>
						<?php if (UserAccesForm($user_acces_form, ['1600'])) { ?>
						<li>
							<a <?=menuIsActive('setting_ttv')?> href="{base_url}setting_ttv"><i class="glyphicon glyphicon-option-horizontal"></i> Setting TTV</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1950'])) { ?>
						<li>
							<a <?=menuIsActive('setting_ttv_ri')?> href="{base_url}setting_ttv_ri"><i class="glyphicon glyphicon-option-horizontal"></i> Setting TTV RI</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1636'])) { ?>
						<li>
							<a <?=menuIsActive('setting_assesmen')?> href="{base_url}setting_assesmen"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Assesmen RJ</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1674'])) { ?>
						<li>
							<a <?=menuIsActive('setting_asmed')?> href="{base_url}setting_asmed"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Asmed RJ</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1747'])) { ?>
						<li>
							<a <?=menuIsActive('setting_assesmen_igd')?> href="{base_url}setting_assesmen_igd"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Assesmen IGD</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['1738'])) { ?>
						<li>
							<a <?=menuIsActive('setting_asmed_igd')?> href="{base_url}setting_asmed_igd"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Asmed IGD</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['1684'])) { ?>
						<li>
							<a <?=menuIsActive('setting_nyeri')?> href="{base_url}setting_nyeri"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Pengk Nyeri</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2189'])) { ?>
						<li>
							<a <?=menuIsActive('setting_nyeri_ri')?> href="{base_url}setting_nyeri_ri"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Pengk Nyeri RI</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1694','1696'])) { ?>
						<li>
							<a <?=menuIsActive('setting_triage')?> href="{base_url}setting_triage"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Triage</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1752','1754'])) { ?>
						<li>
							<a <?=menuIsActive('setting_cppt')?> href="{base_url}setting_cppt"><i class="glyphicon glyphicon-option-horizontal"></i> Setting CPPT</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1758','1760'])) { ?>
						<li>
							<a <?=menuIsActive('setting_ic')?> href="{base_url}setting_ic"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Informed Consent</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['1772','1774'])) { ?>
						<li>
							<a <?=menuIsActive('setting_edukasi')?> href="{base_url}setting_edukasi"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Cat Edukasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1864','1866'])) { ?>
						<li>
							<a <?=menuIsActive('setting_fisio')?> href="{base_url}setting_fisio"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Fisioterapi RJ</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2200','2202'])) { ?>
						<li>
							<a <?=menuIsActive('setting_fisio_ri')?> href="{base_url}setting_fisio_ri"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Fisioterapi RI</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1945'])) { ?>
						<li>
							<a <?=menuIsActive('setting_assesmen_ri')?> href="{base_url}setting_assesmen_ri"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Asmed RI</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1972'])) { ?>
						<li>
							<a <?=menuIsActive('setting_asses_ri')?> href="{base_url}setting_asses_ri"><i class="glyphicon glyphicon-option-horizontal"></i> Setting AsKep RI</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1981'])) { ?>
						<li>
							<a <?=menuIsActive('setting_risiko_jatuh')?> href="{base_url}setting_risiko_jatuh"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Risiko Jatuh</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['1984','1985'])) { ?>
						<li>
							<a <?=menuIsActive('setting_dpjp')?> href="{base_url}setting_dpjp"><i class="glyphicon glyphicon-option-horizontal"></i> Setting DPJP</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1987','1988','1989'])) { ?>
						<li>
							<a <?=menuIsActive('setting_pindah_dpjp')?> href="{base_url}setting_pindah_dpjp"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Pindah DPJP</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2396','2397','2398'])) { ?>
						<li>
							<a <?=menuIsActive('setting_permintaan_dpjp')?> href="{base_url}setting_permintaan_dpjp"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Permintaan DPJP</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['1998','1999'])) { ?>
						<li>
							<a <?=menuIsActive('setting_lokasi_operasi')?> href="{base_url}setting_lokasi_operasi"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Lokasi Operasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2002','2003','2004','2005'])) { ?>
						<li>
							<a <?=menuIsActive('setting_sga')?> href="{base_url}setting_sga"><i class="glyphicon glyphicon-option-horizontal"></i> Setting SGA</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2006','2007','2008','2009'])) { ?>
						<li>
							<a <?=menuIsActive('setting_gizi_anak')?> href="{base_url}setting_gizi_anak"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Gizi Anak</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['2027','2029','2030'])) { ?>
						<li>
							<a <?=menuIsActive('setting_gizi_lanjutan')?> href="{base_url}setting_gizi_lanjutan"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Gizi Lanjutan</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2031','2032','2033'])) { ?>
						<li>
							<a <?=menuIsActive('setting_discarge')?> href="{base_url}setting_discarge"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Discharge Planing</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2034','2035','2036'])) { ?>
						<li>
							<a <?=menuIsActive('setting_rekon_obat')?> href="{base_url}setting_rekon_obat"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Rekonsiliasi Obat</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2042','2043','2044'])) { ?>
						<li>
							<a <?=menuIsActive('setting_intra_hospital')?> href="{base_url}setting_intra_hospital"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Intra Hospital</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2048','2050'])) { ?>
						<li>
							<a <?=menuIsActive('setting_tf_hospital')?> href="{base_url}setting_tf_hospital"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Transfer Hospital</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2051','2053'])) { ?>
						<li>
							<a <?=menuIsActive('setting_implementasi_kep')?> href="{base_url}setting_implementasi_kep"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Implementasi Kep</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2054','2056'])) { ?>
						<li>
							<a <?=menuIsActive('setting_timbang')?> href="{base_url}setting_timbang"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Timbang Terima</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2058'])) { ?>
						<li>
							<a <?=menuIsActive('setting_tindakan_anestesi')?> href="{base_url}setting_tindakan_anestesi"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Tin. Anestesi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2083'])) { ?>
						<li>
							<a <?=menuIsActive('setting_risiko_tinggi')?> href="{base_url}setting_risiko_tinggi"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Risiko Tinggi</a>
						</li>
						<?php } ?>
						
						<?php if (UserAccesForm($user_acces_form, ['2205','2206','2207','2208'])) { ?>
						<li>
							<a <?=menuIsActive('setting_mpp')?> href="{base_url}setting_mpp"><i class="glyphicon glyphicon-option-horizontal"></i> Setting MPP</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2210','2212'])) { ?>
						<li>
							<a <?=menuIsActive('setting_evaluasi_mpp')?> href="{base_url}setting_evaluasi_mpp"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Evaluasi MPP</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2215','2217'])) { ?>
						<li>
							<a <?=menuIsActive('setting_imp_mpp')?> href="{base_url}setting_imp_mpp"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Implementasi MPP</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2223','2224','2225','2226'])) { ?>
						<li>
							<a <?=menuIsActive('setting_assesmen_pulang')?> href="{base_url}setting_assesmen_pulang"><i class="glyphicon glyphicon-option-horizontal"></i> Ringkasan Pulang Kep</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2227','2228','2229','2230'])) { ?>
						<li>
							<a <?=menuIsActive('setting_assesmen_askep')?> href="{base_url}setting_assesmen_askep"><i class="glyphicon glyphicon-option-horizontal"></i> ASKEP Perioperatif</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2231','2233','2234'])) { ?>
						<li>
							<a <?=menuIsActive('setting_keselamatan')?> href="{base_url}setting_keselamatan"><i class="glyphicon glyphicon-option-horizontal"></i> Checklis Keselamatan</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2235','2237'])) { ?>
						<li>
							<a <?=menuIsActive('setting_lap_bedah')?> href="{base_url}setting_lap_bedah"><i class="glyphicon glyphicon-option-horizontal"></i> Lap. Pembedahan</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2238','2240'])) { ?>
						<li>
							<a <?=menuIsActive('setting_pra_sedasi')?> href="{base_url}setting_pra_sedasi"><i class="glyphicon glyphicon-option-horizontal"></i> Pra Anestesi, Sedasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2350','2353','2354'])) { ?>
						<li>
							<a <?=menuIsActive('setting_ringkasan_pulang')?> href="{base_url}setting_ringkasan_pulang"><i class="glyphicon glyphicon-option-horizontal"></i> Ringkasan Pulang</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2358','2359','2360'])) { ?>
						<li>
							<a <?=menuIsActive('setting_pra_bedah')?> href="{base_url}setting_pra_bedah"><i class="glyphicon glyphicon-option-horizontal"></i> Pra Bedah</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2364','2365','2366','2367','2368'])) { ?>
						<li>
							<a <?=menuIsActive('setting_survey_kepuasan')?> href="{base_url}setting_survey_kepuasan"><i class="glyphicon glyphicon-option-horizontal"></i> Survey Kepuasan Pasien</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2377'])) { ?>
						<li>
							<a <?=menuIsActive('setting_ews')?> href="{base_url}setting_ews"><i class="glyphicon glyphicon-option-horizontal"></i> Setting EWS</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2389'])) { ?>
						<li>
							<a <?=menuIsActive('setting_pews')?> href="{base_url}setting_pews"><i class="glyphicon glyphicon-option-horizontal"></i> Setting PEWS</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2392'])) { ?>
						<li>
							<a <?=menuIsActive('setting_observasi')?> href="{base_url}setting_observasi"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Obsesrvasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2405'])) { ?>
						<li>
							<a <?=menuIsActive('setting_aps')?> href="{base_url}setting_aps"><i class="glyphicon glyphicon-option-horizontal"></i> Setting APS</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2409'])) { ?>
						<li>
							<a <?=menuIsActive('setting_info_ods')?> href="{base_url}setting_info_ods"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Info ODS</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2439'])) { ?>
						<li>
							<a <?=menuIsActive('setting_tf_darah')?> href="{base_url}setting_tf_darah"><i class="glyphicon glyphicon-option-horizontal"></i> Setting TF Darah</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2466'])) { ?>
						<li>
							<a <?=menuIsActive('setting_invasif')?> href="{base_url}setting_invasif"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Invasif</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2489'])) { ?>
						<li>
							<a <?=menuIsActive('setting_icu')?> href="{base_url}setting_icu"><i class="glyphicon glyphicon-option-horizontal"></i> Setting ICU</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2513'])) { ?>
						<li>
							<a <?=menuIsActive('setting_isolasi')?> href="{base_url}setting_isolasi"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Isolasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2536'])) { ?>
						<li>
							<a <?=menuIsActive('setting_dnr')?> href="{base_url}setting_dnr"><i class="glyphicon glyphicon-option-horizontal"></i> Setting DNR</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2545'])) { ?>
						<li>
							<a <?=menuIsActive('setting_rohani')?> href="{base_url}setting_rohani"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Kerohanian</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2548'])) { ?>
						<li>
							<a <?=menuIsActive('setting_privasi')?> href="{base_url}setting_privasi"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Privasi</a>
						</li>
						<?php } ?>
						<?php if (UserAccesForm($user_acces_form, ['2551'])) { ?>
						<li>
							<a <?=menuIsActive('setting_opinion')?> href="{base_url}setting_opinion"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Second Opinion</a>
						</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['1600'])) { 
				
			?>
				
				<li <?=(in_array($menu, $subERmDiskon)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide">Setting Diskon</span></a>
					<ul>
							<li>
								<a <?=menuIsActive('mpengaturan_diskon_kelompok_pasien_laboratorium')?> href="{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium"><i class="glyphicon glyphicon-option-horizontal"></i> Kelompok Pasien</a>
							</li>
							<li>
								<a <?=menuIsActive('mpengaturan_diskon_asuransi_laboratorium')?> href="{base_url}mpengaturan_diskon_asuransi_laboratorium"><i class="glyphicon glyphicon-option-horizontal"></i> Asuransi</a>
							</li>
					</ul>
				</li>
			<?php } ?>
			
			<?php if (UserAccesForm($user_acces_form, ['1847'])) { 
				
			?>
				
				<li <?=(in_array($menu, $subERmRM)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide">Setting Rehabilitas Medis</span></a>
					<ul>
							<?php if (UserAccesForm($user_acces_form, ['1847'])) { ?>
							<li>
								<a <?=menuIsActive('setting_rm')?> href="{base_url}setting_rm"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Label</a>
							</li>
							<?php } ?>
							
					</ul>
				</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['2285','2286'])) { 
				
			?>
				
				<li <?=(in_array($menu, $subERmTKO)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide">Kamar Operasi</span></a>
					<ul>
							<?php if (UserAccesForm($user_acces_form, ['2285','2286'])) { ?>
							<li>
								<a <?=menuIsActive('setting_tko')?> href="{base_url}setting_tko"><i class="glyphicon glyphicon-option-horizontal"></i> General & Tarif</a>
							</li>
							<?php } ?>
							
					</ul>
				</li>
			<?php } ?>
			
			<?php if (UserAccesForm($user_acces_form, ['1600'])) { 
				
			?>
				
				<li <?=(in_array($menu, $subERmEResep)) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-caret-right"></i><span class="sidebar-mini-hide">Setting E-Resep</span></a>
					<ul>
							<?php if (UserAccesForm($user_acces_form, ['1810'])) { ?>
							<li>
								<a <?=menuIsActive('setting_margin_barang')?> href="{base_url}setting_margin_barang"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Margin</a>
							</li>
							<?php } ?>
							<?php if (UserAccesForm($user_acces_form, ['1811'])) { ?>
							<li>
								<a <?=menuIsActive('setting_cover_barang')?> href="{base_url}setting_cover_barang"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Coverage</a>
							</li>
							<?php } ?>
							<?php if (UserAccesForm($user_acces_form, ['1812'])) { ?>
							<li>
								<a <?=menuIsActive('setting_akses_barang')?> href="{base_url}setting_akses_barang"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Akses Barang</a>
							</li>
							<?php } ?>
							<?php if (UserAccesForm($user_acces_form, ['1813'])) { ?>
							<li>
								<a <?=menuIsActive('setting_eresep')?> href="{base_url}setting_eresep"><i class="glyphicon glyphicon-option-horizontal"></i> Setting E-Resep</a>
							</li>
							<?php } ?>
							<?php if (UserAccesForm($user_acces_form, ['2101'])) { ?>
							<li>
								<a <?=menuIsActive('setting_eresep_print')?> href="{base_url}setting_eresep_print"><i class="glyphicon glyphicon-option-horizontal"></i> Setting E-Resep Print</a>
							</li>
							<?php } ?>
							<?php if (UserAccesForm($user_acces_form, ['2102'])) { ?>
							<li>
								<a <?=menuIsActive('setting_eresep_copy')?> href="{base_url}setting_eresep_copy"><i class="glyphicon glyphicon-option-horizontal"></i> Setting E-Resep Copy</a>
							</li>
							<?php } ?>
							<?php if (UserAccesForm($user_acces_form, ['1817'])) { ?>
							<li>
								<a <?=menuIsActive('mtujuan_farmasi')?> href="{base_url}mtujuan_farmasi"><i class="glyphicon glyphicon-option-horizontal"></i> Tujuan Farmasi</a>
							</li>
							<?php } ?>
					</ul>
				</li>
			<?php } ?>
			
			<li <?=(in_array($menu, $Menu_App_setting_Tindakan_Ranap)) ? 'class="open"' : ''; ?>>
				<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Setting Ranap</span></a>
				<ul>
					<?php if (UserAccesForm($user_acces_form, ['1891'])) { ?>
					<li><a <?=menuIsActive('ruangan_display')?> href="{base_url}ruangan_display"><i class="glyphicon glyphicon-option-horizontal"></i> Display Ruangan</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1892'])) { ?>
					<li><a <?=menuIsActive('setting_rencana_ranap')?> href="{base_url}setting_rencana_ranap"><i class="glyphicon glyphicon-option-horizontal"></i> Rencana Ranap</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1895'])) { ?>
					<li><a <?=menuIsActive('setting_rencana_bedah')?> href="{base_url}setting_rencana_bedah"><i class="glyphicon glyphicon-option-horizontal"></i> Rencana Bedah</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1902'])) { ?>
					<li><a <?=menuIsActive('setting_rencana_biaya')?> href="{base_url}setting_rencana_biaya"><i class="glyphicon glyphicon-option-horizontal"></i> Estimasi Biaya</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1908'])) { ?>
					<li><a <?=menuIsActive('msetting_tarif_jenis_operasi')?> href="{base_url}msetting_tarif_jenis_operasi"><i class="glyphicon glyphicon-option-horizontal"></i> Tarif Jenis Operasi</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1918'])) { ?>
					<li><a <?=menuIsActive('setting_ranap_kunjungan')?> href="{base_url}setting_ranap_kunjungan"><i class="glyphicon glyphicon-option-horizontal"></i> Rawat Inap</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['2218'])) { ?>
					<li><a <?=menuIsActive('setting_visite')?> href="{base_url}setting_visite"><i class="glyphicon glyphicon-option-horizontal"></i> Visite</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['2220'])) { ?>
					<li><a <?=menuIsActive('setting_pelayanan_ri')?> href="{base_url}setting_pelayanan_ri"><i class="glyphicon glyphicon-option-horizontal"></i> Tindakan Pelayanan</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['2603'])) { ?>
					<li><a <?=menuIsActive('setting_deposit')?> href="{base_url}setting_deposit"><i class="glyphicon glyphicon-option-horizontal"></i> Deposit</a></li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['2623','2624','2625','2626','2627','2628','2629'])) { ?>
					<li><a <?=menuIsActive('setting_trx_ranap')?> href="{base_url}setting_trx_ranap"><i class="glyphicon glyphicon-option-horizontal"></i> Billing Ranap</a></li>
					<?php } ?>
				</ul>
			</li>
			
			<?php if (UserAccesForm($user_acces_form, ['1784'])) { ?>
			<li>
				<a <?=menuIsActive('mpoliklinik_emergency')?> href="{base_url}mpoliklinik_emergency"><i class="glyphicon glyphicon-option-horizontal"></i> Master Emergency Dept</a>
			</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['1831'])) { ?>
			<li>
				<a <?=menuIsActive('mpoliklinik_direct')?> href="{base_url}mpoliklinik_direct"><i class="glyphicon glyphicon-option-horizontal"></i> Master Poliklinik Direct</a>
			</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['1827'])) { ?>
			<li>
				<a <?=menuIsActive('mtelaah')?> href="{base_url}mtelaah"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Telaah Farmasi</a>
			</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['1823'])) { ?>
			<li>
				<a <?=menuIsActive('minterval')?> href="{base_url}minterval"><i class="glyphicon glyphicon-option-horizontal"></i> Master Interval</a>
			</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['1792'])) { ?>
			<li>
				<a <?=menuIsActive('mpoliklinik_konsul')?> href="{base_url}mpoliklinik_konsul"><i class="glyphicon glyphicon-option-horizontal"></i> Master poliklinik Konsul</a>
			</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['2039'])) { ?>
			<li>
				<a <?=menuIsActive('munitpelayanan_tujuan')?> href="{base_url}munitpelayanan_tujuan"><i class="glyphicon glyphicon-option-horizontal"></i> Master Unit Asal / Tujuan</a>
			</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['1898'])) { ?>
			<li>
				<a <?=menuIsActive('erm_jenis_operasi')?> href="{base_url}erm_jenis_operasi"><i class="glyphicon glyphicon-option-horizontal"></i> Jenis Operasi</a>
			</li>
			<?php } ?>
			<?php if (UserAccesForm($user_acces_form, ['2449'])) { ?>
			<li>
				<a <?=menuIsActive('mshift')?> href="{base_url}mshift"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Shift</a>
			</li>
			<?php } ?>
			

        <li>
            <a <?=menuIsActive('mgroup_test')?> href="{base_url}mgroup_test"><i class="glyphicon glyphicon-option-horizontal"></i> Group Test</a>
        </li>
        </li>   
    </ul>
</li>
<?}?>
<?
if (in_array('7', $user_acces_menu, false) == '1') {?>
<li <?=(in_array($menu, $Menu_app)  && !in_array($menu2, $Menu_App_Pengaturan_Slot)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-indent"></i><span class="sidebar-mini-hide"> Web & Apps</span></a>
    <ul>
        <?php if (UserAccesForm($user_acces_form, ['1444','1445','1446','1453','1460','1469','1470'])) { ?>
        <li <?=(in_array($menu, $Menu_App_Pengaturan) && !in_array($menu2, $Menu_App_Pengaturan_Slot)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span class="sidebar-mini-hide"> Pengaturan</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['1444'])) { ?>
                <li>
                    <a <?=cekMenu_Active_2('app_setting','login')?> href="{base_url}app_setting/login"><i class="glyphicon glyphicon-option-horizontal"></i> Login</a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['1445'])) { ?>
                <li>
                    <a <?=cekMenu_Active_2('app_setting','index')?> href="{base_url}app_setting/index"><i class="glyphicon glyphicon-option-horizontal"></i> Index</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['1446','1453'])) { ?>
                <li>
                    <a <?=(in_array($this->uri->segment(2),$menu2_informasi)?"class=\"active\"" :'')?> href="{base_url}app_setting/info"><i class="glyphicon glyphicon-option-horizontal"></i> informasi </a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['1460'])) { ?>
                <li>
                    <a <?=cekMenu_Active_2('app_setting','about_us')?> href="{base_url}app_setting/about_us"><i class="glyphicon glyphicon-option-horizontal"></i> About Us</a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['1469','1470','1485','1486'])) { //Pengaturan General dan Logic?>
                <li <?=(in_array($menu2, array('pendaftaran','pendaftaran_rm'))) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pendaftaran</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1469','1470'])) { //Pengaturan General dan Logic?>
                        <li>
                            <a <?=cekMenu_Active_2('app_setting','pendaftaran')?> href="{base_url}app_setting/pendaftaran"><i class="glyphicon glyphicon-option-horizontal"></i> Rawat Jalan</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1485','1486'])) { //Pengaturan General dan Logic?>
                        <li>
                            <a <?=cekMenu_Active_2('app_setting','pendaftaran_rm')?> href="{base_url}app_setting/pendaftaran_rm"><i class="glyphicon glyphicon-option-horizontal"></i> Rehabilitas Medis</a>
                        </li>
                        <?php } ?>

                    </ul>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['1519'])) { ?>
                <li>
                    <a <?=cekMenu_Active_2('apm_setting','index')?> href="{base_url}apm_setting/index"><i class="glyphicon glyphicon-option-horizontal"></i> APM</a>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>
    </ul>
</li>
<?}?>
<?
$MenuAntrianPendaftaranSetting=array('antrian_layanan','antrian_layanan_counter','antrian_tiket_setting','antrian_display','antrian_print_out','antrian_asset_sound');
$MenuAntrianPendaftaranTrx=array('antrian_caller');
$MenuAntrianPendaftaran=array_merge($MenuAntrianPendaftaranSetting,$MenuAntrianPendaftaranTrx);

$MenuAntrianPoliSetting=array('antrian_poli_kode','antrian_poli_tujuan','antrian_poli_display','antrian_monitoring');
$MenuAntrianPoliTrx=array();
$MenuAntrianPoli=array_merge($MenuAntrianPoliSetting,$MenuAntrianPoliTrx);
$MenuAntrian=array_merge($MenuAntrianPoli,$MenuAntrianPendaftaran);

$MenuPengaturanERMLab=array('mpengaturan_tujuan_laboratorium','mpengaturan_form_laboratorium');
?>

<? if (in_array('9', $user_acces_menu, false) == '1') {?>
<li <?=(in_array($menu, $MenuAntrian)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-screen-tablet"></i><span class="sidebar-mini-hide"> Sistem Antrian</span></a>
    <ul>
        <?php if (UserAccesForm($user_acces_form, ['1546','1528','1532','1536','1537','1543'])) { ?>
        <li <?=(in_array($menu, $MenuAntrianPendaftaran)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Antrian Pendaftaran</span></a>
            <ul>
                <li <?=(in_array($menu, $MenuAntrianPendaftaranSetting)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pengaturan</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1546'])) { ?>
                        <li><a <?=menuIsActive('antrian_asset_sound')?> href="{base_url}antrian_asset_sound"><i class="glyphicon glyphicon-option-horizontal"></i> Asset Sound</a></li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1528'])) { ?>
                        <li><a <?=menuIsActive('antrian_layanan')?> href="{base_url}antrian_layanan"><i class="glyphicon glyphicon-option-horizontal"></i> Pelayanan Antrian</a></li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1532'])) { ?>
                        <li><a <?=menuIsActive('antrian_layanan_counter')?> href="{base_url}antrian_layanan_counter"><i class="glyphicon glyphicon-option-horizontal"></i> Counter</a></li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1536'])) { ?>
                        <li><a  <?=menuIsActive('antrian_tiket_setting')?> href="{base_url}antrian_tiket_setting"><i class="glyphicon glyphicon-option-horizontal"></i> Display Tiket Antrian</a></li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1537'])) { ?>
                        <li><a <?=menuIsActive('antrian_display')?> href="{base_url}antrian_display"><i class="glyphicon glyphicon-option-horizontal"></i> Display TV Antrian</a></li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1543'])) { ?>
                        <li><a <?=menuIsActive('antrian_print_out')?> href="{base_url}antrian_print_out"><i class="glyphicon glyphicon-option-horizontal"></i> Print Out</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li <?=(in_array($menu, $MenuAntrianPendaftaranTrx)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Transaksi</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1544','1545'])) { ?>
                        <li>
                            <a  href="{base_url}antrian_tiket/index/0" target="_blank"><i class="glyphicon glyphicon-option-horizontal"></i> Tiket Antrian</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1545'])) { ?>
                        <li>
                            <a <?=menuIsActive('antrian_caller')?> href="{base_url}antrian_caller"><i class="glyphicon glyphicon-option-horizontal"></i> Panggil Antrian</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
        </li>
        <?}?>
        <?php if (UserAccesForm($user_acces_form, ['1550','1552','1557','1558'])) { ?>
        <li <?=(in_array($menu, $MenuAntrianPoli)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Antrian Poliklinik</span></a>
            <ul>
                <li <?=(in_array($menu, $MenuAntrianPoliSetting)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pengaturan</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['1550'])) { ?>
                        <li><a <?=menuIsActive('antrian_poli_kode')?> href="{base_url}antrian_poli_kode"><i class="glyphicon glyphicon-option-horizontal"></i> Kode Antrian</a></li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1552'])) { ?>
                        <li><a <?=menuIsActive('antrian_poli_tujuan')?> href="{base_url}antrian_poli_tujuan"><i class="glyphicon glyphicon-option-horizontal"></i> Referensi Tujuan</a></li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1557'])) { ?>
                        <li><a <?=menuIsActive('antrian_poli_display')?> href="{base_url}antrian_poli_display"><i class="glyphicon glyphicon-option-horizontal"></i> Display TV Antrian</a></li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['1558'])) { ?>
                        <li><a <?=menuIsActive('antrian_monitoring')?> href="{base_url}antrian_monitoring"><i class="glyphicon glyphicon-option-horizontal"></i> Antrian WEB</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li <?=(in_array($menu, $MenuAntrianPoliTrx)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Transaksi</span></a>
                    <ul>
                        <li><a  href="{base_url}antrian_tiket/index/0" target="_blank"><i class="glyphicon glyphicon-option-horizontal"></i> Tiket Antrian</a></li>
                        <?php if (UserAccesForm($user_acces_form, ['1584'])) { ?>
                        <li><a <?=menuIsActive('tpendaftaran_poli_tindakan')?> href="{base_url}tpendaftaran_poli_tindakan"><i class="glyphicon glyphicon-option-horizontal"></i> Panggil Antrian</a></li>
                        <?}?>
                    </ul>
                </li>
            </ul>
        </li>
        <?}?>
    </ul>
</li>
<?}?>

<? if (in_array('8', $user_acces_menu, false) == '1') {?>
	<?php if (UserAccesForm($user_acces_form, ['1562','1563','1564','1565','1497','1504','1506','1507','1514','1515','1518','1566','1567','1568','1569','1479','1480','1574','1575'])) { ?>
	<li <?=(in_array($menu, $Menu_App_Booking)  || ($menu=='app_setting' && in_array($menu2, $Menu_App_Pengaturan_Slot))) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-sort-numeric-asc"></i><span class="sidebar-mini-hide"> Booking System</span></a>
			<ul>
					<?php if (UserAccesForm($user_acces_form, ['1562','1564','1563','1565','1479','1480'])) { ?>
					<li <?=(in_array($menu, $Menu_App_setting)) ? 'class="open"' : ''; ?>>
							<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pengaturan</span></a>
							<ul>
									<?php if (UserAccesForm($user_acces_form, ['1562','1564','1563','1565'])) { ?>
									<li <?=(in_array($menu2, array('pendaftaran','pendaftaran_rm')) && $menu=='setting_reservasi_petugas') ? 'class="open"' : ''; ?>>
											<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Reservasi Via Petugas</span></a>
											<ul>
													<?php if (UserAccesForm($user_acces_form, ['1562','1564'])) { ?>
													<li><a <?=cekMenu_Active_2('setting_reservasi_petugas','pendaftaran')?> href="{base_url}setting_reservasi_petugas/pendaftaran"><i class="glyphicon glyphicon-option-horizontal"></i> Rawat Jalan</a></li>
													<?php } ?>
													<?php if (UserAccesForm($user_acces_form, ['1563','1565'])) { ?>
													<li><a <?=cekMenu_Active_2('setting_reservasi_petugas','pendaftaran_rm')?> href="{base_url}setting_reservasi_petugas/pendaftaran_rm"><i class="glyphicon glyphicon-option-horizontal"></i> Rehabilitas Medis</a></li>
													<?php } ?>
											</ul>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['1574','1575'])) { ?>
									<li <?=(in_array($menu2, array('pendaftaran','pendaftaran_rm')) && $menu=='setting_checkin_petugas') ? 'class="open"' : ''; ?>>
											<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Checkin Via Petugas</span></a>
											<ul>
													<?php if (UserAccesForm($user_acces_form, ['1574'])) { ?>
													<li><a <?=cekMenu_Active_2('setting_checkin_petugas','pendaftaran')?> href="{base_url}setting_checkin_petugas/pendaftaran"><i class="glyphicon glyphicon-option-horizontal"></i> Rawat Jalan</a></li>
													<?php } ?>
													<?php if (UserAccesForm($user_acces_form, ['1575'])) { ?>
													<li><a <?=cekMenu_Active_2('setting_checkin_petugas','pendaftaran_rm')?> href="{base_url}setting_checkin_petugas/pendaftaran_rm"><i class="glyphicon glyphicon-option-horizontal"></i> Rehabilitas Medis</a></li>
													<?php } ?>
											</ul>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['1585','1586'])) { ?>
									<li <?=(in_array($menu2, array('pendaftaran','pendaftaran_rm')) && $menu=='setting_daftar_petugas') ? 'class="open"' : ''; ?>>
											<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Daftar Via Petugas</span></a>
											<ul>
													<?php if (UserAccesForm($user_acces_form, ['1585'])) { ?>
													<li><a <?=cekMenu_Active_2('setting_daftar_petugas','pendaftaran')?> href="{base_url}setting_daftar_petugas/pendaftaran"><i class="glyphicon glyphicon-option-horizontal"></i> Rawat Jalan</a></li>
													<?php } ?>
													<?php if (UserAccesForm($user_acces_form, ['1586'])) { ?>
													<li><a <?=cekMenu_Active_2('setting_daftar_petugas','pendaftaran_rm')?> href="{base_url}setting_daftar_petugas/pendaftaran_rm"><i class="glyphicon glyphicon-option-horizontal"></i> Rehabilitas Medis</a></li>
													<?php } ?>
											</ul>
									</li>
									<?php } ?>

									<?php $menu2 = $this->uri->segment(2); ?>
									<?php if (UserAccesForm($user_acces_form, ['1479','1480'])) { ?>
									<li <?=(in_array($menu2, $Menu_App_Pengaturan_Slot)) ? 'class="open"' : ''; ?>>
											<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Slot Reservasi RM</span></a>
											<ul>
													<?php if (UserAccesForm($user_acces_form, ['1479'])) { ?>
													<li>
															<a <?=cekMenu_Active_2('app_setting','tujuan')?> href="{base_url}app_setting/tujuan"><i class="glyphicon glyphicon-option-horizontal"></i> Tujuan</a>
													</li>
													<?php } ?>

													<?php if (UserAccesForm($user_acces_form, ['1480'])) { ?>
													<li>
															<a  <?=(in_array($menu2, $Menu_App_Pengaturan_Slot2)) ? 'class="active"' : ''; ?> href="{base_url}app_setting/reservasi"><i class="glyphicon glyphicon-option-horizontal"></i> Reservasi</a>
													</li>
													<?php } ?>

											</ul>
									</li>
									<?php } ?>
							</ul>
					</li>
					<?php } ?>

					<?php if (UserAccesForm($user_acces_form, ['1497','1504','1506','1507','1514','1515','1518','1566','1567','1568','1569'])) { ?>
					<li <?=(in_array($menu, array('tbooking','tbooking_rehab'))) ? 'class="open"' : ''; ?>>
							<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Reservasi</span></a>
							<ul>
									<?php if (UserAccesForm($user_acces_form, ['1497','1504','1506'])) { ?>
									<li>
											<a <?=menuIsActive('tbooking')?> href="{base_url}tbooking/poliklinik"><i class="glyphicon glyphicon-option-horizontal"></i> Poliklinik</a>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['1507','1514','1515','1518'])) { ?>
									<li>
											<a <?=menuIsActive('tbooking_rehab')?> href="{base_url}tbooking_rehab/poliklinik"><i class="glyphicon glyphicon-option-horizontal"></i> Rehabilitas Medis</a>
									</li>
									<?php } ?>
									<li>
							</ul>
					</li>
					<?php } ?>
			</ul>
	</li>
	<?php } ?>
<?php } ?>

<? if (in_array('10', $user_acces_menu, false) == '1') {?>
<?php if (UserAccesForm($user_acces_form, ['1577','1578','1545','1507','1514','1515','1518','1926','1932'])) { ?>
	<li <?=(in_array($menu, $Menu_App_Pendaftaran)) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-user-plus"></i><span class="sidebar-mini-hide">Pendaftaran</span></a>
			<ul>
					<?php if (UserAccesForm($user_acces_form, ['1577'])) { ?>
					<li>
							<a <?=menuIsActive('tpendaftaran_poli')?> href="{base_url}tpendaftaran_poli/index"><i class="glyphicon glyphicon-option-horizontal"></i> Pend. Poliklinik</a>
					</li>
					<?php } ?>
					
					<?php if (UserAccesForm($user_acces_form, ['1545'])) { ?>
					<li>
						<a <?=menuIsActive('antrian_caller')?> href="{base_url}antrian_caller"><i class="glyphicon glyphicon-option-horizontal"></i> Panggil Antrian</a>
					</li>
					<?php } ?>
					
					<?php if (UserAccesForm($user_acces_form, ['1507','1514','1515','1518'])) { ?>
					<li>
						<a <?=menuIsActive('tcheckin')?> href="{base_url}tcheckin"><i class="glyphicon glyphicon-option-horizontal"></i> Check In</a>
					</li>
					<?php } ?>
					
					<?php if (UserAccesForm($user_acces_form, ['1926'])) { ?>
					<li>
							<a <?=menuIsActive('tpendaftaran_ranap')?> href="{base_url}tpendaftaran_ranap"><i class="glyphicon glyphicon-option-horizontal"></i> Pendaftaran Rawat Inap</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1932'])) { ?>
					<li>
							<a <?=menuIsActive('tpendaftaran_ranap_terima')?> href="{base_url}tpendaftaran_ranap_terima"><i class="glyphicon glyphicon-option-horizontal"></i> Penerimaan Rawat Inap</a>
					</li>
					<?php } ?>
			</ul>
	</li>
	<?php } ?>
<?php } ?>


	<?php if (UserAccesForm($user_acces_form, ['1584','1596','1597','1598','1783','1796','1845'])) { ?>
	<li <?=(in_array($menu, $Menu_App_Tindakan)) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-user-md"></i><span class="sidebar-mini-hide">Tindakan Poliklinik</span></a>
			<ul>
					<?php if (UserAccesForm($user_acces_form, ['1584'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_tindakan')?> href="{base_url}tpendaftaran_poli_tindakan"><i class="glyphicon glyphicon-option-horizontal"></i> Antrian Poliklinik</a>
					</li>
					<?php } ?>

					<?php if (UserAccesForm($user_acces_form, ['1596'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_ttv')?> href="{base_url}tpendaftaran_poli_ttv"><i class="glyphicon glyphicon-option-horizontal"></i> Pemeriksaan TTV</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1783'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_perawat')?> href="{base_url}tpendaftaran_poli_perawat"><i class="glyphicon glyphicon-option-horizontal"></i> Pemeriksaan Perawat</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1598'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_jasa')?> href="{base_url}tpendaftaran_poli_jasa"><i class="glyphicon glyphicon-option-horizontal"></i> Penata Jasa</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1597'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_pasien')?> href="{base_url}tpendaftaran_poli_pasien"><i class="glyphicon glyphicon-option-horizontal"></i> My Pasien (Dokter)</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1796'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tkonsul')?> href="{base_url}tkonsul"><i class="glyphicon glyphicon-option-horizontal"></i> My Pasien (Konsul)</a>
					</li>
					<?php } ?>
					
					<?php if (UserAccesForm($user_acces_form, ['1845'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tperubahan')?> href="{base_url}tperubahan"><i class="glyphicon glyphicon-option-horizontal"></i> Perubahan Resep</a>
					</li>
					<?php } ?>
			</ul>
	</li>
<?php } ?>
	
	<?php if (UserAccesForm($user_acces_form, ['1788','1789','1790','1791'])) { ?>
	<li <?=(in_array($menu, $Menu_App_Tindakan_IGD)) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-user-md"></i><span class="sidebar-mini-hide">Tindakan IGD</span></a>
			<ul>
					<?php if (UserAccesForm($user_acces_form, ['1788'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_igd_ttv')?> href="{base_url}tpendaftaran_poli_igd_ttv"><i class="glyphicon glyphicon-option-horizontal"></i> Pemeriksaan TTV</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1789'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_igd_perawat')?> href="{base_url}tpendaftaran_poli_igd_perawat"><i class="glyphicon glyphicon-option-horizontal"></i> Pemeriksaan Perawat</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1790'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_igd_jasa')?> href="{base_url}tpendaftaran_poli_igd_jasa"><i class="glyphicon glyphicon-option-horizontal"></i> Penata Jasa</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1791'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpendaftaran_poli_igd_pasien')?> href="{base_url}tpendaftaran_poli_igd_pasien"><i class="glyphicon glyphicon-option-horizontal"></i> My Pasien (IGD)</a>
					</li>
					<?php } ?>
					
			</ul>
	</li>
	<?php } ?>
<? $menu_session=$this->session->userdata('path_tindakan'); ?>
<? if (in_array('15', $user_acces_menu, false) == '1') {?>
	<li <?=(in_array($menu, $Menu_App_Tindakan_Ranap)) ? 'class="open"' : ''; ?> <?=(in_array($menu_session, $Menu_App_Tindakan_Ranap)) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-bed"></i><span class="sidebar-mini-hide">Rawat Inap & ODS</span></a>
			<ul>
				<?php if (UserAccesForm($user_acces_form, ['1934'])) { ?>
				<li><a <?=menuIsActiveTindakan('ttindakan_ranap')?> href="{base_url}ttindakan_ranap"><i class="glyphicon glyphicon-option-horizontal"></i> Rawat Inap</a></li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1935'])) { ?>
				<li><a <?=menuIsActiveTindakan('ttindakan_ranap_ods')?> href="{base_url}ttindakan_ranap_ods"><i class="glyphicon glyphicon-option-horizontal"></i> One Day Surgery</a></li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1936'])) { ?>
				<li><a <?=menuIsActiveTindakan('ttindakan_ranap_jasa')?> href="{base_url}ttindakan_ranap_jasa"><i class="glyphicon glyphicon-option-horizontal"></i> Penata Jasa Rawat Inap</a></li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1937'])) { ?>
				<li><a <?=menuIsActiveTindakan('ttindakan_ranap_jasa_ods')?> href="{base_url}ttindakan_ranap_jasa_ods"><i class="glyphicon glyphicon-option-horizontal"></i> Penata Jasa ODS</a></li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1938'])) { ?>
				<li><a <?=menuIsActiveTindakan('ttindakan_ranap_pasien')?> href="{base_url}ttindakan_ranap_pasien"><i class="glyphicon glyphicon-option-horizontal"></i> My Pasien</a></li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1990'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tpindah_dpjp')?> href="{base_url}tpindah_dpjp"><i class="glyphicon glyphicon-option-horizontal"></i> My Pasien (Pindah DPJP)</a>
					</li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['2047'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tintra_hospital')?> href="{base_url}tintra_hospital"><i class="glyphicon glyphicon-option-horizontal"></i> Persetujuan TF Intra</a>
					</li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['2038'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('trekon_obat')?> href="{base_url}trekon_obat"><i class="glyphicon glyphicon-option-horizontal"></i> My Pasien (Rekonsiliasi)</a>
					</li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1939'])) { ?>
				<li><a <?=menuIsActiveTindakan('ttindakan_ranap_admin')?> href="{base_url}ttindakan_ranap_admin"><i class="glyphicon glyphicon-option-horizontal"></i> ADM Ranap & ODS</a></li>
				<?php } ?>
				
				
				<?php if (UserAccesForm($user_acces_form, ['1886'])) { ?>
				<li>
						<a <?=menuIsActive('tmonitoring_bed')?> href="{base_url}tmonitoring_bed"><i class="glyphicon glyphicon-option-horizontal"></i> Monitoring Bed</a>
				</li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1909'])) { ?>
				<li>
						<a <?=menuIsActive('tpoliklinik_ranap')?> href="{base_url}tpoliklinik_ranap/estimasi_admin"><i class="glyphicon glyphicon-option-horizontal"></i> Estimasi Biaya</a>
				</li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1911'])) { ?>
				<li>
						<a <?=menuIsActive('treservasi_bed')?> href="{base_url}treservasi_bed"><i class="glyphicon glyphicon-option-horizontal"></i> Reservasi Bed</a>
				</li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['2608'])) { ?>
				<li><a <?=menuIsActiveTindakan('ttindakan_ranap_deposit')?> href="{base_url}ttindakan_ranap_deposit"><i class="glyphicon glyphicon-option-horizontal"></i> Deposit Pasien</a></li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['2609'])) { ?>
				<li><a <?=menuIsActiveTindakan('tdeposit_approval')?> href="{base_url}tdeposit_approval"><i class="glyphicon glyphicon-option-horizontal"></i> Deposit Approval</a></li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['2615'])) { ?>
				<li><a <?=menuIsActiveTindakan('tdeposit_batal')?> href="{base_url}tdeposit_batal"><i class="glyphicon glyphicon-option-horizontal"></i> Deposit Batal</a></li>
				<?php } ?>
			</ul>
	</li>
<?php } ?>
<? if (in_array('19', $user_acces_menu, false) == '1') {?>
	<li <?=(in_array($menu, $Menu_App_Tindakan_KamarBedah)) ? 'class="open"' : ''; ?> <?=(in_array($menu_session, $Menu_App_Tindakan_KamarBedah)) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-stethoscope"></i><span class="sidebar-mini-hide">Kamar Bedah</span></a>
			<ul>
				<?php if (UserAccesForm($user_acces_form, ['2287'])) { ?>
				<li><a <?=menuIsActiveTindakan('tkamar_bedah')?> href="{base_url}tkamar_bedah"><i class="glyphicon glyphicon-option-horizontal"></i> Penjadwalan</a></li>
				<?php } ?>
				
			</ul>
	</li>
<?php } ?>
<? if (in_array('13', $user_acces_menu, false) == '1') {?>
<?php if (UserAccesForm($user_acces_form, ['1835','2103'])) { ?>
	<li <?=(in_array($menu, $Menu_App_Tindakan_Farmasi)) ? 'class="open"' : ''; ?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-cart-plus"></i><span class="sidebar-mini-hide">Farmasi</span></a>
			<ul>
					<?php if (UserAccesForm($user_acces_form, ['1835'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tfarmasi_tindakan')?> href="{base_url}tfarmasi_tindakan"><i class="glyphicon glyphicon-option-horizontal"></i> E-Resep Rajal</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['2103'])) { ?>
					<li>
							<a <?=menuIsActiveTindakan('tfarmasi_tindakan_ri')?> href="{base_url}tfarmasi_tindakan_ri"><i class="glyphicon glyphicon-option-horizontal"></i> E-Resep Rawat Inap</a>
					</li>
					<?php } ?>
					
			</ul>
	</li>
	<?php } ?>
<?php } ?>

<? if (in_array('14', $user_acces_menu, false) == '1') {?>
<?php if (UserAccesForm($user_acces_form, ['1849','1863','2199'])) { ?>
	<li <?=(in_array($menu, $Menu_App_Tindakan_RM)) ? 'class="open"' : ''; ?>> <?=(in_array($menu_session, array('trawatinap_rm'))) ? 'class="open"' : ''; ?>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-universal-access"></i><span class="sidebar-mini-hide">Rehabilitas Medis</span></a>
			<ul>
					<?php if (UserAccesForm($user_acces_form, ['1849'])) { ?>
					<li>
							<a <?=menuIsActive('tpoliklinik_rm_order')?> href="{base_url}tpoliklinik_rm_order"><i class="glyphicon glyphicon-option-horizontal"></i>Rencana Pemeriksaan</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['1863'])) { ?>
					<li>
							<a <?=menuIsActive('tpoliklinik_rm_rj')?> href="{base_url}tpoliklinik_rm_rj"><i class="glyphicon glyphicon-option-horizontal"></i>Rawat Jalan</a>
					</li>
					<?php } ?>
					<?php if (UserAccesForm($user_acces_form, ['2199'])) { ?>
					<li>
							<a <?=menuIsActive('trawatinap_rm')?> href="{base_url}trawatinap_rm"><i class="glyphicon glyphicon-option-horizontal"></i>Rawat Inap</a>
					</li>
					<?php } ?>
					
			</ul>
	</li>
	<?php } ?>
<?php } ?>

<?php
	$menu_laboratorium_referensi = ['merm_referensi', 'mreferensi_rujukan_keluar'];
	$menu_laboratorium_pengaturan = [
		'mpengaturan_tujuan_laboratorium', 'mpengaturan_form_order_laboratorium_umum', 
		'mpengaturan_form_order_laboratorium_patalogi_anatomi', 'mpengaturan_form_order_laboratorium_bank_darah', 
		'mpengaturan_printout_laboratorium', 'mpengaturan_direct_poliklinik_laboratorium', 
		'mpengaturan_pengiriman_email_laboratorium', 'mpengaturan_form_laboratorium_umum_hasil', 
		'mpengaturan_form_laboratorium_patologi_anatomi_hasil', 'mpengaturan_form_laboratorium_bank_darah_hasil', 
		'mpengaturan_printout_laboratorium_umum_hasil', 'mpengaturan_printout_laboratorium_patologi_anatomi_hasil', 
		'mpengaturan_printout_laboratorium_bank_darah_hasil'
	];

	$menu_laboratorium_umum = ['term_laboratorium_umum_permintaan', 'term_laboratorium_umum_hasil'];
	$menu_laboratorium_patologi_anatomi = ['term_laboratorium_patologi_anatomi_permintaan', 'term_laboratorium_patologi_anatomi_hasil', 'term_laboratorium_bank_darah_permintaan', 'term_laboratorium_bank_darah_hasil'];
	$menu_laboratorium_bank_darah = ['term_laboratorium_bank_darah_permintaan', 'term_laboratorium_bank_darah_hasil'];
	
	$menu_laboratorium = array_merge($menu_laboratorium_referensi, $menu_laboratorium_pengaturan, $menu_laboratorium_umum, $menu_laboratorium_patologi_anatomi, $menu_laboratorium_bank_darah);
?>

<? if (in_array('16', $user_acces_menu, false) == '1') {?>
<li <?=(in_array($menu, $menu_laboratorium)) ? 'class="open"' : ''; ?>>
		<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-connectdevelop"></i><span class="sidebar-mini-hide">Laboratorium</span></a>
		<ul>  
				<li <?=(in_array($menu, $menu_laboratorium_referensi)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Referensi</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2104'])) { ?>
								<li>
										<a <?php echo menuIsActive('merm_referensi'); ?> href="{base_url}merm_referensi/index/85"><i class="glyphicon glyphicon-option-horizontal"></i> Prioritas Pemeriksaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2105'])) { ?>
								<li>
										<a <?php echo menuIsActive('merm_referensi'); ?> href="{base_url}merm_referensi/index/90"><i class="glyphicon glyphicon-option-horizontal"></i> Sumber Spesimen Klinis</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2106'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/87"><i class="glyphicon glyphicon-option-horizontal"></i> Status Puasa Pasien</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2107'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/88"><i class="glyphicon glyphicon-option-horizontal"></i> Pengiriman Hasil</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2108'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/91"><i class="glyphicon glyphicon-option-horizontal"></i> Pengambilan Spesimen</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2109'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/118"><i class="glyphicon glyphicon-option-horizontal"></i> Flag Hasil</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2110'])) { ?>
								<li>
										<a <?=menuIsActive('mreferensi_rujukan_keluar')?> href="{base_url}mreferensi_rujukan_keluar"><i class="glyphicon glyphicon-option-horizontal"></i> Rujukan Keluar</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_laboratorium_pengaturan)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pengaturan</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2111'])) { ?>
								<li>
										<a <?=menuIsActive('mpengaturan_tujuan_laboratorium')?> href="{base_url}mpengaturan_tujuan_laboratorium"><i class="glyphicon glyphicon-option-horizontal"></i> Tujuan Laboratorium</a>
								</li>
								<?php } ?>
								<li>
										<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Form Order Lab</span></a>
										<ul>
												<?php if (UserAccesForm($user_acces_form, ['2112'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_form_order_laboratorium_umum')?> href="{base_url}mpengaturan_form_order_laboratorium_umum"><i class="glyphicon glyphicon-option-horizontal"></i> Umum</a>
												</li>
												<?php } ?>
												<?php if (UserAccesForm($user_acces_form, ['2113'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_form_order_laboratorium_patalogi_anatomi')?> href="{base_url}mpengaturan_form_order_laboratorium_patalogi_anatomi"><i class="glyphicon glyphicon-option-horizontal"></i> Patalogi Anatomi</a>
												</li>
												<?php } ?>
												<?php if (UserAccesForm($user_acces_form, ['2114'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_form_order_laboratorium_bank_darah')?> href="{base_url}mpengaturan_form_order_laboratorium_bank_darah"><i class="glyphicon glyphicon-option-horizontal"></i> Bank Darah</a>
												</li>
												<?php } ?>
										</ul>
									</li>
									<?php if (UserAccesForm($user_acces_form, ['2115'])) { ?>
									<li>
											<a <?php echo menuIsActive('mpengaturan_printout_laboratorium'); ?> href="{base_url}mpengaturan_printout_laboratorium"><i class="glyphicon glyphicon-option-horizontal"></i> Print Out Order Lab</a>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['2116'])) { ?>
									<li>
										<a <?=menuIsActive('mpengaturan_direct_poliklinik_laboratorium')?> href="{base_url}mpengaturan_direct_poliklinik_laboratorium"><i class="glyphicon glyphicon-option-horizontal"></i> Poliklnik Direct</a>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['2117'])) { ?>
									<li>
										<a <?=menuIsActive('mpengaturan_pengiriman_email_laboratorium')?> href="{base_url}mpengaturan_pengiriman_email_laboratorium"><i class="glyphicon glyphicon-option-horizontal"></i> Pegiriman E-mail</a>
									</li>
									<?php } ?>
									<li>
										<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Form Input Hasil</span></a>
										<ul>
											<?php if (UserAccesForm($user_acces_form, ['2118'])) { ?>
											<li>
												<a <?=menuIsActive('mpengaturan_form_laboratorium_umum_hasil')?> href="{base_url}mpengaturan_form_laboratorium_umum_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Umum</a>
											</li>
											<?php } ?>
											<?php if (UserAccesForm($user_acces_form, ['2119'])) { ?>
											<li>
												<a <?=menuIsActive('mpengaturan_form_laboratorium_patologi_anatomi_hasil')?> href="{base_url}mpengaturan_form_laboratorium_patologi_anatomi_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Patalogi Anatomi</a>
											</li>
											<?php } ?>
											<?php if (UserAccesForm($user_acces_form, ['2120'])) { ?>
											<li>
												<a <?=menuIsActive('mpengaturan_form_laboratorium_bank_darah_hasil')?> href="{base_url}mpengaturan_form_laboratorium_bank_darah_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Bank Darah</a>
											</li>
											<?php } ?>
										</ul>
									</li>
									<li>
										<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Print Out Hasil</span></a>
										<ul>
												<?php if (UserAccesForm($user_acces_form, ['2121'])) { ?>
												<li>
														<a <?php echo menuIsActive('mpengaturan_printout_laboratorium_umum_hasil'); ?> href="{base_url}mpengaturan_printout_laboratorium_umum_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Umum</a>
												</li>
												<?php } ?>
												<?php if (UserAccesForm($user_acces_form, ['2122'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_printout_laboratorium_patologi_anatomi_hasil')?> href="{base_url}mpengaturan_printout_laboratorium_patologi_anatomi_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Patalogi Anatomi</a>
												</li>
												<?php } ?>
												<?php if (UserAccesForm($user_acces_form, ['2123'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_printout_laboratorium_bank_darah_hasil')?> href="{base_url}mpengaturan_printout_laboratorium_bank_darah_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Bank Darah</a>
												</li>
												<?php } ?>
										</ul>
									</li>
								</li>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_laboratorium_umum)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Laboratorium</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2124'])) { ?>
								<li>
									<a <?php echo menuIsActive('term_laboratorium_umum_permintaan'); ?> href="{base_url}term_laboratorium_umum_permintaan"><i class="glyphicon glyphicon-option-horizontal"></i> Permintaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2125'])) { ?>
								<li>
									<a <?=menuIsActive('term_laboratorium_umum_hasil')?> href="{base_url}term_laboratorium_umum_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Input Hasil</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_laboratorium_bank_darah)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Patologi Anatomi</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2126'])) { ?>
								<li>
									<a <?php echo menuIsActive('term_laboratorium_patologi_anatomi_permintaan'); ?> href="{base_url}term_laboratorium_patologi_anatomi_permintaan"><i class="glyphicon glyphicon-option-horizontal"></i> Permintaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2127'])) { ?>
								<li>
										<a <?=menuIsActive('term_laboratorium_patologi_anatomi_hasil')?> href="{base_url}term_laboratorium_patologi_anatomi_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Input Hasil</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_laboratorium_bank_darah)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Bank Darah</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2128'])) { ?>
								<li>
									<a <?php echo menuIsActive('term_laboratorium_bank_darah_permintaan'); ?> href="{base_url}term_laboratorium_bank_darah_permintaan"><i class="glyphicon glyphicon-option-horizontal"></i> Permintaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2129'])) { ?>
								<li>
										<a <?=menuIsActive('term_laboratorium_bank_darah_hasil')?> href="{base_url}term_laboratorium_bank_darah_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Input Hasil</a>
								</li>
								<?php } ?>
						</ul>
				</li>
		</ul>
</li>
<?php } ?>

<?php
	$menu_radiologi_referensi = ['merm_referensi'];
	$menu_radiologi_master = ['mpemeriksaan_radiologi'];
	$menu_radiologi_pengaturan = [
		'mpengaturan_tujuan_radiologi', 'mpengaturan_form_order_radiologi_xray', 
		'mpengaturan_form_order_radiologi_usg', 'mpengaturan_form_order_radiologi_ctscan', 
		'mpengaturan_form_order_radiologi_mri', 'mpengaturan_form_order_radiologi_lainnya', 
		'mpengaturan_printout_radiologi', 'mpengaturan_direct_poliklinik_radiologi', 
		'mpengaturan_penginputan_ekpertise_radiologi', 'mpengaturan_pengiriman_email_radiologi', 
		'mpengaturan_pengiriman_email_hasil_upload_radiologi', 'mpengaturan_printout_radiologi_ekspertis'
	];
	
	$menu_radiologi_xray = ['term_radiologi_xray_permintaan', 'term_radiologi_xray_hasil', 'term_radiologi_xray_expertise'];
	$menu_radiologi_usg = ['term_radiologi_usg_permintaan', 'term_radiologi_usg_hasil', 'term_radiologi_usg_expertise'];
	$menu_radiologi_ctscan = ['term_radiologi_ctscan_permintaan', 'term_radiologi_ctscan_hasil', 'term_radiologi_ctscan_expertise'];
	$menu_radiologi_mri = ['term_radiologi_mri_permintaan', 'term_radiologi_mri_hasil', 'term_radiologi_mri_expertise'];
	$menu_radiologi_lainnya = ['term_radiologi_lainnya_permintaan', 'term_radiologi_lainnya_hasil', 'term_radiologi_lainnya_expertise'];
	
	$menu_radiologi = array_merge($menu_radiologi_referensi, $menu_radiologi_master, $menu_radiologi_pengaturan, $menu_radiologi_xray, $menu_radiologi_usg, $menu_radiologi_ctscan, $menu_radiologi_mri, $menu_radiologi_lainnya);
?>

<? if (in_array('17', $user_acces_menu, false) == '1') {?>
<li <?=(in_array($menu, $menu_radiologi)) ? 'class="open"' : ''; ?>>
		<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-connectdevelop"></i><span class="sidebar-mini-hide">Radiologi</span></a>
		<ul>  
				<li <?=(in_array($menu, $menu_radiologi_referensi)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Referensi</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2133'])) { ?>
								<li>
										<a <?php echo menuIsActive('merm_referensi'); ?> href="{base_url}merm_referensi/index/245"><i class="glyphicon glyphicon-option-horizontal"></i> Tipe Pemeriksaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2134'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/246"><i class="glyphicon glyphicon-option-horizontal"></i> Jenis Pemeriksaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2135'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/247"><i class="glyphicon glyphicon-option-horizontal"></i> Posisi Pemeriksaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2136'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/248"><i class="glyphicon glyphicon-option-horizontal"></i> Anatomi Tubuh</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2137'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/249"><i class="glyphicon glyphicon-option-horizontal"></i> Prioritas Pemeriksaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2138'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/250"><i class="glyphicon glyphicon-option-horizontal"></i> Status Puasa Pasien</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2139'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/251"><i class="glyphicon glyphicon-option-horizontal"></i> Pengiriman Hasil</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2140'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/252"><i class="glyphicon glyphicon-option-horizontal"></i> Alergi Bahan Kontras</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2141'])) { ?>
								<li>
										<a <?=menuIsActive('merm_referensi')?> href="{base_url}merm_referensi/index/253"><i class="glyphicon glyphicon-option-horizontal"></i> Kehamilan Radiologi</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_radiologi_master)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Master Data</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2142'])) { ?>
								<li>
										<a <?=menuIsActive('mpemeriksaan_radiologi')?> href="{base_url}mpemeriksaan_radiologi"><i class="glyphicon glyphicon-option-horizontal"></i> Master Pemeriksaan</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_radiologi_pengaturan)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pengaturan</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2143'])) { ?>
								<li>
										<a <?php echo menuIsActive('mpengaturan_tujuan_radiologi'); ?> href="{base_url}mpengaturan_tujuan_radiologi"><i class="glyphicon glyphicon-option-horizontal"></i> Tujuan Radiologi</a>
								</li>
								<?php } ?>
								<li>
										<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Form Order Radiologi</span></a>
										<ul>
												<?php if (UserAccesForm($user_acces_form, ['2144'])) { ?>
												<li>
														<a <?php echo menuIsActive('mpengaturan_form_order_radiologi_xray'); ?> href="{base_url}mpengaturan_form_order_radiologi_xray"><i class="glyphicon glyphicon-option-horizontal"></i> X-Ray</a>
												</li>
												<?php } ?>
												<?php if (UserAccesForm($user_acces_form, ['2145'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_form_order_radiologi_usg')?> href="{base_url}mpengaturan_form_order_radiologi_usg"><i class="glyphicon glyphicon-option-horizontal"></i> USG</a>
												</li>
												<?php } ?>
												<?php if (UserAccesForm($user_acces_form, ['2146'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_form_order_radiologi_ctscan')?> href="{base_url}mpengaturan_form_order_radiologi_ctscan"><i class="glyphicon glyphicon-option-horizontal"></i> CT-Scan</a>
												</li>
												<?php } ?>
												<?php if (UserAccesForm($user_acces_form, ['2147'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_form_order_radiologi_mri')?> href="{base_url}mpengaturan_form_order_radiologi_mri"><i class="glyphicon glyphicon-option-horizontal"></i> MRI</a>
												</li>
												<?php } ?>
												<?php if (UserAccesForm($user_acces_form, ['2148'])) { ?>
												<li>
														<a <?=menuIsActive('mpengaturan_form_order_radiologi_lainnya')?> href="{base_url}mpengaturan_form_order_radiologi_lainnya"><i class="glyphicon glyphicon-option-horizontal"></i> Lain-Lain</a>
												</li>
												<?php } ?>
										</ul>
									</li>
									<?php if (UserAccesForm($user_acces_form, ['2149'])) { ?>
									<li>
											<a <?php echo menuIsActive('mpengaturan_printout_radiologi'); ?> href="{base_url}mpengaturan_printout_radiologi"><i class="glyphicon glyphicon-option-horizontal"></i> Print Out Order Radiologi</a>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['2151'])) { ?>
									<li>
										<a <?=menuIsActive('mpengaturan_direct_poliklinik_radiologi')?> href="{base_url}mpengaturan_direct_poliklinik_radiologi"><i class="glyphicon glyphicon-option-horizontal"></i> Poliklnik Direct</a>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['2152'])) { ?>
									<li>
										<a <?=menuIsActive('mpengaturan_penginputan_ekpertise_radiologi')?> href="{base_url}mpengaturan_penginputan_ekpertise_radiologi"><i class="glyphicon glyphicon-option-horizontal"></i> Penginputan Ekspertise</a>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['2153'])) { ?>
									<li>
										<a <?=menuIsActive('mpengaturan_pengiriman_email_radiologi')?> href="{base_url}mpengaturan_pengiriman_email_radiologi"><i class="glyphicon glyphicon-option-horizontal"></i> Pegiriman E-mail</a>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['2153'])) { ?>
									<li>
										<a <?=menuIsActive('mpengaturan_pengiriman_email_hasil_upload_radiologi')?> href="{base_url}mpengaturan_pengiriman_email_hasil_upload_radiologi"><i class="glyphicon glyphicon-option-horizontal"></i> Pegiriman Hasil Upload</a>
									</li>
									<?php } ?>
									<?php if (UserAccesForm($user_acces_form, ['2154'])) { ?>
									<li>
											<a <?=menuIsActive('mpengaturan_printout_radiologi_ekspertise')?> href="{base_url}mpengaturan_printout_radiologi_ekspertise"><i class="glyphicon glyphicon-option-horizontal"></i> Print Out Ekspertise</a>
									</li>
									<?php } ?>
								</li>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_radiologi_xray)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">X-Ray</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2155'])) { ?>
								<li>
									<a <?php echo menuIsActive('term_radiologi_xray_permintaan'); ?> href="{base_url}term_radiologi_xray_permintaan"><i class="glyphicon glyphicon-option-horizontal"></i> Permintaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2156'])) { ?>
								<li>
									<a <?=menuIsActive('term_radiologi_xray_hasil')?> href="{base_url}term_radiologi_xray_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Upload Hasil</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2157'])) { ?>
								<li>
										<a <?=menuIsActive('term_radiologi_xray_expertise')?> href="{base_url}term_radiologi_xray_expertise"><i class="glyphicon glyphicon-option-horizontal"></i> Ekspertise</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_radiologi_usg)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">USG</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2158'])) { ?>
								<li>
									<a <?=menuIsActive('term_radiologi_usg_permintaan')?> href="{base_url}term_radiologi_usg_permintaan"><i class="glyphicon glyphicon-option-horizontal"></i> Permintaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2159'])) { ?>
								<li>
									<a <?=menuIsActive('term_radiologi_usg_hasil')?> href="{base_url}term_radiologi_usg_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Upload Hasil</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2160'])) { ?>
								<li>
										<a <?php echo menuIsActive('term_radiologi_usg_expertise'); ?> href="{base_url}term_radiologi_usg_expertise"><i class="glyphicon glyphicon-option-horizontal"></i> Ekspertise</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_radiologi_ctscan)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">CT-Scan</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2161'])) { ?>
								<li>
									<a <?php echo menuIsActive('term_radiologi_ctscan_permintaan'); ?> href="{base_url}term_radiologi_ctscan_permintaan"><i class="glyphicon glyphicon-option-horizontal"></i> Permintaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2162'])) { ?>
								<li>
									<a <?=menuIsActive('term_radiologi_ctscan_hasil')?> href="{base_url}term_radiologi_ctscan_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Upload Hasil</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2163'])) { ?>
								<li>
										<a <?=menuIsActive('term_radiologi_ctscan_expertise')?> href="{base_url}term_radiologi_ctscan_expertise"><i class="glyphicon glyphicon-option-horizontal"></i> Ekspertise</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_radiologi_mri)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">MRI</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2164'])) { ?>
								<li>
									<a <?=menuIsActive('term_radiologi_mri_permintaan')?> href="{base_url}term_radiologi_mri_permintaan"><i class="glyphicon glyphicon-option-horizontal"></i> Permintaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2165'])) { ?>
								<li>
									<a <?=menuIsActive('term_radiologi_mri_hasil')?> href="{base_url}term_radiologi_mri_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Upload Hasil</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2166'])) { ?>
								<li>
										<a <?=menuIsActive('term_radiologi_mri_expertise')?> href="{base_url}term_radiologi_mri_expertise"><i class="glyphicon glyphicon-option-horizontal"></i> Ekspertise</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li <?=(in_array($menu, $menu_radiologi_lainnya)) ? 'class="open"' : ''; ?>>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Lain-Lain</span></a>
						<ul>
								<?php if (UserAccesForm($user_acces_form, ['2167'])) { ?>
								<li>
									<a <?php echo menuIsActive('term_radiologi_lainnya_permintaan'); ?> href="{base_url}term_radiologi_lainnya_permintaan"><i class="glyphicon glyphicon-option-horizontal"></i> Permintaan</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2168'])) { ?>
								<li>
									<a <?=menuIsActive('term_radiologi_lainnya_hasil')?> href="{base_url}term_radiologi_lainnya_hasil"><i class="glyphicon glyphicon-option-horizontal"></i> Upload Hasil</a>
								</li>
								<?php } ?>
								<?php if (UserAccesForm($user_acces_form, ['2169'])) { ?>
								<li>
										<a <?=menuIsActive('term_radiologi_lainnya_expertise')?> href="{base_url}term_radiologi_lainnya_expertise"><i class="glyphicon glyphicon-option-horizontal"></i> Ekspertise</a>
								</li>
								<?php } ?>
						</ul>
				</li>
				<li>
						<a <?=menuIsActive('dashboard_radiologi')?> href="{base_url}dashboard_radiologi"><i class="glyphicon glyphicon-option-horizontal"></i> Dashboard Radiologi</a>
				</li>
				<li>
						<a <?=menuIsActive('dashboard_dokter')?> href="{base_url}dashboard_dokter"><i class="glyphicon glyphicon-option-horizontal"></i> Dashboard Dokter</a>
				</li>
		</ul>
</li> 
<?php } ?>

<? if (in_array('18', $user_acces_menu, false) == '1') {?>
<li <?=(in_array($menu, ['setting_komunikasi','tkomunikasi'])) ? 'class="open"' : ''; ?>>
		<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-heart"></i><span class="sidebar-mini-hide">Komunikasi Efektif</span></a>
		<ul>  
				<?php if (UserAccesForm($user_acces_form, ['2183','2185','2186'])) { ?>
				<li>
					<a <?=menuIsActive('setting_komunikasi')?> href="{base_url}setting_komunikasi"><i class="glyphicon glyphicon-option-horizontal"></i> Setting Komunikasi Efektif</a>
				</li>
				<?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['2188'])) { ?>
				<li>
						<a <?=menuIsActive('tkomunikasi')?> href="{base_url}tkomunikasi"><i class="glyphicon glyphicon-option-horizontal"></i> Verifikasi</a>
				</li>
				<?php } ?>
				
		</ul>
</li>
<?php } ?>
<? if (in_array('17', $user_acces_menu, false) == '1') {?>
<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
		<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-heart"></i><span class="sidebar-mini-hide">Satu Sehat</span></a>
		<ul>  
				<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">FHIR Resource</span></a>
					<ul>
						<li>
							<a <?=menuIsActive('SatuSehat/practitioner')?> href="{base_url}SatuSehat/practitioner"><i class="glyphicon glyphicon-option-horizontal"></i> Practitioner</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/organization')?> href="{base_url}SatuSehat/organization"><i class="glyphicon glyphicon-option-horizontal"></i> Organization</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/location')?> href="{base_url}SatuSehat/location"><i class="glyphicon glyphicon-option-horizontal"></i> Location</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/encounter')?> href="{base_url}SatuSehat/encounter"><i class="glyphicon glyphicon-option-horizontal"></i> Encounter</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/condition')?> href="{base_url}SatuSehat/condition"><i class="glyphicon glyphicon-option-horizontal"></i> Condition</a>
						</li>
						<!-- <li>
							<a <?=menuIsActive('SatuSehat/observation_ttv')?> href="{base_url}SatuSehat/observation_ttv"><i class="glyphicon glyphicon-option-horizontal"></i> Observation - TTV</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/composition')?> href="{base_url}SatuSehat/composition"><i class="glyphicon glyphicon-option-horizontal"></i> Composition</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/procedure')?> href="{base_url}SatuSehat/procedure"><i class="glyphicon glyphicon-option-horizontal"></i> Procedure</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/medication')?> href="{base_url}SatuSehat/medication"><i class="glyphicon glyphicon-option-horizontal"></i> Medication</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/medication_request')?> href="{base_url}SatuSehat/medication_request"><i class="glyphicon glyphicon-option-horizontal"></i> Medication Request</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/medication_dispense')?> href="{base_url}SatuSehat/medication_dispense"><i class="glyphicon glyphicon-option-horizontal"></i> Medication Dispense</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/diagnostic_report')?> href="{base_url}SatuSehat/diagnostic_report"><i class="glyphicon glyphicon-option-horizontal"></i> Diagnostic Report</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/allergy_intolerance')?> href="{base_url}SatuSehat/allergy_intolerance"><i class="glyphicon glyphicon-option-horizontal"></i> Allergy Intolerance</a>
						</li> -->
						<li>
							<a <?=menuIsActive('SatuSehat/patient')?> href="{base_url}SatuSehat/patient"><i class="glyphicon glyphicon-option-horizontal"></i> Patient</a>
						</li>
					</ul>
				</li>
				<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
					<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pelayanan - Rawat Jalan</span></a>
					<ul>
						<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
							<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Organisasi</span></a>
							<ul>
								<li>
									<a <?=menuIsActive('SatuSehat/organization_ukp')?> href="{base_url}SatuSehat/organization_ukp"><i class="glyphicon glyphicon-option-horizontal"></i> UKP</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/organization_poli')?> href="{base_url}SatuSehat/organization_poli"><i class="glyphicon glyphicon-option-horizontal"></i> Poli</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/organization_farmasi')?> href="{base_url}SatuSehat/organization_farmasi"><i class="glyphicon glyphicon-option-horizontal"></i> Farmasi / Apotek</a>
								</li>
							</ul>
						</li>
						<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
							<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Lokasi</span></a>
							<ul>
								<li>
									<a <?=menuIsActive('SatuSehat/location_poli')?> href="{base_url}SatuSehat/location_poli"><i class="glyphicon glyphicon-option-horizontal"></i> Poli</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/location_farmasi')?> href="{base_url}SatuSehat/location_farmasi"><i class="glyphicon glyphicon-option-horizontal"></i> Farmasi / Apotek</a>
								</li>
							</ul>
						</li>
						<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
							<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pasien & Nakes</span></a>
							<ul>
								<li>
									<a <?=menuIsActive('SatuSehat/patient')?> href="{base_url}SatuSehat/patient"><i class="glyphicon glyphicon-option-horizontal"></i> Patient</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/practitioner')?> href="{base_url}SatuSehat/practitioner"><i class="glyphicon glyphicon-option-horizontal"></i> Practitioneer</a>
								</li>
							</ul>
						</li>
						<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
							<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pendaftaran Kunjungan</span></a>
							<ul>
								<li>
									<a <?=menuIsActive('SatuSehat/encounter')?> href="{base_url}SatuSehat/encounter"><i class="glyphicon glyphicon-option-horizontal"></i> Encounter Kunjungan</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/encounter')?> href="{base_url}SatuSehat/encounter"><i class="glyphicon glyphicon-option-horizontal"></i> Encounter Masuk Ruang</a>
								</li>
							</ul>
						</li>
						<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
							<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Anamnesis</span></a>
							<ul>
								<li>
									<a <?=menuIsActive('SatuSehat/condition')?> href="{base_url}SatuSehat/condition"><i class="glyphicon glyphicon-option-horizontal"></i> Riwayat Penyakit</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/allergy_intolerance')?> href="{base_url}SatuSehat/allergy_intolerance"><i class="glyphicon glyphicon-option-horizontal"></i> Riwayat Alergi</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/medication')?> href="{base_url}SatuSehat/medication"><i class="glyphicon glyphicon-option-horizontal"></i> Riwayat Pengobatan</a>
								</li>
							</ul>
						</li>
						<li <?=(in_array($menu, [])) ? 'class="open"' : ''; ?>>
							<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Pemeriksaan Fisik</span></a>
							<ul>
								<li>
									<a <?=menuIsActive('SatuSehat/observation')?> href="{base_url}SatuSehat/observation"><i class="glyphicon glyphicon-option-horizontal"></i> Tanda Vital</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/observation')?> href="{base_url}SatuSehat/observation"><i class="glyphicon glyphicon-option-horizontal"></i> Tingkat Kesadaran</a>
								</li>
								<li>
									<a <?=menuIsActive('SatuSehat/observation')?> href="{base_url}SatuSehat/observation"><i class="glyphicon glyphicon-option-horizontal"></i> Antropometri</a>
								</li>
							</ul>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/care_plan')?> href="{base_url}SatuSehat/care_plan"><i class="glyphicon glyphicon-option-horizontal"></i> Rencana Rawat Pasien</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/condition_final')?> href="{base_url}SatuSehat/condition_final"><i class="glyphicon glyphicon-option-horizontal"></i> Kondisi Akhir</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/condition_return')?> href="{base_url}SatuSehat/condition_return"><i class="glyphicon glyphicon-option-horizontal"></i> Pasien Pulang</a>
						</li>
						<li>
							<a <?=menuIsActive('SatuSehat/bundle_rawat_jalan')?> href="{base_url}SatuSehat/bundle_rawat_jalan"><i class="glyphicon glyphicon-option-horizontal"></i> Bundle Rawat Jalan</a>
						</li>
					</ul>
				</li>
		</ul>
</li>
<?php } ?>
