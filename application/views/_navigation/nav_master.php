<?php
	$menuMasterDokter = [
		'mdokter', 'mpengajuan_skd', 'msetting_tarif_pengajuan'
	];

	$menuMasterRekamMedis = [
		'mkelompok_diagnosa', 'mlokasi_tubuh', 'mkelompok_tindakan', 'mkontrol', 'msebab_luar', 'mtindakan2icd', 'mlembaran_rm', 'msetting_analisa'
	];

	$menuMasterPasien = [
		'mpasien', 'mpasien_title', 'mpasien_asal', 'mpasien_kelompok'
	];

	$menuMasterBarang = [
		'mdata_alkes', 'mdata_implan', 'mdata_implan_new', 'mdata_obat', 'mdata_logistik', 'mdata_obat_new','mdata_alkes_new', 'mdata_obat_alkes_default', 'mracikan_des'
	];

	$menuMasterTarifPelayanan = [
		'mtarif_administrasi', 'mtarif_rawatjalan', 'mtarif_rawatinap', 'mtarif_visitedokter',
		'mtarif_laboratorium', 'mtarif_fisioterapi', 'mtarif_radiologi', 'mtarif_radiologi_expose',
		'mtarif_radiologi_film', 'mtarif_bpjskesehatan', 'mtarif_bpjstenagakerja', 'mtarif_operasi',
		'mtarif_sewaalat', 'mjenis_operasi', 'mkelompok_operasi'
	];

	$menuMasterTarifPelayananRanap = [
		'mtarif_visitedokter', 'mtarif_rawatinap'
	];

	$menuMasterTarifPelayananRadiologi = [
		'mtarif_radiologi', 'mtarif_radiologi_expose', 'mtarif_radiologi_film'
	];

	$menuMasterTarifPelayananOperasi = [
		'mtarif_operasi', 'mtarif_sewaalat', 'mkelompok_operasi', 'mjenis_operasi'
	];

	$menuMasterTarifRuangan = [
		'mtarif_ruangperawatan','mtarif_harga_kamar'
	];

	$menuMasterLokasi = [
		'mruangan', 'mkelas', 'mbed','mfasilitas_bed', 'mrak', 'mlokasi_ruangan'
	];

	$menuMaster = array_merge(
		[
			'mpegawai', 'mrekanan', 'mpoliklinik', 'mrumahsakit', 'mdistributor', 'mdata_kategori', 'munitpelayanan', 'msatuan', 'mnilainormal', 'mbank', 'malasan_batal'
		],
		$menuMasterDokter,
		$menuMasterRekamMedis,
		$menuMasterPasien,
		$menuMasterBarang,
		$menuMasterTarifPelayanan,
		$menuMasterTarifPelayananRanap,
		$menuMasterTarifPelayananRadiologi,
		$menuMasterTarifPelayananOperasi,
		$menuMasterTarifRuangan,
		$menuMasterLokasi
	);
?>

<li <?=(in_array($menu, $menuMaster)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span
			class="sidebar-mini-hide">Master Data</span></a>
    <ul>

        <?php if (UserAccesForm($user_acces_form, ['2'])) { ?>
        <li>
            <a <?=menuIsActive('mpegawai')?> href="{base_url}mpegawai"><i class="si si-users"></i><span
					class="sidebar-mini-hide">Pegawai</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['6', '16'])) { ?>
        <li <?=(in_array($menu, $menuMasterDokter)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span
					class="sidebar-mini-hide">Dokter</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['6'])) { ?>
                <li>
                    <a <?=menuIsActive('mdokter')?> href="{base_url}mdokter"><i
							class="glyphicon glyphicon-option-horizontal"></i>Data Dokter</a>
                </li>
                <?php } ?>
                <li>
                    <a <?=menuIsActive('mpengajuan_skd')?> href="{base_url}mpengajuan_skd"><i
							class="glyphicon glyphicon-option-horizontal"></i>Pengajuan SKD</a>
                </li>
                <li>
                    <a <?=menuIsActive('msetting_tarif_pengajuan')?> href="{base_url}msetting_tarif_pengajuan"><i
							class="glyphicon glyphicon-option-horizontal"></i>Setting Tarif Pengajuan</a>
                </li>
            </ul>
        </li>
        <?php } ?>
		
		
        <?php if (UserAccesForm($user_acces_form, ['287', '292', '297', '302'])) { ?>
        <li <?=(in_array($menu, $menuMasterRekamMedis)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-object-group"></i><span
					class="sidebar-mini-hide">Rekam Medis</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['287'])) { ?>
                <li>
                    <a <?=menuIsActive('mkelompok_diagnosa')?> href="{base_url}mkelompok_diagnosa"><i
							class="glyphicon glyphicon-option-horizontal"></i>Kel. Diagnosa</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['292'])) { ?>
                <li>
                    <a <?=menuIsActive('mkelompok_tindakan')?> href="{base_url}mkelompok_tindakan"><i
							class="glyphicon glyphicon-option-horizontal"></i>Kel. Tindakan</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['297'])) { ?>
                <li>
                    <a <?=menuIsActive('mlokasi_tubuh')?> href="{base_url}mlokasi_tubuh"><i
							class="glyphicon glyphicon-option-horizontal"></i>Lokasi Tubuh</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['302'])) { ?>
                <li>
                    <a <?=menuIsActive('mkontrol')?> href="{base_url}mkontrol"><i
							class="glyphicon glyphicon-option-horizontal"></i>Master Kontrol</a>
                </li>
                <?php } ?>
                <li>
                    <a <?=menuIsActive('mlembaran_rm')?> href="{base_url}mlembaran_rm"><i
							class="glyphicon glyphicon-option-horizontal"></i>Master Lembaran</a>
                </li>
                <li>
                    <a <?=menuIsActive('msetting_analisa')?> href="{base_url}msetting_analisa"><i
							class="glyphicon glyphicon-option-horizontal"></i>Setting Analisa</a>
                </li>
                <?php if (UserAccesForm($user_acces_form, ['307'])) { ?>
                <li>
                    <a <?=menuIsActive('msebab_luar')?> href="{base_url}msebab_luar"><i
							class="glyphicon glyphicon-option-horizontal"></i>External Cause</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['312'])) { ?>
                <li>
                    <a <?=menuIsActive('mtindakan2icd')?> href="{base_url}mtindakan2icd"><i
							class="glyphicon glyphicon-option-horizontal"></i>Tindakan To ICD</a>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['19', '22', '24', '25'])) { ?>
        <li <?=(in_array($menu, $menuMasterPasien)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span
					class="sidebar-mini-hide">Pasien</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['19'])) { ?>
                <li>
                    <a <?=menuIsActive('mpasien')?> href="{base_url}mpasien"><i
							class="glyphicon glyphicon-option-horizontal"></i>Data Pasien</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['22'])) { ?>
                <li>
                    <a <?=menuIsActive('mpasien_title')?> href="{base_url}mpasien_title"><i
							class="glyphicon glyphicon-option-horizontal"></i>Title Pasien</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['24'])) { ?>
                <li>
                    <a <?=menuIsActive('mpasien_asal')?> href="{base_url}mpasien_asal"><i
							class="glyphicon glyphicon-option-horizontal"></i>Asal Pasien</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['25'])) { ?>
                <li>
                    <a <?=menuIsActive('mpasien_kelompok')?> href="{base_url}mpasien_kelompok"><i
							class="glyphicon glyphicon-option-horizontal"></i>Kelompok Pasien</a>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['27'])) { ?>
        <li>
            <a <?=menuIsActive('mrumahsakit')?> href="{base_url}mrumahsakit/index"><i class="si si-home"></i><span
					class="sidebar-mini-hide">RS & Klinik</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['32'])) { ?>
        <li>
            <a <?=menuIsActive('mrekanan')?> href="{base_url}mrekanan"><i class="si si-briefcase"></i><span
					class="sidebar-mini-hide">Rekanan Asuransi</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['39'])) { ?>
        <li>
            <a <?=menuIsActive('mpoliklinik')?> href="{base_url}mpoliklinik/index"><i class="si si-support"></i><span
					class="sidebar-mini-hide">Poliklinik</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['44'])) { ?>
        <li>
            <a <?=menuIsActive('mdistributor')?> href="{base_url}mdistributor"><i class="si si-bag"></i><span
					class="sidebar-mini-hide">Distributor</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['48'])) { ?>
        <li>
            <a <?=menuIsActive('mdata_kategori')?> href="{base_url}mdata_kategori/index"><i
					class="si si-drawer"></i><span class="sidebar-mini-hide">Kategori Barang</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['85', '96', '107', '118'])) { ?>
        <li <?=(in_array($menu, $menuMasterBarang)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-social-dropbox"></i><span
					class="sidebar-mini-hide">Barang</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['85'])) { ?>
                <li>
                    <a <?=menuIsActive('mdata_alkes')?> href="{base_url}mdata_alkes"><i
							class="glyphicon glyphicon-option-horizontal"></i>Alat Kesehatan</a>
                </li>
                <?php } ?>
				
                <?php if (UserAccesForm($user_acces_form, ['96'])) { ?>
                <li>
                    <a <?=menuIsActive('mdata_implan')?> href="{base_url}mdata_implan"><i
							class="glyphicon glyphicon-option-horizontal"></i>Implan</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['107'])) { ?>
                <li>
                    <a <?=menuIsActive('mdata_obat')?> href="{base_url}mdata_obat"><i
							class="glyphicon glyphicon-option-horizontal"></i>Obat</a>
                </li>
                <?php } ?>
				
                <?php if (UserAccesForm($user_acces_form, ['118'])) { ?>
                <li>
                    <a <?=menuIsActive('mdata_logistik')?> href="{base_url}mdata_logistik"><i
							class="glyphicon glyphicon-option-horizontal"></i>Logistik</a>
                </li>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['85'])) { ?>
                <li>
                    <a <?=menuIsActive('mdata_alkes_new')?> href="{base_url}mdata_alkes_new"><i
							class="glyphicon glyphicon-option-horizontal"></i>Alat Kesehatan <span class="badge badge-success pull-right">New</span></a>
                </li>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['96'])) { ?>
                <li>
                    <a <?=menuIsActive('mdata_implan_new')?> href="{base_url}mdata_implan_new"><i
							class="glyphicon glyphicon-option-horizontal"></i>Implan <span class="badge badge-success pull-right">New</span></a>
                </li>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['107'])) { ?>
                <li>
                    <a <?=menuIsActive('mdata_obat_new')?> href="{base_url}mdata_obat_new"><i
							class="glyphicon glyphicon-option-horizontal"></i>Obat <span class="badge badge-success pull-right">New</span></a>
                </li>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['118'])) { ?>
                <li>
                    <a <?=menuIsActive('mdata_obat_alkes_default')?> href="{base_url}mdata_obat_alkes_default"><i
							class="glyphicon glyphicon-option-horizontal"></i>Setting Default [O,A]</a>
                </li>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['2098'])) { ?>
                <li>
                    <a <?=menuIsActive('mracikan_des')?> href="{base_url}mracikan_des"><i
							class="glyphicon glyphicon-option-horizontal"></i>Jenis Racikan Deskripsi</a>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['129'])) { ?>
        <li>
            <a <?=menuIsActive('munitpelayanan')?> href="{base_url}munitpelayanan"><i class="si si-grid"></i><span
					class="sidebar-mini-hide">Unit Pelayanan</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['133', '138', '143', '229', '148', '234', '239', '153', '158', '163', '168', '173', '224', '278', '283']) == '1') { ?>
        <li <?=(in_array($menu, $menuMasterTarifPelayanan)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-list"></i><span
					class="sidebar-mini-hide">Tarif Pelayanan</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['133'])) { ?>
                <li>
                    <a <?=menuIsActive('mtarif_administrasi')?> href="{base_url}mtarif_administrasi"><i
							class="glyphicon glyphicon-option-horizontal"></i>Administrasi</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['138'])) { ?>
                <li>
                    <a <?=menuIsActive('mtarif_rawatjalan')?> href="{base_url}mtarif_rawatjalan"><i
							class="glyphicon glyphicon-option-horizontal"></i>Rawat Jalan</a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['143', '229'])) { ?>
                <li <?=(in_array($menu, $menuMasterTarifPelayananRanap)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
							class="glyphicon glyphicon-option-horizontal"></i><span class="sidebar-mini-hide">Rawat
							Inap</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['143'])) { ?>
                        <li>
                            <a <?=menuIsActive('mtarif_visitedokter')?> href="{base_url}mtarif_visitedokter"><i
									class="glyphicon glyphicon-option-horizontal"></i>Visite Dokter</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['229'])) { ?>
                        <li>
                            <a <?=menuIsActive('mtarif_rawatinap')?> href="{base_url}mtarif_rawatinap"><i
									class="glyphicon glyphicon-option-horizontal"></i>Lain-lain</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['148', '234', '239'])) { ?>
                <li <?=(in_array($menu, $menuMasterTarifPelayananRadiologi)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
							class="glyphicon glyphicon-option-horizontal"></i><span
							class="sidebar-mini-hide">Radiologi</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['148'])) { ?>
                        <li>
                            <a <?=menuIsActive('mtarif_radiologi')?> href="{base_url}mtarif_radiologi"><i
									class="glyphicon glyphicon-option-horizontal"></i>Tarif</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['234'])) { ?>
                        <li>
                            <a <?=menuIsActive('mtarif_radiologi_expose')?> href="{base_url}mtarif_radiologi_expose"><i
									class="glyphicon glyphicon-option-horizontal"></i>Expose</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['239'])) { ?>
                        <li>
                            <a <?=menuIsActive('mtarif_radiologi_film')?> href="{base_url}mtarif_radiologi_film"><i
									class="glyphicon glyphicon-option-horizontal"></i>Film</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['153'])) { ?>
                <li>
                    <a <?=menuIsActive('mtarif_laboratorium')?> href="{base_url}mtarif_laboratorium"><i
							class="glyphicon glyphicon-option-horizontal"></i>Laboratorium</a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['158'])) { ?>
                <li>
                    <a <?=menuIsActive('mtarif_fisioterapi')?> href="{base_url}mtarif_fisioterapi"><i
							class="glyphicon glyphicon-option-horizontal"></i>Fisioterapi</a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['163'])) { ?>
                <li>
                    <a <?=menuIsActive('mtarif_bpjskesehatan')?> href="{base_url}mtarif_bpjskesehatan"><i
							class="glyphicon glyphicon-option-horizontal"></i>BPJS Kesehatan</a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['168'])) { ?>
                <li>
                    <a <?=menuIsActive('mtarif_bpjstenagakerja')?> href="{base_url}mtarif_bpjstenagakerja"><i
							class="glyphicon glyphicon-option-horizontal"></i>BPJS Tenagakerja</a>
                </li>
                <?php } ?>

                <?php if (UserAccesForm($user_acces_form, ['173', '224', '278', '283'])) { ?>
                <li <?=(in_array($menu, $menuMasterTarifPelayananOperasi)) ? 'class="open"' : ''; ?>>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
							class="glyphicon glyphicon-option-horizontal"></i><span
							class="sidebar-mini-hide">Operasi</span></a>
                    <ul>
                        <?php if (UserAccesForm($user_acces_form, ['278'])) { ?>
                        <li>
                            <a <?=menuIsActive('mjenis_operasi')?> href="{base_url}mjenis_operasi"><i
									class="glyphicon glyphicon-option-horizontal"></i>Jenis Operasi</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['283'])) { ?>
                        <li>
                            <a <?=menuIsActive('mkelompok_operasi')?> href="{base_url}mkelompok_operasi"><i
									class="glyphicon glyphicon-option-horizontal"></i>Kelompok Operasi</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['173'])) { ?>
                        <li>
                            <a <?=menuIsActive('mtarif_sewaalat')?> href="{base_url}mtarif_sewaalat"><i
									class="glyphicon glyphicon-option-horizontal"></i>Sewa Alat</a>
                        </li>
                        <?php } ?>
                        <?php if (UserAccesForm($user_acces_form, ['224'])) { ?>
                        <li>
                            <a <?=menuIsActive('mtarif_operasi')?> href="{base_url}mtarif_operasi"><i
									class="glyphicon glyphicon-option-horizontal"></i>Lain-lain</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['178'])) { ?>
        <li <?=(in_array($menu, $menuMasterTarifRuangan)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-list"></i><span
					class="sidebar-mini-hide">Tarif Ruangan</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['178'])) { ?>
                <li>
                    <a <?=menuIsActive('mtarif_ruangperawatan')?> href="{base_url}mtarif_ruangperawatan"><i
							class="glyphicon glyphicon-option-horizontal"></i>Perawatan</a>
                </li>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1885'])) { ?>
                <li>
                    <a <?=menuIsActive('mtarif_harga_kamar')?> href="{base_url}mtarif_harga_kamar"><i
							class="glyphicon glyphicon-option-horizontal"></i>Setting Harga Kamar</a>
                </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['183', '187', '191', '195', '1881'])) { ?>
        <li <?=(in_array($menu, $menuMasterLokasi)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-pointer"></i><span
					class="sidebar-mini-hide">Lokasi</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['183'])) { ?>
                <li>
                    <a <?=menuIsActive('mruangan')?> href="{base_url}mruangan"><i
							class="glyphicon glyphicon-option-horizontal"></i>Ruangan</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['187'])) { ?>
                <li>
                    <a <?=menuIsActive('mkelas')?> href="{base_url}mkelas"><i
							class="glyphicon glyphicon-option-horizontal"></i>Kelas</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['191'])) { ?>
                <li>
                    <a <?=menuIsActive('mbed')?> href="{base_url}mbed/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Bed</a>
                </li>
                <?php } ?>
				<?php if (UserAccesForm($user_acces_form, ['1881'])) { ?>
                <li>
                    <a <?=menuIsActive('mfasilitas_bed')?> href="{base_url}mfasilitas_bed"><i
							class="glyphicon glyphicon-option-horizontal"></i>Fasilitas Bed</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['195'])) { ?>
                <li>
                    <a <?=menuIsActive('mrak')?> href="{base_url}mrak/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Rak</a>
                </li>
                <?php } ?>
                <li>
                    <a <?=menuIsActive('mlokasi_ruangan')?> href="{base_url}mlokasi_ruangan/index"><i
							class="glyphicon glyphicon-option-horizontal"></i>Lokasi Ruangan</a>
                </li>
            </ul>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['199'])) { ?>
        <li>
            <a <?=menuIsActive('msatuan')?> href="{base_url}msatuan"><i class="si si-drop"></i><span
					class="sidebar-mini-hide">Satuan</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['203'])) { ?>
        <li>
            <a <?=menuIsActive('mnilainormal')?> href="{base_url}mnilainormal"><i class="si si-equalizer"></i><span
					class="sidebar-mini-hide">Nilai Normal</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['207'])) { ?>
        <li>
            <a <?=menuIsActive('mbank')?> href="{base_url}mbank"><i class="si si-credit-card"></i><span
					class="sidebar-mini-hide">Bank</a>
        </li>
        <?php } ?>

        <?php if (UserAccesForm($user_acces_form, ['211'])) { ?>
        <li>
            <a <?=menuIsActive('malasan_batal')?> href="{base_url}malasan_batal"><i class="si si-feed"></i><span
					class="sidebar-mini-hide">Alasan Batal</a>
        </li>
        <?php } ?>
		
    </ul>
</li>
