<?php
  $menuSettingBilling = [
  	'mjenis_pendapatan'
  ];
	$menuSetoran = [
  	'tsetoran_rj','tsetoran_ri'
  ];
  $menuadmRanap = [
  	'tbilling_ranap'
  ];

  $menuSetting = array_merge(
  	$menuSettingBilling,$menuSetoran,$menuadmRanap
  );
?>

<li <?=(in_array($menu, $menuSetting)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-credit-card"></i><span class="sidebar-mini-hide">Billing & Payment</span></a>
    <ul>
        <li <?=(in_array($menu, $menuSettingBilling)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span class="sidebar-mini-hide">Setting</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['2566'])) { ?>
                <li>
                    <a <?=menuIsActive('mjenis_pendapatan')?> href="{base_url}mjenis_pendapatan"><i class="glyphicon glyphicon-option-horizontal"></i>Pendapatan & Loss</a>
                </li>
                <?php } ?>
                
            </ul>
        </li>
		<?php if (UserAccesForm($user_acces_form, ['2573', '2593'])) { ?>
		<li <?=(in_array($menu, $menuSetoran)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-money"></i><span class="sidebar-mini-hide">Setoran</span></a>
            <ul>
        <?php if (UserAccesForm($user_acces_form, ['2573'])) { ?>
               <li>
					<a <?=menuIsActive('tsetoran_rj')?> href="{base_url}tsetoran_rj"><i class="glyphicon glyphicon-option-horizontal"></i>Setoran Rawat Jalan</a>
				</li>
		<?php } ?>
        <?php if (UserAccesForm($user_acces_form, ['2593'])) { ?>
				<li>
					<a <?=menuIsActive('tsetoran_ri')?> href="{base_url}tsetoran_ri"><i class="glyphicon glyphicon-option-horizontal"></i>Setoran Rawat Inap</a>
				</li>
		<?php } ?>
                
            </ul>
        </li>
		<?php } ?>
        <?php if (UserAccesForm($user_acces_form, ['2630'])) { ?>
		<li><a <?=menuIsActive('tbilling_ranap')?> href="{base_url}tbilling_ranap"><i class="glyphicon glyphicon-option-horizontal"></i>ADM Ranap & ODS</a></li>
		<?php } ?>
    </ul>
</li>
