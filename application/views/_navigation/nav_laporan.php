<?php
  $menuLaporanGudang = [
    'lgudang_stok', 'lmonitor_stok', 'mpengaturan_laporan_gudang', 'lpenerimaan_gudang'
  ];

  $menuLaporanKasir = [
    'lkasir'
  ];
 $menuLaporanSurvey = [
    'lsurvey'
  ];

  $menuLaporan = array_merge(
    $menuLaporanGudang,
    $menuLaporanKasir,
    $menuLaporanSurvey
  );
?>

<li <?=(in_array($menu, $menuLaporan)) ? 'class="open"' : ''; ?>>
    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bar-chart"></i><span class="sidebar-mini-hide">Laporan</span></a>
    <ul>
        <?php if (UserAccesForm($user_acces_form, ['251', '1369'])) { ?>
        <li <?=(in_array($menu, $menuLaporanGudang)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-social-dropbox"></i><span class="sidebar-mini-hide">Gudang</span></a>
            <ul>
                <?php if (UserAccesForm($user_acces_form, ['1369'])) { ?>
                <li>
                    <a <?=menuIsActive('lmonitor_stok')?> href="{base_url}lmonitor_stok/index"><i class="glyphicon glyphicon-option-horizontal"></i> Monitoring Stok</a>
                </li>
                <?php } ?>
                <?php if (UserAccesForm($user_acces_form, ['251'])) { ?>
                <li>
                    <a <?=menuIsActive('lgudang_stok')?> href="{base_url}lgudang_stok/index/0"><i class="glyphicon glyphicon-option-horizontal"></i>Stok Gudang</a>
                </li>
                <?php } ?>
                <!-- <php if (UserAccesForm($user_acces_form, ['251'])) { ?> -->
                <li>
                    <a <?=menuIsActive('mpengaturan_laporan_gudang')?> href="{base_url}mpengaturan_laporan_gudang"><i class="glyphicon glyphicon-option-horizontal"></i>Pengaturan Laporan</a>
                </li>
                <!-- <php } ?> -->
                <!-- <php if (UserAccesForm($user_acces_form, ['251'])) { ?> -->
                <li>
                    <a <?=menuIsActive('lpenerimaan_gudang')?> href="{base_url}lpenerimaan_gudang"><i class="glyphicon glyphicon-option-horizontal"></i>Penerimaan Gudang</a>
                </li>
                <!-- <php } ?> -->
            </ul>
        </li>
        <?php } ?>
        <?php if (UserAccesForm($user_acces_form, ['1368'])) { ?>
        <li <?=(in_array($menu, $menuLaporanKasir)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-handbag"></i><span class="sidebar-mini-hide">Kasir</span></a>
            <ul>
                <li>
                    <a <?=menuIsActive('lkasir')?> href="{base_url}lkasir"><i class="glyphicon glyphicon-option-horizontal"></i>Laporan Kasir</a>
                </li>
            </ul>
        </li>
        <?php } ?>
		<?php if (UserAccesForm($user_acces_form, ['2376'])) { ?>
        <li <?=(in_array($menu, $menuLaporanSurvey)) ? 'class="open"' : ''; ?>>
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-line-chart"></i><span class="sidebar-mini-hide">Survey</span></a>
            <ul>
                <li>
                    <a <?=menuIsActive2('internal')?> href="{base_url}lsurvey/internal"><i class="glyphicon glyphicon-option-horizontal"></i>Laporan Internal</a>
                    <a <?=menuIsActive2('external')?> href="{base_url}lsurvey/external"><i class="glyphicon glyphicon-option-horizontal"></i>Laporan External</a>
                </li>
            </ul>
        </li>
        <?php } ?>
    </ul>
</li>
