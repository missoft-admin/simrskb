<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="modal fade in black-overlay" id="modal_cari" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Bagi Hasil</h3>
				</div>
				<div class="block-content">
					<div class="form-horizontal">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" for="" style="margin-top: 5px;">Tanggal</label>
							<div class="col-md-4">
								<div class="input-group date">
									<input class="js-datepicker form-control input"  type="text" id="tanggal_trx" name="tanggal_trx" value="" data-date-format="dd-mm-yyyy"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>	
							</div>
							<div class="col-md-6"> <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button></div>
						</div>
					</div>
					<br>
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_trx" style="width: 100%;">
						<thead>
							<tr>
								<th>X</th>
								<th>X</th>
								<th>NO TRANSAKSI</th>
								<th>DESKRIPSI</th>
								<th>TANGGAL</th>
								<th>TOTAL</th>
								<th>NOMINAL RUMAH SAKIT</th>
								<th>NOMINAL PEMILIK SAHAM</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tbagi_hasil_bayar" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tbagi_hasil_bayar/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="form-group" >
				<label class="col-md-2 control-label" for="nama">Pilih Bagi Hasil</label>
				<div class="col-md-10">
					<div class="input-group" >
						<select id="mbagi_hasil_id" name="mbagi_hasil_id" <?=($id!=''?'disabled':'')?>  class="js-select2 form-control" data-placeholder="Choose one.."  width="100%">
							<option value="#" selected>- Bagi Hasil -</option>
							<?foreach($list_bagi_hasil as $row){?>
								<option value="<?=$row->id?>" <?=($row->id==$mbagi_hasil_id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" type="button" onclick="cari_trx()" disabled id="btn_cari" <?=$disabel?> title="Cari"><i class="fa fa-search-plus"></i></button>
						</span>
					</div>	
				</div>
			</div>
			<div class="form-group" >
				<label class="col-md-2 control-label" for="nama"></label>
				<div class="col-md-10">
					<input type="hidden" class="form-control" readonly id="list_trx" placeholder="Nama Bagi Hasil" name="list_trx" value="<?=$list_trx?>">
					<input type="hidden" class="form-control" readonly id="id" placeholder="Nama Bagi Hasil" name="id" value="<?=$id?>">
					<input type="hidden" class="form-control" readonly id="disabel" placeholder="Nama Bagi Hasil" name="disabel" value="<?=$disabel?>">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_hasil" style="width: 100%;">
						<thead>
							<tr>
								<th>X</th>
								<th>X</th>
								<th>NO TRANSAKSI</th>
								<th>DESKRIPSI</th>
								<th>TANGGAL</th>
								<th>TOTAL</th>
								<th>BIAYA</th>
								<th>NOMINAL RUMAH SAKIT</th>
								<th>NOMINAL PEMILIK SAHAM</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					
				</div>
				</div>
			</div>
			<div class="form-group" >
				<label class="col-md-2 control-label" for="nama"></label>
				<div class="col-md-4">
					<button type="button" class="btn btn-primary" <?=$disabel?> id="btn_generate"><i class="fa fa-angle-double-down"></i> Generate</button>
					<button type="button" class="btn btn-danger" <?=$disabel?> id="btn_clear"><i class="fa fa-refresh"></i> Clear Data</button>
					
				</div>
				<div class="col-md-4">
					
				</div>
			</div>
			<div class="form-group" >
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_generate" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%">NO</th>
								<th style="width:15%">NAMA PEMILIK SAHAM</th>
								<th style="width:8%">TIPE</th>
								<th style="width:5%">JUMLAH LEMBAR</th>
								<th style="width:8%">NOMINAL</th>
								<th  style="width:18%">NOREKENING</th>
								<th style="width:15%">METODE</th>
								<th  style="width:15%">TANGGAL BAYAR</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					
				</div>
				</div>
				<div class="col-md-4">
					
				</div>
			</div>
			
		
			<?if ($disabel==''){?>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mbagi_hasil" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?}?>
			<?php echo form_close() ?>
	</div>

</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
arr_id=[];
	$(document).ready(function(){
		var id=$("#id").val();
		var list_trx=$("#list_trx").val();
		if (id !=''){
			arr_id=list_trx.split(",");
			// alert(arr_id	);
			load_generate();
		}
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,2,'.',',');
		// cari_trx();
		load_hasil();
	})	
	function cari_trx(){
		$("#modal_cari").modal('show');
		load_trx();
	}
	
	
	function load_trx() {
		// alert('sini');
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var list_trx=$("#list_trx").val();
		var tanggal_trx=$("#tanggal_trx").val();
		var id=$("#id").val();
		
		$('#index_trx').DataTable().destroy();
		var table = $('#index_trx').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": false,
		"order": [],
		"ajax": {
			url: '{site_url}tbagi_hasil_bayar/load_trx/',
			type: "POST",
			dataType: 'json',
			data: {
				mbagi_hasil_id: mbagi_hasil_id,
				list_trx: list_trx,
				tanggal_trx: tanggal_trx,
				id: id,
				
			}
		},
		columnDefs: [
					{"targets": [0], "visible": false },
					 { "width": "5%", "targets": [1]},
					 { "width": "10%", "targets": [2,3,4,5,6,7,8]},
					 { "targets": [5,6,7],className: "text-right" },
					 { "targets": [2,4],className: "text-center" },
					 // { "width": "40%", "targets": [3],className: "text-left" },
					 // { "width": "20%", "targets": [4],className: "text-left" }
					]
		});
	}
	function load_hasil() {
		// var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var list_trx=$("#list_trx").val();
		var disabel=$("#disabel").val();
		// alert(list_trx);
		
		$('#index_hasil').DataTable().destroy();
		var table = $('#index_hasil').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": false,
		"order": [],
		"ajax": {
			url: '{site_url}tbagi_hasil_bayar/load_hasil/',
			type: "POST",
			dataType: 'json',
			data: {
				// mbagi_hasil_id: mbagi_hasil_id,
				list_trx: list_trx,disabel: disabel,
				// tanggal_trx: tanggal_trx,
				
			}
		},
		columnDefs: [
					{"targets": [0], "visible": false },
					 { "width": "5%", "targets": [1]},
					 { "width": "10%", "targets": [2,3,4,5,6,7,8,9]},
					 { "targets": [5,6,7,8],className: "text-right" },
					 { "targets": [2,4],className: "text-center" },
					 // { "width": "40%", "targets": [3],className: "text-left" },
					 // { "width": "20%", "targets": [4],className: "text-left" }
					]
		});
		clear_generate();
	}
	$("#mbagi_hasil_id").change(function(){
		if ($("#mbagi_hasil_id").val()!='#'){
			$("#btn_cari").attr('disabled',false);
		}else{
			$("#btn_cari").attr('disabled',true);
			
		}
	});
	$("#btn_filter").click(function(){
		load_trx();
	});
	$("#mbagi_hasil_id").change(function(){
		arr_id=[];
		$("#list_trx").val('');
		load_hasil();
	});
	$(document).on("click",".pilih",function(){	
		var table = $('#index_trx').DataTable();
		var table2 = $('#index_hasil').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		arr_id.push(id);
		$(this).closest('td').parent().remove();
		var list_trx=$("#list_trx").val();
		if (list_trx==''){
			list_trx=id;
		}else{
			list_trx +=','+id;
		}
		$("#list_trx").val(list_trx);
		// table2.ajax.reload( null, false );
		// alert(arr_id)
		load_hasil();
		$.toaster({priority : 'success', title : 'Succes!', message : ' ditambahkan'});
	});
	
	$(document).on("click",".hapus",function(){	
		//fruits.indexOf("Apple")
		var table = $('#index_hasil').DataTable();
		// // var table2 = $('#index_hasil').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		delete arr_id[arr_id.indexOf(id)];
		
		// alert(arr_id);
		var list_trx='';
		var str_list='';
		var arrayLength = arr_id.length;
		for (var i = 0; i < arrayLength; i++) {
			if (arr_id[i]!=null){
				console.log(arr_id[i]);
				if (list_trx==''){
					list_trx=arr_id[i];
				}else{
					list_trx +=','+arr_id[i];
				}
			}
				$("#list_trx").val(list_trx);
			//Do something
		}
		
		load_hasil();
		$.toaster({priority : 'success', title : 'Succes!', message : ' Dihapus'});
	});
	$(document).on("click","#btn_generate",function(){	
		generate();
	});
	$(document).on("click","#btn_clear",function(){	
		clear_generate();
	});
	function clear_generate(){
		$('#index_generate tbody').empty();
	}
	function generate(){
		var list_trx=$("#list_trx").val();
		$('#index_generate tbody').empty();
		$.ajax({
			url: '{site_url}tbagi_hasil_bayar/load_generate/',
			dataType: "json",
			type: "POST",
			data: {list_trx: list_trx},
			success: function(data) {
				$('#index_generate').append(data.tabel);
				$(".opsi").select2({dropdownAutoWidth: false});
				$(".tgl").datepicker();
			}
		});
	}
	function load_generate(){
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		$('#index_generate tbody').empty();
		$.ajax({
			url: '{site_url}tbagi_hasil_bayar/load_generate_edit/',
			dataType: "json",
			type: "POST",
			data: {id: id,disabel: disabel},
			success: function(data) {
				$('#index_generate').append(data.tabel);
				$(".opsi").select2({dropdownAutoWidth: false});
				$(".tgl").datepicker();
			}
		});
	}
	function validate_final(){
		
		var st_lolos='1';
		var kuantitaskirim;
		var idmetode;
		var norek;
		var tgl;
		var rowCount = $('#index_generate tr').length;
		if (rowCount<=1){
				sweetAlert("Maaf...", "Generate belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
		$('#index_generate tbody tr').each(function() {
			idmetode=$(this).find(".opsi_metode").val();
			norek=$(this).find(".opsi_norek").val();
			tgl=$(this).find(".tgl").val();
			if (idmetode=='#'){
				sweetAlert("Maaf...", "Ada metode yang belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
			if (tgl==''){
				sweetAlert("Maaf...", "Ada Tanggal yang belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
			if (norek=='#' && (idmetode=='3' || idmetode=='4')){
				sweetAlert("Maaf...", "Ada Bank / Norekening yang belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
			// kuantitaskirim=$(this).closest('tr').find("td:eq(7) input").val();
			// if (kuantitaskirim>0){
				// st_lolos='1'
			// }
		});
		if (st_lolos=='0'){
			return false;
		}else{
			return true;
			
		}
	}
</script>