<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
       
            <a href="{base_url}tbagi_hasil_bayar/create" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
       
			</ul>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="notransaksi" placeholder="No Registrasi" name="notransaksi" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Bagi Hasil</label>
                    <div class="col-md-8">
                        <select id="mbagi_hasil_id" name="mbagi_hasil_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua -</option>
							<?foreach($list_mbagi_hasil as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				
				       
            </div>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Status -</option>
							<option value="1">Draft</option>
							<option value="2">Proses Persetujuan</option>
							<option value="3">Proses Pembayaran</option>
							<option value="4">Selesai</option>
							
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Perhitungan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_tagihan1" name="tanggal_tagihan1" placeholder="From" value="{tanggal_tagihan1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_tagihan2" name="tanggal_tagihan2" placeholder="To" value="{tanggal_tagihan2}"/>
                        </div>
                    </div>
                </div>
               
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">TANGGAL</th>
                    <th width="10%">NO REGISTRASI</th>
                    <th width="10%">BAGI HASIL NAMA</th>
                    <th width="10%">BAGIAN PEMILIK SAHAM</th>
                    <th width="10%">BAGIAN RUMAH SAKIT</th>
                    <th width="10%">STATUS</th>                   
                    <th width="15%" class="text-center">AKSI</th>
                    
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="id_setuju" placeholder="" name="id_setuju" value="">
						<div class="col-md-12">
							<div class="table-responsive">
							<table width="100%" id="tabel_user" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Jika Setuju</th>
										<th>Jika Menolak</th>								
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka" placeholder="" name="idrka" value="">
						<div class="col-md-12">
							<div class="table-responsive">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var notransaksi=$("#notransaksi").val();
	var status=$("#status").val();
	var mbagi_hasil_id=$("#mbagi_hasil_id").val();
	var tanggal_tagihan1=$("#tanggal_tagihan1").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },
							{ "width": "3%", "targets": [1] },
							{ "width": "8%", "targets": [2,3] },
							{ "width": "10%", "targets": [5,6,7] },
							{ "width": "15%", "targets": [3,8] },
						 // {"targets": [8,3,4], className: "text-left" },
						 {"targets": [1,5,6], className: "text-right" },
						 {"targets": [2,3,7], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tbagi_hasil_bayar/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						notransaksi:notransaksi,status:status,
						mbagi_hasil_id:mbagi_hasil_id,tanggal_tagihan1:tanggal_tagihan1,
						tanggal_tagihan2:tanggal_tagihan2
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});
function hapus_header($id){
	var id=$id;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Menghapus Transaksi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tbagi_hasil_bayar/hapus_header/'+id,
			complete: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				table.ajax.reload( null, false ); 
			}
		});
	});
}
function selesaikan($id){
	var id=$id;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Menyelesaikan Transaksi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tbagi_hasil_bayar/selesaikan/'+id,
			complete: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesaikan'});
				table.ajax.reload( null, false ); 
			}
		});
	});
}
function kirim($id){
	var id=$id;
	$("#modal_approval").modal('show');
	
	load_user_approval(id);
	
}
// btn_simpan_approval
$(document).on("click","#btn_simpan_approval",function(){
	var id=$("#id_setuju").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Kirim Ke User Persetujuan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tbagi_hasil_bayar/simpan_proses_peretujuan/'+id,
			complete: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Kirim Data'});
				table.ajax.reload( null, false ); 
			}
		});
	});
});

function load_user_approval($id){
	var id=$id;
	// alert(id)
	$("#id_setuju").val(id);
	$('#tabel_user').DataTable().destroy();
	table=$('#tabel_user').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"searching": false,
			"lengthChange": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tbagi_hasil_bayar/load_user_approval',
				type: "POST",
				dataType: 'json',
				data : {
					id:id,
											
				   }
			},
			"columnDefs": [
				 {  className: "text-right", targets:[0] },
				 {  className: "text-center", targets:[1,2,3] },
				 { "width": "10%", "targets": [0] },
				 { "width": "40%", "targets": [1] },
				 { "width": "25%", "targets": [2,3] },

			]
		});
}
function load_user($id){
	var id=$id;
	$("#modal_user").modal('show');
	
	$.ajax({
		url: '{site_url}tbagi_hasil_approval/list_user/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_user_proses tbody").empty();
			$("#tabel_user_proses tbody").append(data.detail);
		}
	});
}
</script>