<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tbagi_hasil_bayar" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tbagi_hasil_bayar/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="form-group" style="margin-bottom:10px" >
				<label class="col-md-2 control-label" for="nama">Nama Bagi Hasil</label>
				<div class="col-md-10">
					<input type="text" class="form-control" readonly id="nama_bagi_hasil" placeholder="Nama Bagi Hasil" name="nama_bagi_hasil" value="<?=$nama_bagi_hasil?>">
				</div>
			</div>
			<div class="form-group"  style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">No Transaksi</label>
				<div class="col-md-4">
					<input type="text" class="form-control" readonly id="notransaksi" placeholder="Nama Bagi Hasil" name="notransaksi" value="<?=$notransaksi?>">
				</div>
				<label class="col-md-2 control-label" for="nama">Tanggal</label>
				<div class="col-md-4">
					<input type="text" class="form-control" readonly id="tanggal_trx" placeholder="Nama Bagi Hasil" name="tanggal_trx" value="<?=HumanDateShort($tanggal_trx)?>">
					<input type="hidden" class="form-control" readonly id="list_trx" placeholder="Nama Bagi Hasil" name="list_trx" value="<?=$list_trx?>">
					<input type="hidden" class="form-control" readonly id="id" placeholder="Nama Bagi Hasil" name="id" value="<?=$id?>">
					<input type="hidden" class="form-control" readonly id="disabel" placeholder="Nama Bagi Hasil" name="disabel" value="<?=$disabel?>">
				</div>
			</div>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Bagian Rumah sakit</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="pendapatan_bersih_rs" placeholder="Nama Bagi Hasil" name="pendapatan_bersih_rs" value="<?=$pendapatan_bersih_rs?>">
				</div>
				<label class="col-md-2 control-label" for="nama">Bagian Pemilik Saham</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="pendapatan_bersih_ps" placeholder="Nama Bagi Hasil" name="pendapatan_bersih_ps" value="<?=$pendapatan_bersih_ps?>">
					
				</div>
			</div>
			<hr>
			<div class="form-group" >
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_generate" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%">NO</th>
								<th style="width:15%">NAMA PEMILIK SAHAM</th>
								<th style="width:8%">TIPE</th>
								<th style="width:5%">JUMLAH LEMBAR</th>
								<th style="width:8%">NOMINAL</th>
								<th  style="width:18%">NOREKENING</th>
								<th style="width:15%">METODE</th>
								<th  style="width:15%">TANGGAL BAYAR</th>
								<th  style="width:15%">AKSI</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					
				</div>
				</div>
				<div class="col-md-4">
					
				</div>
			</div>
			
		
			
			<?php echo form_close() ?>
	</div>

</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
arr_id=[];
	$(document).ready(function(){
		var id=$("#id").val();
		load_generate();
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,2,'.',',');
		// cari_trx();
	})	
	
	function clear_generate(){
		$('#index_generate tbody').empty();
	}
	
	function load_generate(){
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		$('#index_generate tbody').empty();
		$.ajax({
			url: '{site_url}tbagi_hasil_bayar/load_generate_edit_bayar/',
			dataType: "json",
			type: "POST",
			data: {id: id,disabel: disabel},
			success: function(data) {
				$('#index_generate').append(data.tabel);
				$(".opsi").select2({dropdownAutoWidth: false});
				$(".tgl").datepicker();
				$(".simpan").hide();
			}
		});
	}
	$(document).on("click",".simpan",function(){
		var tr = $(this).closest('tr');
		iddet=tr.find(".iddet").val();
		idmetode=tr.find(".opsi_metode").val();
		norek=tr.find(".opsi_norek").val();
		tgl=tr.find(".tgl").val();
		
		if (idmetode=='#'){
			sweetAlert("Maaf...", " metode yang belum dipilih!", "error");
			st_lolos='0';
			return false;
		}
		if (tgl==''){
			sweetAlert("Maaf...", " Tanggal yang belum dipilih!", "error");
			st_lolos='0';
			return false;
		}
		if (norek=='#' && (idmetode=='3' || idmetode=='4')){
			sweetAlert("Maaf...", " Bank / Norekening yang belum dipilih!", "error");
			st_lolos='0';
			return false;
		}
		$.ajax({
			url: '{site_url}tbagi_hasil_bayar/update_row',
			type: 'POST',
			data: {iddet: iddet,idmetode: idmetode,norek: norek,tgl: tgl},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update Data'});
				tr.find(".simpan").hide();
			}
		});
		
		
	});
	$(document).on("change",".opsi_metode,.opsi_norek,.tgl",function(){
		
		var tr = $(this).closest('tr');
		tr.find(".simpan").show();
		
	});

	function validate_final(){
		
		var st_lolos='1';
		var kuantitaskirim;
		var idmetode;
		var norek;
		var tgl;
		var rowCount = $('#index_generate tr').length;
		if (rowCount<=1){
				sweetAlert("Maaf...", "Generate belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
		$('#index_generate tbody tr').each(function() {
			idmetode=$(this).find(".opsi_metode").val();
			norek=$(this).find(".opsi_norek").val();
			tgl=$(this).find(".tgl").val();
			if (idmetode=='#'){
				sweetAlert("Maaf...", "Ada metode yang belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
			if (tgl==''){
				sweetAlert("Maaf...", "Ada Tanggal yang belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
			if (norek=='#' && (idmetode=='3' || idmetode=='4')){
				sweetAlert("Maaf...", "Ada Bank / Norekening yang belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
			// kuantitaskirim=$(this).closest('tr').find("td:eq(7) input").val();
			// if (kuantitaskirim>0){
				// st_lolos='1'
			// }
		});
		if (st_lolos=='0'){
			return false;
		}else{
			return true;
			
		}
	}
</script>