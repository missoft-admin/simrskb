<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	@page {
		margin-top: 1em;
		margin-left: 1.6em;
		margin-right: 1.5 em;
		margin-bottom: 1em;
	}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		word-wrap: break-word;
		border: 0px solid #6033FF;
		vertical-align:middle;
      }
	  .content-2 td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-notes{
		font-size: 10px !important;
      }
	  .text-normal{
		font-size: 12px !important;
      }
	  .text-upnormal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 18px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.center {
	   display: block;
		  margin-left: auto;
		  margin-right: auto;
		  width: 50%;
	}
	ul {
	  margin:0;
	  padding-left: 12px;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().'/assets/upload/app_setting/'.$logo?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_rs1?><br><?=$telepon_rs?> <?=$fax_rs?><br><?=$web_rs?> Email : <?=$email_rs?></td>
		</tr>
	</table>
	
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_header_ina?></strong><br><i><?=$judul_header_eng?></i></td>
		</tr>
		
	</table>
	<br>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$label_identitas_ina?></strong><br><i><?=$label_identitas_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$no_reg_ina?></strong><br><i><?=$no_reg_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$no_rm_ina?></strong><br><i><?=$no_rm_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$namapasien?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$ttl_ina?></strong><br><i><?=$ttl_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= HumanDateShort($tanggal_lahir)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$umur_ina?></strong><br><i><?=$umur_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$umur?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$jk_ina?></strong><br><i><?=$jk_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=get_nama_ref($jenis_kelamin,1)?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$asal_pasien_ina?></strong><br><i><?=$asal_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= ($st_ranap=='1'?'RAWAT INAP':'POLIKLINIK')?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$detail_ina?></strong><br><i><?=$detail_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$poli?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$dokter_eng?></strong><br><i><?=$dokter_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=($nama_dokter)?></td>
		</tr>
		
		
	</table>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$profile_ina?></strong><br><i><?=$profile_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$profile_nama_ina?></strong><br><i><?=$profile_nama_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$nama_profile?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$ttl_profile_ina?></strong><br><i><?=$ttl_profile_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=HumanDateShort($ttl_profile)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$umur_profile_ina?></strong><br><i><?=$umur_profile_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$umur_profile?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$jk_profile_ina?></strong><br><i><?=$jk_profile_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= get_nama_ref($jk_profile,1)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$pendidikan_profile_ina?></strong><br><i><?=$pendidikan_profile_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=get_nama_ref($pendidikan_profile,13)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$pekerjaan_profile_ina?></strong><br><i><?=$pekerjaan_profile_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=get_nama_ref($pekerjaan_profile,6)?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$mengetahui_rs_ina?></strong><br><i><?=$mengetahui_rs_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= get_nama_ref($mengetahui_rs,420)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$mengetahui_lain_ina?></strong><br><i><?=$mengetahui_lain_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=($lainnya_profile)?></td>
			<td width="12%" class="text-left text-normal"></td>
			<td width="1%" class="text-left text-normal"></td>
			<td width="23%" class="text-left text-normal"></td>
		</tr>
		
		
	</table>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$hasil_survey_ina?></strong><br><i><?=$hasil_survey_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$waktu_survey_ina?></strong><br><i><?=$waktu_survey_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=HumanDateLong($tanggal_survey)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$nama_survey_ina?></strong><br><i><?=$nama_survey_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=($nama_kajian)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$total_nilai_ina?></strong><br><i><?=$total_nilai_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=number_format($total_skor_survey_kepuasan,0)?> (<?=$hasil_penilaian?>)</td>
		</tr>
			<?if ($tipe=='2'){?>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$nilai_perunsur_ina?></strong><br><i><?=$nilai_perunsur_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= ($nilai_per_unsur)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$nrr_perunsur_ina?></strong><br><i><?=$nrr_perunsur_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=($nrr_per_unsur)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nrr_tertimbang_ina?></strong><br><i><?=$nrr_tertimbang_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=($nrr_tertimbang)?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$satuan_kepuasan_ina?></strong><br><i><?=$satuan_kepuasan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= ($kepuasan_mas)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$hasil_survey_ina?></strong><br><i><?=$hasil_survey_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=($flag_nilai_mas.' - '.$hasil_flag)?></td>
			<td width="12%" class="text-left text-normal"></td>
			<td width="1%" class="text-left text-normal"></td>
			<td width="23%" class="text-left text-normal"></td>
		</tr>
<?}?>
		
		
	</table>
	<br>
	<table class="content">
		<tr>
			<th width="5%" class="text-upnormal border-full text-center"><strong>NO</strong><br></th>
			<th width="65%" class="text-upnormal border-full text-center"><strong><?=$pertanyaan_ina?></strong><br><i><?=$pertanyaan_eng?></i></th>
			<th width="30%" class="text-upnormal border-full text-center"><strong><?=$jawaban_ina?></strong><br><i><?=$jawaban_eng?></i></th>
		</tr>
		<? 
			$no=1;
			$total=0;
		foreach($data_list  as $row){ ?>
			<tr>
			<td class="text-normal border-full text-center"><?=$no?></td>
			<td class="text-normal border-full text-left"><?=strip_tags($row->parameter_nama)?></td>
			<td class="text-normal border-full text-center"><?=strtoupper($row->deskripsi_nama)?></td>
			</tr>
		<?
		
		$no ++;
		}?>
		<tr>
			<td colspan="2" class="text-normal border-full text-right text-success"><strong><?=$total_penilaian_ina?></strong><br><i><?=$total_penilaian_eng?></i></td>
			<td class="text-normal border-full text-center"><strong><?=$total_skor_survey_kepuasan?></strong></td>
		</tr>
		
	</table>
	<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?=$kritik_ina?></strong><br><i><?=$kritik_eng?></i></td>
			
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal "><?=($kritik?$kritik:'-')?></td>
			
		</tr>

	</table>
	<br>
	<br>
	<br>
	<table class="content" style="vertical-align: text-top;">
		<tr>
			<td width="70%" class="text-left text-normal "></td>
			<td width="30%" class="text-center text-normal ">
				<strong><?=$responden_ina?></strong><br><i><?=$responden_eng?></i>
			</td>
		</tr>
		<tr>
			<td width="70%" class="text-left text-normal "><?=($notes_footer_ina)?><i><?=($notes_footer_eng)?></i></td>
			<td class="text-center">
			  <img src="<?=$ttd?>" style="width:120px;height:100px">
			</td>
		</tr>
		<tr>
			<td width="70%" class="text-left text-normal "></td>
			<td width="30%" class="text-center text-normal ">
				<?=$nama_profile?>
				
			</td>
		</tr>
		
	</table>
	
	
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="50%" class="text-left text-normal "><strong><?=($judul_footer_ina)?></strong></td>
			<td width="50%" class="text-right text-normal ">
				Create By : 
				<?=get_nama_ppa($created_ppa).' '.HumanDateLong($created_date)?>
			</td>
		</tr>
	</table>
	
	<br>
	</main>
</body>

</html>
