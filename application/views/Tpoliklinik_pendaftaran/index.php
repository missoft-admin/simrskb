<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?php if (UserAccesForm($user_acces_form, ['317'])) { ?>
<div class="block">
    <div class="block-header">
        <?php if (UserAccesForm($user_acces_form, ['318'])) { ?>
        <ul class="block-options">
            <li>
                <a href="{base_url}tpoliklinik_pendaftaran/create" class="btn"><i class="fa fa-plus"></i> Tambah</a>
            </li>
        </ul>
        <?}?>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <hr style="margin-top:0px">
        <div class="row">
            <?php echo form_open('tpoliklinik_pendaftaran/filter', 'class="form-horizontal" id="form-work"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="namapasien" value="{namapasien}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idkelompokpasien">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" selected>Semua Kelompok Pasien</option>
                            <?php foreach ($list_kelompok_pasien as $row) { ?>
                            <option value="<?=$row->id?>" <?=($idkelompokpasien == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokter">Dokter</label>
                    <div class="col-md-8">
                        <select name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" selected>Semua Dokter</option>
                            <?php foreach ($list_dokter as $row) { ?>
                            <option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idstatus">Status</label>
                    <div class="col-md-8">
                        <select name="idstatus" id="idstatus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($idstatus == '#' ? 'selected' : '')?>>Semua Status</option>
                            <option value="0" <?=($idstatus == '0' ? 'selected' : '')?>>Belum ditindak</option>
                            <option value="1" <?=($idstatus == '1' ? 'selected' : '')?>>Sudah ditindak</option>
                            <option value="2" <?=($idstatus == '2' ? 'selected' : '')?>>Tindakan dibatalkan</option>
                            <option value="3" <?=($idstatus == '3' ? 'selected' : '')?>>Pendaftaran dibatalkan</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="idtipe">Tipe</label>
                    <div class="col-md-9">
                        <select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <?php if (UserAccesForm($user_acces_form, ['322'])) { ?>
                            <option value="#" <?=($idtipe == '#' ? 'selected' : '')?>>Semua Tipe</option>
                            <?}?>
                            <?php if (UserAccesForm($user_acces_form, ['1331'])) { ?>
                            <option value="1" <?=($idtipe == '1' ? 'selected' : '')?>>Poliklinik</option>
                            <?}?>
                            <?php if (UserAccesForm($user_acces_form, ['321'])) { ?>
                            <option value="2" <?=($idtipe == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
                            <?}?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="idpoliklinik">Poliklinik</label>
                    <div class="col-md-9">
                        <select name="idpoliklinik" id="idpoliklinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="#" <?=($idpoliklinik == '#' ? 'selected' : '')?>>Semua Poliklinik</option>
                            <?php foreach ($list_poli as $row) { ?>
                            <option value="<?=$row->id?>" <?=($idpoliklinik == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="tanggaldaftar">Tanggal Daftar</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control js-datepicker" name="tanggaldaftar" data-date-format="dd/mm/yyyy" value="{tanggaldaftar}">
                    </div>
                    <label class="col-md-1 control-label" for="tanggaldaftar2">s/d</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control js-datepicker" name="tanggaldaftar2" data-date-format="dd/mm/yyyy" value="{tanggaldaftar2}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for=""></label>
                    <div class="col-md-9">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>
        <hr>

        <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>No. Pendaftaran</th>
                    <th>Tanggal</th>
                    <th>No. Antrian</th>
                    <th>No. Medrec</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Poliklinik</th>
                    <th>Dokter</th>
                    <th>Kelompok Pasien</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    </div>
</div>
<?}?>

<!-- Modal Action Batal Pendaftaran -->
<div class="modal in" id="alasan-batal-modal" role="dialog" style="text-transform:uppercase;">
    <div class="modal-dialog modal-xs modal-dialog-popout">
        <div class="modal-content">

            <input type="hidden" name="nomedrecdet" id="nomedrecdet" value="">
            <div class="block block-themed remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Alasan Batal</h3>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
                <select name="idalasan" id="idalasan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                    <option value="#" selected>- Alasan -</option>
                    <?php foreach ($list_alasan as $row) { ?>
                    <option value="<?=$row->id?>" <?=($idalasan == $row->id ? 'selected' : '')?>><?=$row->keterangan?></option>
                    <?php } ?>
                </select>
            </div>


            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-sm btn-success" id="alasanbatal" type="submit" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
            </div>

        </div>
    </div>
</div>
<!-- End Modal Action Batal Pendaftaran -->

<!-- Modal Action PRINT -->
<div class="modal in" id="modal-print" role="dialog" style="text-transform:uppercase;">
    <div class="modal-dialog modal-xs modal-dialog-popout">
        <div class="modal-content">
            <input type="hidden" name="nomedrecdet" id="nomedrecdet" value="">
            <div class="block block-themed remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">PRINT</h3>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
                <select id="prints" class="form-control">
                    <option value="1">Pendaftaran</option>
                    <option value="2">Tracer</option>
                    <option value="3">Kartu Status</option>
                    <option value="4">KIB</option>
                    <option value="5">Sticker ID</option>
                </select>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-sm btn-success" id="cetak" type="button" data-dismiss="modal">Tampilkan</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Action END PRINT -->

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}tpoliklinik_pendaftaran/getIndex/' + '<?=$this->uri->segment(2)?>',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 8,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 9,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 10,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 11,
                "orderable": true
            }
        ]
    });
});

$(document).ready(function() {
    var idtipe = "<?=$idtipe?>";
    var idpoliklinik = "<?=($idpoliklinik ? $idpoliklinik : '#')?>";

    $('#alasanbatal').click(function() {
        var idalasan = $("#idalasan").val();
        var idpendaftaran = $("#nomedrecdet").val();
        $.ajax({
            url: "<?= base_url('tpoliklinik_pendaftaran/delete/')?>" + idpendaftaran,
            method: "POST",
            data: {
                idalasan: idalasan,
                idpendaftaran: idpendaftaran
            },
            success: function(data) {
                location.reload();
            }
        });
    });

    // Triger Change Tipe
    $("#idtipe").change(function() {
        var idtipe = $("#idtipe").val();
        $.ajax({
            url: "{site_url}tpoliklinik_pendaftaran/getPoli",
            method: "POST",
            dataType: "json",
            data: {
                "tipepoli": idtipe
            },
            success: function(data) {
                $("#idpoliklinik").empty();
                if (idtipe == 1) {
                    $("#idpoliklinik").append("<option value='#'>Semua Poliklinik</option>");
                } else if (idtipe == 2) {
                    $("#idpoliklinik").append("<option value='#'>Semua Instalasi Gawat Darurat</option>");
                } else {
                    $("#idpoliklinik").append("<option value='#'>Semua Poliklinik</option>");
                }
                for (var i = 0; i < data.length; i++) {
                    $("#idpoliklinik").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
                }
                $("#idpoliklinik").selectpicker('refresh');
            }
        });
    });
});

function myDelete($id) {
    $('#nomedrecdet').val($id);
    $("#alasan-batal-modal").modal();
}
</script>
