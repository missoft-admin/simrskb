<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<!-- Developer : @RendyIchtiarSaputra & @GunaliRezqiMauludi -->
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpoliklinik_pendaftaran/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
<?php echo form_open_multipart('tpoliklinik_pendaftaran/print_e_ticket','class="form-horizontal" id="form-work"') ?>
	<div class="block-content">
		<hr style="margin-top:0px">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="nomedrec">Start Label Ke -</label>
					<div class="col-md-4">
					<select id="start_awal" name="start_awal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
						<?php for ($i=1; $i <= 10; $i++) {?>
							<option value="<?=$i?>" <?=($start_awal == $i ? 'selected' : '')?>><?=$i?></option>
						<?php } ?>
					</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="nomedrec">Jumlah Cetak</label>
					<div class="col-md-4">
					<select id="jml" name="jml" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
						<?php for ($i=1; $i <= 30; $i++) {?>
							<option value="<?=$i?>" <?=($jml == $i ? 'selected' : '')?>><?=$i?></option>
						<?php } ?>
					</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="nomedrec">Margin Kertas</label>
					<div class="col-md-4">
					<select id="margin" name="margin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<option value="kiri" <?=($margin=='kiri'?'selected':'')?>>Rata Kiri</option>
							<option value="tengah" <?=($margin=='tengah'?'selected':'')?>>Rata Tengah</option>
					</select>
					</div>
				</div>
				<input type="hidden" id="idpendaftaran" name="idpendaftaran" value="{idpendaftaran}">
				<input type="hidden" id="sisa" name="sisa" value="10">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-4">
						<button class="btn btn-success text-uppercase" type="submit" id="button" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-print"></i> Cetak E-Ticket</button>
					</div>
				</div>

			</div>


		</div>
		<hr>

	</div>
	<?php echo form_close() ?>
</div>


<!-- End Modal Action END PRINT -->

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">


</script>
