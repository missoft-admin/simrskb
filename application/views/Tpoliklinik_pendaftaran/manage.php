<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?php if (UserAccesForm($user_acces_form, ['318'])) { ?>
<div class="block" style="text-transform:uppercase;">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpoliklinik_pendaftaran/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title_header}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tpoliklinik_pendaftaran/save', 'class="form-horizontal push-10-t" id="form-work"') ?>
		<div class="col-md-6">
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="idtipe">Tipe<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<?php if (UserAccesForm($user_acces_form, ['329']) == true && UserAccesForm($user_acces_form, ['1333']) == true): ?>
							<select tabindex="1" name="idtipe" id="tipepoli" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="1" <?=($idtipe == 1 ? 'selected="selected"' : '')?>>Poliklinik</option>
								<option value="2" <?=($idtipe == 2 ? 'selected="selected"' : '')?>>Instalasi Gawat Darurat</option>
							</select>
					<?php elseif (UserAccesForm($user_acces_form, ['1333'])) : ?>
							<select tabindex="1" name="idtipe" id="tipepoli" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="1" <?=($idtipe == 1 ? 'selected="selected"' : '')?>>Poliklinik</option>
							</select>
					<?php elseif (UserAccesForm($user_acces_form, ['329'])) : ?>
							<select tabindex="1" name="idtipe" id="tipepoli" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="2" <?=($idtipe == 2 ? 'selected="selected"' : '')?>>Instalasi Gawat Darurat</option>
							</select>
					<?php endif ?>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="tglpendaftaran">Tanggal Pendaftaran<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-6">
							<input tabindex="2" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
						</div>
						<div class="col-md-6">
							<input tabindex="3" type="text" class="time-datepicker form-control" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="tglpendaftaran">No Medrec<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<input tabindex="4" readonly type="text" class="form-control" id="no_medrec" placeholder="NO. MEDREC" name="no_medrec" value="{no_medrec}" required>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0px; padding-bottom:0px;">
				<label class="col-md-4 control-label" for="nama">Nama<span style="color:red;">*</span></label>
				<div class="col-md-3" style="padding-right: 10px;">
					<select tabindex="3" id="titlepasien" name="title" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php foreach (get_all('mpasien_title') as $r):?>
							<option value="<?= $r->singkatan; ?>" <?=($title === $r->singkatan ? 'selected="selected"' : '')?>><?= $r->singkatan; ?></option>
						<?php endforeach; ?>
						</select>
				</div>
				<div class="col-md-5" style="padding-left: 0px;">
					<input tabindex="4" type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama" value="{nama}" required>
				</div>
			</div>

			<div class="form-group" style="margin-bottom: 10px;margin-top:10px;padding-top:0px;">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<button type="button" class="btn btn-success" data-toggle="modal" data-target="#cari-pasien-modal" id="cari-pasien" style="width:100%;">CARI PASIEN</button>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="tglpendaftaran">No. Medrec Lama</label>
				<div class="col-md-8">
					<input tabindex="4" readonly type="text" class="form-control" id="no_medrec_lama" placeholder="NO. MEDREC LAMA" name="no_medrec_lama" value="" required>
				</div>
			</div>
			<input type="hidden" id="status_cari" name="status_cari" value="0">
			<input type="hidden" id="ckabupaten" name="ckabupaten" value="0">
			<input type="hidden" id="ckecamatan" name="ckecamatan" value="0">
			<input type="hidden" id="ckelurahan" name="ckelurahan" value="0">

			<input type="hidden" id="idpasien" name="idpasien" value="{idpasien}">


			<input type="hidden" name="created" value="<?= date(' d-M-Y H:i:s ')?>">
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="jeniskelamin">Jenis Kelamin <span style="color:red;">*</span></label>
				<div class="col-md-8">
					<select tabindex="5" id="jeniskelamin" name="jeniskelamin" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="">Pilih Opsi</option>
							<option value="1" <?=($jenis_kelamin == 1 ? 'selected="selected"' : '')?>>Laki-laki</option>
							<option value="2" <?=($jenis_kelamin == 2 ? 'selected="selected"' : '')?>>Perempuan</option>
						</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<textarea tabindex="6" class="form-control" id="alamat" placeholder="Alamat" name="alamat" required><?= $alamat_jalan?></textarea>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="provinsi"><p align="left">Provinsi</p></label>
				<div class="col-md-5">
					<?php if ($id == null) {?>
					<select tabindex="7" name="provinsi" id="provinsi" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
							<option value="<?= $a->id; ?>" <?= ($a->id == '12') ? 'selected' : ''; ?>><?= $a->nama; ?></option>
						<?php endforeach; ?>
						</select>
					<?php } else {?>
					<select tabindex="7" name="provinsi" id="provinsi" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
							<option value="<?= $a->id; ?>" <?=($provinsi_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
						<?php endforeach; ?>
						</select>
					<?php }?>
				</div>
			</div>
			<div id="kabupaten" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4"></div>
					<label class="col-md-3" for="kabupaten"><left>Kabupaten/Kota</left></label>
					<div class="col-md-5">
						<?php if ($id == null) {?>
						<select tabindex="8" name="kabupaten" id="kabupaten1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</<option>
								<?php foreach (get_all('mfwilayah', ['parent_id' => '12']) as $r):?>
								<option value="<?= $r->id; ?>" <?= ($r->id == '213') ? 'selected' : ''; ?>><?= $r->nama; ?></<option>
								<?php endforeach; ?>
							</select>
						<?php } else {?>
						<select tabindex="8" name="kabupaten" id="kabupaten1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</<option>
								<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id]) as $r):?>
								<option value="<?= $r->id; ?>" <?=($kabupaten_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
								<?php endforeach; ?>
							</select>
						<?php }?>
					</div>
				</div>
			</div>
			<div id="kecamatan" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4"></div>
					<label class="col-md-3 control-label" for="kecamatan" style="text-align:left;">Kecamatan</label>
					<div class="col-md-5">
						<?php if ($id == null) {?>
						<select tabindex="9" name="kecamatan" id="kecamatan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => '213']) as $r):?>
							<option value="<?= $r->id; ?>"><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
						<?php } else {?>
						<select tabindex="9" name="kecamatan" id="kecamatan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id]) as $r):?>
							<option value="<?= $r->id; ?>"<?=($kecamatan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
						<?php }?>
					</div>
				</div>
			</div>
			<?php if ($id == null) {?>
			<div id="kelurahan" style="display: none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4"></div>
					<label class="col-md-3 control-label" for="kelurahan" style="text-align:left;">Kelurahan</label>
					<div class="col-md-5">
						<select tabindex="10" name="kelurahan" id="kelurahan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4"></div>
					<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
					<div class="col-md-5">
						<input tabindex="10" type="text" class="form-control" id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true">
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="kelurahan" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4"></div>
					<label class="col-md-3 control-label" for="kelurahan" style="text-align:left;">Kelurahan</label>
					<div class="col-md-5">
						<select tabindex="11" name="kelurahan" id="kelurahan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id]) as $r):?>
							<option value="<?= $r->id; ?>"<?=($kelurahan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4"></div>
					<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
					<div class="col-md-5">
						<input tabindex="12" type="text" class="form-control" id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true">
					</div>
				</div>
			</div>
			<?php }?>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="noidentitas">No Identitas</label>
				<div class="col-md-3" style="padding-right: 10px;">
					<select tabindex="13" id="jenis_id" name="jenisidentitas" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="1" <?=($jenis_id == 1 ? 'selected="selected"' : '')?>>KTP</option>
						<option value="2" <?=($jenis_id == 2 ? 'selected="selected"' : '')?>>SIM</option>
						<option value="3" <?=($jenis_id == 3 ? 'selected="selected"' : '')?>>Pasport</option>
						<option value="4" <?=($jenis_id == 4 ? 'selected="selected"' : '')?>>Kartu Pelajar/Mahasiswa</option>
					</select>
				</div>
				<div class="col-md-5" style="margin-left:0px;padding-left: 0px;">
					<input tabindex="14" type="text" class="form-control" id="noidentitas" placeholder="No Identitas" name="noidentitas" value="{ktp}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="nohp">No Hp</label>
				<div class="col-md-8">
					<input tabindex="15" type="text" class="form-control" id="nohp" placeholder="No HP" name="nohp" value="{hp}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="telprumah">Telepon Rumah</label>
				<div class="col-md-8">
					<input tabindex="16" type="text" class="form-control" id="telprumah" placeholder="Telepon Rumah" name="telprumah" value="{telepon}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="email">Email</label>
				<div class="col-md-8">
					<input tabindex="17" type="text" class="form-control" id="email" placeholder="Email" name="email" value="{email}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="tempatlahir">Tempat Lahir</label>
				<div class="col-md-8">
					<input tabindex="18" type="text" class="form-control" id="tempatLahir" placeholder="Tempat Lahir" name="tempatlahir" value="{tempat_lahir}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
				<div class="col-md-3" style="padding-right: 10px;">
					<select tabindex="19" name="tgl_lahir" id="hari" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="0">Hari</option>
						<?php for ($i = 1; $i < 32; $i++) {?>
							<?php $hari = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?= $hari; ?>" <?=($tgl_hari == $hari ? 'selected="selected"' : '')?>><?= $hari; ?></option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-2" style="padding-right: 10px;padding-left:0px;">
					<select tabindex="20" name="bulan_lahir" id="bulan" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="00">Bulan</option>
						<option value="01" <?=($tgl_bulan == '01' ? 'selected="selected"' : '')?>>Januari</option>
						<option value="02" <?=($tgl_bulan == '02' ? 'selected="selected"' : '')?>>Februari</option>
						<option value="03" <?=($tgl_bulan == '03' ? 'selected="selected"' : '')?>>Maret</option>
						<option value="04" <?=($tgl_bulan == '04' ? 'selected="selected"' : '')?>>April</option>
						<option value="05" <?=($tgl_bulan == '05' ? 'selected="selected"' : '')?>>Mei</option>
						<option value="06" <?=($tgl_bulan == '06' ? 'selected="selected"' : '')?>>Juni</option>
						<option value="07" <?=($tgl_bulan == '07' ? 'selected="selected"' : '')?>>Juli</option>
						<option value="08" <?=($tgl_bulan == '08' ? 'selected="selected"' : '')?>>Agustus</option>
						<option value="09" <?=($tgl_bulan == '09' ? 'selected="selected"' : '')?>>September</option>
						<option value="10" <?=($tgl_bulan == '10' ? 'selected="selected"' : '')?>>Oktober</option>
						<option value="11" <?=($tgl_bulan == '11' ? 'selected="selected"' : '')?>>November</option>
						<option value="12" <?=($tgl_bulan == '12' ? 'selected="selected"' : '')?>>Desember</option>
					</select>
				</div>
				<div class="col-md-3" style="padding-left:0px;">
					<select tabindex="21" name="tahun_lahir" id="tahun" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="0">Tahun</option>
						<?php for ($i = date('Y'); $i >= 1900; $i--) {?>
							<option value="<?= $i; ?>" <?=($tgl_tahun == $i ? 'selected="selected"' : '')?>><?= $i; ?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="umur">Umur</label>
				<div class="col-md-3">
					<input tabindex="22" type="text" class="form-control" id="tahun1" name="umurtahun" value="{umur_tahun}" placeholder="Tahun" readonly="true">
				</div>
				<div class="col-md-2">
					<input tabindex="23" type="text" class="form-control" id="bulan1" name="umurbulan" value="{umur_bulan}" placeholder="Bulan" readonly="true">
				</div>
				<div class="col-md-3">
					<input tabindex="24" type="text" class="form-control" id="hari1" name="umurhari" value="{umur_hari}" placeholder="Hari" readonly="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="golongandarah">Golongan Darah</label>
				<div class="col-md-8">
					<select tabindex="25" id="golongandarah" name="golongandarah" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($golongan_darah == 1 ? 'selected="selected"' : '')?>>A</option>
						<option value="2" <?=($golongan_darah == 2 ? 'selected="selected"' : '')?>>B</option>
						<option value="3" <?=($golongan_darah == 3 ? 'selected="selected"' : '')?>>AB</option>
						<option value="4" <?=($golongan_darah == 4 ? 'selected="selected"' : '')?>>O</option>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="agama">Agama</label>
				<div class="col-md-8">
					<select tabindex="26" id="agama" name="agama" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('magama', ['status' => 1]) as $r):?>
						<option value="<?= $r->id; ?>" <?=($agama_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="kewarganegaraan">Kewarganegaraan</label>
				<div class="col-md-8">
					<select tabindex="27" id="kewarganegaraan" name="kewarganegaraan" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($warganegara == 1 ? 'selected="selected"' : '')?>>WNI</option>
						<option value="2" <?=($warganegara == 2 ? 'selected="selected"' : '')?>>WNA</option>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="suku">Suku</label>
				<div class="col-md-8">
					<input tabindex="28" type="text" class="form-control" id="suku" placeholder="Suku" name="suku" value="{suku}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="statuskawin">Status Kawin</label>
				<div class="col-md-8">
					<select tabindex="29" id="statuskawin" name="statuskawin" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($status_kawin == '1' ? 'selected' : '')?>>Belum Menikah</option>
						<option value="2" <?=($status_kawin == '2' ? 'selected' : '')?>>Sudah Menikah</option>
						<option value="3" <?=($status_kawin == '3' ? 'selected' : '')?>>Janda</option>
						<option value="4" <?=($status_kawin == '4' ? 'selected' : '')?>>Duda</option>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="pendidikan">Pendidikan</label>
				<div class="col-md-8">
					<select tabindex="30" id="pendidikan" name="pendidikan" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mpendidikan') as $r):?>
							<option value="<?= $r->id; ?>" <?=($pendidikan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="pekerjaan">Pekerjaan</label>
				<div class="col-md-8">
					<select tabindex="31" id="pekerjaan" name="pekerjaan" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mpekerjaan') as $r):?>
							<option value="<?= $r->id; ?>" <?=($pekerjaan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="asalpasien">Asal Pasien<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<select tabindex="32" name="asalpasien" id="asalpasien" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mpasien_asal') as $r):?>
							<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="asalpasien">Tipe Pasien<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<select tabindex="32" name="idtipepasien" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="1" <?=($idtipepasien == 1 ? 'selected="selected"' : '')?>>Pasien RS</option>
						<option value="2" <?=($idtipepasien == 2 ? 'selected="selected"' : '')?>>Pasien Pribadi</option>
					</select>
				</div>
			</div>
			<?php if ($id != null && ($idasalpasien == '2' || $idasalpasien == '3')) {?>
			<?php
				if ($idasalpasien == 2) {
					$idtipeRS = 1;
				} elseif ($idasalpasien == 3) {
					$idtipeRS = 2;
				}
			?>
			<div id="rujukan" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="rujukan">Rujukan</label>
					<div class="col-md-8">
						<div class="col-md-10" style="padding-left: 0px;padding-right: 0px;">
							<select tabindex="33" name="rujukan" id="rujukan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach (get_all('mrumahsakit', ['idtipe' => $idtipeRS]) as $r) {?>
									<option value="<?= $r->id?>" <?=($idrujukan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
								<?php }?>
							</select>
						</div>
						<div class="col-md-2" style="padding-left: 10px;padding-right: 0px;">
							<a href="#" class="btn btn-success" data-toggle="modal" data-target="#tambah-rujukan-modal" id="tambah-rujukan" style="width:100%;"><i class="fa fa-plus"></i></a>
						</div>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="rujukan" style="display: none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="rujukan">Rujukan</label>
					<div class="col-md-8">
						<div class="col-md-10" style="padding-left: 0px;padding-right: 0px;">
							<select tabindex="33" name="rujukan" id="rujukan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							</select>
						</div>
						<div class="col-md-2" style="padding-left: 10px;padding-right: 0px;">
							<a href="#" class="btn btn-success" data-toggle="modal" data-target="#tambah-rujukan-modal" id="tambah-rujukan" style="width:100%;"><i class="fa fa-plus"></i></a>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="">Kelompok Pasien 1<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<?php if ($id == null) {?>
					<select tabindex="34" name="kelompokpasien" id="kelompokpasien" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mpasien_kelompok') as $r):?>
						<option value="<?= $r->id; ?>" <?=($r->id == $kelompokpasien ? 'selected' : '')?>><?= $r->nama; ?></option>
						<?php endforeach; ?>
					</select>
					<?php } else {?>
					<select tabindex="34" name="kelompokpasien" id="kelompokpasien" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mpasien_kelompok') as $r):?>
						<option value="<?= $r->id; ?>" <?=($idkelompokpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
						<?php endforeach; ?>
					</select>
					<?php }?>
				</div>
			</div>
			<?php if ($id != null && $idrekanan != null) {?>
			<div id="rekanan" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Rekanan 1</label>
					<div class="col-md-8">
						<select tabindex="35" name="rekanan" id="rekanan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('mrekanan') as $r) {?>
							<option value="<?= $r->id?>" <?=($idrekanan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="rekanan" style="display: none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Rekanan 1</label>
					<div class="col-md-8">
						<select tabindex="35" name="rekanan" id="rekanan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						</select>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if ($id != null && $idtarifbpjskesehatan != null) {?>
			<div id="bpjskesehatan" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">BPJS Kesehatan 1</label>
					<div class="col-md-8">
						<select tabindex="36" name="bpjskesehatan" id="bpjskesehatan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('mtarif_bpjskesehatan') as $r) {?>
								<option value="<?= $r->id?>" <?=($idtarifbpjskesehatan == $r->id ? 'selected="selected"' : '')?>><?= $r->kode?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="bpjskesehatan" style="display: none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">BPJS Kesehatan 1</label>
					<div class="col-md-8">
						<select tabindex="36" name="bpjskesehatan" id="bpjskesehatan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						</select>
					</div>
				</div>
			</div>
			<?php }?>

			<?php if ($id != null && $idtarifbpjstenagakerja != null) {?>
			<div id="bpjstenagakerja" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">BPJSTenagaKerja 1</label>
					<div class="col-md-8">
						<select tabindex="37" name="bpjstenagakerja" id="bpjstenagakerja1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('mtarif_bpjstenagakerja') as $r) {?>
								<option value="<?= $r->id?>" <?=($idtarifbpjstenagakerja == $r->id ? 'selected="selected"' : '')?>><?= $r->id?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="bpjstenagakerja" style="display: none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">BPJSTenagaKerja 1</label>
					<div class="col-md-8">
						<select tabindex="37" name="bpjstenagakerja" id="bpjstenagakerja1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						</select>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if ($id != null && $idkelompokpasien == 2) {?>
			<div id="jp" style="display:block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Jenis Pasien 1</label>
					<div class="col-md-8">
						<select tabindex="38" name="jenispasien" id="jenispasien" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1" <?=($idjenispasien == 1 ? 'selected="selected"' : '')?>>Non COB</option>
							<option value="2" <?=($idjenispasien == 2 ? 'selected="selected"' : '')?>>COB</option>
						</select>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="jp" style="display:none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Jenis Pasien 1</label>
					<div class="col-md-8">
						<select tabindex="38" name="jenispasien" id="jenispasien" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" <?=($idjenispasien == '' ? 'selected' : '')?>>Pilih Opsi</option>
							<option value="1" <?=($idjenispasien == '1' || $idjenispasien == '' ? 'selected' : '')?>>Non COB</option>
							<option value="2" <?=($idjenispasien == '2' ? 'selected' : '')?>>COB</option>
						</select>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if ($id != null && $idjenispasien == '2') {?>
			<div id="kelompok1" style="display:block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Kelompok Pasien 2</label>
					<div class="col-md-8">
						<select tabindex="39" name="kelompokpasien2" id="kelompokpasien1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php $data = $this->db->query("SELECT * FROM mpasien_kelompok WHERE id NOT IN('0',$idkelompokpasien)")->result();
							foreach ($data as $r) {?>
							<option value="<?= $r->id?>" <?=($idkelompokpasien2 == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="kelompok1" style="display:none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Kelompok Pasien 2</label>
					<div class="col-md-8">
						<select tabindex="39" name="kelompokpasien2" id="kelompokpasien1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						</select>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if ($id != null && $idrekanan2 != null) {?>
			<div id="rekanan2" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Rekanan 2</label>
					<div class="col-md-8">
						<select tabindex="40" name="rekanan2" id="rekanan3" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('mrekanan') as $r) {?>
							<option value="<?= $r->id?>" <?=($idrekanan2 == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="rekanan2" style="display: none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Rekanan 2</label>
					<div class="col-md-8">
						<select tabindex="40" name="rekanan2" id="rekanan3" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						</select>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if ($id != null && $idtarifbpjskesehatan2 != null) {?>
			<div id="bpjskesehatan2" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">BPJS Kesehatan 2</label>
					<div class="col-md-8">
						<select tabindex="41" name="bpjskesehatan2" id="bpjskesehatan3" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('mtarif_bpjskesehatan') as $r) {?>
							<option value="<?= $r->id?>" <?=($idtarifbpjskesehatan2 == $r->id ? 'selected="selected"' : '')?>><?= $r->kode?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="bpjskesehatan2" style="display: none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">BPJS Kesehatan 2</label>
					<div class="col-md-8">
						<select tabindex="41" name="bpjskesehatan2" id="bpjskesehatan3" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						</select>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if ($id != null && $idtarifbpjstenagakerja2 != null) {?>
			<div id="bpjstenagakerja2" style="display: block;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">BPJS TenagaKerja 2</label>
					<div class="col-md-8">
						<select tabindex="42" name="bpjstenagakerja2" id="bpjstenagakerja3" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('mtarif_bpjstenagakerja') as $r) {?>
							<option value="<?= $r->id?>" <?=($idtarifbpjstenagakerja2 == $r->id ? 'selected="selected"' : '')?>><?= $r->id?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<?php } else {?>
			<div id="bpjstenagakerja2" style="display: none;">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">BPJS TenagaKerja 2</label>
					<div class="col-md-8">
						<select tabindex="42" name="bpjstenagakerja2" id="bpjstenagakerja3" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						</select>
					</div>
				</div>
			</div>
			<?php }?>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="poliklinik">Poliklinik<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<?php if ($id != null) {?>
					<select tabindex="43" name="poliklinik" id="poliklinik" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mpoliklinik', ['idtipe' => $idtipe]) as $r) {?>
						<option value="<?= $r->id?>" <?=($idpoliklinik == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
						<?php }?>
					</select>
					<?php } else {?>
					<select tabindex="43" name="poliklinik" id="poliklinik" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
					</select>
					<?php }?>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="pertemuan">Pertemuan</label>
				<div class="col-md-8">
					<select tabindex="44" name="pertemuan" id="pertemuan" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idjenispertemuan == 1 ? 'selected="selected"' : '')?>>Berdasarkan Janji</option>
						<option value="2" <?=($idjenispertemuan == 2 ? 'selected="selected"' : '')?>>Tidak Berdasarkan Janji</option>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="iddokter">Dokter<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<?php if ($id != null) {?>
					<select tabindex="45" name="iddokter" id="iddokter" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mpoliklinik_dokter', ['idpoliklinik' => $idpoliklinik]) as $r) {
								foreach (get_all('mdokter', ['id' => $r->iddokter]) as $s) {?>
								<option value="<?= $s->id?>" <?=($iddokter == $s->id ? 'selected="selected"' : '')?>><?= $s->nama?></option>
							<?php } ?>
						<?php
							}?>
					</select>
					<?php } else {?>
					<select tabindex="45" name="iddokter" id="iddokter" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="">Pilih Opsi</option>
						</select>
					<?php }?>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left"><b>Data Lain Lain</b></h5></label>
				<div class="col-md-12">
					<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="catatan">Catatan</label>
				<div class="col-md-8">
					<textarea tabindex="46" class="form-control" id="catatan" placeholder="Catatan" name="catatan"><?= $catatan?></textarea>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left"><b>Data Penanggung Jawab</b></h5></label>
				<div class="col-md-12">
					<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="namapenanggungjawab">Nama</label>
				<div class="col-md-8">
					<input tabindex="47" type="text" class="form-control" id="namapenanggungjawab" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab" value="{namapenanggungjawab}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="hubungan">Hubungan</label>
				<div class="col-md-8">
					<select tabindex="48" id="hubungan" name="hubungan" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="Ayah" <?=($hubungan_dengan_pasien == 'Ayah' ? 'selected="selected"' : '')?>>Ayah</option>
						<option value="Ibu" <?=($hubungan_dengan_pasien == 'Ibu' ? 'selected="selected"' : '')?>>Ibu</option>
						<option value="Anak" <?=($hubungan_dengan_pasien == 'Anak' ? 'selected="selected"' : '')?>>Anak</option>
						<option value="Suami" <?=($hubungan_dengan_pasien == 'Suami' ? 'selected="selected"' : '')?>>Suami</option>
						<option value="Istri" <?=($hubungan_dengan_pasien == 'Istri' ? 'selected="selected"' : '')?>>Istri</option>
						<option value="Wali" <?=($hubungan_dengan_pasien == 'Wali' ? 'selected="selected"' : '')?>>Wali</option>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="alamatpenanggungjawab">Alamat</label>
				<div class="col-md-8">
					<textarea tabindex="49" class="form-control" id="alamatpenanggungjawab" placeholder="Alamat Penanggung Jawab" name="alamatpenanggungjawab"><?= $alamat_keluarga?></textarea>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label" for="telepon">Telepon</label>
				<div class="col-md-8">
					<input tabindex="50" type="text" class="form-control" id="telepon" placeholder="Telepon" name="telepon" value="{teleponpenanggungjawab}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 20px;">
				<label class="col-md-4 control-label" for="noidentitaspenanggung">No Identitas</label>
				<div class="col-md-8">
					<input tabindex="51" type="text" class="form-control" id="noidentitaspenanggung" placeholder="No Identitas Penanggung Jawab" name="noidentitaspenanggung" value="{noidentitaspenanggungjawab}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left"><b>History Kunjungan</b></h5></label>
				<div class="col-md-12" id="historykunjungan">
					<?php if ($idpasien != null) { ?>
						<div class="block">
							<div class="block-content">
								<ul class="list list-activity push">
									<?php foreach ($historykunjungan as $row) { ?>
									<li>
										<i class="si si-pencil text-info"></i>
										<div class="font-w600">
											<?=($row->idtipe == 1 ? 'Poliklinik' : 'IGD')?> -
												<?=$row->poli?>
										</div>
										<div><a href="javascript:void(0)"><?=$row->nama?></a></div>
										<div><small class="text-muted"><?=$row->tanggaldaftar?></small></div>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
						<?php } ?>
				</div>
			</div>
		</div>
		<div class="form-group"></div>
		<hr>
		<div class="text-right bg-light lter">
			<button class="btn btn-info" id="btn_simpan" type="submit">Simpan</button>
			<a href="{base_url}tpoliklinik_pendaftaran/index" class="btn btn-default" style="text-transform:initial"><i class="pg-close"></i> Batal</a>
		</div>
		<br>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<?}?>

<!-- Modal Cari Pasien -->
<div class="modal in" id="cari-pasien-modal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Data Pasien</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snama_keluarga">Penanggung Jawab</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snama_keluarga" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;display:none">
									<label class="col-md-4 control-label" for="sjeniskelamin">Jenis Kelamin</label>
									<div class="col-md-8">
										<label class="css-input css-radio css-radio-sm css-radio-default push-10-r">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="1"><span></span> Laki-laki
										</label>
										<label class="css-input css-radio css-radio-sm css-radio-default">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="2"><span></span> Perempuan
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="salamat">Alamat</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="salamat" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="stanggallahir" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snotelepon">Telepon</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snotelepon" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>

					<table width="100%" class="table table-bordered table-striped" id="datatable-pasien">
						<thead>
							<tr>
								<th>No. Medrec</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Keluarga</th>
								<th>Tlp. Keluarga</th>
							</tr>
						</thead>
						<tbody id="pasienmodaltabel">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Cari Pasien -->

<!-- Modal Tambah Rujukan Baru -->
<div class="modal in" id="tambah-rujukan-modal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-md modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tambah Rujukan Baru</h3>
				</div>
				<div class="block-content">

					<div class="form-group" style="margin-bottom:10px;">
						<label class="col-md-3 control-label" for="idtipe">Tipe</label>
						<div class="col-md-7" id="tiperujukan">
						</div>
					</div>
					<div class="form-group" style="margin-bottom:10px;">
						<label class="col-md-3 control-label" for="nama">Nama</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="namarujukan" placeholder="Nama" name="namarujukan">
						</div>
					</div>
					<div class="form-group" style="margin-bottom:10px;">
						<label class="col-md-3 control-label" for="idkategori">Kategori</label>
						<div class="col-md-7">
							<select class="select2 form-control" id="idkategorirujukan" name="idkategori" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<option value="1">Berekanan</option>
								<option value="2">Tidak Berekanan</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:10px;display:none;" id="frm-persentase">
						<label class="col-md-3 control-label" for="persentase">Persentase</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="persentase" placeholder="Persentase" name="persentase">
						</div>
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-success" id="simpanrujukan" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Tambah Rujukan Baru -->

<!-- <script src="{js_path}custom/basic.js"></script> -->
<!-- <link rel="stylesheet" type="text/css" href="{css_path}custom/timeline_css.css"> -->
<!-- <script src="{js_path}custom/pendaftaran_klinik.js"></script> -->
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$(".time-datepicker").datetimepicker({
			format: "HH:mm:ss"
		});

		$('.select2').select2();
		show_hide(1,$("#kelompokpasien").val());
		show_hide(2,$("#kelompokpasien1").val());

		// loadDataPasien();

		// Get Default Data Poliklinik
		<?php if ($this->uri->segment(2) != 'update') { ?>
			$.ajax({
				url: "{site_url}tpoliklinik_pendaftaran/getPoli",
				method: "POST",
				dataType: "json",
				data: {
					"tipepoli": 1
				},
				success: function(data) {
					$("#poliklinik").empty();
					for (var i = 0; i < data.length; i++) {
						$("#poliklinik").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}
					$('#poliklinik').val('1').trigger('change');
				}
			});
		<?php } ?>

		// DataTable

		var delay = (function() {
			var timer = 0;
			return function(callback, ms) {
				clearTimeout(timer);
				timer = setTimeout(callback, ms);
			};
		})();

		// Apply the search
		$("#snomedrec").keyup(function() {
			var snomedrec = $(this).val();
			var snamapasien = $("#snamapasien").val();
			var snama_keluarga = $("#snama_keluarga").val();
			var salamat = $("#salamat").val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
				delay(function() {
					$('#datatable-pasien').DataTable().destroy();
					loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
				}, 1000);
			}
		});
		$("#snamapasien").keyup(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $(this).val();
			var snama_keluarga = $("#snama_keluarga").val();
			var salamat = $("#salamat").val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});
		$("#snama_keluarga").keyup(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var snama_keluarga = $(this).val();
			var salamat = $("#salamat").val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});

		$("#salamat").keyup(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var snama_keluarga = $("#snama_keluarga").val();
			var salamat = $(this).val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});

		$("#stanggallahir").change(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var snama_keluarga = $("#snama_keluarga").val();
			var salamat = $("#salamat").val();
			var stanggallahir = $(this).val();
			var snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});

		$("#snotelepon").keyup(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var snama_keluarga = $("#snama_keluarga").val();
			var salamat = $("#salamat").val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $(this).val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});

		// Validasi Input UpperCase Dan Number
		$('input').keyup(function() {
			this.value = this.value.toUpperCase();
		});

		$('textarea').keyup(function() {
			this.value = this.value.toUpperCase();
		});

		$('input[name="nohp"]').bind('keypress', function(e) {
			var keyCode = (e.which) ? e.which : event.keyCode
			return !(keyCode > 31 && (keyCode < 48 || keyCode > 57));
		});

		$('input[name="telprumah"]').bind('keypress', function(e) {
			var keyCode = (e.which) ? e.which : event.keyCode
			return !(keyCode > 31 && (keyCode < 48 || keyCode > 57));
		});

		$('input[name="telepon"]').bind('keypress', function(e) {
			var keyCode = (e.which) ? e.which : event.keyCode
			return !(keyCode > 31 && (keyCode < 48 || keyCode > 57));
		});

		$(document).on("click", ".selectPasien", function() {
			var idpasien = ($(this).data('idpasien'));
			$.ajax({
				url: "{site_url}tpoliklinik_pendaftaran/getDataPasien/" + idpasien,
				method: "POST",
				dataType: "json",
				success: function(data) {
					if (data[0].noid_lama){
						sweetAlert("Informasi", "Data PASIEN <strong>'"+data[0].noid_lama+"'</strong> tidak akan digunakan karena <br> telah digabungkan dengan data PASIEN <strong>'"+data[0].no_medrec+"'</strong> <br><br> data PASIEN <strong>'"+data[0].no_medrec+"'</strong> akan digunakan.", "info");
					}

					var status_kawin = (data[0].status_kawin != 0 ? data[0].status_kawin : 1);
					var jenis_kelamin = (data[0].jenis_kelamin != 0 ? data[0].jenis_kelamin : 1);

					$('#status_cari').val(1);
					$('#ckabupaten').val(data[0].kabupaten_id);
					$('#ckecamatan').val(data[0].kecamatan_id);
					$('#ckelurahan').val(data[0].kelurahan_id);

					$('#idpasien').val(data[0].id);
					$('#no_medrec').val(data[0].no_medrec);
					$('#no_medrec_lama').val(data[0].noid_lama);
					$('#nama').val(data[0].nama);
					$('#alamat').val(data[0].alamat_jalan);
					$('#jeniskelamin').select2("val", jenis_kelamin);
					$('#jenis_id').select2("val", data[0].jenis_id);
					$('#noidentitas').val(data[0].ktp);
					$('#nohp').val(data[0].hp);
					$('#telprumah').val(data[0].telepon);
					$('#email').val(data[0].email);
					$('#tempatLahir').val(data[0].tempat_lahir);


					// Pecahan Tanggal Lahir (Hari, Bulan, Tahun)
					var tanggalLahir = data[0].tanggal_lahir;
					var tahun = tanggalLahir.substr(0, 4);
					var bulan = tanggalLahir.substr(5, 2);
					var hari = tanggalLahir.substr(8, 2);

					$('#tahun').val(tahun);
					$('#bulan').val(bulan);
					$('#hari').val(hari);

					// Perhitungan Umur (Hari, Bulan, Tahun)
					$.ajax({
						url: '{site_url}tpoliklinik_pendaftaran/getDate',
						method: "POST",
						dataType: "json",
						data: {
							"hari": hari,
							"bulan": bulan,
							"tahun": tahun,
							"tanggal": tanggalLahir
						},
						success: function(data) {
							$("#tahun1").val(data.date[0].tahun);
							$("#bulan1").val(data.date[0].bulan);
							$("#hari1").val(data.date[0].hari);

							getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari, status_kawin, jenis_kelamin);
						}
					});

					$('#golongandarah').val(data[0].golongan_darah);
					$('#agama').val(data[0].agama_id);
					$('#kewarganegaraan').val(data[0].warganegara);
					$('#suku').val(data[0].suku);
					$('#statuskawin').val(status_kawin).trigger('change');
					$('#pendidikan').val(data[0].pendidikan_id).trigger('change');
					$('#pekerjaan').val(data[0].pekerjaan_id).trigger('change');
					$('#catatan').val(data[0].catatan);
					$('#namapenanggungjawab').val(data[0].nama_keluarga);
					$('#hubungan').val(data[0].hubungan_dengan_pasien).trigger('change');
					$('#alamatpenanggungjawab').val(data[0].alamat_keluarga);
					$('#telepon').val(data[0].telepon_keluarga);
					$('#noidentitaspenanggung').val(data[0].ktp_keluarga);

					$('#provinsi').val(data[0].provinsi_id).trigger('change');
					$('#kabupaten1').val(data[0].kabupaten_id).trigger('change');
					$('#kecamatan1').val(data[0].kecamatan_id).trigger('change');
					$('#kelurahan').val(data[0].kelurahan_id).trigger('change');

					getHistoryKunjungan(idpasien);
					get_duplicate_hariini();
				}
			});
			return false;
		});

		// Triger Rujukan
		$("#tambah-rujukan").click(function() {
			var asalpasien = $("#asalpasien").val();
			if (asalpasien == "2") {
				$("#tiperujukan").empty();
				$("#tiperujukan").append("<input type='text' class='form-control' value='Klinik/Puskesmas' readonly='true'>");
			} else if (asalpasien == "3") {
				$("#tiperujukan").empty();
				$("#tiperujukan").append("<input type='text' class='form-control' value='Rumah Sakit' readonly='true'>");
			}
		});

		$("#idkategorirujukan").change(function() {
			if ($(this).val() == 1) {
				$("#frm-persentase").show();
			} else {
				$("#frm-persentase").hide();
			}
		});

		$("#simpanrujukan").click(function() {
			var idtipe = $("#asalpasien").val();
			var nama = $("#namarujukan").val();
			var idkategori = $("#idkategorirujukan").val();
			var persentase = $("#persentase").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/rujukanBaru',
				method: "POST",
				dataType: "json",
				data: {
					"idtipe": idtipe,
					"nama": nama,
					"idkategori": idkategori,
					"persentase": persentase
				},
				success: function(data) {
					$.toaster({
						priority: 'success',
						title: 'Success',
						message: 'Rujukan Berhasil Ditambahkan'
					});
					$("#rujukan1").empty();
					$("#rujukan1").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.length; i++) {
						$("#rujukan1").append("<option value='" + data[i].id + "' selected>" + data[i].nama + "</option>");
					}
					$("#rujukan1").selectpicker("refresh");
				},
				error: function(data) {
					$.toaster({
						priority: 'danger',
						title: 'Failed',
						message: 'Rujukan Gagal Ditambahkan'
					});
				}
			});
		});

		// Trigger Alamat (Provinsi, Kota, Kecamatan)
		$("#provinsi").change(function() {
			$("#kabupaten").css("display", "block");
			var kodeprovinsi = $("#provinsi").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKabupaten',
				method: "POST",
				dataType: "json",
				data: {
					"kodeprovinsi": kodeprovinsi
				},
				success: function(data) {
					$("#kabupaten1").empty();
					$("#kabupaten1").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.length; i++) {
							$("#kabupaten1").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}

					$("#kabupaten1").selectpicker("refresh");
					if ($("#status_cari").val()=='1'){
						$('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
					}
				},
				error: function(data) {
					console.log(data);
				}
			});
		});

		$("#kabupaten1").change(function() {
			$("#kecamatan").css("display", "block");
			var kodekab = $("#kabupaten1").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKecamatan',
				method: "POST",
				dataType: "json",
				data: {
					"kodekab": kodekab
				},
				success: function(data) {
					$("#kecamatan1").empty();
					$("#kecamatan1").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.length; i++) {
						$("#kecamatan1").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}
					$("#kecamatan1").selectpicker("refresh");
					if ($("#status_cari").val()=='1'){
						$('#kecamatan1').val($("#ckecamatan").val()).trigger('change');
					}
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#kecamatan1").change(function() {
			$("#kelurahan").css("display", "block");
			var kodekec = $("#kecamatan1").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKelurahan',
				method: "POST",
				dataType: "json",
				data: {
					"kodekec": kodekec
				},
				success: function(data) {
					$("#kelurahan1").empty();
					$("#kelurahan1").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.length; i++) {
						$("#kelurahan1").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}
					$("#kelurahan1").selectpicker("refresh");
					if ($("#status_cari").val()=='1'){
						$('#kelurahan1').val($("#ckelurahan").val()).trigger('change');
					}
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#kelurahan1").change(function() {
			var kelurahan = $("#kelurahan1").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKodepos',
				method: "POST",
				dataType: "json",
				data: {
					"kodekel": kelurahan
				},
				success: function(data) {
					$("#kodepos").empty();
					if (data.length > 0) {
						$("#kodepos").val(data[0].kodepos);
					}
				}
			});
		});

		// Triger Umum Pasien
		$("#tahun").change(function() {
			var hari = $("#hari").val();
			var bulan = $("#bulan").val();
			var tahun = $("#tahun").val();
			var tanggal = tahun + "-" + bulan + "-" + hari;
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getDate',
				method: "POST",
				dataType: "json",
				data: {
					"hari": hari,
					"bulan": bulan,
					"tahun": tahun,
					"tanggal": tanggal
				},
				success: function(data) {
					$("#tahun1").val(data.date[0].tahun);
					$("#bulan1").val(data.date[0].bulan);
					$("#hari1").val(data.date[0].hari);

					let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
					let jeniskelamin = ($("#jeniskelamin option:selected").val() != '' ? $("#jeniskelamin option:selected").val() : 1);

					getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari, statuskawin, jeniskelamin);
				}
			});
		});

		$("#bulan").change(function() {
			var hari = $("#hari").val();
			var bulan = $("#bulan").val();
			var tahun = $("#tahun").val();
			var tanggal = tahun + "-" + bulan + "-" + hari;
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getDate',
				method: "POST",
				dataType: "json",
				data: {
					"hari": hari,
					"bulan": bulan,
					"tahun": tahun,
					"tanggal": tanggal
				},
				success: function(data) {
					if (tahun == "0") {
						$("#tahun1").val("0");
						$("#bulan1").val(data.date[0].bulan);
						$("#hari1").val(data.date[0].hari);
					} else {
						$("#tahun1").val(data.date[0].tahun);
						$("#bulan1").val(data.date[0].bulan);
						$("#hari1").val(data.date[0].hari);
					}

					let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
					let jeniskelamin = ($("#jeniskelamin option:selected").val() != '' ? $("#jeniskelamin option:selected").val() : 1);

					getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari, statuskawin, jeniskelamin);
				}
			});
		});

		$("#hari").change(function() {
			var hari = $("#hari").val();
			var bulan = $("#bulan").val();
			var tahun = $("#tahun").val();
			var tanggal = tahun + "-" + bulan + "-" + hari;
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getDate',
				method: "POST",
				dataType: "json",
				data: {
					"hari": hari,
					"bulan": bulan,
					"tahun": tahun,
					"tanggal": tanggal
				},
				success: function(data) {
					if (tahun == "0" && bulan == "0") {
						$("#tahun1").val("0");
						$("#bulan1").val("0");
						$("#hari1").val(hari);
					} else {
						$("#tahun1").val(data.date[0].tahun);
						$("#bulan1").val(data.date[0].bulan);
						$("#hari1").val(data.date[0].hari);
					}

					let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
					let jeniskelamin = ($("#jeniskelamin option:selected").val() != '' ? $("#jeniskelamin option:selected").val() : 1);

					getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari, statuskawin, jeniskelamin);
				}
			});
		});

		$("#jeniskelamin").change(function() {
			let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
			let jeniskelamin = ($("#jeniskelamin option:selected").val() != '' ? $("#jeniskelamin option:selected").val() : 1);

			getTitlePasien($("#tahun1").val(), $("#bulan").val(), $("#hari").val(), statuskawin, jeniskelamin);
		});

		$("#statuskawin").change(function() {
			let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
			let jeniskelamin = ($("#jeniskelamin option:selected").val() != '' ? $("#jeniskelamin option:selected").val() : 1);

			getTitlePasien($("#tahun1").val(), $("#bulan").val(), $("#hari").val(), statuskawin, jeniskelamin);
		});

		// Triger Data Penanggung Jawab
		$("#telprumah").keyup(function() {
			var telprumah = $("#telprumah").val();
			$("#telepon").val(telprumah);
		});

		$("#alamat").keyup(function() {
			var alamat = $("#alamat").val();
			$("#alamatpenanggungjawab").val(alamat);
		});

		// Triger Form Pendaftaran
		$("#tipepoli").change(function() {
			var tipepoli = $("#tipepoli").val();
			$.ajax({
				url: "{site_url}tpoliklinik_pendaftaran/getPoli",
				method: "POST",
				dataType: "json",
				data: {
					"tipepoli": tipepoli
				},
				success: function(data) {
					$("#poliklinik").empty();
					for (var i = 0; i < data.length; i++) {
						$("#poliklinik").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}

					if (tipepoli=='2'){
						$('#poliklinik').val('22').trigger('change');
					}else{
						$('#poliklinik').val('1').trigger('change');
					}
				}
			});
		});

		$("#poliklinik").change(function() {
			get_duplicate_hariini();

			var idpoliklinik = $(this).val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getDokter/' + idpoliklinik,
				dataType: "json",
				success: function(data) {
					$("#iddokter").empty();
					$("#iddokter").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.length; i++) {
						$("#iddokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}
					$("#iddokter").selectpicker("refresh");
				}
			});
		});

		$("#tglpendaftaran").change(function() {
			get_duplicate_hariini();
		});

		$("#iddokter").change(function() {
			get_duplicate_hariini();
		});

		$("#asalpasien").change(function() {
			var asal = $("#asalpasien").val();
			if (asal == "1") {
				$("#rujukan").css("display", "none");
			} else {
				$("#rujukan").css("display", "block");
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getRujukan',
					method: "POST",
					dataType: "json",
					data: {
						"asal": asal
					},
					success: function(data) {
						$("#rujukan1").empty();
						$("#rujukan1").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#rujukan1").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
						}
						$("#rujukan1").selectpicker("refresh");
					}
				});
			}
		});

		$("#kelompokpasien").change(function() {
			var kp = $("#kelompokpasien").val();
			if (kp == "1") {
				// $("#rekanan").css("display", "block");
				// $("#bpjskesehatan").css("display", "none");
				// $("#bpjstenagakerja").css("display", "none");
				// $("#jp").css("display", "block");
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getRekanan',
					dataType: "json",
					success: function(data) {
						$("#rekanan1").empty();
						$("#rekanan1").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#rekanan1").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
						}
						$("#rekanan1").selectpicker("refresh");
					}
				});
			} else if (kp == "3") {
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getBpjsKesehatan',
					dataType: "json",
					success: function(data) {
						$("#bpjskesehatan1").empty();
						$("#bpjskesehatan1").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#bpjskesehatan1").append("<option value='" + data[i].id + "'>" + data[i].kode + "</option>");
						}
						$("#bpjskesehatan1").selectpicker("refresh");
					}
				});
			} else if (kp == "4") {
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getBpjsTenagakerja',
					dataType: "json",
					success: function(data) {
						$("#bpjstenagakerja1").empty();
						$("#bpjstenagakerja1").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#bpjstenagakerja1").append("<option value='" + data[i].id + "'>" + data[i].id + "</option>");
						}
						$("#bpjstenagakerja1").selectpicker("refresh");
					}
				});
			} else if (kp == "0") {
				// $("#rekanan").css("display", "none");
				// $("#bpjskesehatan").css("display", "none");
				// $("#bpjstenagakerja").css("display", "none");
				// $("#jp").css("display", "none");
			} else if (kp == "5") {
				// $("#jp").css("display", "none");
				// alert('Umum');
			} else {
				// $("#rekanan").css("display", "none");
				// $("#bpjskesehatan").css("display", "none");
				// $("#bpjstenagakerja").css("display", "none");
				// $("#jp").css("display", "block");
				// $("#jenispasien").css("display", "block");
			}
			show_hide(1,kp);
		});

		$("#jenispasien").change(function() {
			var kelompok = $("#kelompokpasien").val();
			var jenispasien = $("#jenispasien").val();
			if (jenispasien == "1") {
				$("#kelompok1").css("display", "none");
			} else {
				$("#kelompok1").css("display", "block");
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getKelompok',
					dataType: "json",
					method: "POST",
					data: {
						"kelompok": kelompok
					},
					success: function(data) {
						$("#kelompokpasien1").empty();
						$("#kelompokpasien1").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.kelompok.length; i++) {
							$("#kelompokpasien1").append("<option value='" + data.kelompok[i].id + "'>" + data.kelompok[i].nama + "</option>");
						}
						$("#kelompokpasien1").selectpicker('refresh');
					}
				});
			}
			hide_all(2);
		});

		$("#kelompokpasien1").change(function() {
			var kp = $("#kelompokpasien1").val();
			if (kp == "1") {
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getRekanan',
					dataType: "json",
					success: function(data) {
						$("#rekanan3").empty();
						$("#rekanan3").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#rekanan3").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
						}
						$("#rekanan3").selectpicker("refresh");
					}
				});
			} else if (kp == "3") {
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getBpjsKesehatan',
					dataType: "json",
					success: function(data) {
						$("#bpjskesehatan3").empty();
						$("#bpjskesehatan3").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#bpjskesehatan3").append("<option value='" + data[i].id + "'>" + data[i].kode + "</option>");
						}
						$("#bpjskesehatan3").selectpicker("refresh");
					}
				});
			} else if (kp == "4") {
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getBpjsTenagakerja',
					dataType: "json",
					success: function(data) {
						$("#bpjstenagakerja3").empty();
						$("#bpjstenagakerja3").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#bpjstenagakerja3").append("<option value='" + data[i].id + "'>" + data[i].id + "</option>");
						}
						$("#bpjstenagakerja3").selectpicker("refresh");
					}
				});
			} else {
				// $("#rekanan").css("display", "none");
				// $("#bpjskesehatan").css("display", "none");
				// $("#bpjstenagakerja").css("display", "none");
			}
			show_hide(2,kp);
		});

		$("#pertemuan").change(function() {
			var pertemuan = $("#pertemuan").val();
			var idpoli = $("#poliklinik").val();
			if (pertemuan == "1") {
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getDokter',
					method: "POST",
					dataType: "json",
					data: {
						"idpoli": idpoli
					},
					success: function(data) {
						$("#iddokter").empty();
						$("#iddokter").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#iddokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
						}
						$("#iddokter").selectpicker("refresh");
					}
				});
			} else {
				$.ajax({
					url: '{site_url}tpoliklinik_pendaftaran/getDokter',
					method: "POST",
					dataType: "json",
					data: {
						"idpoli": idpoli
					},
					success: function(data) {
						$("#iddokter").empty();
						$("#iddokter").append("<option value=''>Pilih Opsi</option>");
						for (var i = 0; i < data.length; i++) {
							$("#iddokter").append("<option value='" + data[i].iddokter + "'>" + data[i].namadokter + "</option>");
						}
						$("#iddokter").selectpicker("refresh");
					}
				});
			}
		});

		$("#form-work").submit(function(e) {
			var form = this;

			if ($("#kelompokpasien").val() == "1") {
				if ($("#rekanan1").val() == "") {
					e.preventDefault();
					sweetAlert("Maaf...", "Rekanan 1 Belum Dipilih!", "error");
					return false;
				}
			}

			if ($("#kelompokpasien").val() == "3") {
				if ($("#bpjskesehatan1").val() == "") {
					e.preventDefault();
					sweetAlert("Maaf...", "BPJS Kesehatan Belum Dipilih!", "error");
					return false;
				}
			}

			if ($("#kelompokpasien").val() == "4") {
				if ($("#bpjstenagakerja1").val() == "") {
					e.preventDefault();
					sweetAlert("Maaf...", "BPJS Tenagakerja Belum Dipilih!", "error");
					return false;
				}
			}

			if ($("#iddokter").val() == "") {
				e.preventDefault();
				sweetAlert("Maaf...", "Dokter Belum Dipilih!", "error");
				return false;
			}

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});

	function loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon) {
		var tablePasien = $('#datatable-pasien').DataTable({
			"pageLength": 8,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpoliklinik_pendaftaran/getPasien',
				type: "POST",
				dataType: 'json',
				data: {
					snomedrec: snomedrec,
					snamapasien: snamapasien,
					snama_keluarga: snama_keluarga,
					salamat: salamat,
					stanggallahir: stanggallahir,
					snotelepon: snotelepon,
				}
			},
			"columnDefs": [{
				"targets": [0],
				"orderable": true
			}]
		});
	}

	function getHistoryKunjungan(idpasien) {
		$.ajax({
			url: "{site_url}tpoliklinik_pendaftaran/getDataHistory/" + idpasien,
			method: "GET",
			success: function(data) {
				$("#historykunjungan").html(data);
			}
		});
	}

	function get_duplicate_hariini() {
		var idpasien=$("#idpasien").val();
		var idpoliklinik=$("#poliklinik").val();
		var iddokter=$("#iddokter").val();
		var tglpendaftaran=$("#tglpendaftaran").val();

		<?php if ($this->uri->segment(2) != 'update') { ?>
			var validationIdpasien = idpasien != '';
			var validationIdpoliklinik = idpoliklinik != '';
			var validationIddokter = iddokter != '';
			var validationTglpendaftaran = tglpendaftaran != '';
		<?php } else { ?>
			var validationIdpasien = idpasien != '' && idpasien != '<?=$idpasien?>';
			var validationIdpoliklinik = idpoliklinik != '' && idpoliklinik != '<?=$idpoliklinik?>';
			var validationIddokter = iddokter != '' && iddokter != '<?=$iddokter?>';
			var validationTglpendaftaran = tglpendaftaran != '' && tglpendaftaran != '<?=$tanggaldaftar?>';
		<?php } ?>

		if (validationIdpasien && validationIdpoliklinik && validationIddokter && validationTglpendaftaran){
			$.ajax({
				url: "{site_url}tpoliklinik_pendaftaran/get_duplicate_hariini/" + idpasien + '/' + idpoliklinik + '/'+iddokter + '/' + tglpendaftaran,
				method: "GET",
				success: function(data) {
					if (data == 1){
						alert('Pasien Sudah Daftar Ke Dokter : ' + $("#iddokter option:selected").text());
						$('#btn_simpan').attr('disabled','disabled');
					}else{
						$('#btn_simpan').removeAttr('disabled');
					}
				}
			});
		}
	}

	function getTitlePasien(tahun, bulan, hari, statuskawin, jeniskelamin) {
		if (tahun == 0 && bulan <=5) {
			$('#titlepasien').val("By");
		} else if ((tahun == 0 && bulan > 5) || tahun <= 15) {
			$('#titlepasien').val("An");
		} else if (tahun >= 16 && jeniskelamin == '1') {
			$('#titlepasien').val("Tn");
		} else if (tahun >= 15 && jeniskelamin == '2' && statuskawin == 1) {
			$('#titlepasien').val("Nn");
		} else if (jeniskelamin == '2' && statuskawin == 2) {
			$('#titlepasien').val("Ny");
		} else if (jeniskelamin == '2' && statuskawin == 3) {
			$('#titlepasien').val("Ny");
		} else if (jeniskelamin == '2' && statuskawin == 4) {
			$('#titlepasien').val("Tn");
		}
	}

	function show_hide($cob,$kel_pasien){
		hide_all($cob);
		if ($cob=='1'){
			if ($kel_pasien=='1'){
				$("#rekanan").css("display", "block");
				$("#jp").css("display", "block");
			}else if($kel_pasien=='2'){
				$("#jp").css("display", "block");
			}else if($kel_pasien=='3'){
				$("#bpjskesehatan").css("display", "block");
				$("#jp").css("display", "block");
			}else if($kel_pasien=='4'){
				$("#bpjstenagakerja").css("display", "block");
				$("#jp").css("display", "block");
			}else if($kel_pasien=='5'){//UMUM
				$("#jp").css("display", "none");
				hide_all(2);
				$("#kelompok1").css("display", "none");
			}else{
				$("#jp").css("display", "block");
			}
		}else{
			if ($kel_pasien=='1'){
				$("#rekanan2").css("display", "block");
			}else if($kel_pasien=='2'){
			}else if($kel_pasien=='3'){
				$("#bpjskesehatan2").css("display", "block");
			}else if($kel_pasien=='4'){
				$("#bpjstenagakerja2").css("display", "block");
			}else if($kel_pasien=='5'){
			}else{
			}
		}
	}

	function hide_all($cob){
		if ($cob=='1'){
			$("#rekanan").css("display", "none");
			$("#bpjskesehatan").css("display", "none");
			$("#bpjstenagakerja").css("display", "none");
		}else{
			$("#rekanan2").css("display", "none");
			$("#bpjskesehatan2").css("display", "none");
			$("#bpjstenagakerja2").css("display", "none");
		}
		// $("#bpjstenagakerja").css("display", "none");
	}
</script>