<!DOCTYPE html>
<html>

<head>
	<title>Bukti Pendaftaran</title>
	<style type="text/css">
		@page {
			size: 80mm 200mm;
            margin-top: 1em;
            margin-left: 2em;
            margin-bottom: 0.5em;
        }
		* {
			font-size: 24px;
		}
		body {

		}
		table {
			border: 0px solid black;
			font-size: 24px !important;
			border-collapse: collapse !important;
            font-family: "Courier", Arial,sans-serif;
			/*font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;*/
		  }
		  td {
			border: 0px solid black;
			padding: 0px;
			height : 10px;
			vertical-align: middle;
		  }
		  .text-center{
			text-align: center !important;
		  }
		  .text-bold{
			font-weight: bold;
		  }
		  .border-thick-bottom{
			border-bottom:1px dashed #000 !important;
		  }
		  .border-thick-top{
			border-top:1px dashed #000 !important;
		  }
	</style>
</head>

<body>
<table style="width:60mm;">
	<tr>
		<td class="text-bold text-center" colspan="3">RSKB HALMAHERA SIAGA <br>
							  JL.LLRE.Martadinata No.28<br>
								     Bandung, 40015<br></td>
	</tr>

</table>
<table style="width:60mm;">

	<tr>
		<td class="border-thick-bottom" colspan="3"></td>
	</tr>
	<tr>
		<td class="text-bold text-center" colspan="3">BUKTI PENDAFTARAN</td>
	</tr>
	<tr>
		<td class="border-thick-top" colspan="3"></td>
	</tr>
	<tr>
		<td width="24%">No. Pendaftaran</td>
		<td width="1%">:</td>
		<td width="75%"><?=$nopendaftaran?></td>
	</tr>
	<tr>
		<td>No. Medrec</td>
		<td>:</td>
		<td><?=$nomedrec?></td>
	</tr>
	<tr>
		<td>Nama</td>
        <td width="1%">:</td>
		<td><?=$title.'. '.$namapasien?></td>
	</tr>
	<tr>
		<td>Tgl Lahir</td>
        <td width="1%">:</td>
		<td><?=HumanDateShort($tanggallahir)?></td>
	</tr>
	<tr>
		<td >Pen. Jawab</td>
        <td width="1%">:</td>
		<td><?=$namapenanggungjawab?></td>
	</tr>
	<tr>
		<td>Kel. Pasien</td>
        <td width="1%">:</td>
		<td><?=$namakelompok?></td>
	</tr>
	<tr>
		<td>Asuransi</td>
        <td width="1%">:</td>
		<td><?=$namarekanan?></td>
	</tr>
	<tr>
		<td>Klinik</td>
        <td width="1%">:</td>
		<td><?=$namapoliklinik?></td>
	</tr>
	<tr>
		<td>Dokter</td>
        <td width="1%">:</td>
		<td><?=$namadokter?></td>
	</tr>
	<tr>
		<td >No. Antrian</td>
        <td width="1%">:</td>
		<td><?=$noantrian?></td>
	</tr>

	<tr>
		<td class="border-thick-bottom" colspan="3" ></td>
	</tr>
	<tr>
		<td colspan="3"><?= date("d-m-Y H:m:s")?> <?=$namauserinput?></td>
	</tr>
</table>


</body>

</html>
