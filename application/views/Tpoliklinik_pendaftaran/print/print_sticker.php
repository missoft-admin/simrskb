<!DOCTYPE html>
<html>

<head>
	<title>Sticker ID</title>
	<style type="text/css">
		* {
			color: black;
			font-size: 16px;
			font-weight: 700;
			font-style: normal;
			text-decoration: none;
			font-family: "Arial Rounded MT Bold", sans-serif;
			text-align: left;
			vertical-align: top;
			white-space: pre-wrap;
		}

		.row:after {
	    content: "";
	    display: table;
	    clear: both;
		}

		.column {
			float: left;
    	width: 50%;
		}
	</style>
</head>

<body>
	<?php for ($i=1; $i <= 5; $i++) { ?>
	<div class="row">
		<div class="column">
			<table>
				<tr>
					<td><?=$nomedrec?> (<?=$jeniskelamin?>)</td>
				</tr>
				<tr>
					<td>An. <?=$namapasien?></td>
				</tr>
				<tr>
					<td><?=wordwrap($alamat,40,"<br>\n",TRUE);?></td>
				</tr>
				<tr>
					<td><?=DMYFormat($tanggallahir)?></td>
					<td><?=$umurtahun?> Th <?=$umurbulan?> Bln</td>
				</tr>
				<tr>
					<td><?=$pekerjaan?></td>
					<td><?=$notelepon?></td>
				</tr>
			</table>
		</div>
		<div class="column">
			<table>
				<tr>
					<td><?=$nomedrec?> (<?=$jeniskelamin?>)</td>
				</tr>
				<tr>
					<td>An. <?=$namapasien?></td>
				</tr>
				<tr>
					<td><?=wordwrap($alamat,40,"<br>\n",TRUE);?></td>
				</tr>
				<tr>
					<td><?=DMYFormat($tanggallahir)?></td>
					<td><?=$umurtahun?> Th <?=$umurbulan?> Bln</td>
				</tr>
				<tr>
					<td><?=$pekerjaan?></td>
					<td><?=$notelepon?></td>
				</tr>
			</table>
		</div>
	</div>
	<br><br><br>
	<?php } ?>
</body>

</html>
