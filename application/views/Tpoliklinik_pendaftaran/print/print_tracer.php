<!DOCTYPE html>
<html>

<head>
	<title>Kartu Tracer</title>
	<style type="text/css">
		@page {
			size: 80mm 200mm;
            margin-top: 1em;
            margin-left: 2.5em;
            margin-bottom: 0.5em;
        }
		* {
			font-size: 16px;
		}
		body {

		}
		table {
			border: 0px solid black;
			font-size: 16px !important;
			border-collapse: collapse !important;
			font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;
		  }
		  td {
			border: 0px solid black;
			padding: 0px;
			height : 10px;
			vertical-align: middle;
		  }
		  .text-center{
			text-align: center !important;
		  }
		  .text-bold{
			font-weight: bold;
		  }
		  .border-thick-bottom{
			border-bottom:1px dashed #000 !important;
		  }
	</style>
</head>

<body>

<table style="width:68mm;">
	<tr>
		<td class="text-bold text-center" colspan="3">KARTU TRACER</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td width="30%">No. Kunjungan</td>
		<td>:</td>
		<td width="70%"><?=$noantrian?></td>
	</tr>

	<tr>
		<td>No. Medrec</td>
		<td>:</td>
		<td><?=$nomedrec?></td>
	</tr>
	<tr>
		<td>Nama</td>
		<td>:</td>
		<td><?=$namapasien?></td>
	</tr>
	<tr>
		<td>Kel. Pasien</td>
		<td>:</td>
		<td><?=$namakelompok?></td>
	</tr>
	<tr>
		<td>Klinik</td>
		<td>:</td>
		<td><?=$namapoliklinik?></td>
	</tr>
	<tr>
		<td>Dokter</td>
		<td>:</td>
		<td><?=$namadokter?></td>
	</tr>
	<tr>
		<td class="border-thick-bottom" colspan="3" ></td>
	</tr>
	<tr>
		<td colspan="3"><?= date("d-m-Y H:m:s")?> <?=$namauserinput?></td>
	</tr>
</table>

</body>

</html>
