<!DOCTYPE html>
<html>

<head>
	<title>Bukti Pendaftaran</title>
	<style type="text/css">
		@page {
			size: 80mm 250mm;
            margin-top: 1em;
            margin-left: 2em;
            margin-bottom: 0.5em;
        }
		* {
			font-size: 24px;
		}
		body {

		}
		br {
		   display: block;
		   margin: 2px 0;
		}
		table {
			border: 0px solid black;
			font-size: 23px !important;
			border-collapse: collapse !important;
            font-family: "Courier", Arial,sans-serif;
			/*font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;*/
		  }
		  td {
			border: 0px solid black;
			padding: 2px;
			height : 10px;
			vertical-align: top;
		  }
		  .text-center{
			text-align: center !important;
		  }
		  .text-bold{
			font-weight: bold;
		  }
		  .border-thick-bottom{
			border-bottom:1px dashed #000 !important;
		  }
		  .border-thick-top{
			border-top:1px dashed #000 !important;
		  }
		  .font-antrian-top{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-top:1px solid #000 !important;
			font-size: 29px !important;
		  }
		  .font-antrian{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-bottom:1px solid #000 !important;
			font-size: 50px !important;
		  }
		  .font-kode{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-bottom:1px solid #000 !important;
			font-size: 44px !important;
		  }

		  .font-biasa-top{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-top:1px solid #000 !important;
			border-bottom:1px dashed #000 !important;
			font-size: 24px !important;
		  }
		  .font-biasa-bottom{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-bottom:1px solid #000 !important;

			font-size: 24px !important;
		  }.
		  .font-daftar{
			font-size: 24px !important;
		  }
		   input[type=checkbox] { display: inline; }
		  .font-checkbox{
			font-size: 23px !important;
		  }
		  .border-full {
			border: 1px solid #000 !important;
			
		  }
	</style>
</head>

<body>
<table style="width:70mm;">
	<tr>
		<td class="text-bold text-center" colspan="3">RSKB HALMAHERA SIAGA <br>
							  JL.LLRE.Martadinata No.28<br>
								     Bandung, 40015<br></td>
	</tr>
</table>
<table style="width:70mm;">
	<tr>
		<td class="border-thick-bottom" colspan="3"></td>
	</tr>
	<tr>
		<td class="text-bold text-center font-daftar" colspan="3">BUKTI PENDAFTARAN</td>
	</tr>
	<tr>
		<td class="border-thick-top" colspan="3"></td>
	</tr>

</table>
<br>
<table style="width:60mm;">
	<tr>
		<td class="font-biasa-top text-center" style="width:40mm;"><?=$tipe_nama?> <br> <?=$namapoliklinik?></td>
		<td class="font-antrian-top text-center text-bold"  style="width:20mm;">ANTRIAN</td>
	</tr>
	<tr>
		<td class="font-biasa-bottom text-center" style="width:40mm;"><?=$namadokter?></td>
		<td class="font-kode text-center text-bold"  style="width:20mm;"><?=$kode_antrian?></td>
	</tr>
</table>
<br>
<table style="width:70mm;">
	<tr>
		<td width="24%">No. Pendaftaran</td>
		<td width="1%">:</td>
		<td width="75%"><?=$nopendaftaran?></td>
	</tr>
	<tr>
		<td >No. Medrec</td>
		<td >:</td>
		<td ><?=$nomedrec?></td>
	</tr>
	<tr>
		<td >Nama Pasien</td>
		<td >:</td>
		<td ><?=$title.'. '.$namapasien?></td>
	</tr>
	<tr>
		<td >Tanggal Lahir</td>
		<td >:</td>
		<td ><?=HumanDateShort($tanggallahir)?></td>
	</tr>
	<tr>
		<td >Pen. Jawab</td>
		<td >:</td>
		<td ><?=$namapenanggungjawab?></td>
	</tr>
	<?if ($nopendaftaran=='1'){?>
	<tr>
		<td >Tujuan</td>
		<td >:</td>
		<td ><?=$namapoliklinik?></td>
	</tr>
	<tr>
		<td >Dokter</td>
		<td >:</td>
		<td ><?=$namadokter?></td>
	</tr>
	<tr>
		<td >Antrian</td>
		<td >:</td>
		<td ><?=$noantrian?></td>
	</tr>
	<?}?>
	<tr>
		<td >Pembayaran</td>
		<td >:</td>
		<td ><?=$namakelompok?></td>
	</tr>
	<tr>
		<td >Rekanan</td>
		<td >:</td>
		<td ><?=$namarekanan?></td>
	</tr>

	<tr>
		<td colspan="3"></td>
	</tr>
	<tr>
		<td class="border-thick-top" colspan="3"></td>
	</tr>
	<tr>
		<td colspan="3"><?= date("d-m-Y H:m:s")?> <?=$namauserinput?></td>
	</tr>

</table>
<br>
<br>
<br>
<table style="width:70mm;margin-top:10px">
	<tr>
		<td colspan="2" class="text-bold "><br></td>
	</tr>
	<tr>
		<td colspan="2" class="text-bold">PEMERIKSAAN PENUNJANG<br></td>
	</tr>
	<tr>
		<td class="font-checkbox ">
			
		</td>
		<td class="font-checkbox ">
			
		</td>
		
	</tr>
	<tr>
		<td colspan="2" style="width:100%;" class="font-checkbox">
			<input type="checkbox" class="cls_checkbox"><label class="font-checkbox"> Radiologi</label>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="width:100%;" class="font-checkbox">
			<input type="checkbox" class="cls_checkbox"><label class="font-checkbox"> Laboratorium</label>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="width:100%;" class="font-checkbox">
			<input type="checkbox" class="cls_checkbox"><label class="font-checkbox"> Farmasi</label>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="text-bold"></td>
	</tr>
	<tr>
		<td colspan="2" class="text-bold"><br>KONSULTASI<br></td>
	</tr>
	<tr>
		<td colspan="2" class="text-bold"><br><br></td>
	</tr>
	<tr>
		<td colspan="2" style="width:100%;" class="font-checkbox">
			<input type="checkbox" class="cls_checkbox"><label class="font-checkbox"> Dokter Specialis</label>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="width:100%;" class="font-checkbox">
			<input type="checkbox" class="cls_checkbox"><label class="font-checkbox"> Dokter Sub Specialis</label>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="width:100%;" class="font-checkbox">
			<input type="checkbox" class="cls_checkbox"><label class="font-checkbox"> Dokter Umum</label>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="text-bold"></td>
	</tr>
	<tr>
		<td colspan="2" class="text-bold"><br>TINDAKAN / PEMAKAIAN<br></td>
	</tr>
	
	<?for ($i=1;$i<=10;$i++){?>
	<tr>
		<td colspan="2" style="width:100%;" class="font-checkbox">
			<label class="font-checkbox"><?=$i?>.</label>
		</td>
	</tr>
	<?}?>
	
</table>
</body>

</html>
