<!DOCTYPE html>
<html>

<head>
	<title>Kartu Status</title>
	<style type="text/css">
		* {
			font-family:Arial;
		}

		.header {
			font-size: 20px;
			font-family: "Arial", Times, serif;
			font-weight: bold;
			margin-bottom: 20px;
		}

		b {
			font-size: 10px;
		}

		h3,
		h4,
		h5 {
			text-align: center;
			text-transform: uppercase;
			padding-bottom: 2px;
		}

		table {
			font-size: 12px;
		}

		#detail td {
			text-align: center;
			padding: 4px 3px;
			border: 1px solid #aaa;
		}
		td {
			padding: 1px;
			border: 0px solid black;
		}
		.border-full {
				border: 1px solid #000 !important;
			}
		.border-full-text-bold {
				border: 1px solid #000 !important;
				font-size: 50px;
				text-align: center;
				height: 50px;
				padding: 0px 0.5px;
			}
		#detail th {
			background: #000;
			color: #fff;
			padding: 2px 0.5px;
			text-transform: uppercase;
			font-size: 11px
		}
		.box {
			background: rgb(210, 223, 231);
			color: #000;
			padding: 2px 0.5px;
			text-transform: uppercase;
			font-size: 11px
		}
		.left { text-align:left; }
		#detail {
			border: 0.5px solid #aaa;
			font-family: "Times New Roman", Times, serif;
		}

		#box {
			width: 300px;
			height: 400px;
		}
	</style>
</head>

<body>
	<?php $generator = new Picqer\Barcode\BarcodeGeneratorPNG(); ?>
	<table width="100%">
	  <tr>
		<td width="25%" align="center"><img style="width:60px;" src="assets/upload/logo/logo.png"></td>
		<td rowspan="2" width="2%"></td>
		<td rowspan="2" class="border-full-text-bold" width="7.3%"><?=substr($nomedrec, 0, 1)?></td>
		<td rowspan="2" class="border-full-text-bold"  width="7.3%"><?=substr($nomedrec, 1, 1)?></td>
		<td rowspan="2" class="border-full-text-bold"  width="7.3%"><?=substr($nomedrec, 2, 1)?></td>
		<td rowspan="2" class="border-full-text-bold"  width="7.3%"><?=substr($nomedrec, 3, 1)?></td>
		<td rowspan="2" class="border-full-text-bold"  width="7.3%"><?=substr($nomedrec, 4, 1)?></td>
		<td rowspan="2" class="border-full-text-bold"  width="7.3%"><?=substr($nomedrec, 5, 1)?></td>
		<td rowspan="2" class="border-full-text-bold"  width="7.3%"><?=substr($nomedrec, 6, 1)?></td>
		<td rowspan="2" class="border-full-text-bold"  width="7.3%"><?=substr($nomedrec, 7, 1)?></td>
		<td rowspan="2" class="border-full-text-bold"  width="7.3%"><?=substr($nomedrec, 8, 1)?></td>
	  </tr>
	  <tr>
		<td align="center">JL. L.L.R.E Martadinata No. 28 Bandung <br>Telp. (022) 4206061</td>
	  </tr>
	  
	</table>
	
	<br>

		<div class="header">
			
			<center>RINGKASAN RIWAYAT KLINIK</center>
		</div>
		<table id="detail" cellpadding="0" cellspacing="0" width="100%">
			<tr style="display: none;">
				<th hidden="true">&nbsp;</th>
				<th hidden="true">&nbsp;</th>
				<th hidden="true">&nbsp;</th>
				<th hidden="true">&nbsp;</th>
				<th hidden="true">&nbsp;</th>
				<th hidden="true">&nbsp;</th>
			</tr>
			<tr>
				<td class="box" colspan="3" width="50%">
					Nama Lengkap					
				</td>
				<td class="box" colspan="3" width="50%">
					Nomor Barcode Rekam Medis					
				</td>
			</tr>
			<tr>
				<td colspan="3" width="50%">
					
					<b><?=$title?>. <?=$namapasien?></b>
					<br>
				</td>
				<td colspan="3" width="50%">
					<br>
					<?='<img width="100" height="35px" src="data:image/png;base64,' . base64_encode($generator->getBarcode($nomedrec, $generator::TYPE_CODE_128)) . '">'; ?>
					<br>
				</td>
			</tr>
			<tr>
				<td class="box" colspan="1" align="center" width="16.6%">Umur</td>
				<td  class="box" colspan="1" align="center" width="16.6%">Kelamin</td>
				<td  class="box" colspan="1" align="center" width="16.6%">Gol Darah</td>
				<td  class="box" colspan="1" align="center" width="16.6%">Status</td>
				<td  class="box" colspan="1" align="center" width="14.6%">Agama</td>
				<td  class="box" colspan="1" align="center" width="18.6%">Pekerjaan</td>
			</tr>
			<tr>
				<td colspan="1" align="center" width="16.6%"><?=$umurtahun.' Thn '.$umurbulan.' Bln '.$umurhari.' Hr'?></td>
				<td colspan="1" align="center" width="16.6%"><?=$jeniskelamin?></td>
				<td colspan="1" align="center" width="16.6%"><?=GetGolonganDarah($golongandarah)?></td>
				<td colspan="1" align="center" width="16.6%"><?=GetStatusKawin($statuskawin)?></td>
				<td colspan="1" align="center" width="14.6%"><?=$agama?></td>
				<td colspan="1" align="center" width="18.6%"><?=$pekerjaan?></td>
			</tr>
			<tr>
				<td class="box" colspan="6" width="100%" style="text-align: center;">&nbsp;Alamat Lengkap
				</td>
			</tr>
			<tr>
				<td colspan="6" width="100%" style="text-align: center;"><?=$alamat?>
				</td>
			</tr>
			<tr>
				<td class="box" colspan="1" width="18.75%">Tanggal Lahir</td>
				<td class="box" colspan="1" width="18.75%">Kelompok Pasien</td>
				<td class="box" colspan="2" width="25%">Rujukan Dari</td>
				<td class="box" colspan="1" width="18.75%">Nomor Asuransi</td>
				<td class="box" colspan="1" width="18.75%">Nomor Telepon</td>
			</tr>
			<tr>
				<td colspan="1" width="18.75%"><?=DMYFormat($tanggallahir)?></td>
				<td colspan="1" width="18.75%"><?=$namakelompok?></td>
				<td colspan="2" width="25%"><?=$asalpasien?></td>
				<td colspan="1" width="18.75%">-</td>
				<td colspan="1" width="18.75%"><?=$notelepon?></td>
			</tr>
			<tr>
				<td colspan="6" style="height: 5px; padding: 0px 0px 0px 0px;">&nbsp;</td>
			</tr>
			
		</table>
		<table id="detail" cellpadding="0" cellspacing="0" width="100%">			
			<tr>
				<td class="box" colspan="1" width="16.6%">Tanggal</td>
				<td class="box" colspan="1" width="16.6%">Klinik</td>
				<td class="box" colspan="1" width="16.6%">Anamnesa</td>
				<td class="box" colspan="1" width="16.6%">Diagnosa Utama</td>
				<td class="box" colspan="1" width="26.6%">Terapi</td>
				<td class="box" colspan="1" width="6.6%">Paraf</td>
			</tr>
			<?foreach($list_riwayat as $row){?>
			<tr>
				<td colspan="1" width="16.6%"><?=HumanDateShort($row->tanggaldaftar)?><br><?=HumanTime($row->tanggaldaftar)?></td>
				<td colspan="1" width="16.6%"><?=$row->nama_poli?><br><?=$row->nama_dokter?></td>
				<td colspan="1" width="16.6%"><?=$row->keluhan?></td>
				<td colspan="1" width="16.6%"><?=$row->diagnosa?></td>
				<td colspan="1" width="26.6%"><div class="left"><p><b>Tindakan : </b> <?=$row->layanan?> 
											  <br>  <br><b>Farmasi : </b> <?=$row->far?> <br>
											  <br> <b>Radiologi : : </b> <?=$row->Rad?> <br>
											  <b>Laboratorium : </b> <?=$row->lab?> <br>
											  <b>Fisioterapi : </b> <?=$row->fisio?> <br>
											  <b>Obat & Alkes Ruangan : </b> <?=$row->obat.' - '.$row->alkes?> 
											  </div>
											  </td>
				<td colspan="1" width="6.6%">&nbsp;</td>
			</tr>
			<?}?>
		</table>
</body>

</html>
