<!DOCTYPE html>
<html>

<head>
	<title>KIB</title>
	<style type="text/css">
		@page {
			size: 85mm 55mm;
            margin-top: 4.6em;
            margin-left: 1.3em;
            margin-bottom: 0.5em;
        }
		* {
			font-size: 24px;
			font-family:Arial, sans-serif;
            font-weight: bold;
		}
	</style>
</head>

<body>
<?php $generator = new Picqer\Barcode\BarcodeGeneratorPNG(); ?>
<?=$namapasien?><br>
<?=HumanDateShort($tanggallahir)?><br>
<?=$nomedrec?><br>
<?='<img width="150" height="50" src="data:image/png;base64,' . base64_encode($generator->getBarcode($nomedrec, $generator::TYPE_CODE_128)) . '">'; ?>
</body>

</html>
