<!DOCTYPE html>
<html>

<head>
	<title>Bukti Pendaftaran</title>
	<style type="text/css">
		@page {
			size: 100mm 200mm;
            margin-top: 1.5em;
            margin-left: 1em;
            margin-bottom: 0.5em;
        }
		* {
			font-size: 24px;
		}
		body {

		}
		br {
		   display: block;
		   margin: 2px 0;
		}
		table {
			border: 0px solid black;
			font-size: 24px !important;
			border-collapse: collapse !important;
            font-family: "Courier", Arial,sans-serif;
			/*font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;*/
		  }
		  td {
			border: 0px solid black;
			padding: 2px;
			height : 10px;
			vertical-align: middle;
		  }
		  .text-center{
			text-align: center !important;
		  }
		  .text-bold{
			font-weight: bold;
		  }
		  .border-thick-bottom{
			border-bottom:1px dashed #000 !important;
		  }
		  .border-thick-top{
			border-top:1px dashed #000 !important;
		  }
		  .font-antrian-top{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-top:1px solid #000 !important;
			font-size: 29px !important;
		  }
		  .font-antrian{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-bottom:1px solid #000 !important;
			font-size: 84px !important;
		  }

		  .font-biasa-top{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-top:1px solid #000 !important;
			border-bottom:1px dashed #000 !important;
			font-size: 24px !important;
		  }
		  .font-biasa-bottom{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-bottom:1px solid #000 !important;

			font-size: 24px !important;
		  }.
		  .font-daftar{
			font-size: 34px !important;
		  }
	</style>
</head>

<body>
<table style="width:90mm;">
	<tr>
		<td class="text-bold text-center" colspan="3">RSKB HALMAHERA SIAGA <br>
							  JL.LLRE.Martadinata No.28<br>
								     Bandung, 40015<br></td>
	</tr>
</table>
<table style="width:90mm;">
	<tr>
		<td class="border-thick-bottom" colspan="3"></td>
	</tr>
	<tr>
		<td class="text-bold text-center font-daftar" colspan="3">BUKTI PENDAFTARAN</td>
	</tr>
	<tr>
		<td class="border-thick-top" colspan="3"></td>
	</tr>

</table>
<br>
<table style="width:90mm;">
	<tr>
		<td class="font-biasa-top text-center" style="width:60mm;"><?=$tipe_nama?> <br> <?=$namapoliklinik?></td>
		<td class="font-antrian-top text-center text-bold"  style="width:30mm;">ANTRIAN</td>
	</tr>
	<tr>
		<td class="font-biasa-bottom text-center" style="width:60mm;"><?=$namadokter?></td>
		<td class="font-antrian text-center text-bold"  style="width:30mm;"><?=$noantrian?></td>
	</tr>
</table>
<br>
<table style="width:90mm;">
	<tr>
		<td style="width:30mm;">No. Pendaftaran</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$nopendaftaran?></td>
	</tr>
	<tr>
		<td style="width:30mm;">No. Medrec</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$nomedrec?></td>
	</tr>
	<tr>
		<td style="width:30mm;">Nama Pasien</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$title.'. '.$namapasien?></td>
	</tr>
	<tr>
		<td style="width:30mm;">Tanggal Lahir</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=HumanDateShort($tanggallahir)?></td>
	</tr>
	<tr>
		<td style="width:30mm;">Pen. Jawab</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$namapenanggungjawab?></td>
	</tr>
	<tr>
		<td style="width:30mm;">Tujuan</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$namapoliklinik?></td>
	</tr>
	<tr>
		<td style="width:30mm;">Dokter</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$namadokter?></td>
	</tr>
	<tr>
		<td style="width:30mm;">Antrian</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$noantrian?></td>
	</tr>
	<tr>
		<td style="width:30mm;">Pembayaran</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$namakelompok?></td>
	</tr>
	<tr>
		<td style="width:30mm;">Rekanan</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;"><?=$namarekanan?></td>
	</tr>

	<tr>
		<td colspan="3"></td>
	</tr>
	<tr>
		<td class="border-thick-top" colspan="3"></td>
	</tr>
	<tr>
		<td colspan="3"><?= date("d-m-Y H:m:s")?> <?=$namauserinput?></td>
	</tr>

</table>


</body>

</html>
