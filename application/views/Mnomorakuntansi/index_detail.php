<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        
        <h3 class="block-title">{title}</h3>
    </div>
	<div class="block-content">
		  <?php echo form_open('Mnomorakuntansi/export','class="form-horizontal" id="form-work" target="_blank"') ?>
		<div class="form-horizontal row col-md-12">
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">No Akun</label>
				<div class="col-md-5">
					<input type="hidden" class="form-control" disabled id="idakun" name="idakun" value="{idakun}" />
					<input type="text" class="form-control" disabled name="namaakun" value="{namaakun}" />
				</div>
				<label class="col-md-1" for="">Nama Akun</label>
				<div class="col-md-5">
					<input type="text" class="form-control"  disabled name="noakun" value="{noakun}" />
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Tanggal</label>
				<div class="col-md-5">
					<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
						<input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
					</div>
				</div>
				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">No. Bukti</label>
				<div class="col-md-5">
					<input type="text" class="form-control" <?=$disabel?> placeholder="No. Bukti" id="notransaksi" value="{notransaksi}" />
				</div>
				<label class="col-md-1" for="">No. Validasi</label>
				<div class="col-md-5">
					<input type="text" class="form-control" <?=$disabel?> placeholder="No. Validasi" id="nojurnal" name="nojurnal" value="{nojurnal}" />
				</div>
			</div>
			
			<div class="form-group" style="margin-bottom: 15px;margin-top: 5px;">
				<label class="col-md-1" for="">Jenis Jurnal</label>
				<div class="col-md-11">
					<select name="ref_validasi[]" id="ref_validasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
						<?php foreach  ($list_ref as $row) { ?>
							<option value="'<?=$row->ref_validasi?>'"><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-1" for=""></label>
				<div class="col-md-11">
					<button class="btn btn-info" type="button" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
					<button class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
					<button class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
					<button class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th width="15%" hidden>Tanggal</th>
                    <th width="15%">Tanggal</th>
                    <th width="10%">No Validasi</th>
                    <th width="20%">No. Bukti</th>
                    <th width="10%">Nama Akun</th>
                    <th width="5%">Debit</th>
                    <th width="5%">Kredit</th>
                    <th width="15%">Keterangan</th>
                    <th width="10%">Asal Jurnal</th>
                    <th width="15%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var idakun;
	$(document).ready(function(){
        idakun=$("#idakun").val();
		$(".decimal").number(true,2,'.',',');
	   // alert(tanggalkontrabon);
		load_detail();
    })
	
	$(document).on("click","#btn_filter",function(){	
		load_detail();		
	});
	function load_detail() {
		// alert('sini');
		var idakun=$("#idakun").val();
		var ref_validasi=$("#ref_validasi").val();
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var notransaksi=$("#notransaksi").val();
		var nojurnal=$("#nojurnal").val();
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}Mnomorakuntansi/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				idakun: idakun,
				ref_validasi: ref_validasi,
				tanggal_trx1: tanggal_trx1,
				tanggal_trx2: tanggal_trx2,
				notransaksi: notransaksi,
				nojurnal: nojurnal,
				
			}
		},
		columnDefs: [
					 {"targets": [0], "visible": false },
					 {  className: "text-right", targets:[6,5] },
					 {  className: "text-center", targets:[1,2,3,8] },
					 { "width": "5%", "targets": [0] },
					 { "width": "8%", "targets": [1,2,3,8,5,6] },
					 { "width": "15%", "targets": [4,9] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
</script>