<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mperiode_akun" class="btn btn-primary" style="color: #fff;">
					<i class="fa fa-file"></i> Atur Saldo
				</a>
            </li>
            <li>
                <a href="{base_url}mnomorakuntansi/create" class="btn btn-success" style="color: #fff;">
					<i class="fa fa-plus"></i> Tambah No. Akun
				</a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
		<div class="row">
			<?php echo form_open('Mnomorakuntansi/export','class="form-horizontal" id="form-work" target="_blank"') ?>
			 <div class="col-md-12">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-1" for="">Kategori Akun</label>
					<div class="col-md-5">
						<select name="idkategori" id="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>- All Kategori -</option>
							<?php foreach  ($list_kategori as $row) { ?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
					<label class="col-md-1" for="">Status</label>
					<div class="col-md-5">
						<select class="form-control js-select2" name="status" id="status">
							<option value="#" selected>Semua Status</option>
							<option value="1" <?= ($status== '1' ? "selected" : ""); ?>>Aktif</option>
							<option value="0" <?= ($status== '0' ? "selected" : ""); ?>>Tidak Aktif</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-1" for="">Jika Bertambah</label>
					<div class="col-md-5">
						<select class="form-control js-select2" id="bertambah" name="bertambah" style="width:100%">
							<option value="#" selected>- All -</option>
							<option value="DB">Debit</option>
							<option value="CR">Kredit</option>
						</select>
					</div>
					<label class="col-md-1" for="">Jika Berkurang</label>
					<div class="col-md-5">
						<select class="form-control js-select2" id="berkurang" name="berkurang">
							<option value="#" selected>- All -</option>
							<option value="DB">Debit</option>
							<option value="CR">Kredit</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-12">
			<div class="form-group" style="margin-bottom: 5px;margin-top: 15px;">
				<label class="col-md-1" for="">Header Akun</label>
				<div class="col-md-11">
					<select name="headerakun[]" id="headerakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
						<?php foreach  ($list_header as $row) { ?>
							<option value="'<?=$row->noakun?>'"><?=$row->namaakun?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Nomor Akun</label>
				<div class="col-md-11">
					<select name="noakun[]" id="noakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
						<?php foreach  ($list_akun as $row) { ?>
							<option value="<?=$row->id?>"><?=$row->noakun.'-'.$row->namaakun?></option>
						<?php } ?>
					</select>
					
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px;">
				
			</div>
			<div class="form-group">
				<label class="col-md-1" for=""></label>
				<div class="col-md-11">
					<button class="btn btn-info" type="button" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
					<button class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
					<button class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
					<button class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
				</div>
			</div>
		</div>
		</div>
		<?php echo form_close(); ?>
    </div>
	<div class="block-content">
		  <div class="table-responsive">
          <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="15%">No Header</th>
                    <th width="10%">No Akun</th>
                    <th width="20%">Nama Akun</th>
                    <th width="10%">Kategori</th>
                    <th width="5%">Status</th>
                    <th width="10%">Saldo</th>
                    <th width="20%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
		</div>
    </div>
</div>
<div class="modal" id="modal_saldo" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title" id='title_barang'>Atur Saldo</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal">
						<input type="hidden" id="retur_id">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-3 control-label">No. Akun</label>
									<div class="col-md-9">
										<input type="text" class="form-control detail" id="nama_akun" disabled>
										<input type="hidden" class="form-control" name="idakun" id="idakun" readonly>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Saldo Berjalan Rp.</label>									
									<div class="col-md-4">
										<div class="input-group">
											<input class="form-control decimal" disabled type="text" id="saldo_berjalan" name="saldo_berjalan">
											<span class="input-group-btn">
												<button class="btn btn-primary" type="button"  onclick="goto_retur()" id="button_lihat_retur"><i class="fa fa-eye"></i></button>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group" id="div_tgl_kbo">
									<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Terakhir</label>
									<div class="col-md-4">
										<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
											<input class="js-datepicker form-control" disabled type="text" id="tanggal_terakhir" name="tanggal_terakhir" placeholder="Choose a date..">
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Set Saldo Rp.</label>
									<div class="col-md-4">
										<div class="input-group">
											<input class="form-control decimal" type="text" id="saldo_set" name="saldo_set">
											<span class="input-group-btn">
												<button class="btn btn-primary" type="button" onclick="goto_pengganti()" id="button_lihat_ganti"><i class="fa fa-eye"></i></button>
											</span>
										</div>
									</div>
									<div class="col-md-5">
										<h4><span class="label label-primary" id="div_info"></span></h4>
									</div>
								</div>
								
								
								<div class="form-group" id="div_tgl_kbo">
									<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Saldo</label>
									<div class="col-md-4">
										<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
											<input class="js-datepicker form-control" type="text" id="tanggal_saldo" name="tanggal_saldo" placeholder="Choose a date..">
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
					<button class="btn btn-sm btn-success" id="btn_update" type="button">Update</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
       var idkategori=$("#idkategori").val();
		$(".decimal").number(true,2,'.',',');
	   // alert(tanggalkontrabon);
		load_detail();
    })
	function atur_saldo($id){
		$("#modal_saldo").modal('show');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Mnomorakuntansi/get_data_saldo/'+$id,
			dataType: "json",
			success: function(data) {
				$("#nama_akun").val(data.noakun+' - '+data.namaakun);				
				$("#tanggal_terakhir").val(data.tanggal_terakhir);				
				$("#saldo_berjalan").val(data.saldo);				
				$("#saldo_set").val(0);				
				$("#idakun").val(data.id);	
				$("#cover-spin").hide();				
			}
		});
	}
	function validate_detail() {
		// alert('sini');
		if ($("#tanggal_saldo").val() == "" || $("#tanggal_saldo").val() == null) {
			sweetAlert("Maaf...", "Tanggal Harus diisi!", "error");
			$("#tanggal_saldo").focus();
			return false;
		}
		if ($("#saldo_set").val() == "") {
			sweetAlert("Maaf...", "Saldo Harus diisi!", "error");
			$("#saldo_set").focus();
			return false;
		}
		
		return true;
	}
	$(document).on("click","#btn_update",function(){	
		if (!validate_detail()) return false;
		var tanggal=$("#tanggal_saldo").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Set Saldo Untuk Tanggal "+tanggal+"?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			simpan_saldo();
		});
	
	});
	$(document).on("change","#tanggal_saldo",function(){	
		
		var tanggal_saldo=$("#tanggal_saldo").val();
		var tanggal_terakhir=$("#tanggal_terakhir").val();
		if (tanggal_saldo){	
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}Mnomorakuntansi/komparasi_tanggal/',
				type: 'POST',
				data: {
					tanggal_saldo: tanggal_saldo,
					tanggal_terakhir: tanggal_terakhir,
					},
				dataType: "json",
				success: function(data) {
					$("#cover-spin").hide();
					if (data==false){
						sweetAlert("Maaf...", "Tanggal Lebih Kecil dari Tanggal terakhir kali update", "error");
					}		
				}
			});
		}
		
		console.log(tanggal_saldo + ' ' + tanggal_terakhir);
		
	});
	function simpan_saldo(){
		$("#cover-spin").show();
		var idakun=$("#idakun").val();
		var tanggal_saldo=$("#tanggal_saldo").val();
		var saldo_berjalan=$("#saldo_berjalan").val();
		var saldo_set=$("#saldo_set").val();
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}Mnomorakuntansi/simpan_saldo',
			type: 'POST',
			data: {
				idakun: idakun,
				tanggal_saldo: tanggal_saldo,
				saldo_berjalan: saldo_berjalan,
				saldo_set: saldo_set,
				},
			complete: function() {
				$("#modal_saldo").modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Set Saldo'});
				table.ajax.reload( null, false ); 
				$("#cover-spin").hide();
			}
		});
	}
	$(document).on("click","#btn_filter",function(){	
		load_detail();		
	});
	function load_detail() {
		// alert('sini');
		var idkategori=$("#idkategori").val();
		var headerakun=$("#headerakun").val();
		var noakun=$("#noakun").val();
		var idkategori=$("#idkategori").val();
		var bertambah=$("#bertambah").val();
		var berkurang=$("#berkurang").val();
		var status=$("#status").val();
		// alert(headerakun);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}Mnomorakuntansi/load_index/',
			type: "POST",
			dataType: 'json',
			data: {
				idkategori: idkategori,
				headerakun: headerakun,
				noakun: noakun,
				idkategori: idkategori,
				bertambah: bertambah,
				berkurang: berkurang,
				status: status,
			}
		},
		columnDefs: [
					 {  className: "text-right", targets:[0,6] },
					 {  className: "text-center", targets:[4,5] },
					 { "width": "5%", "targets": [0] },
					 { "width": "8%", "targets": [2,4,6] },
					 { "width": "15%", "targets": [7] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
</script>