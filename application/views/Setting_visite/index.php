<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2218'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2218'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('setting_visite/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-2" >
					<div class="form-group">
						<label class="col-xs-12" for="st_duplikate">Duplikasi Setiap Tanggal</label>
						<div class="col-xs-12">
							<select id="st_duplikate" name="st_duplikate" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikate=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikate=='0'?'selected':'')?>>TIDAK</option>
							
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2" >
					<div class="form-group">
						<label class="col-xs-12" for="st_ruangan">Pilihan Ruangan</label>
						<div class="col-xs-12">
							<select id="st_ruangan" name="st_ruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_ruangan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_ruangan=='0'?'selected':'')?>>TIDAK</option>
							
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2" >
					<div class="form-group">
						<label class="col-xs-12" for="st_kelas">Pilihan Kelas</label>
						<div class="col-xs-12">
							<select id="st_kelas" name="st_kelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_kelas=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_kelas=='0'?'selected':'')?>>TIDAK</option>
							
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2" >
					<div class="form-group">
						<label class="col-xs-12" for="st_diskon">Pilihan Discount</label>
						<div class="col-xs-12">
							<select id="st_diskon" name="st_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_diskon=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_diskon=='0'?'selected':'')?>>TIDAK</option>
							
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2" >
					<div class="form-group">
						<label class="col-xs-12" for="st_kuantitas">Pilihan Kuantitas</label>
						<div class="col-xs-12">
							<select id="st_kuantitas" name="st_kuantitas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_kuantitas=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_kuantitas=='0'?'selected':'')?>>TIDAK</option>
							
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2" >
					<div class="form-group">
						
						<label class="col-xs-12" for="st_kuantitas">&nbsp;</label>
						<div class="col-xs-12">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
				</div>
					
				
				
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<div class="col-xs-12">
							<h5><?=text_primary('AKSES TARIF VISITE DOKTER')?></h5>
						</div>
					</div>
						
						
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_tarif">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Kelompok Pasien</th>
										<th width="15%">Perusahaan</th>
										<th width="10%">Ruangan</th>
										<th width="10%">Kategori Dokter</th>
										<th width="15%">Nama Dokter</th>
										<th width="15%">Tarif Pelayanan</th>
										<th width="5%">Action</th>										   
									</tr>
									<tr>
										<th>#<input type="hidden" id="iddetail" value="" ></th>
										<th>
											<select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($idkelompokpasien=='#'?'selected':'')?>>-Pilih-</option>
												<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
													<option value="<?=$r->id?>" <?=($idkelompokpasien==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
											
										</th>
										<th>
											<select id="idrekanan" name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="0" <?=($idrekanan=='0'?'selected':'')?>>-SEMUA-</option>
											</select>
										</th>
										<th>
											<select id="idruangan" name="idruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="0" <?=($idruangan=='0'?'selected':'')?>>-Pilih-</option>
												<?foreach(get_all('mruangan',array('status'=>1,'idtipe'=>1)) as $r){?>
													<option value="<?=$r->id?>" <?=($idruangan==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="kategori_dokter" name="kategori_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="0" <?=($kategori_dokter=='0'?'selected':'')?>>-SEMUA-</option>
												<?foreach(get_all('mdokter_kategori') as $r){?>
													<option value="<?=$r->id?>" <?=($kategori_dokter==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="0" <?=($iddokter=='0'?'selected':'')?>>-SEMUA-</option>
												<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
													<option value="<?=$r->id?>" <?=($iddokter==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										
										
										<th>
											<select id="idtarif" name="idtarif" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($idtarif=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(get_all('mtarif_visitedokter',array('status'=>1)) as $r){?>
													<option value="<?=$r->id?>" <?=($idtarif==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2219'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_tarif" title="Simpan" name="btn_tambah_tarif"><i class="fa fa-save"></i> </button>
											<button class="btn btn-warning btn-sm" type="button" onclick="clear_detail()" title="Clear" id="btn_cler" name="btn_cler"><i class="fa fa-refresh"></i></button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$("#form-work").on("submit", function(){
	$("#cover-spin").show();
});


$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
if (tab=='1' || tab=='3'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}

})	
function load_general(){
	clear_detail();
}
function load_tab2(){
	load_tarif();		
}

// LOGIC
function load_tarif(){
	$('#index_tarif').DataTable().destroy();	
	table = $('#index_tarif').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_visite/load_tarif', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function clear_detail(){
	$("#iddetail").val('');
	$("#idkelompokpasien").val('#').trigger('change');
	// $("#idrekanan").val('0').trigger('change');
	$("#idruangan").val('0').trigger('change');
	$("#kategori_dokter").val('0').trigger('change');
	$("#iddokter").val('0').trigger('change');
	$("#idtarif").val('#').trigger('change');
	load_tarif();
}
$("#btn_tambah_tarif").click(function() {
	let iddetail=$("#iddetail").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
	let idrekanan=$("#idrekanan").val();
	let idruangan=$("#idruangan").val();
	let kategori_dokter=$("#kategori_dokter").val();
	let iddokter=$("#iddokter").val();
	let idtarif=$("#idtarif").val();
	
	
	if (idkelompokpasien=='#'){
		sweetAlert("Maaf...", "Tentukan Kelompok Pasien", "error");
		return false;
	}
	if (idruangan=='0'){
		sweetAlert("Maaf...", "Tentukan Ruangan", "error");
		return false;
	}
	if (idtarif=='#'){
		sweetAlert("Maaf...", "Tentukan Tarif", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_visite/simpan_tarif', 
		dataType: "JSON",
		method: "POST",
		data : {
				idkelompokpasien:idkelompokpasien,
				idrekanan:idrekanan,
				idruangan:idruangan,
				kategori_dokter:kategori_dokter,
				iddokter:iddokter,
				idtarif:idtarif,
				iddetail:iddetail,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				clear_detail();
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_tarif(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_visite/hapus_tarif',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tarif').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function edit_tarif(id){
	$("#iddetail").val(id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_visite/get_tarif',
		type: 'POST',
		dataType: "JSON",
		data: {idtarif: id},
		success: function(data) {
			console.log(data);
			$("#cover-spin").hide();
			$("#idkelompokpasien").val(data.idkelompokpasien).trigger('change.select2');
			$("#idrekanan").val(data.idrekanan).trigger('change.select2');
			$("#idruangan").val(data.idruangan).trigger('change.select2');
			$("#kategori_dokter").val(data.kategori_dokter).trigger('change.select2');
			$("#iddokter").val(data.iddokter).trigger('change.select2');
			$("#idtarif").val(data.idtarif).trigger('change.select2');
		}
	});
}

$("#idkelompokpasien").change(function(){
		$("#idrekanan").empty();
		let idkelompokpasien=($(this).val());
	if ($(this).val()=='#' || $(this).val()=='1'){
		$.ajax({
			url: '{site_url}setting_visite/find_rekanan/',
			type: 'POST',
				dataType: "JSON",
		data: {idkelompokpasien: idkelompokpasien},
			success: function(data) {
				// alert(data);
				
				$("#idrekanan").append(data);
			}
		});
	}else{
		
		$("#idrekanan").append('<option value="0" selected>-SEMUA-</option>');
	}

});

$("#kategori_dokter").change(function(){
		$.ajax({
			url: '{site_url}setting_visite/find_dokter/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#iddokter").empty();
				$("#iddokter").append(data);
			}
		});

});

</script>