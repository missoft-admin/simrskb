<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1831'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_poli()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1831'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_poli">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="20%">Tipe</th>
											<th width="25%">Poliklinik</th>
											<th width="25%">Tujuan</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											
											<th>
												<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													
													<option value="#" <?=($idtipe=='#'?'selected':'')?>>-Tipe-</option>
													<option value="1" <?=($idtipe=='1'?'selected':'')?>>POLIKINIK</option>
													<option value="2" <?=($idtipe=='2'?'selected':'')?>>IGD</option>
													
												</select>
											</th>
											<th>
												<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Pilih Poli-</option>
													<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</th>
											<th>
												<select id="tujuan_farmasi_id" name="tujuan_farmasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($tujuan_farmasi_id=='#'?'selected':'')?>>-Pilih Tujuan-</option>
													<?foreach(get_all('mtujuan_farmasi',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>" <?=($tujuan_farmasi_id==$r->id?'selected':'')?>><?=$r->nama_tujuan?></option>
													<?}?>
													
													
												</select>
											</th>
											
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('1832'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_poli" name="btn_tambah_poli"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_poli()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>
<div class="modal" id="modal_dokter" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Dokter Poliklinik <label id="nama_poli"></label></h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_dokter">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="85%">Dokter</th>
											<th width="10%">Pilih</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	load_poli();		
	
})	
function load_dokter(id,nama_poli){
	$('#index_dokter').DataTable().destroy();	
	$("#modal_dokter").modal('show');
	$("#nama_poli").text(nama_poli);
	table = $('#index_dokter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mpoliklinik_direct/load_dokter', 
                type: "POST" ,
                dataType: 'json',
				data : {
						id:id
					   }
            }
        });
}
function check_save_dokter(idpoli,iddokter,pilih){
	// alert('id : '+id+' Pilih :'+pilih);
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}mpoliklinik_direct/check_save_dokter',
			type: 'POST',
			data: {idpoli: idpoli,iddokter:iddokter,pilih:pilih},
			complete: function() {
				$("#cover-spin").hide();
				$('#index_dokter').DataTable().ajax.reload( null, false ); 
				$('#index_poli').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update'});
				
			}
	});
}
$("#idtipe").change(function(){
		$.ajax({
			url: '{site_url}mpoliklinik_direct/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli").empty();
				$("#idpoli").append(data);
			}
		});

});
function clear_poli(){
	$("#idtipe").val('#').trigger('change');
	$("#idpoli").val('#').trigger('change');
	
	$("#btn_tambah_poli").html('<i class="fa fa-save"></i> Save');
	
}
function load_poli(){
	$('#index_poli').DataTable().destroy();	
	table = $('#index_poli').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mpoliklinik_direct/load_poli', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_poli").click(function() {
	let idpoli=$("#idpoli").val();
	let idtipe=$("#idtipe").val();
	let tujuan_farmasi_id=$("#tujuan_farmasi_id").val();
	
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tipe", "error");
		return false;
	}
	if (idpoli=='#'){
		sweetAlert("Maaf...", "Tentukan Poliklinik", "error");
		return false;
	}
	if (tujuan_farmasi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Tujuan", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mpoliklinik_direct/simpan_poli', 
		dataType: "JSON",
		method: "POST",
		data : {
				idpoli:idpoli,
				tujuan_farmasi_id:tujuan_farmasi_id,
				
			},
		complete: function(data) {
			clear_poli();
			$('#index_poli').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});

function hapus_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Poliklinik?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mpoliklinik_direct/hapus_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_poli').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>