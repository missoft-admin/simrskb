<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdiagnosa" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('mdiagnosa/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="kode_diagnosa">Kode Diagnosa</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="kode_diagnosa" placeholder="Kode Diagnosa" name="kode_diagnosa" value="{kode_diagnosa}">
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="kode_diagnosa">Nama Diagnosa</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="nama" placeholder="Nama Diagnosa" name="nama" value="{nama}">
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="kode_diagnosa">Template</label>
						<div class="col-md-10">
							<select id="template_id" name="template_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="0" <?=($template_id=='0'?'selected':'')?>>Tentukan Nanti / Rencana Asuhan</option>
								<?foreach(get_all('mtemplate',array('staktif'=>1)) as $r){?>
								<option value="<?=$r->id?>" <?=($template_id==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="kode_diagnosa">Deskripsi</label>
						<div class="col-md-10">
							<textarea class="form-control js-summernote" name="deskripsi" id="deskripsi"><?=$deskripsi?></textarea>
						</div>
						
					</div>
					<div class="form-group">
						<div class="col-md-2 col-md-offset-2">
							<button class="btn btn-success" type="submit">Simpan</button>
							<a href="{base_url}mdiagnosa" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
						</div>
					</div>
					
					<?if ($id){?>
				<div class="row push-20-t">
					
				</div>
				<div class="row">
					<?php if (UserAccesForm($user_acces_form,array('1660'))){?>
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama"></label>
						<div class="col-md-10">
							<button class="btn btn-xs btn-primary" type="button" onclick="add_section()"><i class="fa fa-plus"></i> Add Section</button>
						</div>
					</div>
					<?}?>
					<div class="form-group">
						<div class="col-md-10 col-md-offset-2">
							<div class="table-responsive">
								<table class="table table-bordered" id="index_template_data">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="75%">Nama</th>
											<th width="20%">Action</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<?}?>
				
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<input type="hidden" id="id" value="{id}">
<input type="hidden" id="section_id" value="">
<input type="hidden" id="kategori_id" value="">
<input type="hidden" id="kategori_data_id" value="">

<div class="modal in" id="modal_section" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">TAMBAH SECTION</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input type="text" tabindex="1"  class="form-control" id="nama_section" placeholder="Nama Section" name="nama_section" value="">
									<label for="nama_section">Nama Section</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="2" id="no_urut_section" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Urutan</option>
										<?for($i=1;$i<100;$i++){?>
										<option value="<?=$i?>" ><?=$i?></option>
										<?}?>
										
									</select>
									<label for="no_urut_section">Urutan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-danger" tabindex="3"  type="button" onclick="simpan_section()" id="btn_hapus"><i class="fa fa-refresh"></i> Simpan</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_kategori" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">TAMBAH KATEGORI</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input type="text" tabindex="1"  class="form-control" id="nama_kategori" placeholder="Nama Kategori" name="nama_kategori" value="">
									<label for="nama_kategori">Nama Kategori</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="2" id="no_urut_kategori" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Urutan</option>
										<?for($i=1;$i<100;$i++){?>
										<option value="<?=$i?>" ><?=$i?></option>
										<?}?>
										
									</select>
									<label for="no_urut_kategori">Urutan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-danger" tabindex="3"  type="button" onclick="simpan_kategori()" id="btn_hapus"><i class="fa fa-refresh"></i> Simpan</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="modal_kategori_data" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">TAMBAH DATA</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<input type="text" tabindex="1"  class="form-control" id="nama_kategori_data" placeholder="Nama Data" name="nama_kategori_data" value="">
									<label for="nama_kategori_data">Nama Data</label>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" tabindex="2"  id="no_urut_kategori_data" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Urutan</option>
										<?for($i=1;$i<100;$i++){?>
										<option value="<?=$i?>" ><?=$i?></option>
										<?}?>
										
									</select>
									<label for="no_urut_kategori_data">Urutan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-danger" tabindex="3"  type="button" onclick="simpan_kategori_data()" id="btn_hapus"><i class="fa fa-refresh"></i> Simpan</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
		if ($("#id").val()!=''){
			load_data_template();
		}
	   
	});
	$("#template_id").change(function(){
		load_data_template();
	});

	function load_data_template(){
		var id_trx=$("#template_id").val();
		$("#cover-spin").show();
		$('#tabel_dokter_anestesi tbody').empty();
		var content='';
		$.ajax({
		  url: '{site_url}mtemplate/load_data_template', 
		  type: "POST",
		  dataType: 'json',
		  data: {
				template_id: id_trx
			},
		  success: function(data) {
			$("#cover-spin").hide();
				 $('#index_template_data tbody').empty();
				 $('#index_template_data tbody').append(data.tabel);
			 // });
		  }
		});

	}
	//SECTION
	function edit_section(section_id){
		
		$("#section_id").val(section_id);
		$.ajax({
		  url: '{site_url}mtemplate/get_data_section', 
		  type: "POST",
		  dataType: 'json',
		  data: {
				section_id: section_id
			},
		  success: function(data) {
			 $("#nama_section").val(data.nama);
			 $("#no_urut_section").val(data.no_urut).trigger('change');
		  }
		});
		$("#modal_section").modal('show');
	}
	function simpan_section(){
		if ($("#nama_section").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama", "error");
			return false;
		}
		if ($("#no_urut_section").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		
		$("#cover-spin").show();
		let template_id=$("#template_id").val();
		let section_id=$("#section_id").val();
		let nama_section=$("#nama_section").val();
		let no_urut=$("#no_urut_section").val();
		$.ajax({
			url: '{site_url}mtemplate/simpan_section', 
			dataType: "JSON",
			method: "POST",
			data : {
				template_id: template_id,
				section_id: section_id,
				nama_section: nama_section,
				no_urut:no_urut,
				},
			success: function(data) {
				// $("#idtipe").val('#').trigger('change');
				// $('#index_template').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				clear_section();
				$("#modal_section").modal('hide');
				load_data_template();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});
		

	}
	function clear_section(){
		$("#section_id").val('')
		$("#nama_section").val('')
		$("#no_urut_section").val('').trigger('change');
	}
	function add_section(){
		clear_section();
		$("#modal_section").modal('show');
	}
	function hapus_section($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Section?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mtemplate/hapus_section',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$("#cover-spin").hide();
					load_data_template();
				}
			});
		});
	}
	//KATEGORI
	function edit_kategori(kategori_id){
		
		$("#kategori_id").val(kategori_id);
		$.ajax({
		  url: '{site_url}mtemplate/get_data_kategori', 
		  type: "POST",
		  dataType: 'json',
		  data: {
				kategori_id: kategori_id
			},
		  success: function(data) {
			 $("#section_id").val(data.section_id);
			 $("#nama_kategori").val(data.nama);
			 $("#no_urut_kategori").val(data.no_urut).trigger('change');
		  }
		});
		$("#modal_kategori").modal('show');
	}
	function simpan_kategori(){
		if ($("#nama_kategori").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama", "error");
			return false;
		}
		if ($("#no_urut_kategori").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		
		$("#cover-spin").show();
		let template_id=$("#template_id").val();
		let section_id=$("#section_id").val();
		let kategori_id=$("#kategori_id").val();
		let nama_kategori=$("#nama_kategori").val();
		let no_urut=$("#no_urut_kategori").val();
		$.ajax({
			url: '{site_url}mtemplate/simpan_kategori', 
			dataType: "JSON",
			method: "POST",
			data : {
				template_id: template_id,
				section_id: section_id,
				kategori_id: kategori_id,
				nama_kategori: nama_kategori,
				no_urut:no_urut,
				},
			success: function(data) {
				// $("#idtipe").val('#').trigger('change');
				// $('#index_template').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				clear_kategori();
				$("#modal_kategori").modal('hide');
				load_data_template();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});
		

	}
	function clear_kategori(){
		$("#kategori_id").val('')
		$("#nama_kategori").val('')
		$("#no_urut_kategori").val('').trigger('change');
	}
	function add_kategori(section_id){
		$("#section_id").val(section_id)
		clear_kategori();
		$("#modal_kategori").modal('show');
	}
	function hapus_kategori($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Kategori?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mtemplate/hapus_kategori',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$("#cover-spin").hide();
					load_data_template();
				}
			});
		});
	}
	//KATEGORI DATA
	function edit_kategori_data(kategori_data_id){
		
		$("#kategori_data_id").val(kategori_data_id);
		$.ajax({
		  url: '{site_url}mtemplate/get_data_kategori_data', 
		  type: "POST",
		  dataType: 'json',
		  data: {
				kategori_data_id: kategori_data_id
			},
		  success: function(data) {
			 $("#section_id").val(data.section_id);
			 $("#kategori_id").val(data.kategori_id);
			 $("#nama_kategori_data").val(data.nama);
			 $("#no_urut_kategori_data").val(data.no_urut).trigger('change');
		  }
		});
		$("#modal_kategori_data").modal('show');
	}
	function simpan_kategori_data(){
		if ($("#nama_kategori_data").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama", "error");
			return false;
		}
		if ($("#no_urut_kategori_data").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		
		$("#cover-spin").show();
		let template_id=$("#template_id").val();
		let section_id=$("#section_id").val();
		let kategori_id=$("#kategori_id").val();
		let kategori_data_id=$("#kategori_data_id").val();
		let nama_kategori_data=$("#nama_kategori_data").val();
		let no_urut=$("#no_urut_kategori_data").val();
		$.ajax({
			url: '{site_url}mtemplate/simpan_kategori_data', 
			dataType: "JSON",
			method: "POST",
			data : {
				template_id: template_id,
				section_id: section_id,
				kategori_id: kategori_id,
				kategori_data_id: kategori_data_id,
				nama_kategori_data: nama_kategori_data,
				no_urut:no_urut,
				},
			success: function(data) {
				// $("#idtipe").val('#').trigger('change');
				// $('#index_template').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				clear_kategori_data();
				$("#modal_kategori_data").modal('hide');
				load_data_template();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});
		

	}
	function clear_kategori_data(){
		$("#kategori_data_id").val('')
		$("#nama_kategori_data").val('')
		$("#no_urut_kategori_data").val('').trigger('change');
	}
	function add_kategori_data(kategori_id,section_id){
		$("#section_id").val(section_id)
		$("#kategori_id").val(kategori_id)
		clear_kategori_data();
		$("#modal_kategori_data").modal('show');
	}
	function hapus_kategori_data($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Data?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mtemplate/hapus_kategori_data',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$("#cover-spin").hide();
					load_data_template();
				}
			});
		});
	}
	
	function validate_final(){
		
		
		if ($("#kode_diagnosa").val()==''){
			sweetAlert("Maaf...", "Tentukan Kode Diagnosa", "error");
			return false;
		}
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Diagnosa", "error");
			return false;
		}
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#cover-spin").show();
		return true;

		
	}
	
	
</script>