<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_deposit/save_detail','class="form-horizontal push-10-t"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No. Pendaftaran</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="nopendaftaran" placeholder="No. Jurnal" name="nopendaftaran" value="{nopendaftaran}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="no_medrec" placeholder="No. Jurnal" name="no_medrec" value="{no_medrec}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Pasien</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="namapasien" placeholder="No. Jurnal" name="namapasien" value="{namapasien}">
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=HumanDateShort($tanggaldaftar)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Ruangan</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="{ruangan}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kelas / Bed</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="<?=$kelas?>">
                    </div>
                </div>
                
				
               
			</div>
            
        </div>
	</div>
	
	<div class="block-content">
		<div class="row">
			<label class="col-md-2 control-label" for="tanggal"></label>
            <div class="col-md-10">
					<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:25%"  class="text-center">Tanggal </th>
								<th style="width:25%"  class="text-center">Metode</th>
								<th style="width:25%"  class="text-center">Bank</th>
								<th style="width:25%"  class="text-center">Nominal</th>
								<th style="width:25%"  class="text-center">Terima Dari</th>								
								<th style="width:25%"  class="text-center">User</th>								
								<th style="width:25%"  class="text-center">Aksi</th>								
							</tr>
						</thead>
						<tbody>
							<?foreach($list as $r){?>
								<tr>
								<td><?=HumanDateShort($r->tanggal)?></td>
								<td><?=metode_pembayaran($r->idmetodepembayaran)?></td>
								<td><?=($r->bank)?></td>
								<td class="text-right"><?=number_format($r->nominal,2)?></td>
								<td><?=($r->terimadari)?></td>
								<td><?=($r->nama_user)?></td>
								<td><a href="<?=base_url()?>trawatinap_tindakan/print_kwitansi_deposit/<?=$r->id?>" target="_blank" title="Print Kwitansi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a></td>
								
								</tr>
							<?}?>
							
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
</div>
<?php echo form_close(); ?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   
});


</script>