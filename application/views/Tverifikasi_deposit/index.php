<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">

        <hr style="margin-top:0px">

        <div class="row">
            <?php echo form_open('tverifikasi_deposit/filter','class="form-horizontal" id="form-work"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="namapasien" value="{namapasien}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6"></div>
            <?php echo form_close() ?>
        </div>

        <hr style="margin-top:10px">

        <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal Deposit</th>
                    <th>No. Medrec</th>
                    <th>Nama Pasien</th>
                    <th>Metode Pembayaran</th>
                    <th>Nama Bank</th>
                    <th>Nominal Deposit</th>
                    <th>User Created</th>
                    <th>Status</th>
                    <th>User Delete</th>
                    <th>Alasan Pembatalan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade in black-overlay" id="RejectNoteModal" role="dialog" style="text-transform:uppercase;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tidak Disetujui</h3>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
                <input type="hidden" id="iddeposit" value="">
                <textarea class="form-control" id="alasantidaksesuai" placeholder="Alasan Tidak Disetujui" rows="10" required></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" id="rowUnverifikasi" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "scrollX": true,
        "order": [],
        "ajax": {
            url: '{site_url}tverifikasi_deposit/getIndex/' + '<?=$this->uri->segment(2);?>',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "15%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 8,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 9,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 10,
                "orderable": true
            }
        ]
    });
});

$(document).ready(function() {
    $(".number").number(true, 0, '.', ',');

    $(document).on('click', '.rowEdit', function() {
        $('#iddeposit').val($(this).data('iddeposit'));
    });

    $(document).on('click', '.rowVerifikasi', function() {
        swal({
            title: "Apakah anda yakin ingin menyetujui data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            updateTransaksi(1);
        });
    });

    $(document).on('click', '#rowUnverifikasi', function() {
        updateTransaksi(0);
    });

});

function updateTransaksi(statusverifikasi) {
    var iddeposit = $('#iddeposit').val();
    var alasantidaksesuai = $('#alasantidaksesuai').val();

    event.preventDefault();

    $.ajax({
        url: '{site_url}tverifikasi_deposit/updateTransaksi',
        method: 'POST',
        data: {
            iddeposit: iddeposit,
            alasantidaksesuai: alasantidaksesuai,
            statusverifikasi: statusverifikasi
        },
        success: function(data) {
            swal({
                title: "Berhasil!",
                text: "Status Data Deposit Telah Diubah.",
                type: "success",
                timer: 1500,
                showConfirmButton: false
            });

            location.reload();
        }
    });
}

</script>
