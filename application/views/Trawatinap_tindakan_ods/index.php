<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<style media="screen">
#datatable-simrs td {
	cursor: pointer;
}
</style>
<?php if (UserAccesForm($user_acces_form,array('1199'))){?>
<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trawatinap_tindakan_ods/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="namapasien" value="{namapasien}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="iddokterpenanggungjawab">Dokter (DPJP)</label>
					<div class="col-md-8">
						<select name="iddokterpenanggungjawab" id="iddokterpenanggungjawab" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Semua Dokter</option>
							<?php foreach (get_all('mdokter') as $row) {?>
								<option value="<?=$row->id ?>" <?=($iddokterpenanggungjawab == $row->id ? 'selected="selected"': '')?>><?=$row->nama ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tanggaltransaksi">Tanggal Transaksi</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" name="tanggaldari_transaksi" placeholder="From" value="{tanggaldari_transaksi}">
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" name="tanggalsampai_transaksi" placeholder="To" value="{tanggalsampai_transaksi}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" <?=($status == '#' ? 'selected="selected"': '')?>>Semua Status</option>
							<option value="0" <?=($status == '0' ? 'selected="selected"': '')?>>Menunggu Transaksi</option>
							<option value="1" <?=($status == '1' ? 'selected="selected"': '')?>>Telah Transaksi</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<?php if (UserAccesForm($user_acces_form,array('1301'))){?>
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						<?}?>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr style="margin-top:10px">

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>No. Pendaftaran</th>
					<th>No. Medrec</th>
					<th>Nama Pasien</th>
					<th>Jenis Kelamin</th>
					<th>Umur</th>
					<th>Dokter Penanggung Jawab</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<!-- start modal estimasi biaya -->
<div class="modal in" id="EstimasiModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<h3 class="block-title">Estimasi Biaya</h3>
				</div>
				<div class="block-content">
					<table id="historyEstimasiBiaya" class="table table-bordered table-striped" style="margin-top: 10px;">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>

					<div id="viewEstimasiBiaya" style="display:none">

						<div class="col-md-5">
							<button class="btn btn-sm btn-default closeDetailEstimasiBiaya" href="#"><i class="fa fa-reply"></i> Kembali</button>
							<div class="col-md-12" style="margin-top: 90px; text-align:center">
								<a id="vEstimasiScan" class="img-link" href="">
									<i class="fa fa-file-photo-o" style="font-size: 70px;"></i>
								</a>
							</div>
						</div>

						<div class="col-md-7">
							<div class="input-group" style="width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
								<input type="text" id="vEstimasiNomedrec" class="form-control" value="" readonly="true">
							</div>
							<div class="input-group" style="margin-top:5px; width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
								<input type="text" id="vEstimasiNamapasien" class="form-control" value="" readonly="true">
							</div>
							<div class="input-group" style="margin-top:5px; width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">Umur</span>
								<input type="text"  id="vEstimasiUmur" class="form-control" value="" readonly="true">
							</div>
							<div class="input-group" style="margin-top:5px; width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">Dokter</span>
								<input type="text" id="vEstimasiDokter" class="form-control" value="" readonly="true">
							</div>
							<div class="input-group" style="margin-top:5px; width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">Diagnosa</span>
								<input type="text" id="vEstimasiDiagnosa" class="form-control" value="" readonly="">
							</div>
							<div class="input-group" style="margin-top:5px; width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">Operasi</span>
								<input type="text" id="vEstimasiOperasi" class="form-control" value="" readonly="">
							</div>
							<div class="input-group" style="margin-top:5px; width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">Jenis Operasi</span>
								<input type="text" id="vEstimasiJenisOperasi" class="form-control" value="" readonly="">
							</div>
						</div>

						<div class="col-md-12" style="margin-top:10px">
							<hr>
						</div>

						<div class="col-md-6">
							<h5><b>KAMAR OPERASI</b></h5>
							<hr>
							<div class="form-group">
								<label class="col-md-4 control-label" for="" style="margin-top:5px">Sewa Kamar</label>
								<div class="col-md-8" style="padding-left: 0px;">
									<div class="input-group">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiSewaKamar" class="form-control number" placeholder="Sewa Kamar" value="" readonly="">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="" style="margin-top:5px">Dokter Operator</label>
								<div class="col-md-8" style="padding-left: 0px;">
									<div class="input-group" style="margin-top:5px">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiJasaMedisOperator" class="form-control number" value="" placeholder="Dokter Operator" readonly="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="" style="margin-top:5px">Dokter Anesthesi</label>
								<div class="col-md-8" style="padding-left: 0px;">
									<div class="input-group" style="margin-top:5px">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiJasaMedisAnesthesi" class="form-control number" value="" placeholder="Dokter Anesthesi" readonly="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="" style="margin-top:5px">Assisten</label>
								<div class="col-md-8" style="padding-left: 0px;">
									<div class="input-group" style="margin-top:5px">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiJasaMedisAssisten" class="form-control number" value="" placeholder="Assisten" readonly="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="" style="margin-top:5px">Obat & BHP</label>
								<div class="col-md-8" style="padding-left: 0px;">
									<div class="input-group" style="margin-top:5px">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiObatBhp" class="form-control number" placeholder="Obat & BHP" value="" readonly="">
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<h5><b>PERAWATAN</b></h5>
							<hr>
							<div class="form-group">
								<label class="col-md-4 control-label" for="">Ruang Perawatan</label>
								<div class="col-md-8" style="padding-left: 0px;">
									<div class="col-md-2" style="padding-left: 0px;"><input type="text" id="vEstimasiQtyRuangPerawatan" class="form-control number" value="" readonly=""></div>
									<div class="input-group">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiRuangPerawatan" class="form-control number" value="" readonly="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="" style="margin-top:5px">HCU</label>
								<div class="col-md-8" style="padding-left: 0px; ">
									<div class="col-md-2" style="padding-left: 0px; margin-top:5px"><input type="text" id="vEstimasiQtyHCU" class="form-control number" value="" readonly=""></div>
									<div class="input-group" style="margin-top:5px">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiHCU" class="form-control number" value="" readonly="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="" style="margin-top:5px">Obat & Perawatan</label>
								<div class="col-md-8" style="padding-left: 0px;">
									<div class="input-group" style="margin-top:5px">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiObatPerawatan" class="form-control number" value="" readonly="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="" style="margin-top:5px">Penunjang Lain</label>
								<div class="col-md-8" style="padding-left: 0px;">
									<div class="input-group" style="margin-top:5px">
										<span class="input-group-addon"><center><b>+ -</b></center></span>
										<input type="text" id="vEstimasiPenunjang" class="form-control number" value="" readonly="">
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<hr style="margin-bottom: 8px;">
							<h5><b>IMPLAN</b></h5>
							<hr style="margin-bottom: 8px;margin-top: 8px;">
							<table id="historyEstimasiImplant" class="table table-bordered table-striped" style="margin-top: 10px;">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Harga Satuan</th>
										<th>Kuantitas</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>
<!-- eof modal estimasi biaya -->

<!-- start modal operasi -->
<div class="modal in" id="OperasiModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-c">
				<div class="block-header bg-primary">
					<h3 class="block-title">Operasi</h3>
				</div>
				<div class="block-content">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tabDaftarOperasi" id="actionTabDaftarOperasi" data-toggle="tab">Daftar Operasi</a>
						</li>
						<li>
							<a href="#tabDataOperasi" id="actionTabDataOperasi" data-toggle="tab">Data Operasi</a>
						</li>
					</ul>
					<div class="tab-content ">
						<div class="tab-pane active" id="tabDaftarOperasi">
							<div id="StatusOperasiDisabled" class="alert alert-danger alert-dismissable" style="margin:10px">
								<h3 class="font-w300 push-15">Opps!</h3>
								<p>Tidak Dapat Mendaftarkan Operasi, Dikarenakan Operasi Tanggal <a id="TanggalOperasiDisabled" class="alert-link" href="javascript:void(0)"></a> belum diproses!</p>
							</div>

							<div id="FormOperasi">
								<div class="form-group">
									<label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Tanggal Operasi</label>
									<div class="col-md-8" style="margin-bottom: 10px;">
										<input type="text" id="operasiTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal Operasi" value="<?= date("d/m/Y")?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Nama Pasien</label>
									<div class="col-md-8" style="margin-bottom: 10px;">
										<input type="text" id="operasiNamapasien" class="form-control" placeholder="Nama Pasien" value="" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Diagnosa</label>
									<div class="col-md-8" style="margin-bottom: 10px;">
										<input type="text" id="operasiDiagnosa" class="form-control" placeholder="Diagnosa">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Operasi</label>
									<div class="col-md-8" style="margin-bottom: 10px;">
										<input type="text" id="operasiOperasi" class="form-control" placeholder="Operasi" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Jam Operasi</label>
									<div class="col-md-8" style="margin-bottom: 10px;">
										<input type="text" id="operasiJam" class="form-control" placeholder="Jam Operasi" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Nama Petugas</label>
									<div class="col-md-8" style="margin-bottom: 10px;">
										<input type="text" id="operasiNamaPetugas" class="form-control" placeholder="Nama Petugas" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Catatan</label>
									<div class="col-md-8" style="margin-bottom: 10px;">
										<textarea id="operasiCatatan" class="form-control" placeholder="Catatan"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane" id="tabDataOperasi">
							<table id="historyDaftarOperasi" class="table table-bordered table-striped" style="margin-top: 10px;">
								<thead>
									<tr>
										<th>Tanggal Operasi</th>
										<th>Diagnosa</th>
										<th>Operasi</th>
										<th>Nama Petugas</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button id="saveDaftarOperasi" class="btn btn-sm btn-primary" type="button" style="width: 20%;">Simpan</button>
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;">Tutup</button>
			</div>
		</div>
	</div>
</div>
<!-- eof modal operasi -->

<!-- start rincian modal -->
<div class="modal in" id="RincianModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title titleRincian">RINCIAN</h3>
				</div>
				<div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					<ul class="nav nav-tabs">
						<?php if (UserAccesForm($user_acces_form,array('1305'))){?>
						<li class="<?=(UserAccesForm($user_acces_form,array('1305'))?'active':'')?>">
							<a href="#tabDeposit" data-toggle="tab">Deposit</a>
						</li>
						<?}?>
						<?php if (UserAccesForm($user_acces_form,array('1310'))){?>
						<li class="<?=(UserAccesForm($user_acces_form,array('1305'))==false?'active':'')?>">
							<a href="#tabEstimasi" data-toggle="tab">Show Estimasi</a>
						</li>
						<?}?>
					</ul>

					<div class="tab-content ">
						<?php if (UserAccesForm($user_acces_form,array('1305'))){?>

						<div class="tab-pane <?=(UserAccesForm($user_acces_form,array('1305'))?'active':'')?>" id="tabDeposit">
							<div class="input-group" style="margin-top:15px; width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
								<input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
							</div>
							<div class="input-group" style="margin-top:5px; width:100%">
								<span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
								<input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
							</div>
							<br>

						  <div class="form-group" style="margin-bottom: 8px;">
						    <label class="col-md-4 control-label">Tanggal</label>
						    <div class="col-md-8" style="margin-bottom: 5px;">
						      <input type="text" id="depositTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?= date("d/m/Y")?>">
						    </div>
						  </div>
						  <div class="form-group" style="margin-bottom: 8px;">
						    <label class="col-md-4 control-label">Metode Pembayaran</label>
						    <div class="col-md-8" style="margin-bottom: 5px;">
						      <select class="js-select2 form-control" id="depositIdMetodePembayaran" style="width: 100%;" data-placeholder="Pilih Opsi">
						        <option value=""></option>
						        <option value="1">Tunai</option>
						        <option value="2">Debit</option>
						        <option value="3">Kredit</option>
						        <option value="4">Transfer</option>
						      </select>
						    </div>
						  </div>
						  <div class="form-group" style="display: none;margin-bottom:8px;" id="depositMetodeBank">
						    <label class="col-md-4 control-label">Pembayaran</label>
						    <div class="col-md-8" style="margin-bottom: 5px;">
						      <select class="js-select2 form-control" id="depositIdBank" style="width: 100%;" data-placeholder="Pilih Opsi">
						        <option value="">Pilih Opsi</option>
						      </select>
						    </div>
						  </div>
						  <div class="form-group" style="margin-bottom:8px">
						    <label class="col-md-4 control-label" for="">Nominal</label>
						    <div class="col-md-8" style="margin-bottom: 5px;">
					        <input type="text" class="form-control number" id="depositNominal" placeholder="Nominal" value="">
						    </div>
						  </div>
						  <div class="form-group" style="margin-bottom:8px">
						    <label class="col-md-4 control-label" for="">Terima Dari</label>
						    <div class="col-md-8" style="margin-bottom: 5px;">
					        <input type="text" class="form-control" id="depositTerimaDari" placeholder="Terima Dari" value="">
						    </div>
						  </div>
						  <?php if (UserAccesForm($user_acces_form,array('1306'))){?>
						  <div class="form-group" style="margin-bottom:8px">
						    <label class="col-md-4 control-label" for="">&nbsp;</label>
						    <div class="col-md-8" style="margin-bottom: 5px;">
					        <button id="saveDataDeposit" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus-circle"></i> Tambah</button>
						    </div>
						  </div>
						  <?}?>
					    <div class="col-md-12">
				        <hr style="margin-top:10px; margin-bottom:5px;">
					    </div>

						  <?php if (UserAccesForm($user_acces_form,array('1309'))){?>
							<a href="" target="_blank" id="historyPrintAll" class="btn btn-sm btn-success" style="width: 20%;margin-top: 10px;margin-bottom: 10px;float:right"><i class="fa fa-print"></i> Print All</a>
						  <?}?>
							<button class="btn btn-sm btn-success" type="button" style="width: 20%;margin-top: 10px;margin-bottom: 10px;margin-right: 5px;float:right" disabled><i class="fa fa-money"></i> Refund</button>
							<table id="historyDeposit" class="table table-bordered table-striped" style="margin-top: 10px;">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>Metode</th>
										<th>Nominal</th>
										<th>Terima Dari</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
									<tr style="background-color:white;">
										<td colspan="3" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
										<td id="depositTotal" style="font-weight:bold;"></td>
										<td></td>
									</tr>
								</tfoot>
							</table>
						</div>
						<?}?>
						<?php if (UserAccesForm($user_acces_form,array('1310'))){?>
						<div class="tab-pane <?=(UserAccesForm($user_acces_form,array('1305'))==false?'active':'')?>" id="tabEstimasi">
							<div id="rincianViewHeadEstimasiBiaya">
								<div class="input-group" style="margin-top:15px; width:100%">
									<span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
									<input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
								</div>
								<div class="input-group" style="margin-top:5px; width:100%">
									<span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
									<input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
								</div>
								<br>
								<?php if (UserAccesForm($user_acces_form,array('1311'))){?>
								<button id="rincianTambahEstimasi" class="btn btn-sm btn-primary rOpenDetailEstimasiBiaya" style="width: 20%;float: right; margin-bottom: 8px;"><i class="fa fa-plus-circle"></i> Tambah</button>
								<?}?>
							</div>

							<table id="rincianHistoryEstimasiBiaya" class="table table-bordered table-striped" style="margin-top: 10px;">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>

							<div id="rincianViewEstimasiBiaya" style="display:none">
								<br>
								<div class="col-md-5">
									<button class="btn btn-sm btn-default rincianCloseDetailEstimasiBiaya" href="#"><i class="fa fa-reply"></i> Kembali</button>
									<div class="col-md-12" style="margin-top: 90px; text-align:center">
										<a id="rEstimasiScan" class="img-link" href="">
											<i class="fa fa-file-photo-o" style="font-size: 70px;"></i>
										</a>
										<div id="rEstimasiUpload" style="display:none">
											<div id="rEstimasiPriview"></div>
											<div class="input-group" style="display: inline-flex; width: 100%;">
												<input type="hidden" id="rEstimasiId" value="">
												<input type="file" id="rEstimasiFile" name="file" class="form-control" onchange="previewImage(this);">
											  <div class="input-group-append">
											    <span class="btn btn-primary input-group-text" id="rEstimasiActionUpload">Upload</span>
											  </div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-7">
									<div class="input-group" style="width:100%">
										<span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
										<input type="text" id="rEstimasiNomedrec" class="form-control" value="" readonly="true">
									</div>
									<div class="input-group" style="margin-top:5px; width:100%">
										<span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
										<input type="text" id="rEstimasiNamapasien" class="form-control" value="" readonly="true">
									</div>
									<div class="input-group" style="margin-top:5px; width:100%">
										<span class="input-group-addon" style="width:125px; text-align:left;">Umur</span>
										<input type="text"  id="rEstimasiUmur" class="form-control" value="" readonly="true">
									</div>
									<div class="input-group" style="margin-top:5px; width:100%">
										<span class="input-group-addon" style="width:125px; text-align:left;">Dokter</span>
										<input type="text" id="rEstimasiDokter" class="form-control" value="" readonly="true">
									</div>
									<div class="input-group" style="margin-top:5px; width:100%">
										<span class="input-group-addon" style="width:125px; text-align:left;">Diagnosa</span>
										<input type="text" id="rEstimasiDiagnosa" class="form-control" placeholder="Diagnosa" value="" readonly="">
									</div>
									<div class="input-group" style="margin-top:5px; width:100%">
										<span class="input-group-addon" style="width:125px; text-align:left;">Operasi</span>
										<input type="text" id="rEstimasiOperasi" class="form-control" placeholder="Operasi" value="" readonly="">
									</div>
									<div class="input-group fEstimasiView" style="margin-top:5px; width:100%">
										<span class="input-group-addon" style="width:125px; text-align:left;">Jenis Operasi</span>
										<input type="text" id="rEstimasiJenisOperasi" class="form-control" placeholder="Jenis Operasi" value="" readonly="">
									</div>
									<div class="input-group fEstimasiInput" style="margin-top:5px; width:100%; display:none">
										<span class="input-group-addon" style="width:125px; text-align:left;">Jenis Operasi</span>
										<select id="rEstimasiIdJenisOperasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							        <option value="">Pilih Opsi</option>
											<?php foreach  (get_all('mjenis_operasi') as $rows){ ?>
												<option value="<?= $rows->id ?>"><?= $rows->nama ?></option>
											<?php } ?>
							      </select>
									</div>
								</div>

								<div class="col-md-12" style="margin-top:10px">
									<hr>
								</div>

								<div class="col-md-6">
									<h5><b>KAMAR OPERASI</b></h5>
									<hr>
									<div class="form-group">
										<label class="col-md-4 control-label" for="" style="margin-top:5px">Sewa Kamar</label>
										<div class="col-md-8" style="padding-left: 0px;">
											<div class="input-group">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiSewaKamar" class="form-control number" placeholder="Sewa Kamar" value="" readonly="">
											</div>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label" for="" style="margin-top:5px">Dokter Operator</label>
										<div class="col-md-8" style="padding-left: 0px;">
											<div class="input-group" style="margin-top:5px">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiJasaMedisOperator" class="form-control number" value="" placeholder="Dokter Operator" readonly="">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="" style="margin-top:5px">Dokter Anesthesi</label>
										<div class="col-md-8" style="padding-left: 0px;">
											<div class="input-group" style="margin-top:5px">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiJasaMedisAnesthesi" class="form-control number" value="" placeholder="Dokter Anesthesi" readonly="">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="" style="margin-top:5px">Assisten</label>
										<div class="col-md-8" style="padding-left: 0px;">
											<div class="input-group" style="margin-top:5px">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiJasaMedisAssisten" class="form-control number" value="" placeholder="Assisten" readonly="">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="" style="margin-top:5px">Obat & BHP</label>
										<div class="col-md-8" style="padding-left: 0px;">
											<div class="input-group" style="margin-top:5px">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiObatBhp" class="form-control number" placeholder="Obat & BHP" value="" readonly="">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<h5><b>PERAWATAN</b></h5>
									<hr>
									<div class="form-group">
										<label class="col-md-4 control-label" for="">Ruang Perawatan</label>
										<div class="col-md-8" style="padding-left: 0px;">
											<div class="col-md-2" style="padding-left: 0px;"><input type="text" id="rEstimasiQtyRuangPerawatan" class="form-control number" value="" readonly=""></div>
											<div class="input-group">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiRuangPerawatan" class="form-control number" value="" readonly="">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="" style="margin-top:5px">HCU</label>
										<div class="col-md-8" style="padding-left: 0px; ">
											<div class="col-md-2" style="padding-left: 0px; margin-top:5px"><input type="text" id="rEstimasiQtyHCU" class="form-control number" value="" readonly=""></div>
											<div class="input-group" style="margin-top:5px">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiHCU" class="form-control number" value="" readonly="">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="" style="margin-top:5px">Obat & Perawatan</label>
										<div class="col-md-8" style="padding-left: 0px;">
											<div class="input-group" style="margin-top:5px">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiObatPerawatan" class="form-control number" value="" readonly="">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="" style="margin-top:5px">Penunjang Lain</label>
										<div class="col-md-8" style="padding-left: 0px;">
											<div class="input-group" style="margin-top:5px">
												<span class="input-group-addon"><center><b>+ -</b></center></span>
												<input type="text" id="rEstimasiPenunjang" class="form-control number" value="" readonly="">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<hr style="margin-bottom: 8px;">
									<h5><b>IMPLAN</b></h5>
									<hr style="margin-bottom: 8px;margin-top: 8px;">
									<table id="rincianHisotryEstimasiImplant" class="table table-bordered table-striped" style="margin-top: 10px;">
										<thead>
											<tr class="fEstimasiInput" style="display:none">
												<th style="width:10%">
													<select id="fImplanId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
														<option value="">Pilih Opsi</option>
													</select>
												</th>
												<th style="width:10%">
													<input type="hidden" id="fImplanHargaDasar" value="" readonly="true">
													<input type="hidden" id="fImplanMargin" value="" readonly="true">
													<input type="text" class="form-control number" id="fImplanHargaJual" placeholder="Tarif" value="" readonly="true">
												</th>
												<th style="width:10%">
													<input type="text" class="form-control number" id="fImplanKuantitas" placeholder="Kuantitas" value="">
												</th>
												<th style="width:10%">
													<select id="fImplanStatus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
														<option value="">Pilih Opsi</option>
														<option value="Primary">Primary</option>
														<option value="Alternatif">Alternatif</option>
													</select>
												</th>
												<th style="width:10%">
													<button id="tambahDataImplan"  class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>
												</th>
											</tr>
											<tr>
												<th>Nama</th>
												<th>Harga Satuan</th>
												<th>Kuantitas</th>
												<th>Status</th>
												<th class="fEstimasiInput" style="display:none">Aksi</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot class="fEstimasiInput" style="display:none">
											<tr>
												<th colspan="5"><button id="saveDataEstimasi" class="btn btn-success" type="button"><i class="fa fa-check"></i> Simpan</button></th>
											</tr>
										</tfoot>
									</table>
									<input type="hidden" id="fImplanDataArray" value="" readonly="true">
								</div>

							</div>
						</div>
						<?}?>
					</div>

					<div id="tabEstimasi" style="display: none;">
						<h5 style="margin-top: 10px;">List Estimasi</h5>
						<table class="table table-bordered table-striped" style="margin-top: 10px;">
							<thead>
								<tr>
									<th>Tanggal</th>
									<th>Id Estimasi</th>
									<th>Status</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>
<!-- eof rincian modal -->

<input type="hidden" id="tempIdRawatInap" name="" value="">
<input type="hidden" id="tempNoMedrec" name="" value="">
<input type="hidden" id="tempNamaPasien" name="" value="">
<input type="hidden" id="tempUmurPasien" name="" value="">
<input type="hidden" id="tempDokterPJ" name="" value="">

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<!-- magnificPopup -->
<link rel="stylesheet" href="{plugins_path}magnific-popup/magnific-popup.css">
<script src="{plugins_path}magnific-popup/magnific-popup.min.js"></script>

<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		var dt = $('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trawatinap_tindakan_ods/getIndex/' + '<?=$this->uri->segment(2)?>',
					type: "POST",
					dataType: 'json'
				},
				"columns": [
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
	      ],
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "5%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true },
					{ "width": "10%", "targets": 8, "orderable": true },
					{ "width": "10%", "targets": 9, "orderable": true }
				]
			});

			// Array to track the ids of the details displayed rows
			var detailRows = [];

			$('#datatable-simrs tbody').on( 'click', 'tr td.details-control', function (){
					var tr = $(this).closest('tr');
					var row = dt.row( tr );
					var idx = $.inArray( tr.attr('id'), detailRows );

					if ( row.child.isShown() ) {
							tr.removeClass( 'details' );
							row.child.hide();

							// Remove from the 'open' array
							detailRows.splice( idx, 1 );
					} else {
							var idpendaftaran = tr.find("td:eq(0) span").data('idpendaftaran');

							$.ajax({
								url:"{site_url}trawatinap_tindakan/getDataRawatInap/" + idpendaftaran,
								dataType:"json",
								success:function(data){
									tr.addClass('details');
									row.child( format(data) ).show();

									// Add to the 'open' array
									if ( idx === -1 ) {
										detailRows.push( tr.attr('id') );
									}
								}
							});

					}
			});

			// On each draw, loop over the `detailRows` array and show any child rows
			dt.on('draw', function(){
					$.each( detailRows, function(i, id){
							$('#'+id+' td.details-control').trigger('click');
					});
			});
	});

	$(document).ready(function() {
		var nomedrec = '';
		var namapasien = '';
		var umurpasien = '';
		var dokterpj = '';

		$('.img-link').magnificPopup({type:'image'});
		$('.number').number(true, 0, '.', ',');

		// Open Estimasi Biaya Modal
		$(document).on('click', '#openModalEstimasi', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();

			getHistoryEstimasiBiaya(idpendaftaran);
		});

		// Estimasi Biaya
		$(document).on('click', '.openDetailEstimasiBiaya', function() {
			var idrow = $(this).data('idrow');
			var nomedrec = $("#tempNoMedrec").val();
			var namapasien = $("#tempNamaPasien").val();
			var umurpasien = $("#tempUmurPasien").val();
			var dokterpj = $("#tempDokterPJ").val();

			$('#historyEstimasiBiaya').hide();
			$('#viewEstimasiBiaya').show();

			// Get Data Estimasi
			$.ajax({
				url:"{site_url}trawatinap_tindakan/getDataEstimasiBiaya/" + idrow,
				dataType:"json",
				success:function(data){
					$("#vEstimasiScan").attr("href", "{site_url}assets/upload/estimasi/" + data.fotoscan);
					$("#vEstimasiNomedrec").val(nomedrec);
					$("#vEstimasiNamapasien").val(namapasien);
					$("#vEstimasiUmur").val(umurpasien);
					$("#vEstimasiDokter").val(dokterpj);
					$("#vEstimasiDiagnosa").val(data.diagnosa);
					$("#vEstimasiOperasi").val(data.operasi);
					$("#vEstimasiJenisOperasi").val(data.namajenisoperasi);
					$("#vEstimasiSewaKamar").val(data.sewakamar);
					$("#vEstimasiJasaMedisOperator").val(data.jasadokteroperator);
					$("#vEstimasiJasaMedisAnesthesi").val(data.jasadokteranaesthesi);
					$("#vEstimasiJasaMedisAssisten").val(data.jasaassisten);
					$("#vEstimasiObatBhp").val(data.obatbhp);
					$("#vEstimasiQtyRuangPerawatan").val(data.kuantitasruangperawatan);
					$("#vEstimasiRuangPerawatan").val(data.ruangperawatan);
					$("#vEstimasiQtyHCU").val(data.kuantitasruanghcu);
					$("#vEstimasiHCU").val(data.ruanghcu);
					$("#vEstimasiObatPerawatan").val(data.obatperawatan);
					$("#vEstimasiPenunjang").val(data.penunjanglain);
				}
			});

			// Get Data Estimasi Implant
			$.ajax({
				url:"{site_url}trawatinap_tindakan/getDataEstimasiBiayaImplant/" + idrow,
				dataType:"json",
				success:function(data){
					$('#historyEstimasiImplant tbody').empty();
					for (var i = 0; i < data.length; i++) {
						$('#historyEstimasiImplant tbody').append('<tr><td>' + data[i].namaimplan + '</td><td>' + $.number(data[i].harga) + '</td><td>' + $.number(data[i].kuantitas) + '</td><td>' + data[i].status + '</td></tr>');
					}
				}
			});
		});

		$(document).on('click', '.closeDetailEstimasiBiaya', function() {
			$('#historyEstimasiBiaya').show();
			$('#viewEstimasiBiaya').hide();
		});

		// Daftar Operasi
		$("#operasiJam").datetimepicker({
			format: "HH:mm",
			stepping: 30
		});

		$(document).on('click', '#actionTabDaftarOperasi', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var namapasien = $("#tempNamaPasien").val();

			$("#operasiNamapasien").val(namapasien);

			// Get Status Operasi
			$.ajax({
				url:"{site_url}trawatinap_tindakan/getDataOperasi/" + idpendaftaran,
				dataType:"json",
				success:function(data){
					if(data != null){
						if(data.status == 1 || data.status == 2){
							$("#StatusOperasiDisabled").show();
							$("#FormOperasi").hide();
							$("#TanggalOperasiDisabled").html(data.tanggaloperasi);
							$("#saveDaftarOperasi").hide();
						}else{
							$("#StatusOperasiDisabled").hide();
							$("#FormOperasi").show();
							$("#saveDaftarOperasi").show();
						}
					}else{
						$("#StatusOperasiDisabled").hide();
						$("#FormOperasi").show();
						$("#saveDaftarOperasi").show();
					}
				}
			});
		});

		$(document).on('click', '#actionTabDataOperasi', function() {
			$("#saveDaftarOperasi").hide();
		});

		$(document).on('click', '#openModalOperasi', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var namapasien = $("#tempNamaPasien").val();

			$("#operasiNamapasien").val(namapasien);

			// Get Status Operasi
			$.ajax({
				url:"{site_url}trawatinap_tindakan/getDataOperasi/" + idpendaftaran,
				dataType:"json",
				success:function(data){
					if(data != null){
						if(data.status == 1 || data.status == 2){
							$("#StatusOperasiDisabled").show();
							$("#FormOperasi").hide();
							$("#TanggalOperasiDisabled").html(data.tanggaloperasi);
							$("#saveDaftarOperasi").hide();
						}else{
							$("#StatusOperasiDisabled").hide();
							$("#FormOperasi").show();
							$("#saveDaftarOperasi").show();
						}
					}else{
						$("#StatusOperasiDisabled").hide();
						$("#FormOperasi").show();
						$("#saveDaftarOperasi").show();
					}
				}
			});

			getHistoryOperasi(idpendaftaran);
		});

		$(document).on('click', '#saveDaftarOperasi', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var tanggaloperasi = $('#operasiTanggal').val();
			var idpasienoperasi = $('#operasiIdPasien').val();
			var namapasienoperasi = $('#operasiNamapasien').val();
			var diagnosaoperasi = $('#operasiDiagnosa').val();
			var operasi = $('#operasiOperasi').val();
			var jamoperasi = $('#operasiJam').val();
			var petugasoperasi = $('#operasiNamaPetugas').val();
			var catatanoperasi = $('#operasiCatatan').val();

			if(tanggaloperasi == ''){
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#operasiTanggal').focus();
				});
				return false;
			}

			if(namapasienoperasi == ''){
				sweetAlert("Maaf...", "Nama Pasien tidak boleh kosong !", "error").then((value) => {
					$('#operasiNamapasien').focus();
				});
				return false;
			}

			if(diagnosaoperasi == ''){
				sweetAlert("Maaf...", "Diagnosa Operasi tidak boleh kosong !", "error").then((value) => {
					$('#operasiDiagnosa').focus();
				});
				return false;
			}

			if(jamoperasi == ''){
				sweetAlert("Maaf...", "Jam Operasi tidak boleh kosong !", "error").then((value) => {
					$('#operasiJam').focus();
				});
				return false;
			}

			if(petugasoperasi == ''){
				sweetAlert("Maaf...", "Petugas Operasi tidak boleh kosong !", "error").then((value) => {
					$('#operasiNamaPetugas').focus();
				});
				return false;
			}

			if(catatanoperasi == ''){
				sweetAlert("Maaf...", "Catatan Operasi tidak boleh kosong !", "error").then((value) => {
					$('#operasiCatatan').focus();
				});
				return false;
			}

			$.ajax({
				url: '{site_url}trawatinap_tindakan/saveDaftarOperasi',
				method: 'POST',
				data: {
					tipe: 2,
					idpendaftaran: idpendaftaran,
					idpasien: idpasienoperasi,
					tanggaloperasi: tanggaloperasi,
					namapasien: namapasienoperasi,
					diagnosaoperasi: diagnosaoperasi,
					operasi: operasi,
					jamoperasi: jamoperasi,
					petugasoperasi: petugasoperasi,
					catatanoperasi: catatanoperasi,
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Operasi Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					getHistoryOperasi(idpendaftaran);

					// Disabled Form Daftar Operasi
					$("#saveDaftarOperasi").hide();

					// Clear Form
					$('#operasiNamapasien').val('');
					$('#operasiDiagnosa').val('');
					$('#operasiOperasi').val('');
					$('#operasiJam').val('');
					$('#operasiNamaPetugas').val('');
					$('#operasiCatatan').val('');
				}
			});
		});

		// Rincian : Deposit, Estimasi & Verifikasi
		$(document).on('click', '#openModalRincian', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var nomedrec = $("#tempNoMedrec").val();
			var namapasien = $("#tempNamaPasien").val();

			$(".tindakanIdPendaftaran").val(idpendaftaran);
			$(".tindakanNoMedrec").val(nomedrec);
			$(".tindakanNamaPasien").val(namapasien);

			$("#historyPrintAll").attr('href', '{site_url}trawatinap_tindakan/print_rincian_deposit/' + idpendaftaran);

			getHistoryDeposit(idpendaftaran);
			getHistoryRincianEstimasiBiaya(idpendaftaran);
		});

		$(document).on('change', '#depositIdMetodePembayaran', function() {
			var idmetodepembayaran = $(this).val();

			if (idmetodepembayaran == 1) {
				$('#depositMetodeBank').hide();
			} else {

				// Get Bank
				$.ajax({
					url: '{site_url}trawatinap_tindakan/getBank',
					dataType: 'json',
					success: function(data) {
						$("#depositIdBank").empty();
						$("#depositIdBank").append("<option value=''>Pilih Opsi</option>");
						for(var i = 0;i < data.length;i++){
							$("#depositIdBank").append("<option value='"+data[i].id+"'>"+data[i].nama+"</option>");
						}
					}
				});

				$('#depositMetodeBank').show();
			}
		});

		$(document).on('click', '#saveDataDeposit', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var tanggal = $('#depositTanggal').val();
			var idmetodepembayaran = $('#depositIdMetodePembayaran option:selected').val();
			var idbank = $('#depositIdBank option:selected').val();
			var nominal = $('#depositNominal').val();
			var terimadari = $('#depositTerimaDari').val();
			var idtipe = 1;

			if(tanggal == ''){
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#depositTanggal').focus();
				});
				return false;
			}

			if(idmetodepembayaran == ''){
				sweetAlert("Maaf...", "Metode Pembayaran belum dipilih !", "error").then((value) => {
					$('#depositIdMetodePembayaran').select2('open');
				});
				return false;
			}

			if(idmetodepembayaran != 1){
				if(idbank == ''){
					sweetAlert("Maaf...", "Bank belum dipilih !", "error").then((value) => {
						$('#depositIdBank').select2('open');
					});
					return false;
				}
			}

			if(nominal == '' || nominal <= 0){
				sweetAlert("Maaf...", "Nominal tidak boleh kosong !", "error").then((value) => {
					$('#depositNominal').focus();
				});
				return false;
			}

			$.ajax({
				url: '{site_url}trawatinap_tindakan/saveDataDeposit',
				method: 'POST',
				data: {
					idrawatinap: idpendaftaran,
					tanggal: tanggal,
					idmetodepembayaran: idmetodepembayaran,
					idbank: idbank,
					nominal: nominal,
					idtipe: idtipe,
					terimadari: terimadari
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Deposit Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					getHistoryDeposit(idpendaftaran);

					$('#depositIdMetodePembayaran').select2("trigger", "select", {data : {id: '',text: ''}});
					$('#depositMetodeBank').hide();
					$('#depositIdBank').select2("trigger", "select", {data : {id: '',text: ''}});
					$('#depositNominal').val('');
					$('#depositTerimaDari').val('');
				}
			});
		});

		$(document).on('click', '.removeDeposit', function() {
		  var idrow = $(this).data('idrow');
		  var idrawatinap = $('#tempIdRawatInap').val();

		  event.preventDefault();
		  swal({
		      title: "Apakah anda yakin ingin menghapus data ini?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
		      // Approve Status
		      $.ajax({
		        url: '{site_url}trawatinap_tindakan/removeDataDeposit',
		        method: 'POST',
		        data: {
		          idrow: idrow
		        },
		        success: function(data) {
		          swal({
		            title: "Berhasil!",
		            text: "Data Deposit Telah Dihapus.",
		            type: "success",
		            timer: 1500,
		            showConfirmButton: false
		          });

		          getHistoryDeposit(idrawatinap);
		        }
		      });
		    });
		});

		// Rincian : Estimasi Biaya
		$(document).on('click', '.rOpenDetailEstimasiBiaya', function() {
			var idrow = $(this).data('idrow');
			var nomedrec = $("#tempNoMedrec").val();
			var namapasien = $("#tempNamaPasien").val();
			var umurpasien = $("#tempUmurPasien").val();
			var dokterpj = $("#tempDokterPJ").val();

			$('#rincianViewHeadEstimasiBiaya').hide();
			$('#rincianHistoryEstimasiBiaya').hide();
			$('#rincianViewEstimasiBiaya').show();

			// Get Data Estimasi
			$.ajax({
				url:"{site_url}trawatinap_tindakan/getDataEstimasiBiaya/" + idrow,
				dataType:"json",
				success:function(data){

					$("#rEstimasiNomedrec").val(nomedrec);
					$("#rEstimasiNamapasien").val(namapasien);
					$("#rEstimasiUmur").val(umurpasien);
					$("#rEstimasiDokter").val(dokterpj);

					if(data != null){
						if(data.fotoscan == null){
							$('#rEstimasiScan').hide();
							$('#rEstimasiUpload').show();
							$('#rEstimasiActionUpload').show();
						}

						$("#rEstimasiId").val(idrow);
						$("#rEstimasiScan").attr("href", "{site_url}assets/upload/estimasi/" + data.fotoscan);
						$("#rEstimasiDiagnosa").val(data.diagnosa);
						$("#rEstimasiOperasi").val(data.operasi);
						$("#rEstimasiJenisOperasi").val(data.namajenisoperasi);
						$("#rEstimasiSewaKamar").val(data.sewakamar);
						$("#rEstimasiJasaMedisOperator").val(data.jasadokteroperator);
						$("#rEstimasiJasaMedisAnesthesi").val(data.jasadokteranaesthesi);
						$("#rEstimasiJasaMedisAssisten").val(data.jasaassisten);
						$("#rEstimasiObatBhp").val(data.obatbhp);
						$("#rEstimasiQtyRuangPerawatan").val(data.kuantitasruangperawatan);
						$("#rEstimasiRuangPerawatan").val(data.ruangperawatan);
						$("#rEstimasiQtyHCU").val(data.kuantitasruanghcu);
						$("#rEstimasiHCU").val(data.ruanghcu);
						$("#rEstimasiObatPerawatan").val(data.obatperawatan);
						$("#rEstimasiPenunjang").val(data.penunjanglain);

						$("#rEstimasiDiagnosa").prop('readonly', true);
						$("#rEstimasiOperasi").prop('readonly', true);
						$("#rEstimasiJenisOperasi").prop('readonly', true);
						$("#rEstimasiSewaKamar").prop('readonly', true);
						$("#rEstimasiJasaMedisOperator").prop('readonly', true);
						$("#rEstimasiJasaMedisAnesthesi").prop('readonly', true);
						$("#rEstimasiJasaMedisAssisten").prop('readonly', true);
						$("#rEstimasiObatBhp").prop('readonly', true);
						$("#rEstimasiQtyRuangPerawatan").prop('readonly', true);
						$("#rEstimasiRuangPerawatan").prop('readonly', true);
						$("#rEstimasiQtyHCU").prop('readonly', true);
						$("#rEstimasiHCU").prop('readonly', true);
						$("#rEstimasiObatPerawatan").prop('readonly', true);
						$("#rEstimasiPenunjang").prop('readonly', true);
					}
				}
			});

			// Rincian : Get Data Estimasi Implant
			$.ajax({
				url:"{site_url}trawatinap_tindakan/getDataEstimasiBiayaImplant/" + idrow,
				dataType:"json",
				success:function(data){
					$('#rincianHisotryEstimasiImplant tbody').empty();
					for (var i = 0; i < data.length; i++) {
						$('#rincianHisotryEstimasiImplant tbody').append('<tr><td>' + data[i].namaimplan + '</td><td>' + $.number(data[i].harga) + '</td><td>' + $.number(data[i].kuantitas) + '</td><td>' + data[i].status + '</td></tr>');
					}
				}
			});
		});

		$(document).on('click', '.rApproveEstimasi', function() {
			var idrow = $(this).data('idrow');
			var idrawatinap = $('#tempIdRawatInap').val();

			event.preventDefault();
			swal({
					title: "Apakah anda yakin ingin menyetujui data ini?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Ya",
					cancelButtonText: "Tidak",
					closeOnConfirm: false,
					closeOnCancel: false
				}).then(function() {
					// Approve Status
					$.ajax({
						url: '{site_url}trawatinap_tindakan/approveDataEstimasi',
						method: 'POST',
						data: {
							idrow: idrow
						},
						success: function(data) {
							swal({
								title: "Berhasil!",
								text: "Data Estimasi Telah Disetujui.",
								type: "success",
								timer: 1500,
								showConfirmButton: false
							});

							getHistoryRincianEstimasiBiaya(idrawatinap);
						}
					});
				});
		});

		$(document).on('click', '#rincianTambahEstimasi', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var nomedrec = $("#tempNoMedrec").val();
			var namapasien = $("#tempNamaPasien").val();
			var umurpasien = $("#tempUmurPasien").val();
			var dokterpj = $("#tempDokterPJ").val();

			$("#rincianHisotryEstimasiImplant tbody").html("");

			$("#rEstimasiScan").attr("href", "");
			$("#rEstimasiNomedrec").val(nomedrec);
			$("#rEstimasiNamapasien").val(namapasien);
			$("#rEstimasiUmur").val(umurpasien);
			$("#rEstimasiDokter").val(dokterpj);
			$("#rEstimasiDiagnosa").val("");
			$("#rEstimasiOperasi").val("");
			$("#rEstimasiJenisOperasi").val("");
			$("#rEstimasiSewaKamar").val("");
			$("#rEstimasiJasaMedisOperator").val("");
			$("#rEstimasiJasaMedisAnesthesi").val("");
			$("#rEstimasiJasaMedisAssisten").val("");
			$("#rEstimasiObatBhp").val("");
			$("#rEstimasiQtyRuangPerawatan").val("");
			$("#rEstimasiRuangPerawatan").val("");
			$("#rEstimasiQtyHCU").val("");
			$("#rEstimasiHCU").val("");
			$("#rEstimasiObatPerawatan").val("");
			$("#rEstimasiPenunjang").val("");

			$("#rEstimasiDiagnosa").prop('readonly', false);
			$("#rEstimasiOperasi").prop('readonly', false);
			$("#rEstimasiJenisOperasi").prop('readonly', false);
			$("#rEstimasiSewaKamar").prop('readonly', false);
			$("#rEstimasiJasaMedisOperator").prop('readonly', false);
			$("#rEstimasiJasaMedisAnesthesi").prop('readonly', false);
			$("#rEstimasiJasaMedisAssisten").prop('readonly', false);
			$("#rEstimasiObatBhp").prop('readonly', false);
			$("#rEstimasiQtyRuangPerawatan").prop('readonly', false);
			$("#rEstimasiRuangPerawatan").prop('readonly', false);
			$("#rEstimasiQtyHCU").prop('readonly', false);
			$("#rEstimasiHCU").prop('readonly', false);
			$("#rEstimasiObatPerawatan").prop('readonly', false);
			$("#rEstimasiPenunjang").prop('readonly', false);

			$(".fEstimasiInput").show();
			$(".fEstimasiView").hide();
		});

		$(document).on('click', '.rincianCloseDetailEstimasiBiaya', function() {
			$('#rincianHistoryEstimasiBiaya').show();
			$('#rincianViewHeadEstimasiBiaya').show();
			$('#rincianViewEstimasiBiaya').hide();

			$(".fEstimasiInput").hide();
			$(".fEstimasiView").show();
		});

		$("#fImplanId").select2({
		  minimumInputLength: 2,
		  ajax: {
		    url: '{site_url}trawatinap_tindakan/getImplan',
		    dataType: 'json',
		    type: "POST",
		    quietMillis: 50,
		    data: function(params) {
		      var query = {
		        search: params.term,
		      }
		      return query;
		    },
		    processResults: function(data) {
		      return {
		        results: $.map(data, function(item) {
		          return {
		            text: item.nama,
		            id: item.id
		          }
		        })
		      };
		    }
		  }
		});

		$(document).on('change', '#fImplanId', function() {
			$.ajax({
				url:"{site_url}trawatinap_tindakan/getDetailImplan/" + $(this).val(),
				dataType:"json",
				success:function(data){
					$("#fImplanHargaDasar").val(data.hargadasar);
					$("#fImplanMargin").val(data.margin);
					$("#fImplanHargaJual").val(data.hargajual);
					$("#fImplanKuantitas").val(1);

					$("#fImplanKuantitas").focus();
				}
			});
		});

		$(document).on('click', '#tambahDataImplan', function() {
			var idimplan = $("#fImplanId option:selected").val();
			var hargajual = $("#fImplanHargaJual").val();
			var kuantitas = $("#fImplanKuantitas").val();
			var status = $("#fImplanStatus option:selected").val();

			if(idimplan == ''){
				sweetAlert("Maaf...", "Implan belum dipilih !", "error").then((value) => {
					$('#fImplanId').select2('open');
				});
				return false;
			}

			if(hargajual == '' || hargajual <= 0){
				sweetAlert("Maaf...", "Harga Jual tidak boleh kosong !", "error").then((value) => {
					$('#fImplanHargaJual').focus();
				});
				return false;
			}

			if(kuantitas == '' || kuantitas <= 0){
				sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
					$('#fImplanKuantitas').focus();
				});
				return false;
			}

			if(status == ''){
				sweetAlert("Maaf...", "Status Implan belum dipilih !", "error").then((value) => {
					$('#fImplanStatus').select2('open');
				});
				return false;
			}

			var content = "";
			content += "<tr>";
			content += "<td style='display:none;'>" + $("#fImplanId option:selected").val() + "</td>";
			content += "<td>" + $("#fImplanId option:selected").text() + "</td>";
			content += "<td style='display:none;'>" + $("#fImplanHargaDasar").val(); + "</td>";
			content += "<td style='display:none;'>" + $("#fImplanMargin").val(); + "</td>";
			content += "<td>" + $("#fImplanHargaJual").val(); + "</td>";
			content += "<td>" + $("#fImplanKuantitas").val(); + "</td>";
			content += "<td>" + $("#fImplanStatus").val(); + "</td>";
			content += "<td><a href='#' class='btn btn-danger hapusDataImplan' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash'></i></a></td>";
			content += "</tr>";

			$('#rincianHisotryEstimasiImplant tbody').append(content);

			$('#fImplanId').select2("trigger", "select", {data : {id: '',text: ''}});
			$("#fImplanHargaDasar").val('');
			$("#fImplanMargin").val('');
			$("#fImplanHargaJual").val('');
			$("#fImplanKuantitas").val('');
			$('#fImplanStatus').select2("trigger", "select", {data : {id: '',text: ''}});
		});

		$(document).on("click", ".hapusDataImplan", function() {
			var thisRow = $(this);
			swal({
		      title: "Hapus data ?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
					thisRow.closest('td').parent().remove();
		    });

			return false;
		});

		$(document).on("click", "#saveDataEstimasi", function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var diagnosa = $("#rEstimasiDiagnosa").val();
			var operasi = $("#rEstimasiOperasi").val();
			var jenisoperasi = $("#rEstimasiIdJenisOperasi").val();
			var jenisoperasi = $("#rEstimasiIdJenisOperasi").val();
			var sewakamar = $("#rEstimasiSewaKamar").val();
			var jasadokteroperator = $("#rEstimasiJasaMedisOperator").val();
			var jasadokteranaesthesi = $("#rEstimasiJasaMedisAnesthesi").val();
			var jasaassisten = $("#rEstimasiJasaMedisAssisten").val();
			var obatbhp = $("#rEstimasiObatBhp").val();
			var ruangperawatan = $("#rEstimasiRuangPerawatan").val();
			var kuantitasruangperawatan = $("#rEstimasiQtyRuangPerawatan").val();
			var ruanghcu = $("#rEstimasiHCU").val();
			var kuantitasruanghcu = $("#rEstimasiQtyHCU").val();
			var obatperawatan = $("#rEstimasiObatPerawatan").val();
			var penunjanglain = $("#rEstimasiPenunjang").val();

			var implanTable = $('table#rincianHisotryEstimasiImplant tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});

			$("#fImplanDataArray").val(JSON.stringify(implanTable));

			$.ajax({
				url: '{site_url}trawatinap_tindakan/saveDataEstimasi',
				method: 'POST',
				data: {
					idpendaftaran: idpendaftaran,
					diagnosa: diagnosa,
					operasi: operasi,
					jenisoperasi: jenisoperasi,
					sewakamar: sewakamar,
					jasadokteroperator: jasadokteroperator,
					jasadokteranaesthesi: jasadokteranaesthesi,
					jasaassisten: jasaassisten,
					obatbhp: obatbhp,
					ruangperawatan: ruangperawatan,
					kuantitasruangperawatan: kuantitasruangperawatan,
					ruanghcu: ruanghcu,
					kuantitasruanghcu: kuantitasruanghcu,
					obatperawatan: obatperawatan,
					penunjanglain: penunjanglain,
					dataImplan: $("#fImplanDataArray").val()
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Estimasi Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					getHistoryRincianEstimasiBiaya(idpendaftaran);

					// Back Form to List
					$('#rincianHistoryEstimasiBiaya').show();
					$('#rincianViewHeadEstimasiBiaya').show();
					$('#rincianViewEstimasiBiaya').hide();

					$(".fEstimasiInput").hide();
					$(".fEstimasiView").show();
				}
			});
		});

		$(document).on("click", "#rEstimasiActionUpload", function() {

			var formData = new FormData();
      var fotoscan = $('#rEstimasiFile')[0].files[0];
      formData.append('fotoscan',fotoscan);

			var idestimasi = $("#rEstimasiId").val();

			$.ajax({
				url: '{site_url}trawatinap_tindakan/uploadFotoScan/' + idestimasi,
				method: 'POST',
				data: formData,
				contentType: false,
        processData: false,
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Foto Scan Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					// Back Form to List
					$('#rincianHistoryEstimasiBiaya').show();
					$('#rincianViewHeadEstimasiBiaya').show();
					$('#rincianViewEstimasiBiaya').hide();

					$(".fEstimasiInput").hide();
					$(".fEstimasiView").show();

					$('#rEstimasiScan').show();
					$('#rEstimasiUpload').hide();
					$('#rEstimasiActionUpload').hide();
				}
			});
		});

	});

	function getHistoryEstimasiBiaya(idpendaftaran) {
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getHistoryEstimasiBiaya/' + idpendaftaran,
			dataType: 'json',
			success: function(data) {
				$('#historyEstimasiBiaya tbody').empty();
				if(data.length > 0){
					for (var i = 0; i < data.length; i++) {
						var number = (i+1);
						$('#historyEstimasiBiaya tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + statusEstimasi(data[i].status) + '</td><td><a href="#" data-idrow="' + data[i].id + '" class="btn btn-sm btn-primary openDetailEstimasiBiaya"><i class="fa fa-eye"></i> Lihat</a></td></tr>');
					}
				}else{
					$('#historyEstimasiBiaya').attr('class', 'table');
					$('#historyEstimasiBiaya').html('<div class="alert alert-danger alert-dismissable" style="margin:10px"><h3 class="font-w300 push-15">Maaf...</h3><p>Belum terdapat Estimasi Biaya yang dapat ditampilkan !</p></div>');
				}
			}
		});
	}

	function getHistoryRincianEstimasiBiaya(idpendaftaran) {
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getHistoryEstimasiBiaya/' + idpendaftaran,
			dataType: 'json',
			success: function(data) {
				$('#rincianHistoryEstimasiBiaya tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					var action = '';

					action = '<div class="btn-group">';
					<?php if (UserAccesForm($user_acces_form,array('1312'))){?>
					action += '<a href="#" data-idrow="' + data[i].id + '" class="btn btn-sm btn-primary rOpenDetailEstimasiBiaya"><i class="fa fa-eye"></i> Lihat</a>';
					<?}?>
					if(data[i].status == 1){
						<?php if (UserAccesForm($user_acces_form,array('1313'))){?>
						action += '<a href="#" data-idrow="' + data[i].id + '" class="btn btn-sm btn-success rApproveEstimasi"><i class="fa fa-check"></i> Setujui</a>';
						<?}?>
					}
					action += '</div>';

					$('#rincianHistoryEstimasiBiaya tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + statusEstimasi(data[i].status) + '</td><td>' + action + '</td></tr>');
				}
			}
		});
	}

	function getHistoryOperasi(idpendaftaran) {
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getHistoryOperasi/' + idpendaftaran,
			dataType: 'json',
			success: function(data) {
				$('#historyDaftarOperasi tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					$('#historyDaftarOperasi tbody').append('<tr><td>' + data[i].tanggaloperasi + '</td><td>' + data[i].diagnosa + '</td><td>' + data[i].operasi + '</td><td>' + data[i].namapetugas + '</td></tr>');
				}
			}
		});
	}

	function getHistoryDeposit(idpendaftaran) {
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getHistoryDeposit/' + idpendaftaran,
			dataType: 'json',
			success: function(data) {
				$('#historyDeposit tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					var metode = '';
					if(data[i].idmetodepembayaran == 1){
						metode = data[i].metodepembayaran;
					}else{
						metode = data[i].metodepembayaran + ' ( ' + data[i].namabank+ ' )';
					}
					var action = '';
					action = '<div class="btn-group">';
					<?php if (UserAccesForm($user_acces_form,array('1308'))){?>
					action += '<a href="{site_url}trawatinap_tindakan/print_kwitansi_deposit/' + data[i].id + '" target="_blank" class="btn btn-sm btn-success">Print <i class="fa fa-print"></i></a>';
					<?}?>
					<?php if (UserAccesForm($user_acces_form,array('1307'))){?>
					action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-danger removeDeposit">Hapus <i class="fa fa-close"></i></button>';
					<?}?>
					action += '</div>';

					$('#historyDeposit tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + metode + '</td><td>' + $.number(data[i].nominal) + '</td><td>' + (data[i].terimadari ? data[i].terimadari : "-")  + '</td><td>' + action + '</td></tr>');
				}

				getTotalHistoryDeposit();
			}
		});
	}

	function getTotalHistoryDeposit()
	{
		var totalkeseluruhan = 0;
		$('#historyDeposit tbody tr').each(function() {
			totalkeseluruhan +=parseFloat($(this).find('td:eq(2)').text().replace(/,/g, ''));
		});

		$("#depositTotal").html($.number(totalkeseluruhan));
	}

	function statusEstimasi(status){
	    if(status == '2'){
	      return '<span class="label label-success">Disetujui</span>';
	    }else{
	      return '<span class="label label-default">Menunggu Konfirmasi</span>';
	    }
	}

	function previewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#rEstimasiPriview').html('<div class="col-sm-12 animated fadeIn" style="margin-top:-80px"><a id="previewLink" href="#" class="img-link img-thumb"><img id="previewImage" class="img-responsive" src="#" alt=""></a></div>');
				$('#previewLink').attr('href', e.target.result);
				$('#previewImage').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	function format(data) {
		var rows = '';

		rows += '<div class="col-md-12">';
		rows += '	<div class="col-md-12">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<tr>';
		rows += '				<td width="30%"><b>Tanggal Pendaftaran</b></td>';
		rows += '				<td width="70%">' + data.tanggaldaftar + '</td>';
		rows += '			</tr>';
		rows += '			<tr>';
		rows += '				<td width="30%"><b>No. Medrec</b></td>';
		rows += '				<td width="70%">' + data.nomedrec + '</td>';
		rows += '			</tr>';
		rows += '			<tr>';
		rows += '				<td width="30%"><b>Nama Pasien</b></td>';
		rows += '				<td width="70%">' + data.namapasien + '</td>';
		rows += '			</tr>';
		rows += '			<tr>';
		rows += '				<td width="30%"><b>Jenis Kelamin</b></td>';
		rows += '				<td width="70%">' + data.jeniskelamin + '</td>';
		rows += '			</tr>';
		rows += '			<tr>';
		rows += '				<td width="30%"><b>Umur Pasien</b></td>';
		rows += '				<td width="70%">' + data.umur + '</td>';
		rows += '			</tr>';
		rows += '	</table>';
		rows += '	</div>';
		rows += '</div>';
		rows += '<div class="col-md-12" style="margin: 10px;">';
    rows += '	<div class="btn-group">';
	<?php if (UserAccesForm($user_acces_form,array('1302'))){?>
		rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalEstimasi" data-toggle="modal" data-target="#EstimasiModal" class="btn btn-primary btn-sm"><i class="fa fa-paste"></i> Estimasi Biaya</button>';
	<?}?>
<?php if (UserAccesForm($user_acces_form,array('1303'))){?>
		rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalOperasi" data-toggle="modal" data-target="#OperasiModal" class="btn btn-primary btn-sm"><i class="fa fa-sign-language"></i> Operasi</button>';
<?}?>
<?php if (UserAccesForm($user_acces_form,array('1304'))){?>
		rows += '		<button id="openModalRincian" data-toggle="modal" data-target="#RincianModal" class="btn btn-success btn-sm"><i class="fa fa-newspaper-o"></i> Rincian</button>';
<?}?>
<?php if (UserAccesForm($user_acces_form,array('1314'))){?>
		rows += '		<a href="{site_url}trawatinap_tindakan/verifikasi/' + data.id + '/ods" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Verifikasi</a>';
<?}?>
   rows += '	</div>';
    rows += '	&nbsp;&nbsp;&nbsp;';
		rows += '</div>';

		$("#tempIdRawatInap").val(data.id);
		$("#tempNoMedrec").val(data.nomedrec);
		$("#tempNamaPasien").val(data.namapasien);
		$("#tempUmurPasien").val(data.umur);
		$("#tempDokterPJ").val(data.namadokterpenanggungjawab);

		return rows;
	}
</script>
