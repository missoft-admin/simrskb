<?php echo ErrorSuccess($this->session)?>
<?php
    if ($error != '') {
        echo ErrorMessage($error);
    }
?>

<style>
	.control-label {
		text-align: left !important;
	}
</style>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
		<ul class="block-options">
			<li>
				<a href="{base_url}trm_pengelolaan_informasi/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
	</div>
	<div class="block-content">

		<hr style="margin-top:-10px">

		<div class="col-12">
			<?php echo form_open_multipart('trm_pengelolaan_informasi/save', 'class="form-horizontal" id="form-work"') ?>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Tanggal Penyerahan</label>
				<div class="col-md-10">
					<input class="form-control js-datepicker" type="text" name="tanggal_penyerahan" id="tanggal_penyerahan" placeholder="Tanggal Penyerahan" value="{tanggal_penyerahan}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Nama Penerima</label>
				<div class="col-md-10">
					<input class="form-control" type="text" name="nama_penerima" placeholder="Nama Penerima" value="{nama_penerima}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="">Keterangan</label>
				<div class="col-md-10">
					<textarea class="form-control summernote js-summernote-custom-height" placeholder="Keterangan" name="keterangan" rows="1">{keterangan}</textarea>
				</div>
			</div>

			<hr>

			<h4>Informasi Medis Yang Diambil</h4>
			<br>

			<div class="col-12">
				<table class="table table-bordered" id="detailPengajuan">
					<thead>
						<tr>
							<td>No. Medrec</td>
							<td>Nama Pasien</td>
							<td>Tanggal Lahir</td>
							<td>Tanggal Kunjungan</td>
							<td>Jenis Kunjungan</td>
							<td>Dokter</td>
							<td>Jenis Pengajuan</td>
							<td>Nama Pemohon</td>
							<td>Estimasi Selesai</td>
							<td>Keterangan</td>
							<td>User Verifikasi</td>
							<td>Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php if ($this->uri->segment(2) == 'view_data') { ?>
							<?php foreach($dataPengelolaan as $row) { ?>
								<td><?= $row->no_medrec; ?></td>
								<td><?= $row->nama_pasien; ?></td>
								<td><?= $row->tanggal_lahir; ?></td>
								<td><?= $row->tanggal_kunjungan; ?></td>
								<td><?= $row->jenis_kunjungan; ?></td>
								<td><?= $row->nama_dokter; ?></td>
								<td><?= $row->jenis_pengajuan; ?></td>
								<td><?= $row->nama_pemohon; ?></td>
								<td><?= $row->estimasi_selesai; ?></td>
								<td><?= $row->keterangan; ?></td>
								<td><?= $row->verified_by; ?> - <?= $row->verified_at; ?></td>
								<td>
									<button type="button" disabled class="btn btn-danger btn-sm">
										<i class="fa fa-trash"></i>
									</button>
								</td>
							<? } ?>
						<? } ?>
					</tbody>
				</table>
			</div>

			<hr>

			<div class="form-group">
				<label class="col-md-2 control-label" for="example-hf-email">Upload Bukti</label>
				<div class="col-md-5">
					<?php if ($this->uri->segment(2) != 'view_data') { ?>
					<div class="box">
						<input type="file" id="file-3" class="inputfile inputfile-3" style="display:none;" name="file" value="{file}" />
						<label for="file-3" style="border: 1px solid; border-radius: 5px;"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Pilih Berkas&hellip;</span></label>
					</div>
					<? } else { ?>
						<div class="box">
							<input id="file-3" class="inputfile inputfile-3" style="display:none;" name="file" value="{file}" />
							<label for="file-3" style="border: 1px solid; border-radius: 5px;"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>{file}&hellip;</span></label>
						</div>
					<? } ?>
				</div>
				<div class="col-md-5">
					<?php if ($this->uri->segment(2) != 'view_data') { ?>
					<button type="submit" class="btn btn-success" id="btnSubmit" style="float: right;">
						<i class="fa fa-save"></i> Simpan
					</button>
					<? } ?>
				</div>
			</div>

			<br><br><br>
			
			<?php echo form_hidden('id', $id) ?>
			<?php echo form_close() ?>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script>
    $(document).ready(function() {
		<?php if ($this->uri->segment(2) != 'view_data') { ?>
			loadDataKunjungan();
		<? } else { ?>
			$('input, select, #btnSearch').attr('disabled', 'disabled');
			$('#btnSubmit, #addDataKunjungan, #formDataKunjungan').hide();
			$('.summernote').summernote('disable');
		<? } ?>

		$(document).on("click", ".removeDataKunjungan", function() {
			var id = $(this).data('id');

			swal({
				title: "Anda Yakin ?",
				text : "untuk menghapus data kunjungan ini ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				removeDataKunjungan(id);
			});
		});
	});

	function loadDataKunjungan() {
		$.ajax({
			url: '{site_url}trm_pengelolaan_informasi/getDataKunjungan',
			dataType: "json",
			success: function(data) {
				$('#detailPengajuan tbody').html('');

				renderingTableDataKunjungan(data);
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	function renderingTableDataKunjungan(data) {
		data.map((item) => {
			$('#detailPengajuan tbody').append(`
				<tr>
					<td>${item.no_medrec}</td>
					<td>${item.nama_pasien}</td>
					<td>${item.tanggal_lahir}</td>
					<td>${item.tanggal_kunjungan}</td>
					<td>${item.jenis_kunjungan}</td>
					<td>${item.nama_dokter}</td>
					<td>${item.jenis_pengajuan}</td>
					<td>${item.nama_pemohon}</td>
					<td>${item.estimasi_selesai}</td>
					<td>${item.keterangan}</td>
					<td>${item.verified_by} - ${item.verified_at}</td>
					<td>
						<button type="button" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeDataKunjungan" data-id="${item.id}">
							<i class="fa fa-trash"></i>
						</button>
					</td>
				</tr>
			`);
		});
	}

	function removeDataKunjungan(id) {
		$.ajax({
			url: '{site_url}trm_pengelolaan_informasi/removeDataKunjungan/' + id,
			dataType: "json",
			success: function(data) {
				loadDataKunjungan();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
</script>