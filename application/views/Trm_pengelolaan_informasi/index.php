<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trm_pengelolaan_informasi/filter', 'class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">No. Pengajuan</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="notransaksi" placeholder="No. Pengajuan" value="{notransaksi}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Nama Pemohon</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama_pemohon" placeholder="Nama Pemohon" value="{nama_pemohon}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Tanggal Pengajuan</label>
					<div class="col-md-8">
						<div class="input-group date">
							<input class="form-control js-datepicker" type="text" name="tanggal_dari" placeholder="Tanggal Dari" value="{tanggal_dari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control js-datepicker" type="text" name="tanggal_sampai" placeholder="Tanggal Sampai" value="{tanggal_sampai}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Jenis Pengajuan</label>
					<div class="col-md-8">
						<select class="select2 form-control" name="jenis_pengajuan" style="width: 100%;" data-placeholder="">
							<option value="#">Semua Jenis Pengajuan</option>
							<?php foreach (get_all('mpengajuan_skd', array('status' => 1)) as $row) { ?>
								<option value="<?= $row->id; ?>" <?= ($row->id == $jenis_pengajuan ? 'selected' : ''); ?>"><?= $row->nama; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">No. Medrec</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nomedrec" placeholder="No. Medrec" value="{nomedrec}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Nama Pasien</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama_pasien" placeholder="Nama Pasien" value="{nama_pasien}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Estimasi</label>
					<div class="col-md-8">
						<div class="input-group date">
							<input class="form-control js-datepicker" type="text" name="estimasi_dari" placeholder="Estimasi Dari" value="{estimasi_dari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control js-datepicker" type="text" name="estimasi_sampai" placeholder="Estimasi Sampai" value="{estimasi_sampai}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select class="select2 form-control" name="status_pengajuan" style="width: 100%;" data-placeholder="">
							<option value="#">Semua Status</option>
							<option value="0" <?= ($status_pengajuan == '0' ? 'selected' : ''); ?>>Belum Selesai</option>
							<option value="1" <?= ($status_pengajuan == '1' ? 'selected' : ''); ?>>Selesai</option>
						</select>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<form class="" action="{base_url}trm_pengelolaan_informasi/proses" method="post">
			<hr>
				<button href="#" class="btn btn-danger actionDetail" id="prosesPengambilanAll" name="proses_pengambilan" value="1" style="display:none;" type="submit"><i class="fa fa-arrow-circle-right"></i> Proses Pengambilan</button>
				<button href="#" class="btn btn-primary actionDetail" id="prosesVerifikasiAll" name="proses_verifikasi" value="1" style="display:none;" type="submit"><i class="fa fa-check-circle"></i> Proses Verifikasi</button>
			<hr>
			<div class="table-responsive">
			<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
				<thead>
					<tr>
						<th>
							<label class="css-input css-checkbox css-checkbox-success">
							<input type="checkbox" id="checkAll"><span></span>
							</label>
						</th>
						<th>No. Pengajuan</th>
						<th>Tanggal Pengajuan</th>
						<th>No. Medrec</th>
						<th>Nama Pasien</th>
						<th>Tipe Kunjungan</th>
						<th>Dokter</th>
						<th>Jenis Pengajuan</th>
						<th>Nama Pemohon</th>
						<th>Keterangan</th>
						<th>Estimasi</th>
						<th>Status Pengajuan</th>
						<th>Status Waktu</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
	</div>
		</form>
		<br>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trm_pengelolaan_informasi/getIndex/' + '<?= $this->uri->segment(2); ?>',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": false },
					{ "width": "5%", "targets": 1, "orderable": false },
					{ "width": "5%", "targets": 2, "orderable": true },
					{ "width": "5%", "targets": 3, "orderable": false },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": false },
					{ "width": "10%", "targets": 6, "orderable": false },
					{ "width": "5%", "targets": 7, "orderable": false },
					{ "width": "10%", "targets": 8, "orderable": false },
					{ "width": "10%", "targets": 9, "orderable": false },
					{ "width": "5%", "targets": 10, "orderable": false },
					{ "width": "5%", "targets": 11, "orderable": false },
					{ "width": "5%", "targets": 12, "orderable": false },
					{ "width": "10%", "targets": 13, "orderable": false },
				]
			});
	});

	$(document).ready(function () {
		$('#checkAll').click(function(event) {
		if(this.checked) {
			$('.checkboxTrx').each(function() {
				this.checked = true;
				$('.actionDetail').show();
			});
		} else {
			$('.checkboxTrx').each(function() {
				this.checked = false;
				$('.actionDetail').hide();
			});
		}
		});

		$(document).on('click', '.checkboxTrx', function () {
		if($(".checkboxTrx:checked").length > 0) {
			$('.actionDetail').show();
		} else {
			$('.actionDetail').hide();
		}
		});
	});
</script>