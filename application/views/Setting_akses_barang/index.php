<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1812'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_akses()"><i class="fa fa-refresh"></i> Setting Akses</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1812'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="akses-bottom: 15px;">
						<h5><?=text_primary('SETTING AKSES')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="akses-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_akses">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Jenis Profesi</th>
											<th width="10%">Spesialisasi</th>
											<th width="15%">Nama PPA</th>
											<th width="10%">Tipe Barang</th>
											<th width="15%">Kategori</th>
											<th width="5%">Status High Alert</th>
											<th width="20%">Barang</th>
												<th width="10%">Action</th>										   
										</tr>
										<?
											$profesi_id='#';
											$spesialisasi_id='#';
											$idtipe_barang='#';
											$idkategori_barang='0';
											$idbarang='0';
											$idtipe='0';
											$idpoli_kelas='0';
											$mppa_id='#';
										?>
										<tr>
											<th>#</th>
											<th>
												<select id="profesi_id" name="profesi_id" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													
													<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
													<?foreach(list_variable_ref(21) as $r){?>
														<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="spesialisasi_id" name="spesialisasi_id" style="width:100%"  class="js-select2 form-control" data-placeholder="Choose one..">
													<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
													<?foreach(list_variable_ref(22) as $r){?>
														<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</th>
											<th>
												<select id="mppa_id" name="mppa_id" class="js-select2 form-control"  style="width:100%"  data-placeholder="Choose one..">
													<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
													
												</select>
											</th>
											<th>
												<select id="idtipe_barang" name="idtipe_barang" class="js-select2 form-control"  style="width:100%"  data-placeholder="Choose one..">
													<option value="#" <?=($idtipe_barang=='#'?'selected':'')?>>-Pilih Tipe-</option>
													<?foreach(get_all('mdata_tipebarang') as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idkategori_barang" name="idkategori_barang" class="js-select2 form-control" style="width:100%"  data-placeholder="Choose one..">
													<option value="0" <?=($idkategori_barang=='0'?'selected':'')?>>-All-</option>
													
												</select>
											</th>
											<th>
												<select tabindex="4" id="high_alert" class="js-select2 form-control"  style="width:100%"  data-placeholder="Pilih Opsi" required>
													<option value="0" selected>TIDAK</option>
													<option value="1" selected>YA</option>
													
												</select>
											</th>
											<th>
												<select id="idbarang" name="idbarang"  style="width:100%" class="form-control" data-placeholder="ALL">
												</select>
											</th>
											
											<th>
												<button class="btn btn-primary btn-sm"  type="button" id="btn_tambah_akses" onclick="add_akses()"><i class="fa fa-plus"></i> Tambah</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_asmed/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
function selec2Barang(){
	$("#idbarang").select2({
		minimumInputLength: 2,
		noResults: 'Barang Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		ajax: {
			// url: '{site_url}tpasien_penjualan/get_obat/',
			url: '{site_url}setting_akses_barang/get_obat/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term,
				idtipe: $("#idtipe_barang").val(),
				idkategori: $("#idkategori_barang").val(),
			  }
			  return query;
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.idbarang,
						}
					})
				};
			}
		}
	});
}

$(document).ready(function(){	
		$(".number").number(true,1,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_akses();
	}
	if (tab=='2'){
			load_tab2();
	}
	set_poli();
	selec2Barang();
})	
$("#idtipe_barang").change(function(){
	// $("#idbarang").val(null).trigger('change');
	$.ajax({
		url: '{site_url}setting_akses_barang/get_kategori/',
		dataType: "json",
		 type: "POST" ,
		dataType: 'json',
		data : {
				idtipe : $("#idtipe_barang").val(),
			   },
		success: function(data) {
			// alert(data);
			$("#idkategori_barang").empty();
			$("#idkategori_barang").append(data);
		}
	});
	// selec2Barang();
});
function set_poli(){
		$.ajax({
			url: '{site_url}setting_akses_barang/get_poli/',
			dataType: "json",
			 type: "POST" ,
			dataType: 'json',
			data : {
					idtipe : $("#idtipe").val(),
				   },
			success: function(data) {
				// alert(data);
				$("#idpoli_kelas").empty();
				$("#idpoli_kelas").append(data);
			}
		});
}
function set_asuransi(){
	$.ajax({
		url: '{site_url}setting_akses_barang/get_asuransi/',
		dataType: "json",
		 type: "POST" ,
		dataType: 'json',
		data : {
				idkelompokpasien : $("#idkelompokpasien").val(),
			   },
		success: function(data) {
			// alert(data);
			$("#idrekanan").empty();
			$("#idrekanan").append(data);
		}
	});
}
$("#idtipe").change(function(){
	set_poli();
});
$("#idkelompokpasien").change(function(){
	set_asuransi();
});

function add_akses(){
	let idtipe_barang=$("#idtipe_barang").val();
	let profesi_id=$("#profesi_id").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi ", "error");
		return false;
	}
	if (idtipe_barang=='#'){
		sweetAlert("Maaf...", "Tentukan Tipe Barang", "error");
		return false;
	}
	
	
	$("#akses-spin").show();
	$.ajax({
		url: '{site_url}setting_akses_barang/simpan_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtipe_barang : $("#idtipe_barang").val(),
				idkategori_barang : $("#idkategori_barang").val(),
				idbarang : $("#idbarang").val(),
				profesi_id : $("#profesi_id").val(),
				spesialisasi_id : $("#spesialisasi_id").val(),
				mppa_id : $("#mppa_id").val(),
				high_alert : $("#high_alert").val(),

			},
		success: function(data) {
			// alert(data);
				$("#akses-spin").hide();
			if (data==true){
				load_akses();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}

function load_akses(){
	$('#index_akses').DataTable().destroy();	
	table = $('#index_akses').DataTable({
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// scrollX: true,
			"columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [3,2,4,5,6] },
					// { "width": "15%", "targets": [1]},
					// { "width": "8%", "targets": [8]},
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}setting_akses_barang/load_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#akses-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_akses_barang/hapus_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_akses').DataTable().ajax.reload( null, false ); 
					$("#akses-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>