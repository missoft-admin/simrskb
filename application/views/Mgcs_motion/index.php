<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1718'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_gcs_motion()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1718'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
						<div class="form-group">
							<label class="col-xs-12" for="satuan_gcs_motion">Satuan GCS Motion</label>
							<div class="col-xs-4">
								<div class="input-group">
									<input class="form-control" type="text" id="satuan_gcs_motion" name="satuan_gcs_motion" value="{satuan_gcs_motion}" placeholder="Satuan GCS Motion">
									<span class="input-group-btn">
										<button class="btn btn-success" onclick="update_satuan()" type="button"><i class="fa fa-save"></i>  Update</button>
									</span>
								</div>
								
							</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_gcs_motion">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="40%">Range GCS Motion</th>
											<th width="25%">Kategori</th>
											<th width="15%">Warna</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="gcs_motion_id" name="gcs_motion_id" value="" placeholder="id"></th>
											<th>
												<div class="input-group" style="width:100%">
													<input class="form-control decimal" type="text" id="gcs_motion_1" name="gcs_motion_1" placeholder="<?=$satuan_gcs_motion?>" value="" >
													<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
													
													<input class="form-control decimal" type="text" id="gcs_motion_2" name="gcs_motion_2" placeholder="<?=$satuan_gcs_motion?>" value="" >
													
													
												</div>
											</th>
											<th>
												<input class="form-control" type="text" style="width:100%" id="kategori_gcs_motion" name="kategori_gcs_motion" placeholder="Kategori" value="" >
											</th>
											<th>
												
												<div class="js-colorpicker input-group colorpicker-element">
													<input class="form-control" type="text" id="warna" name="warna" value="#0000">
													<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
												</div>
											
											</th>
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('1719'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_gcs_motion" name="btn_tambah_gcs_motion"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_gcs_motion()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".decimal").number(true,1,'.',',');	
	load_gcs_motion();		
	
})	
function clear_gcs_motion(){
	$("#gcs_motion_id").val('');
	$("#gcs_motion_1").val('');
	$("#gcs_motion_2").val('');
	$("#kategori_gcs_motion").val('');
	$("#warna").val('#000');
	$("#btn_tambah_gcs_motion").html('<i class="fa fa-save"></i> Save');
	
}
function load_gcs_motion(){
	$('#index_gcs_motion').DataTable().destroy();	
	table = $('#index_gcs_motion').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mgcs_motion/load_gcs_motion', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function update_satuan(){
	let satuan_gcs_motion=$("#satuan_gcs_motion").val();
	
	if (satuan_gcs_motion==''){
		sweetAlert("Maaf...", "Tentukan Ssatuan", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mgcs_motion/update_satuan', 
		dataType: "JSON",
		method: "POST",
		data : {
				satuan_gcs_motion:satuan_gcs_motion,
				
				
			},
		complete: function(data) {
			location.reload();
		}
	});
}
$("#btn_tambah_gcs_motion").click(function() {
	let gcs_motion_id=$("#gcs_motion_id").val();
	let gcs_motion_1=$("#gcs_motion_1").val();
	let gcs_motion_2=$("#gcs_motion_2").val();
	let kategori_gcs_motion=$("#kategori_gcs_motion").val();
	let warna=$("#warna").val();
	
	if (gcs_motion_1==''){
		sweetAlert("Maaf...", "Tentukan Range GCS Motion", "error");
		return false;
	}
	if (gcs_motion_2==''){
		sweetAlert("Maaf...", "Tentukan Range GCS Motion", "error");
		return false;
	}
	if (kategori_gcs_motion==''){
		sweetAlert("Maaf...", "Tentukan Kategori", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mgcs_motion/simpan_gcs_motion', 
		dataType: "JSON",
		method: "POST",
		data : {
				gcs_motion_id:gcs_motion_id,
				gcs_motion_1:gcs_motion_1,
				gcs_motion_2:gcs_motion_2,
				kategori_gcs_motion:kategori_gcs_motion,
				warna:warna,
				
			},
		complete: function(data) {
			clear_gcs_motion();
			$('#index_gcs_motion').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function edit_gcs_motion(gcs_motion_id){
	$("#gcs_motion_id").val(gcs_motion_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mgcs_motion/find_gcs_motion',
		type: 'POST',
		dataType: "JSON",
		data: {id: gcs_motion_id},
		success: function(data) {
			$("#btn_tambah_gcs_motion").html('<i class="fa fa-save"></i> Edit');
			$("#gcs_motion_id").val(data.id);
			$("#gcs_motion_1").val(data.gcs_motion_1);
			$("#gcs_motion_2").val(data.gcs_motion_2);
			$("#warna").val(data.warna).trigger('change');
			$("#kategori_gcs_motion").val(data.kategori_gcs_motion);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_gcs_motion(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus GCS Motion?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mgcs_motion/hapus_gcs_motion',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_gcs_motion').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>