<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>General Consent Rawat Jalan</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
	  font-family: Verdana, sans-serif;
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
    }
   table {
		font-family: Verdana, sans-serif;
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 13px;
		 
      }
      .content td {
        padding: 10px;
		border: 0px solid #6033FF;
      }
	  .content-bawah td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 20px !important;
      }
	  .text-judul{
        font-size: 14px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
		border-bottom:1px solid #000 !important;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
	
    </style>
  </head>
  <body>
 <table>
		<tr>
			<td width="10%"><img width="100" height="100" src="{upload_path}app_setting/<?=($logo?$logo:'no_image.png')?>" /></td>
			<td  width="85%" class="text-center text-bold text-header">{judul}</td>
			<td  width="5%" class="text-center text-bold"></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td style="width:5%" class="border-full text-center text-bold text-italic text-judul"></td>
			<td style="width:85%" class="border-full text-center text-bold text-italic text-judul">{sub_header}</td>
			<td style="width:10%" class="border-full text-center text-bold text-italic text-judul">{sub_header_side}</td>
		</tr>
		
	</table>
	<table class="content">
		
		<?foreach($list_content as $row){?>
		<tr>
			<td style="width:5%" class="border-full text-center"><?=$row->no?></td>
			<td style="width:85%" class="border-full text-left "><?=$row->isi?></td>
			<td style="width:10%" class="border-full "></td>
		</tr>
		<?}?>
	</table>
	{footer_1}
	{footer_2}
	<table class="content-bawah">
		<tr>
			<td  width="70%" class="text-left">Nama Pasien<br>&nbsp;</td>
			<td  width="5%" class="text-left"></td>
			<td  width="25%" class="text-left">Jenis Kelamin</td>
		</tr>
		<tr>
			<td  width="70%" class="text-left border-full">&nbsp;<br>&nbsp;</td>
			<td  width="5%" class="text-left ">&nbsp;&nbsp;</td>
			<td  width="25%" class="text-left border-full">&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td  width="70%" class="text-left">Jika pasien tidak dapat menandatangani <br>Keluarga Terdeket</td>
			<td  width="5%" class="text-left"></td>
			<td  width="25%" class="text-left">Hubungan</td>
		</tr>
		<tr>
			<td  width="70%" class="text-left border-full">&nbsp;<br>&nbsp;</td>
			<td  width="5%" class="text-left ">&nbsp;&nbsp;</td>
			<td  width="25%" class="text-left border-full">&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td  width="70%" class="text-left">&nbsp;<br>Penanggung Jawab<br></td>
			<td  width="5%" class="text-left"></td>
			<td  width="25%" class="text-left">&nbsp;<br>Hubungan</td>
		</tr>
		<tr>
			<td  width="70%" class="text-left border-full">&nbsp;<br>&nbsp;</td>
			<td  width="5%" class="text-left ">&nbsp;&nbsp;</td>
			<td  width="25%" class="text-left border-full">&nbsp;&nbsp;</td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="30%" class="">{ttd_1}</td>
			<td  width="30%" class="">&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;</td>
			<td  width="30%" class="">Bandung, <br>Petugas Pemberi Informasi</td>
		</tr>
		<tr>
			<td width="30%" class="border-dotted"><br><br><br></td>
			<td  width="30%" class=""></td>
			<td  width="30%" class="border-dotted"></td>
		</tr>
		<tr>
			<td width="30%" class="">{ttd_2}</td>
			<td  width="30%" class="">&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;</td>
			<td  width="30%" class=""></td>
		</tr>
		<tr>
			<td width="30%" class="border-dotted"><br><br><br></td>
			<td  width="30%" class=""></td>
			<td  width="30%" class=""></td>
		</tr>
		
	</table>
	&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;
	<table class="content">
		<tr>
			<td width="50%" class="text-left text-bold">{footer_form_1}</td>
			<td width="50%" class="text-right text-bold">{footer_form_2}</td>
		</tr>
		
	</table>
  </body>
</html>
