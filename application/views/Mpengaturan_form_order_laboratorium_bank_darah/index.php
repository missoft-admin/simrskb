<?php declare(strict_types=1);
echo ErrorSuccess($this->session); ?>
<?php if ('' !== $error) {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpengaturan_form_order_laboratorium_bank_darah" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('mpengaturan_form_order_laboratorium_bank_darah/save', 'class="form-horizontal push-10-t" id="form-work"'); ?>
			<div class="form-group">
				<label class="col-md-12" for="">Pesan Informasi Berhasil</label>
				<div class="col-md-12">
					<textarea class="form-control summernote js-summernote-custom-height" name="pesan_informasi_berhasil" placeholder="Pesan Informasi Berhasil">{pesan_informasi_berhasil}</textarea>
				</div>
			</div>

			<h5 style="margin-bottom: 10px;" for="">Judul Formulir</h5>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_judul" placeholder="Judul Formulir (Indonesia Version)">{label_judul}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_judul_eng" placeholder="Judul Formulir (English Version)">{label_judul_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Tujuan Laboratorium</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_tujuan_laboratorium" <?php echo 1 === $required_tujuan_laboratorium ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_tujuan_laboratorium" placeholder="Tujuan Laboratorium (Indonesia Version)">{label_tujuan_laboratorium}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_tujuan_laboratorium_eng" placeholder="Tujuan Laboratorium (English Version)">{label_tujuan_laboratorium_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Dokter Peminta Pemeriksaan</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_dokter_peminta_pemeriksaan" <?php echo 1 === $required_dokter_peminta_pemeriksaan ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_dokter_peminta_pemeriksaan" placeholder="Dokter Peminta Pemeriksaan (Indonesia Version)">{label_dokter_peminta_pemeriksaan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_dokter_peminta_pemeriksaan_eng" placeholder="Dokter Peminta Pemeriksaan (English Version)">{label_dokter_peminta_pemeriksaan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Diagnosa</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_diagnosa" <?php echo 1 === $required_diagnosa ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_diagnosa" placeholder="Diagnosa (Indonesia Version)">{label_diagnosa}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_diagnosa_eng" placeholder="Diagnosa (English Version)">{label_diagnosa_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Catatan Pemeriksaan</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_catatan_pemeriksaan" <?php echo 1 === $required_catatan_pemeriksaan ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan_pemeriksaan" placeholder="Catatan Pemeriksaan (Indonesia Version)">{label_catatan_pemeriksaan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan_pemeriksaan_eng" placeholder="Catatan Pemeriksaan (English Version)">{label_catatan_pemeriksaan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Waktu Permintaan</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_waktu_permintaan" <?php echo 1 === $required_waktu_permintaan ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_waktu_permintaan" placeholder="Waktu Permintaan (Indonesia Version)">{label_waktu_permintaan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_waktu_permintaan_eng" placeholder="Waktu Permintaan (English Version)">{label_waktu_permintaan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Prioritas</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_prioritas" <?php echo 1 === $required_prioritas ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_prioritas" placeholder="Prioritas (Indonesia Version)">{label_prioritas}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_prioritas_eng" placeholder="Prioritas (English Version)">{label_prioritas_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Indikasi Transfusi</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_indikasi_transfusi" <?php echo 1 === $required_indikasi_transfusi ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_indikasi_transfusi" placeholder="Indikasi Transfusi (Indonesia Version)">{label_indikasi_transfusi}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_indikasi_transfusi_eng" placeholder="Pasien Puasa (English Version)">{label_indikasi_transfusi_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">HB</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_hb" <?php echo 1 === $required_hb ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_hb" placeholder="HB (Indonesia Version)">{label_hb}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_hb_eng" placeholder="HB (English Version)">{label_hb_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Riwayat Transfusi Sebelumnya</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_riwayat_transfusi_sebelumnya" <?php echo 1 === $required_riwayat_transfusi_sebelumnya ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_riwayat_transfusi_sebelumnya" placeholder="Riwayat Transfusi Sebelumnya (Indonesia Version)">{label_riwayat_transfusi_sebelumnya}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_riwayat_transfusi_sebelumnya_eng" placeholder="Riwayat Transfusi Sebelumnya (English Version)">{label_riwayat_transfusi_sebelumnya_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Riwayat Kehamilan Sebelumnya</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_riwayat_kehamilan_sebelumnya" <?php echo 1 === $required_riwayat_kehamilan_sebelumnya ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_riwayat_kehamilan_sebelumnya" placeholder="Riwayat Kehamilan Sebelumnya (Indonesia Version)">{label_riwayat_kehamilan_sebelumnya}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_riwayat_kehamilan_sebelumnya_eng" placeholder="Riwayat Kehamilan Sebelumnya (English Version)">{label_riwayat_kehamilan_sebelumnya_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Keterangan Lainnya</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_keterangan_lainnya" <?php echo 1 === $required_keterangan_lainnya ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_keterangan_lainnya" placeholder="Keterangan Lainnya (Indonesia Version)">{label_keterangan_lainnya}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_keterangan_lainnya_eng" placeholder="Keterangan Lainnya (English Version)">{label_keterangan_lainnya_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Catatan / Keterangan</label>&nbsp; # &nbsp;
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_catatan" <?php echo 1 === $required_catatan ? 'checked onclick="return false;"' : ''; ?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan" placeholder="Catatan / Keterangan (Indonesia Version)">{label_catatan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan_eng" placeholder="Catatan / Keterangan (English Version)">{label_catatan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpengaturan_form_order_laboratorium_bank_darah" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>

			<br /><br />

	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
	});
</script>
