<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}antrian_poli_kode" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('antrian_poli_kode/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
				
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_tipe">Tipe</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control" id="idpoliklinik" placeholder="" name="idpoliklinik" value="{idpoli}">
						<input type="hidden" class="form-control" id="idpoli" placeholder="" name="idpoli" value="{idpoli}">
						<input type="text" class="form-control" disabled id="nama_tipe" placeholder="Nama Poliklinik" name="nama_tipe" value="{nama_tipe}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_poli">Nama Poliklinik</label>
					<div class="col-md-8">
						<input type="text" class="form-control" disabled id="nama_poli" placeholder="Nama Poliklinik" name="nama_poli" value="{nama_poli}">
					</div>
				</div>
				<div class="form-group" id="div_3">
					<label class="col-md-2 control-label" for="kodeantrian_poli">Kode Antrian Poliklinik</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="kodeantrian_poli" placeholder="Kode" name="kodeantrian_poli" value="{kodeantrian_poli}">
					</div>
				</div>
				
				
			</div>
				
			
			
			<div class="row">
				<div class="form-group">
					<label class="col-md-8 col-md-offset-2 text-danger">SOUND POLIKLINIK</label>
					
					<input type="hidden" class="form-control" id="arr_sound" placeholder="No Urut" name="arr_sound" value="">
				</div>
				<div class="form-group">
					<div class="col-md-8 col-md-offset-2">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_sound">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Urutan</th>
										<th width="65%">Asset Sound</th>
										<th width="20%">Action</th>										   
									</tr>
									<input id="idsound" type="hidden" value="">
									<tr>
										<th>#</th>
										<th>
											<input tabindex="0" type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut">
										</th>
										<th>
											<select id="sound_id"  name="sound_id" class="js-select2 form-control" style="width: 100%;">
												<option value="#" selected>-Pilih Asset Sound-</option>
												<?foreach($list_sound as $r){?>
												  <option value="<?=$r->id?>" ><?=$r->nama_asset?> (<?=$r->file_sound?>)</option>
												<?}?>
											</select>
										
										</th>
										
										<th>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah_sound"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh_sound"><i class="fa fa-refresh"></i></button>
												<button class="btn btn-danger btn-sm"  title="Play All" type="button" id="btn_play_all"><i class="fa fa-play"></i></button>
											</div>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<label class="col-md-8 col-md-offset-2 text-danger">DOKTER & SOUND</label>
					
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label"></label>
					<div class="col-md-8">
						<table class="table table-bordered">
							<thead>
								<th class="text-center" style="width:10%">x</th>
								<th class="text-center" style="width:40%">DOKTER</th>
								<th class="text-center" style="width:20%">KODE</th>
								<th class="text-center" style="width:30%">SOUND DOKTER</th>
							</thead>
							<tbody>
								<?
								$no=0;
								foreach($list_dokter as $r){
									$no=$no+1;
									?>
								<tr>
									<td  class="text-center"><?=$no?></td>
									<td><?=$r->nama?><input type="hidden" class="form-control iddokter" name="iddokter[]" value="<?=$r->iddokter?>"><input type="hidden" class="form-control idpoli" name="idpoli[]" value="<?=$r->idpoli?>"></td>
									<td  class="text-center"><input type="text" class="form-control" name="kodeantrian_dokter[]" value="<?=$r->kodeantrian_dokter?>"></td>
									<td  class="text-center">
										<select id="sound_dokter_id" name="sound_dokter_id[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($sound_dokter_id==''?'selected':'')?>>-Pilih Sound-</option>
											<?foreach($list_sound as $row){?>
											<option value="<?=$row->id?>" <?=($r->sound_dokter_id==$row->id?'selected':'')?>><?=$row->nama_asset?> (<?=$row->file_sound?>)</option>
											<?}?>
										</select>
									</td>
								</tr>
								<?}?>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
			
			<div class="form-group">
				<div class="col-md-2 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}antrian_poli_kode" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>

</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		if ($("#idpoli").val()!=''){
			load_sound();
		}
	})	
	$(".chk_status").on("click", function(){
		let id_det;
		 check = $(this).is(":checked");
			if(check) {
				$(this).closest('tr').find(".sound_idField").removeAttr("disabled");
			} else {
				$(this).closest('tr').find(".sound_idField").attr('disabled', 'disabled');
			}
	}); 
	
	function validate_final(){
		
		if ($("#kodeantrian_poli").val()==''){
			sweetAlert("Maaf...", "Tentukan Kode", "error");
			return false;
		}
		if ($("#nama_poli").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Poliklinik", "error");
			return false;
		}
		if ($("#kuota").val()==''){
			sweetAlert("Maaf...", "Tentukan kuota", "error");
			return false;
		}
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#cover-spin").show();
		return true;

		
	}
	$("#btn_tambah_sound").click(function() {
		let idpoli=$("#idpoli").val();
		let idsound=$("#idsound").val();
		let nourut=$("#nourut").val();
		let sound_id=$("#sound_id").val();
		if ($("#nourut").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		if ($("#sound_id").val()==''){
			sweetAlert("Maaf...", "Tentukan Isi", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}antrian_poli_kode/simpan_sound', 
			dataType: "JSON",
			method: "POST",
			data : {
				idpoli:idpoli,
				idsound:idsound,
				nourut:nourut,
				sound_id:sound_id,
				},
			complete: function(data) {
				// $("#idtipe").val('#').trigger('change');
				$('#index_sound').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				clear_sound();
				
				load_data_sound();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});

	});
	$("#btn_refresh_sound_text").click(function() {
	clear_sound();
	});
	$("#btn_play_all").click(function() {
		play_sound_arr();
	});
	function clear_sound(){
		$("#idsound").val('');
		$("#nourut").val('');
		$("#sound_id").val('#').trigger('change');
	}
	function load_sound(){
		var idpoli=$("#idpoli").val();
		$('#index_sound').DataTable().destroy();	
		table = $('#index_sound').DataTable({
			autoWidth: false,
			searching: false,
			serverSide: true,
			"processing": true,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}antrian_poli_kode/load_sound', 
				type: "POST" ,
				dataType: 'json',
				data : {
						idpoli:idpoli
					   }
			}
		});
		load_data_sound();
	}
	function hapus_sound($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Sound?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}antrian_poli_kode/hapus_sound',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$('#index_sound').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					load_data_sound();
				}
			});
		});
	}
	function edit_sound($id){
		let id=$id;
		$.ajax({
			url: '{site_url}antrian_poli_kode/edit_sound', 
			dataType: "JSON",
			method: "POST",
			data : {id:id},
			success: function(data) {
				$("#nourut").val(data.nourut);
				$("#sound_id").val(data.sound_id).trigger('change');
				$("#idsound").val(data.id);
			}
		});

	}
	function load_data_sound(){
		let idpoli=$("#idpoli").val();
		$.ajax({
			url: '{site_url}antrian_poli_kode/load_data_sound', 
			dataType: "JSON",
			method: "POST",
			data : {idpoli:idpoli},
			success: function(data) {
				$("#arr_sound").val(data.file);
				
			}
		});

	}
	function play_sound_arr(){
		let str_file=$("#arr_sound").val();
		var myArray = str_file.split(":");
		console.log(myArray[0]);
		queue_sounds(myArray);
	}
	function queue_sounds(sounds){
		// $config['upload_path'] = './assets/upload/sound_antrian/';
		let path_sound='{site_url}assets/upload/sound_antrian/';
		
		var index = 0;
		function recursive_play()
		{
			let file_sound = new Audio(path_sound+sounds[index]);
		  if(index+1 === sounds.length)
		  {
			play(file_sound);
		  }
		  else
		  {
			play(file_sound,function(){
				index++; recursive_play();
				});
		  }
		}

	recursive_play();   
	}
	 function play(audio, callback) {

		audio.play();
		if(callback)
		{
			audio.onended = callback;
		}
	}
</script>