<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<button onclick="btn_add()" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Poliklinik</button>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content tab-content">
		<div class="col-md-12">
			<div class="form-group" style="margin-bottom: 15px;">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_poli">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="30%">Tipe</th>
									<th width="40%">Poliklinik</th>
									<th width="10%">Kode Antrian</th>
									<th width="15%">Action</th>										   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_add" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pilih Poliklinik</h3>
				</div>
				<div class="block-content">
					<div class="row">
						
						<div class="col-md-12"> 
							<div class="row">
								<div class="form-group">
									<label class="col-md-12 control-label">Poliklinik</label>
									<div class="col-md-12"> 
										<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											
										</select>
									</div>
									
								</div>
								<div class="form-group pull-t">
									
									<label class="col-md-12 control-label">Kode Antrian</label>
									<div class="col-md-12"> 
										<input type="text" class="form-control" id="kodeantrian_poli" placeholder="Kode" name="kodeantrian_poli" value="">
									</div>
								</div>
								<div class="form-group pull-t">
									
									<label class="col-md-12 control-label">Asset Sound</label>
									<div class="col-md-12"> 
										<select id="sound_poli_id" name="sound_poli_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" selected>-Pilih Sound-</option>
											<?foreach($list_sound as $r){?>
											<option value="<?=$r->id?>" ><?=$r->nama_asset?> (<?=$r->file_sound?>)</option>
											<?}?>
										</select>
									</div>
								</div>
							</div>
							
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-warning" type="button" onclick="simpan_poli()" id="btn-tf"><i class="fa fa-mail-forward"></i> Add</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>	
<div class="modal" id="modal_dokter" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Dokter Poliklinik <label id="nama_poli"></label></h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_dokter">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="85%">Dokter</th>
											<th width="10%">Pilih</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){	
	load_poliklinik();
	
})	
function load_dokter(id,nama_poli){
	$('#index_dokter').DataTable().destroy();	
	$("#modal_dokter").modal('show');
	$("#nama_poli").text(nama_poli);
	table = $('#index_dokter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}antrian_poli_kode/load_dokter', 
                type: "POST" ,
                dataType: 'json',
				data : {
						id:id
					   }
            }
        });
}
function check_save_dokter(idpoli,iddokter,pilih){
	// alert('id : '+id+' Pilih :'+pilih);
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}antrian_poli_kode/check_save_dokter',
			type: 'POST',
			data: {idpoli: idpoli,iddokter:iddokter,pilih:pilih},
			complete: function() {
				$("#cover-spin").hide();
				$('#index_dokter').DataTable().ajax.reload( null, false ); 
				$('#index_poli').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update'});
				
			}
	});
}
function btn_add(){
	$("#modal_add").modal('show');
	load_list_poli();
	$("#kodeantrian_poli").val('');
	$("#sound_poli_id").val('').trigger('change');
}
function load_poliklinik(){
	$('#index_poli').DataTable().destroy();	
	table = $('#index_poli').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}antrian_poli_kode/load_poliklinik_tujuan', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function load_list_poli(){
	$(".js-select2").select2({
              dropdownParent: $("#modal_add")
      });
	$.ajax({
		url: '{site_url}antrian_poli_kode/load_list_poli',
		method: "POST",
		dataType: "json",
		data: {
			// "idpoli": idpoli
		},
		success: function(data) {
			$("#idpoli").empty();
			$("#idpoli").append("<option value=''>Pilih Dokter</option>");
			for (var i = 0; i < data.length; i++) {
					$("#idpoli").append("<option value='" + data[i].id + "'>" + data[i].tipe + ' - ' +data[i].nama + "</option>");
			}

			// $("#idpoli").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}
function simpan_poli(){
	if ($("#idpoli").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Poliklinik!", "error");
		return false;
	}
	if ($("#kodeantrian_poli").val()==''){
		sweetAlert("Maaf...", "Tentukan Kode Antrian Poliklinik!", "error");
		return false;
	}
	if ($("#sound_poli_id").val()==''){
		sweetAlert("Maaf...", "Tentukan Sound Antrian Poliklinik!", "error");
		return false;
	}
	let idpoli=$("#idpoli").val();
	let kodeantrian_poli=$("#kodeantrian_poli").val();
	let sound_poli_id=$("#sound_poli_id").val();
	$("#cover-spin").show();
	 $.ajax({
			url: '{site_url}antrian_poli_kode/simpan_poli',
			type: 'POST',
			dataType: "json",
			data: {
					idpoli: idpoli,
					kodeantrian_poli: kodeantrian_poli,
					sound_poli_id: sound_poli_id,
				},
			success: function(data) {
				$("#cover-spin").hide();
				$('#modal_add').modal('hide');
				swal("Berhasil!", "Tambah Poliklinik Berhasil", "success");
				load_poliklinik();
			}
		});
}
function hapus_poli_tujuan(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Poliklinik Tujuan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}antrian_poli_kode/hapus_poli_tujuan',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_poli').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>