<script type="text/javascript">
function uploadFotoPemeriksaan(transaksiId) {
    window.location = '{base_url}term_radiologi_ctscan_hasil/proses/' + transaksiId;
}

function editFotoPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan_hasil/proses/' + transaksiId + '/edit_hasil_pemeriksaan', '_blank');
}

function lihatFotoPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan_hasil/proses/' + transaksiId + '/lihat_hasil_pemeriksaan', '_blank');
}

function editTarifPemeriksaan(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_rad/ctscan_pemeriksaan/' + transaksiId + '/' + 'edit_pemeriksaan' , '_blank');
}

function batalTransaksi(transaksiId) {
    swal({
        title: 'Batalkan Permintaan',
        text: 'Apakah Anda yakin ingin membatalkan permintaan ini? Jika dilakukan pembatalan, permintaan Anda tidak akan terkirim ke radiologi tujuan. Tekan "Ya" untuk membatalkan.',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#d33",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $.ajax({
                url: '{site_url}term_radiologi_ctscan/batal_draft_permintaan/' + transaksiId,
                success: function(result) {
                    $.toaster({
                        priority: 'success',
                        title: 'Berhasil!',
                        message: 'Data berhasil dihapus.'
                    });

                    // Re-load Datatable
                    loadListTransaksi();
                },
                error: function(error) {
                    console.error('Error deleting order:', error);
                    alert('Error deleting order. Please try again.');
                }
            });
        }
    });
}

function merubahStatusExpertise(transaksiId, newStatus) {
    let confirmationMessage = '';
    let successMessage = '';
    
    if (newStatus == 1) {
        confirmationMessage = 'Apakah Anda yakin ingin langsung ke expertise radiologi untuk transaksi ini? Tekan "Ya" untuk langsung atau "Batalkan" untuk kembali ke expertise.';
        successMessage = 'Status expertise radiologi berhasil diubah menjadi Menolak.';
    } else if (newStatus == 2) {
        confirmationMessage = 'Apakah Anda yakin ingin menolak expertise radiologi untuk transaksi ini? Tekan "Ya" untuk menolak atau "Batalkan" untuk kembali ke expertise.';
        successMessage = 'Status expertise radiologi berhasil diubah menjadi Menolak.';
    } else if (newStatus == 3) {
        confirmationMessage = 'Apakah Anda yakin ingin kembali ke expertise untuk transaksi ini? Tekan "Ya" untuk kembali atau "Batalkan" untuk tetap.';
        successMessage = 'Status expertise radiologi berhasil diubah menjadi Kembali ke Expertise.';
    } else {
        console.error('Status tidak valid.');
        return;
    }

    swal({
        title: 'Perubahan Status Expertise Radiologi',
        text: confirmationMessage,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#d33",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $.ajax({
                url: '{site_url}term_radiologi_ctscan_hasil/merubah_status_expertise/' + transaksiId + '/' + newStatus,
                success: function(result) {
                    $.toaster({
                        priority: 'success',
                        title: 'Berhasil!',
                        message: successMessage
                    });

                    // Re-load Datatable
                    loadListTransaksi();
                },
                error: function(error) {
                    console.error('Error updating expertise status:', error);
                    alert('Terjadi kesalahan saat memperbarui status expertise. Silakan coba lagi.');
                }
            });
        }
    });
}

function merubahPrioritasUrgent(transaksiId) {
    const confirmationMessage = 'Apakah Anda yakin ingin mengubah prioritas expertise menjadi Urgent? Tekan "Ya" untuk mengubah atau "Batalkan" untuk tetap.';
    const successMessage = 'Prioritas expertise berhasil diubah menjadi Urgent.';

    swal({
        title: 'Perubahan Prioritas Expertise',
        text: confirmationMessage,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#d33",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $.ajax({
                url: '{site_url}term_radiologi_ctscan_hasil/merubah_prioritas_urgent/' + transaksiId,
                success: function(result) {
                    $.toaster({
                        priority: 'success',
                        title: 'Berhasil!',
                        message: successMessage
                    });

                    // Re-load Datatable
                    loadListTransaksi();
                },
                error: function(error) {
                    console.error('Error updating expertise priority:', error);
                    alert('Terjadi kesalahan saat memperbarui prioritas expertise. Silakan coba lagi.');
                }
            });
        }
    });
}

function cetakPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_pemeriksaan/' + transaksiId, '_blank');
}

function cetakBuktiPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_bukti_pemeriksaan/' + transaksiId, '_blank');
}

function cetakBuktiPemeriksaanSmall(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_bukti_pemeriksaan_small/' + transaksiId, '_blank');
}

function cetakBuktiPengambilanPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_bukti_pengambilan_pemeriksaan/' + transaksiId, '_blank');
}

function cetakBuktiPengambilanPemeriksaanSmall(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/cetak_bukti_pengambilan_pemeriksaan_small/' + transaksiId, '_blank');
}

function cetakHasilPemeriksaan(hasilPemeriksaanId, typeFormat) {
    window.open('{base_url}term_radiologi_ctscan_expertise/cetak_hasil_pemeriksaan/' + hasilPemeriksaanId + '/' + typeFormat, '_blank');
}

function kirimEmailHasilPemeriksaan(transaksiId, pasienId, tujuanRadiologi) {
    loadDataPetugasPengirimHasil(tujuanRadiologi, '');
    loadDataPetugasPenerimaHasil(tujuanRadiologi, '');
    getEmailPasien(pasienId);

    $('#transaksiId').val(transaksiId);
}

function kirimUlangEmailHasilPemeriksaan(transaksiId) {
    getEmailPengirimanHasilPemeriksaanTerakhir(transaksiId);

    $('#transaksiId').val(transaksiId);
}

function kirimEmailHasilUpload(transaksiId, pasienId, tujuanRadiologi) {
    getEmailPasien(pasienId);

    $('#transaksiId').val(transaksiId);
}

function riwayatPengirimanEmailHasilPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan_expertise/riwayat_pengiriman_email_hasil_pemeriksaan/' + transaksiId, '_blank');
}

function riwayatHasilPemeriksaan(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan_expertise/riwayat_hasil_pemeriksaan/' + transaksiId, '_blank');
}

function riwayatCetakHasilPemeriksaan(hasilPemeriksaanId) {
    $.ajax({
        url: '{base_url}term_radiologi_ctscan_expertise/riwayat_cetak_hasil_pemeriksaan/' + hasilPemeriksaanId,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            if (response.status == 'success') {
                var tableBody = $('#table-history-cetak tbody');
                tableBody.empty();

                $.each(response.data, function(index, item) {
                    var row = '<tr>' +
                        '<td class="text-center">' + (index + 1) + '</td>' +
                        '<td class="text-center">' + item.urutan_cetak + '</td>' +
                        '<td class="text-center">' + item.petugas_cetak_hasil + '<br>' + item.waktu_cetak_hasil + '</td>' +
                        '</tr>';
                    tableBody.append(row);
                });
            } else {
                alert('Error fetching data');
            }
        },
        error: function(xhr, status, error) {
            console.error(error);
            alert('Error fetching data');
        }
    });
}

function riwayatRadiologi(pendaftaranId, pasienId) {
    window.open('{base_url}term_radiologi_ctscan/riwayat_radiologi/' + pendaftaranId + '/' + pasienId, '_blank');
}

function dataRadiologi(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/data_radiologi/' + asalRujukan + '/' + pendaftaranId + '/' + transaksiId, '_blank');
}

function stikerIdentitas(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/stiker_identitas/' + transaksiId, '_blank');
}

function labelTabungDarah(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/label_tabung_darah/' + transaksiId, '_blank');
}

function labelRadiologi(transaksiId) {
    window.open('{base_url}term_radiologi_ctscan/label_radiologi/' + transaksiId, '_blank');
}

function inputBMHPTagihan(pendaftaranId) {
    window.open('{base_url}tpoliklinik_trx/tindakan/' + pendaftaranId + '/erm_trx/input_bmhp', '_blank');
}                    

function inputBMHPNonTagihan(pendaftaranId) {
    window.open('{base_url}tpoliklinik_trx/tindakan/' + pendaftaranId + '/erm_trx/input_bmhp', '_blank');
}

function loadDataPetugasPengirimHasil(params, value) {
    $.ajax({
        url: '{site_url}term_radiologi_ctscan/petugas_pengiriman_hasil/' + params,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#petugasPengirimHasil").select2('destroy');
            $("#petugasPengirimHasil").empty();

            response.petugas.map(function(petugas) {
                let selected = '';
                if (petugas.iduser == value) {
                    selected = 'selected';
                } else if (petugas.status_default == '1') {
                    selected = 'selected';
                }

                $("#petugasPengirimHasil").append('<option value="' + petugas.iduser + '" ' + selected + '>' + petugas.nama_petugas + '</option>');
            });

            $("#petugasPengirimHasil").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function loadDataPetugasProsesPemeriksaan(value) {
    $.ajax({
        url: '{site_url}term_radiologi_ctscan/petugas_proses_pemeriksaan/' + value,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#petugas_pemeriksaan").select2('destroy');
            $("#petugas_pemeriksaan").empty();

            response.petugas.map(function(petugas) {
                $("#petugas_pemeriksaan").append('<option value="' + petugas.iduser + '">' + petugas.nama_petugas + '</option>');
            });

            $("#petugas_pemeriksaan").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function loadDataDokterRadiologi(params, value) {
    $.ajax({
        url: '{site_url}term_radiologi_ctscan/dokter_radiologi/' + params,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#dokter_radiologi").select2('destroy');
            $("#dokter_radiologi").empty();
            
            response.dokter.map(function(dokter) {
                let selected = '';
                if (dokter.iddokter == value) {
                    selected = 'selected';
                }

                $("#dokter_radiologi").append('<option value="' + dokter.iddokter + '" ' + selected + '>' + dokter.nama_dokter + '</option>');
            });

            $("#dokter_radiologi").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function loadDataPetugasPenerimaHasil(params, value) {
    $.ajax({
        url: '{site_url}term_radiologi_ctscan/petugas_penerimaan_hasil/' + params,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#petugasPenerimaHasil").select2('destroy');
            $("#petugasPenerimaHasil").empty();

            response.petugas.map(function(petugas) {
                let selected = '';
                if (petugas.iduser == value) {
                    selected = 'selected';
                } else if (petugas.status_default == '1') {
                    selected = 'selected';
                }

                $("#petugasPenerimaHasil").append('<option value="' + petugas.iduser + '" ' + selected + '>' + petugas.nama_petugas + '</option>');
            });

            $("#petugasPenerimaHasil").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function getEmailPasien(pasienId) {
    $('#emailKirimHasil').importTags('');
    $('#emailKirimHasilUpload').importTags('');
    $.ajax({
        url: '{base_url}term_radiologi_ctscan_expertise/get_email_pasien/' + pasienId,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            $('#emailKirimHasil').addTag(data.email);
            $('#emailKirimHasilUpload').addTag(data.email);
        },
        error: function(xhr, status, error) {
            console.error('Error fetching email pasien:', error);
        }
    });
}

function getEmailPengirimanHasilPemeriksaanTerakhir(transaksiId) {
    $('#emailKirimUlangHasil').importTags('');
    $.ajax({
        url: '{base_url}term_radiologi_ctscan_expertise/get_email_pengiriman_hasil_pemeriksaan_terakhir/' + transaksiId,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            $('#emailKirimUlangHasil').importTags(data.email);
        },
        error: function(xhr, status, error) {
            console.error('Error fetching email pasien:', error);
        }
    });
}

function previewDICOM(fileId, tipe) {
    window.open('{base_url}term_radiologi/preview_dicom/' + fileId + '/' + tipe, '_blank');
}

function previewPhoto(fileId, tipe, pemeriksaanId) {
    window.open('{base_url}term_radiologi/preview_photo/' + fileId + '/' + tipe + '/' + pemeriksaanId, '_blank');
}
</script>
