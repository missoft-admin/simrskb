<!-- Modal Filter -->
<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                </ul>
                <h5 class="modal-title">Filter Mode</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="selectTujuanRad">Tujuan Radiologi</label>
                                <select id="selectTujuanRad" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="0" selected>Semua</option>
                                    <?php foreach ($tujuan_radiologi as $r) { ?>
                                    <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputTanggalPermintaanDari">Tanggal Permintaan</label>
                                <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                    <input class="form-control" type="text" id="inputTanggalPermintaanDari" name="inputTanggalPermintaanDari" value="{tanggal}">
                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                    <input class="form-control" type="text" id="inputTanggalPermintaanSampai" name="inputTanggalPermintaanSampai" value="{tanggal}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputNomorMedrec">Nomor Medrec</label>
                                <input type="text" class="form-control" id="inputNomorMedrec" placeholder="Masukkan Nomor Medrec">
                            </div>
                            <div class="form-group">
                                <label for="selectDokterPeminta">Dokter Peminta</label>
                                <select id="selectDokterPeminta" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="0" selected>Semua</option>
                                    <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                    <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputNomorPendaftaran">Nomor Pendaftaran</label>
                                <input type="text" class="form-control" id="inputNomorPendaftaran" placeholder="Masukkan Nomor Pendaftaran">
                            </div>
                            <div class="form-group">
                                <label for="inputNomorRadiologi">Nomor Radiologi</label>
                                <input type="text" class="form-control" id="inputNomorRadiologi" placeholder="Masukkan Nomor Radiologi">
                            </div>
                            <div class="form-group">
                                <label for="selectStatusCitoExpertise">Status CITO Expertise</label>
                                <select id="selectStatusCitoExpertise" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="#" selected>Semua</option>
                                    <?php foreach (list_variable_ref(249) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="selectAsalPasien">Asal Pasien</label>
                                <select id="selectAsalPasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="0" selected>Semua</option>
                                    <option value="1">Poliklinik</option>
                                    <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                    <option value="3">Rawat Inap</option>
                                    <option value="4">One Day Surgery (ODS)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="selectKelompokPasien">Kelompok Pasien</label>
                                <select id="selectKelompokPasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="0" selected>Semua</option>
                                    <?php foreach (get_all('mpasien_kelompok', ['status' => 1]) as $row) { ?>
                                    <option value="<?=$row->id?>"><?=$row->nama?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputNamaPasien">Nama Pasien</label>
                                <input type="text" class="form-control" id="inputNamaPasien" placeholder="Masukkan Nama Pasien">
                            </div>
                            <div class="form-group">
                                <label for="selectDokterRadiologi">Dokter Radiologi</label>
                                <select id="selectDokterRadiologi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="0" selected>Semua</option>
                                    <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                    <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputNomorPermintaan">Nomor Permintaan</label>
                                <input type="text" class="form-control" id="inputNomorPermintaan" placeholder="Masukkan Nomor Permintaan">
                            </div>
                            <div class="form-group">
                                <label for="selectStatusPemeriksaan">Status Tindakan</label>
                                <select id="selectStatusPemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="#" selected>Semua</option>
                                    <option value="4">Menunggu Upload</option>
                                    <option value="5">Telah Upload</option>
                                    <option value="6">Selesai Expertise</option>
                                    <option value="7">Telah Dikirim</option>
                                    <option value="0">Dibatalkan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="selectStatusExpertise">Status Expertise</label>
                                <select id="selectStatusExpertise" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="#" selected>Semua</option>
                                    <option value="1">Langsung</option>
                                    <option value="2">Menolak</option>
                                    <option value="3">Kembali</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn-filter">Terapkan Filter</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", "#btn-filter", function() {
        $("#cover-spin").show();
        loadListTransaksi();
    });

    function bindChangeEvent(sourceId, targetId) {
        $(document).on("change", sourceId, function() {
            let value = $(this).val();
            $(targetId).val(value).trigger('change');
        });
    }

    bindChangeEvent("#tujuan_radiologi", "#selectTujuanRad");
    bindChangeEvent("#dokter_peminta", "#selectDokterPeminta");
    bindChangeEvent("#tanggal_dari", "#inputTanggalPermintaanDari");
    bindChangeEvent("#tanggal_sampai", "#inputTanggalPermintaanSampai");
    bindChangeEvent("#dokter_radiologi", "#selectDokterRadiologi");
    bindChangeEvent("#nomor_medrec", "#inputNomorMedrec");
    bindChangeEvent("#nama_pasien", "#inputNamaPasien");

    $('#modal-filter').on('hidden.bs.modal', function() {
        function updateValue(sourceId, targetId) {
            let value = $(sourceId).val();
            $(targetId).val(value).trigger('change');
        }

        updateValue('#selectTujuanRad', '#tujuan_radiologi');
        updateValue('#selectDokterPeminta option:selected', '#dokter_peminta');
        updateValue('#inputTanggalPermintaanDari', '#tanggal_dari');
        updateValue('#inputTanggalPermintaanSampai', '#tanggal_sampai');
        updateValue('#selectDokterRadiologi', '#dokter_radiologi');
        updateValue('#inputNomorMedrec', '#nomor_medrec');
        updateValue('#inputNamaPasien', '#nama_pasien');

        let statusPemeriksaanValue = $('#selectStatusPemeriksaan option:selected').val();
        $('#status_pemeriksaan').val(statusPemeriksaanValue);
        setTab(statusPemeriksaanValue);
    });
});

</script>
