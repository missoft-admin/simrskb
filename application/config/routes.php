<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Dashboard';
$route['sign_in'] = 'Musers/sign_in';
$route['get/datapenjualan'] = 'tpasien_pengembalian/getDataPenjualan';
$route['get/detailpenjualan'] = 'tpasien_pengembalian/getDetailPenjualan';
$route['get/datapengembalian'] = 'tpasien_pengembalian/getDataPengembalian';
$route['get/datanonracikan'] = 'tpasien_pengembalian/getNonRacikan';
$route['get/dataracikan'] = 'tpasien_pengembalian/getRacikan';
$route['get/dataracikanobat'] = 'tpasien_pengembalian/getRacikanObat';
$route['404_override'] = 'page404';
$route['translate_uri_dashes'] = FALSE;
