<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class EncounterLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function getById($encounterId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Encounter/{$encounterId}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($encounterData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        // FormData
        $resourceType = $encounterData['resource_type'];
        $statusEncounter = $encounterData['status_encounter'];
        $classCode = $encounterData['class_code'];
        $className = $encounterData['class_name'];
        $patientId = $encounterData['patient_id'];
        $patientName = $encounterData['patient_name'];
        $participantTypeCode = $encounterData['participant_type_code'];
        $participantTypeName = $encounterData['participant_type_name'];
        $practitionerId = $encounterData['practitioner_id'];
        $practitionerName = $encounterData['practitioner_name'];
        $arrivedStartDate = $encounterData['arrived_start_date'];
        $arrivedEndDate = $encounterData['arrived_end_date'];
        $inprogressStartDate = $encounterData['inprogress_start_date'];
        $inprogressEndDate = $encounterData['inprogress_end_date'];
        $finishedStartDate = $encounterData['finished_start_date'];
        $finishedEndDate = $encounterData['finished_end_date'];
        $locationId = $encounterData['location_id'];
        $locationName = $encounterData['location_name'];
        $diagnosisConditionId = $encounterData['diagnosis_condition_id'];
        $diagnosisConditionName = $encounterData['diagnosis_condition_name'];
        $diagnosisCode = $encounterData['diagnosis_code'];
        $diagnosisName = $encounterData['diagnosis_name'];
        $organizationId = $encounterData['organization_id'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Encounter",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "Encounter",
            "status": "'.$statusEncounter.'",
            "class": {
                "system": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
                "code": "'.$classCode.'",
                "display": "'.$className.'"
            },
            "subject": {
                "reference": "Patient/'.$patientId.'",
                "display": "'.$patientName.'"
            },
            "participant": [
                {
                    "type": [
                        {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
                                    "code": "'.$participantTypeCode.'",
                                    "display": "'.$participantTypeName.'"
                                }
                            ]
                        }
                    ],
                    "individual": {
                        "reference": "Practitioner/'.$practitionerId.'",
                        "display": "'.$practitionerName.'"
                    }
                }
            ],
            "period": {
                "start": "'.$arrivedStartDate.'",
                "end": "'.$inprogressEndDate.'"
            },
            "location": [
                {
                    "location": {
                        "reference": "Location/'.$locationId.'",
                        "display": "'.$locationName.'"
                    }
                }
            ],
            "statusHistory": [
                {
                    "status": "arrived",
                    "period": {
                        "start": "'.$arrivedStartDate.'",
                        "end": "'.$arrivedEndDate.'"
                    }
                },
                {
                    "status": "in-progress",
                    "period": {
                        "start": "'.$inprogressStartDate.'",
                        "end": "'.$inprogressEndDate.'"
                    }
                }
            ],
            "serviceProvider": {
                "reference": "Organization/'.$organizationId.'"
            },
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/encounter/'.$organizationId.'",
                    "value": "P20240001"
                }
            ]
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

        return $response;
    }

    public function updateInprogress($encounterId, $encounterData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        // FormData
        $resourceType = $encounterData['resource_type'];
        $statusEncounter = $encounterData['status_encounter'];
        $classCode = $encounterData['class_code'];
        $className = $encounterData['class_name'];
        $patientId = $encounterData['patient_id'];
        $patientName = $encounterData['patient_name'];
        $participantTypeCode = $encounterData['participant_type_code'];
        $participantTypeName = $encounterData['participant_type_name'];
        $practitionerId = $encounterData['practitioner_id'];
        $practitionerName = $encounterData['practitioner_name'];
        $arrivedStartDate = $encounterData['arrived_start_date'];
        $arrivedEndDate = $encounterData['arrived_end_date'];
        $inprogressStartDate = $encounterData['inprogress_start_date'];
        $inprogressEndDate = $encounterData['inprogress_end_date'];
        $finishedStartDate = $encounterData['finished_start_date'];
        $finishedEndDate = $encounterData['finished_end_date'];
        $locationId = $encounterData['location_id'];
        $locationName = $encounterData['location_name'];
        $diagnosisConditionId = $encounterData['diagnosis_condition_id'];
        $diagnosisConditionName = $encounterData['diagnosis_condition_name'];
        $diagnosisCode = $encounterData['diagnosis_code'];
        $diagnosisName = $encounterData['diagnosis_name'];
        $organizationId = $encounterData['organization_id'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Encounter/{$encounterId}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "Encounter",
            "id": "'.$encounterId.'",
            "identifier": [
                {
                "system": "http://sys-ids.kemkes.go.id/encounter/'.$organizationId.'",
                "value": "P20240001"
                }
            ],
            "status": "in-progress",
            "class": {
                "system": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
                "code": "'.$classCode.'",
                "display": "'.$className.'"
            },
            "subject": {
                "reference": "Patient/'.$patientId.'",
                "display": "'.$patientName.'"
            },
            "participant": [
                {
                    "type": [
                        {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
                                    "code": "'.$participantTypeCode.'",
                                    "display": "'.$participantTypeName.'"
                                }
                            ]
                        }
                    ],
                    "individual": {
                        "reference": "Practitioner/'.$practitionerId.'",
                        "display": "'.$practitionerName.'"
                    }
                }
            ],
            "period": {
                "start": "'.$arrivedStartDate.'",
                "end": "'.$inprogressEndDate.'"
            },
            "location": [
                {
                    "location": {
                        "reference": "Location/'.$locationId.'",
                        "display": "'.$locationName.'"
                    }
                }
            ],
            "statusHistory": [
                {
                "status": "arrived",
                    "period": {
                        "start": "'.$arrivedStartDate.'",
                        "end": "'.$arrivedEndDate.'"
                    }
                },
                {
                    "status": "in-progress",
                    "period": {
                        "start": "'.$inprogressStartDate.'",
                        "end": "'.$inprogressEndDate.'"
                    }
                }
            ],
            "serviceProvider": {
                "reference":"Organization/'.$organizationId.'"
            }
        }
        ',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function updateDischargeDisposition($encounterId, $encounterData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Encounter/{$encounterId}",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "Encounter",
            "id": "'.$encounterId.'",
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/encounter/'.$organizationId.'",
                    "value": "P20240001"
                }
            ],
            "status": "in-progress",
            "class": {
                "system": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
                "code": "'.$classCode.'",
                "display": "'.$className.'"
            },
            "subject": {
                "reference": "Patient/'.$patientId.'",
                "display": "'.$patientName.'"
            },
            "participant": [
                {
                    "type": [
                        {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
                                    "code": "'.$participantTypeCode.'",
                                    "display": "'.$participantTypeName.'"
                                }
                            ]
                        }
                    ],
                    "individual": {
                        "reference": "Practitioner/'.$practitionerId.'",
                        "display": "'.$practitionerName.'"
                    }
                }
            ],
            "period": {
                "start": "'.$arrivedStartDate.'",
                "end": "'.$inprogressEndDate.'"
            },
            "location": [
                {
                    "location": {
                        "reference": "Location/'.$locationId.'",
                        "display": "'.$locationName.'"
                    }
                }
            ],
            "statusHistory": [
                {
                    "status": "arrived",
                    "period": {
                        "start": "'.$arrivedStartDate.'",
                        "end": "'.$arrivedEndDate.'"
                    }
                },
                {
                    "status": "in-progress",
                    "period": {
                        "start": "'.$inprogressStartDate.'",
                        "end": "'.$inprogressEndDate.'"
                    }
                }
            ],
            "hospitalization": {
                "dischargeDisposition": {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/discharge-disposition",
                            "code": "home",
                            "display": "Home"
                        }
                    ],
                    "text" : "Anjuran dokter untuk pulang dan kontrol kembali 1 bulan setelah minum obat"
                }
            },
            "serviceProvider": {
                "reference": "Organization/'.$organizationId.'"
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function updateFinished($encounterId, $encounterData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        // FormData
        $resourceType = $encounterData['resource_type'];
        $statusEncounter = $encounterData['status_encounter'];
        $classCode = $encounterData['class_code'];
        $className = $encounterData['class_name'];
        $patientId = $encounterData['patient_id'];
        $patientName = $encounterData['patient_name'];
        $participantTypeCode = $encounterData['participant_type_code'];
        $participantTypeName = $encounterData['participant_type_name'];
        $practitionerId = $encounterData['practitioner_id'];
        $practitionerName = $encounterData['practitioner_name'];
        $arrivedStartDate = $encounterData['arrived_start_date'];
        $arrivedEndDate = $encounterData['arrived_end_date'];
        $inprogressStartDate = $encounterData['inprogress_start_date'];
        $inprogressEndDate = $encounterData['inprogress_end_date'];
        $finishedStartDate = $encounterData['finished_start_date'];
        $finishedEndDate = $encounterData['finished_end_date'];
        $locationId = $encounterData['location_id'];
        $locationName = $encounterData['location_name'];
        $diagnosisConditionId = $encounterData['diagnosis_condition_id'];
        $diagnosisConditionName = $encounterData['diagnosis_condition_name'];
        $diagnosisCode = $encounterData['diagnosis_code'];
        $diagnosisName = $encounterData['diagnosis_name'];
        $organizationId = $encounterData['organization_id'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Encounter/$encounterId",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "Encounter",
            "id": "'.$encounterId.'",
            "identifier": [
                {
                "system": "http://sys-ids.kemkes.go.id/encounter/'.$organizationId.'",
                "value": "P20240001"
                }
            ],
            "status": "finished",
            "class": {
                "system": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
                "code": "'.$classCode.'",
                "display": "'.$className.'"
            },
            "subject": {
                "reference": "Patient/'.$patientId.'",
                "display": "'.$patientName.'"
            },
            "participant": [
                {
                    "type": [
                        {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
                                    "code": "'.$participantTypeCode.'",
                                    "display": "'.$participantTypeName.'"
                                }
                            ]
                        }
                    ],
                    "individual": {
                        "reference": "Practitioner/'.$practitionerId.'",
                        "display": "'.$practitionerName.'"
                    }
                }
            ],
            "period": {
                "start": "'.$arrivedStartDate.'",
                "end": "'.$finishedEndDate.'"
            },
            "location": [
                {
                    "location": {
                        "reference": "Location/'.$locationId.'",
                        "display": "'.$locationName.'"
                    }
                }
            ],
            "statusHistory": [
                {
                    "status": "arrived",
                    "period": {
                        "start": "'.$arrivedStartDate.'",
                        "end": "'.$arrivedEndDate.'"
                    }
                },
                {
                    "status": "in-progress",
                    "period": {
                        "start": "'.$inprogressStartDate.'",
                        "end": "'.$inprogressEndDate.'"
                    }
                },
                {
                    "status": "finished",
                    "period": {
                        "start": "'.$finishedStartDate.'",
                        "end": "'.$finishedEndDate.'"
                    }
                }
            ],
            "diagnosis": [
                {
                    "condition": {
                        "reference": "Condition/'.$diagnosisConditionId.'",
                        "display": "'.$diagnosisConditionName.'"
                    },
                    "use": {
                        "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/diagnosis-role",
                            "code": "'.$diagnosisCode.'",
                            "display": "'.$diagnosisName.'"
                        }
                        ]
                    },
                    "rank": 1
                }
            ],
            "serviceProvider": {
                "reference":"Organization/'.$organizationId.'"
            }
        }
        ',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
