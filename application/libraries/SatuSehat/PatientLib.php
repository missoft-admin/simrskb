<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class PatientLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI = &get_instance();
        $this->loadDependencies();
    }

    public function searchByNIK($nik)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Patient?identifier=https%3A%2F%2Ffhir.kemkes.go.id%2Fid%2Fnik%7C{$nik}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getById($patientId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Patient/{$patientId}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function createByNIK($patientData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        // FormData
        $identifier = $patientData->identifier;
        $name = $patientData->name;
        $phone = $patientData->phone;
        $gender = $patientData->gender;
        $birthDate = $patientData->birth_date;
        $addressHome = $patientData->address_home;
        $city = $patientData->city;
        $postalCode = $patientData->postal_code;
        $provinceCode = $patientData->province_code;
        $cityCode = $patientData->city_code;
        $districtCode = $patientData->district_code;
        $villageCode = $patientData->village_code;
        $birthPlace = $patientData->birth_place;
        $rt = $patientData->rt;
        $rw = $patientData->rw;

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl.'/Patient',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
            "resourceType": "Patient",
            "meta": {
                "profile": [
                    "https://fhir.kemkes.go.id/r4/StructureDefinition/Patient"
                ]
            },
            "identifier": [
                {
                    "use": "official",
                    "system": "https://fhir.kemkes.go.id/id/nik",
                    "value": "'.$identifier.'"
                }
            ],
            "active": true,
            "name": [
                {
                    "use": "official",
                    "text": "'.$name.'"
                }
            ],
            "telecom": [
                {
                    "system": "phone",
                    "value": "'.$phone.'",
                    "use": "mobile"
                }
            ],
            "gender": "'.$gender.'",
            "birthDate": "'.$birthDate.'",
            "deceasedBoolean": false,
            "address": [
                {
                    "use": "home",
                    "line": [
                        "'.$addressHome.'"
                    ],
                    "city": "'.$city.'",
                    "postalCode": "'.$postalCode.'",
                    "country": "ID",
                    "extension": [
                        {
                        "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                        "extension": [
                            {
                                "url": "province",
                                "valueCode": "'.$provinceCode.'"
                            },
                            {
                                "url": "city",
                                "valueCode": "'.$cityCode.'"
                            },
                            {
                                "url": "district",
                                "valueCode": "'.$districtCode.'"
                            },
                            {
                                "url": "village",
                                "valueCode": "'.$villageCode.'"
                            },
                            {
                                "url": "rt",
                                "valueCode": "'.$rt.'"
                            },
                            {
                                "url": "rw",
                                "valueCode": "'.$rw.'"
                            }
                        ]
                        }
                    ]
                }
            ],
            "maritalStatus": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/v3-MaritalStatus",
                        "code": "M",
                        "display": "Married"
                    }
                ],
                "text": "Married"
            },
            "multipleBirthInteger": 0,
            "contact": [
                {
                    "relationship": [
                        {
                        "coding": [
                            {
                                "system": "http://terminology.hl7.org/CodeSystem/v2-0131",
                                "code": "C"
                            }
                        ]
                        }
                    ],
                    "name": {
                        "use": "official",
                        "text": "'.$name.'"
                    },
                    "telecom": [
                        {
                        "system": "phone",
                        "value": "'.$phone.'",
                        "use": "mobile"
                        }
                    ]
                }
            ],
            "communication": [
                {
                    "language": {
                        "coding": [
                        {
                            "system": "urn:ietf:bcp:47",
                            "code": "id-ID",
                            "display": "Indonesian"
                        }
                        ],
                        "text": "Indonesian"
                    },
                    "preferred": true
                }
            ],
            "extension": [
                {
                    "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/birthPlace",
                    "valueAddress": {
                        "city": "'.$birthPlace.'",
                        "country": "ID"
                    }
                },
                {
                    "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/citizenshipStatus",
                    "valueCode": "WNI"
                }
            ]
            }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);
        // print_r($patientData);exit();
        // print_r($response);exit();

        curl_close($curl);

        return $response;
    }

    public function searchByNameGenderBirthdate($name, $birthdate, $gender)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Patient?name={$name}&birthdate={$birthdate}&gender={$gender}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
