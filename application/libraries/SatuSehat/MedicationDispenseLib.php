<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class MedicationDispenseLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function searchBySubject($name)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/MedicationDispense?subject=100000030009",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer v5TLrNv6E6iVpCGAaQGghr6e48Z3'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getById($medicationDispenseId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/MedicationDispense/71e27aa4-89d1-49a0-80ab-20e970a939cc",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($medicationDispenseId, $medicationDispenseData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/MedicationDispense",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "resourceType": "MedicationDispense",
        "identifier": [
            {
                "system": "http://sys-ids.kemkes.go.id/prescription/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "use": "official",
                "value": "123456788"
            },
            {
                "system": "http://sys-ids.kemkes.go.id/prescription-item/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "use": "official",
                "value": "123456788-1"
            }
        ],
        "status": "completed",
        "category": {
            "coding": [
                {
                    "system": "http://terminology.hl7.org/fhir/CodeSystem/medicationdispense-category",
                    "code": "outpatient",
                    "display": "Outpatient"
                }
            ]
        },
        "medicationReference": {
            "reference": "Medication/2b78a453-dd36-4d5f-8264-d575e3321a8b",
            "display": "Obat Anti Tuberculosis / Rifampicin 150 mg / Isoniazid 75 mg / Pyrazinamide 400 mg / Ethambutol 275 mg Kaplet Salut Selaput (KIMIA FARMA)"
        },
        "subject": {
            "reference": "Patient/100000030009",
            "display": "Budi Santoso"
        },
        "context": {
            "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f"
        },
        "performer": [
            {
                "actor": {
                    "reference": "Practitioner/N10000003",
                    "display": "John Miller"
                }
            }
        ],
        "location": {
            "reference": "Location/52e135eb-1956-4871-ba13-e833e662484d",
            "display": "Apotek RSUD Jati Asih"
        },
        "authorizingPrescription": [
            {
                "reference": "MedicationRequest/b5293e6d-31c6-4111-8214-609ae5890838"
            }
        ],
        "quantity": {
            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
            "code": "TAB",
            "value": 120
        },
        "daysSupply": {
            "value": 30,
            "unit": "Day",
            "system": "http://unitsofmeasure.org",
            "code": "d"
        },
        "whenPrepared": "2022-01-15T10:20:00Z",
        "whenHandedOver": "2022-01-15T16:20:00Z",
        "dosageInstruction": [
            {
                "sequence": 1,
                "text": "Diminum 4 tablet sekali dalam sehari",
                "timing": {
                    "repeat": {
                        "frequency": 1,
                        "period": 1,
                        "periodUnit": "d"
                    }
                },
                "doseAndRate": [
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/dose-rate-type",
                                    "code": "ordered",
                                    "display": "Ordered"
                                }
                            ]
                        },
                        "doseQuantity": {
                            "value": 4,
                            "unit": "TAB",
                            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                            "code": "TAB"
                        }
                    }
                ]
            }
        ]
        }
        ',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer v5TLrNv6E6iVpCGAaQGghr6e48Z3'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($medicationDispenseId, $medicationDispenseData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/MedicationDispense/71e27aa4-89d1-49a0-80ab-20e970a939cc",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => '{
        "resourceType": "MedicationDispense",
        "id": "71e27aa4-89d1-49a0-80ab-20e970a939cc",
        "identifier": [
            {
                "system": "http://sys-ids.kemkes.go.id/prescription/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "use": "official",
                "value": "123456788"
            },
            {
                "system": "http://sys-ids.kemkes.go.id/prescription-item/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "use": "official",
                "value": "123456788-1"
            }
        ],
        "status": "in-progress",
        "category": {
            "coding": [
                {
                    "system": "http://terminology.hl7.org/fhir/CodeSystem/medicationdispense-category",
                    "code": "outpatient",
                    "display": "Outpatient"
                }
            ]
        },
        "medicationReference": {
            "reference": "Medication/2b78a453-dd36-4d5f-8264-d575e3321a8b",
            "display": "Obat Anti Tuberculosis / Rifampicin 150 mg / Isoniazid 75 mg / Pyrazinamide 400 mg / Ethambutol 275 mg Kaplet Salut Selaput (KIMIA FARMA)"
        },
        "subject": {
            "reference": "Patient/100000030009",
            "display": "Budi Santoso"
        },
        "context": {
            "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f"
        },
        "performer": [
            {
                "actor": {
                    "reference": "Practitioner/N10000003",
                    "display": "John Miller"
                }
            }
        ],
        "location": {
            "reference": "Location/52e135eb-1956-4871-ba13-e833e662484d",
            "display": "Apotek RSUD Jati Asih"
        },
        "authorizingPrescription": [
            {
                "reference": "MedicationRequest/b5293e6d-31c6-4111-8214-609ae5890838"
            }
        ],
        "quantity": {
            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
            "code": "TAB",
            "value": 120
        },
        "daysSupply": {
            "value": 30,
            "unit": "Day",
            "system": "http://unitsofmeasure.org",
            "code": "d"
        },
        "whenPrepared": "2022-01-15T10:20:00Z",
        "whenHandedOver": "2022-01-15T16:20:00Z",
        "dosageInstruction": [
            {
                "sequence": 1,
                "text": "Diminum 4 tablet sekali dalam sehari",
                "timing": {
                    "repeat": {
                        "frequency": 1,
                        "period": 1,
                        "periodUnit": "d"
                    }
                },
                "doseAndRate": [
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/dose-rate-type",
                                    "code": "ordered",
                                    "display": "Ordered"
                                }
                            ]
                        },
                        "doseQuantity": {
                            "value": 4,
                            "unit": "TAB",
                            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                            "code": "TAB"
                        }
                    }
                ]
            }
        ]
        }
        ',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
