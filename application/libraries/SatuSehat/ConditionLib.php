<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class ConditionLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function searchByEncounter($encounterId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Condition?encounter=$encounterId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getById($conditionId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Condition/$conditionId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function diagnosis($conditionData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        // FormData
        $encounterId = $conditionData['encounter_id'];
        $categoryCode = $conditionData['category_code'];
        $categoryName = $conditionData['category_name'];
        $conditionCode = $conditionData['condition_code'];
        $conditionName = $conditionData['condition_name'];
        $patientId = $conditionData['patient_id'];
        $patientName = $conditionData['patient_name'];
        $encounterDisplay = $conditionData['encounter_display'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Condition",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "resourceType": "Condition",
        "clinicalStatus": {
            "coding": [
                {
                    "system": "http://terminology.hl7.org/CodeSystem/condition-clinical",
                    "code": "active",
                    "display": "Active"
                }
            ]
        },
        "category": [
            {
                "coding": [
                    {
                    "system": "http://terminology.hl7.org/CodeSystem/condition-category",
                    "code": "'.$categoryCode.'",
                    "display": "'.$categoryName.'"
                    }
                ]
            }
        ],
        "code": {
            "coding": [
                {
                    "system": "http://hl7.org/fhir/sid/icd-10",
                    "code": "'.$conditionCode.'",
                    "display": "'.$conditionName.'"
                }
            ]
        },
        "subject": {
            "reference": "Patient/'.$patientId.'",
            "display": "'.$patientName.'"
        },
        "encounter": {
            "reference": "Encounter/'.$encounterId.'",
            "display": "'.$encounterDisplay.'"
        }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function meninggalkanFaskes($conditionData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        // FormData
        $encounterId = $conditionData['encounter_id'];
        $categoryCode = $conditionData['category_code'];
        $categoryName = $conditionData['category_name'];
        $conditionCode = $conditionData['condition_code'];
        $conditionName = $conditionData['condition_name'];
        $patientId = $conditionData['patient_id'];
        $patientName = $conditionData['patient_name'];
        $encounterDisplay = $conditionData['encounter_display'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Condition",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "Condition",
            "clinicalStatus": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/condition-clinical",
                        "code": "active",
                        "display": "Active"
                    }
                ]
            },
            "category": [
                {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/condition-category",
                            "code": "'.$categoryCode.'",
                            "display": "'.$categoryName.'"
                        }
                    ]
                }
            ],
            "code": {
                "coding": [
                    {
                        "system": "http://snomed.info/sct",
                        "code": "359746009",
                        "display": "Patient\'s condition stable"
                    }
                ]
            },
            "subject": {
                "reference": "Patient/'.$patientId.'",
                "display": "'.$patientName.'"
            },
            "encounter": {
                "reference": "Encounter/'.$encounterId.'",
                "display": "'.$encounterDisplay.'"
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($conditionId, $conditionData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        // FormData
        $encounterId = $conditionData['encounter_id'];
        $categoryCode = $conditionData['category_code'];
        $categoryName = $conditionData['category_name'];
        $conditionCode = $conditionData['condition_code'];
        $conditionName = $conditionData['condition_name'];
        $patientId = $conditionData['patient_id'];
        $patientName = $conditionData['patient_name'];
        $encounterDisplay = $conditionData['encounter_display'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Condition/f1369adf-26f6-47a5-90f2-ce08442639aa",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "Condition",
            "id": "'.$conditionId.'",
            "clinicalStatus": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/condition-clinical",
                        "code": "remission",
                        "display": "Remission"
                    }
                ]
            },
            "category": [
                {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/condition-category",
                            "code": "'.$categoryCode.'",
                            "display": "'.$categoryName.'"
                        }
                    ]
                }
            ],
            "code": {
                "coding": [
                    {
                        "system": "http://hl7.org/fhir/sid/icd-10",
                        "code": "'.$conditionCode.'",
                        "display": "'.$conditionName.'"
                    }
                ]
            },
            "subject": {
                "reference": "Patient/'.$patientId.'",
                "display": "'.$patientName.'"
            },
            "encounter": {
                "reference": "Encounter/'.$encounterId.'",
                "display": "'.$encounterDisplay.'"
            }
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
