<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class MedicationLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function getById($medicationId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Medication/8f299a19-5887-4b8e-90a2-c2c15ecbe1d1",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($medicationId, $medicationData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Medication",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
        "resourceType": "Medication",
        "meta": {
            "profile": [
                "https://fhir.kemkes.go.id/r4/StructureDefinition/Medication"
            ]
        },
        "identifier": [
            {
                "system": "http://sys-ids.kemkes.go.id/medication/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "use": "official",
                "value": "123456789"
            }
        ],
        "code": {
            "coding": [
                {
                    "system": "http://sys-ids.kemkes.go.id/kfa",
                    "code": "93001019",
                    "display": "Obat Anti Tuberculosis / Rifampicin 150 mg / Isoniazid 75 mg / Pyrazinamide 400 mg / Ethambutol 275 mg Kaplet Salut Selaput (KIMIA FARMA)"
                }
            ]
        },
        "status": "active",
        "manufacturer": {
            "reference": "Organization/900001"
        },
        "form": {
            "coding": [
                {
                    "system": "http://terminology.kemkes.go.id/CodeSystem/medication-form",
                    "code": "BS023",
                    "display": "Kaplet Salut Selaput"
                }
            ]
        },
        "ingredient": [
            {
                "itemCodeableConcept": {
                    "coding": [
                        {
                            "system": "http://sys-ids.kemkes.go.id/kfa",
                            "code": "91000330",
                            "display": "Rifampin"
                        }
                    ]
                },
                "isActive": true,
                "strength": {
                    "numerator": {
                        "value": 150,
                        "system": "http://unitsofmeasure.org",
                        "code": "mg"
                    },
                    "denominator": {
                        "value": 1,
                        "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                        "code": "TAB"
                    }
                }
            },
            {
                "itemCodeableConcept": {
                    "coding": [
                        {
                            "system": "http://sys-ids.kemkes.go.id/kfa",
                            "code": "91000328",
                            "display": "Isoniazid"
                        }
                    ]
                },
                "isActive": true,
                "strength": {
                    "numerator": {
                        "value": 75,
                        "system": "http://unitsofmeasure.org",
                        "code": "mg"
                    },
                    "denominator": {
                        "value": 1,
                        "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                        "code": "TAB"
                    }
                }
            },
            {
                "itemCodeableConcept": {
                    "coding": [
                        {
                            "system": "http://sys-ids.kemkes.go.id/kfa",
                            "code": "91000329",
                            "display": "Pyrazinamide"
                        }
                    ]
                },
                "isActive": true,
                "strength": {
                    "numerator": {
                        "value": 400,
                        "system": "http://unitsofmeasure.org",
                        "code": "mg"
                    },
                    "denominator": {
                        "value": 1,
                        "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                        "code": "TAB"
                    }
                }
            },
            {
                "itemCodeableConcept": {
                    "coding": [
                        {
                            "system": "http://sys-ids.kemkes.go.id/kfa",
                            "code": "91000288",
                            "display": "Ethambutol"
                        }
                    ]
                },
                "isActive": true,
                "strength": {
                    "numerator": {
                        "value": 275,
                        "system": "http://unitsofmeasure.org",
                        "code": "mg"
                    },
                    "denominator": {
                        "value": 1,
                        "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                        "code": "TAB"
                    }
                }
            }
        ],
        "extension": [
            {
                "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/MedicationType",
                "valueCodeableConcept": {
                    "coding": [
                        {
                            "system": "http://terminology.kemkes.go.id/CodeSystem/medication-type",
                            "code": "NC",
                            "display": "Non-compound"
                        }
                    ]
                }
            }
        ]
        }',
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($medicationId, $medicationData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Medication/8f299a19-5887-4b8e-90a2-c2c15ecbe1d1",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "Medication",
            "id": "8f299a19-5887-4b8e-90a2-c2c15ecbe1d1",
            "meta": {
                "profile": [
                    "https://fhir.kemkes.go.id/r4/StructureDefinition/Medication"
                ]
            },
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/medication/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                    "use": "official",
                    "value": "123456789"
                }
            ],
            "code": {
                "coding": [
                    {
                        "system": "http://sys-ids.kemkes.go.id/kfa",
                        "display": "Obat Kapsul TB Bintang Toedjoe"
                    }
                ]
            },
            "status": "active",
            "manufacturer": {
                "reference": "Organization/900001"
            },
            "form": {
                "coding": [
                    {
                        "system": "http://terminology.kemkes.go.id/CodeSystem/medication-form",
                        "code": "BS019",
                        "display": "Kapsul"
                    }
                ]
            },
            "ingredient": [
                {
                    "itemCodeableConcept": {
                        "coding": [
                            {
                                "system": "http://sys-ids.kemkes.go.id/kfa",
                                "code": "91000330",
                                "display": "Rifampin"
                            }
                        ]
                    },
                    "isActive": true,
                    "strength": {
                        "numerator": {
                            "value": 150,
                            "system": "http://unitsofmeasure.org",
                            "code": "mg"
                        },
                        "denominator": {
                            "value": 1,
                            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                            "code": "TAB"
                        }
                    }
                },
                {
                    "itemCodeableConcept": {
                        "coding": [
                            {
                                "system": "http://sys-ids.kemkes.go.id/kfa",
                                "code": "91000328",
                                "display": "Isoniazid"
                            }
                        ]
                    },
                    "isActive": true,
                    "strength": {
                        "numerator": {
                            "value": 75,
                            "system": "http://unitsofmeasure.org",
                            "code": "mg"
                        },
                        "denominator": {
                            "value": 1,
                            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                            "code": "TAB"
                        }
                    }
                },
                {
                    "itemCodeableConcept": {
                        "coding": [
                            {
                                "system": "http://sys-ids.kemkes.go.id/kfa",
                                "code": "91000329",
                                "display": "Pyrazinamide"
                            }
                        ]
                    },
                    "isActive": true,
                    "strength": {
                        "numerator": {
                            "value": 400,
                            "system": "http://unitsofmeasure.org",
                            "code": "mg"
                        },
                        "denominator": {
                            "value": 1,
                            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                            "code": "TAB"
                        }
                    }
                },
                {
                    "itemCodeableConcept": {
                        "coding": [
                            {
                                "system": "http://sys-ids.kemkes.go.id/kfa",
                                "code": "91000288",
                                "display": "Ethambutol"
                            }
                        ]
                    },
                    "isActive": true,
                    "strength": {
                        "numerator": {
                            "value": 275,
                            "system": "http://unitsofmeasure.org",
                            "code": "mg"
                        },
                        "denominator": {
                            "value": 1,
                            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                            "code": "TAB"
                        }
                    }
                }
            ],
            "extension": [
                {
                    "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/MedicationType",
                    "valueCodeableConcept": {
                        "coding": [
                            {
                                "system": "http://terminology.kemkes.go.id/CodeSystem/medication-type",
                                "code": "NC",
                                "display": "Non-compound"
                            }
                        ]
                    }
                }
            ]
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
