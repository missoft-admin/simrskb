<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class MedicationRequestLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function searchByEncounter($name)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/MedicationRequest?encounter=b0f26937-2340-43ca-a9ae-367d0160b47f",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getById($medicationRequestId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/MedicationRequest/b5293e6d-31c6-4111-8214-609ae5890838",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($medicationRequestId, $medicationRequestData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/MedicationRequest",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
        "resourceType": "MedicationRequest",
        "identifier": [
            {
                "system": "http://sys-ids.kemkes.go.id/prescription/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "use": "official",
                "value": "123456788"
            },
            {
                "system": "http://sys-ids.kemkes.go.id/prescription-item/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "use": "official",
                "value": "123456788-1"
            }
        ],
        "status": "completed",
        "intent": "order",
        "category": [
            {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/medicationrequest-category",
                        "code": "outpatient",
                        "display": "Outpatient"
                    }
                ]
            }
        ],
        "priority": "routine",
        "medicationReference": {
            "reference": "Medication/8f299a19-5887-4b8e-90a2-c2c15ecbe1d1",
            "display": "Obat Anti Tuberculosis / Rifampicin 150 mg / Isoniazid 75 mg / Pyrazinamide 400 mg / Ethambutol 275 mg Kaplet Salut Selaput (KIMIA FARMA)"
        },
        "subject": {
            "reference": "Patient/100000030009",
            "display": "Budi Santoso"
        },
        "encounter": {
            "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f"
        },
        "authoredOn": "2022-08-04",
        "requester": {
            "reference": "Practitioner/N10000001",
            "display": "Dokter Bronsig"
        },
        "reasonCode": [
            {
                "coding": [
                    {
                        "system": "http://hl7.org/fhir/sid/icd-10",
                        "code": "A15.0",
                        "display": "Tuberculosis of lung, confirmed by sputum microscopy with or without culture"
                    }
                ]
            }
        ],
        "courseOfTherapyType": {
            "coding": [
                {
                    "system": "http://terminology.hl7.org/CodeSystem/medicationrequest-course-of-therapy",
                    "code": "continuous",
                    "display": "Continuing long term therapy"
                }
            ]
        },
        "dosageInstruction": [
            {
                "sequence": 1,
                "text": "4 tablet per hari",
                "additionalInstruction": [
                    {
                        "text": "Diminum setiap hari"
                    }
                ],
                "patientInstruction": "4 tablet perhari, diminum setiap hari tanpa jeda sampai prose pengobatan berakhir",
                "timing": {
                    "repeat": {
                        "frequency": 1,
                        "period": 1,
                        "periodUnit": "d"
                    }
                },
                "route": {
                    "coding": [
                        {
                            "system": "http://www.whocc.no/atc",
                            "code": "O",
                            "display": "Oral"
                        }
                    ]
                },
                "doseAndRate": [
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/dose-rate-type",
                                    "code": "ordered",
                                    "display": "Ordered"
                                }
                            ]
                        },
                        "doseQuantity": {
                            "value": 4,
                            "unit": "TAB",
                            "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                            "code": "TAB"
                        }
                    }
                ]
            }
        ],
        "dispenseRequest": {
            "dispenseInterval": {
                "value": 1,
                "unit": "days",
                "system": "http://unitsofmeasure.org",
                "code": "d"
            },
            "validityPeriod": {
                "start": "2022-01-01",
                "end": "2022-01-30"
            },
            "numberOfRepeatsAllowed": 0,
            "quantity": {
                "value": 120,
                "unit": "TAB",
                "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                "code": "TAB"
            },
            "expectedSupplyDuration": {
                "value": 30,
                "unit": "days",
                "system": "http://unitsofmeasure.org",
                "code": "d"
            },
            "performer": {
                "reference": "Organization/ed617f64-c335-458b-bcb3-7a8cb5ae6655"
            }
        }
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($medicationRequestId, $medicationRequestData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/MedicationRequest/b5293e6d-31c6-4111-8214-609ae5890838",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "MedicationRequest",
            "id": "b5293e6d-31c6-4111-8214-609ae5890838",
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/prescription/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                    "use": "official",
                    "value": "123456788"
                },
                {
                    "system": "http://sys-ids.kemkes.go.id/prescription-item/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                    "use": "official",
                    "value": "123456788-1"
                }
            ],
            "status": "cancelled",
            "intent": "order",
            "category": [
                {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/medicationrequest-category",
                            "code": "outpatient",
                            "display": "Outpatient"
                        }
                    ]
                }
            ],
            "priority": "routine",
            "medicationReference": {
                "reference": "Medication/8f299a19-5887-4b8e-90a2-c2c15ecbe1d1",
                "display": "Obat Anti Tuberculosis / Rifampicin 150 mg / Isoniazid 75 mg / Pyrazinamide 400 mg / Ethambutol 275 mg Kaplet Salut Selaput (KIMIA FARMA)"
            },
            "subject": {
                "reference": "Patient/100000030009",
                "display": "Budi Santoso"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f"
            },
            "authoredOn": "2022-08-04",
            "requester": {
                "reference": "Practitioner/N10000001",
                "display": "Dokter Bronsig"
            },
            "reasonCode": [
                {
                    "coding": [
                        {
                            "system": "http://hl7.org/fhir/sid/icd-10",
                            "code": "A15.0",
                            "display": "Tuberculosis of lung, confirmed by sputum microscopy with or without culture"
                        }
                    ]
                }
            ],
            "courseOfTherapyType": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/medicationrequest-course-of-therapy",
                        "code": "continuous",
                        "display": "Continuing long term therapy"
                    }
                ]
            },
            "dosageInstruction": [
                {
                    "sequence": 1,
                    "text": "4 tablet per hari",
                    "additionalInstruction": [
                        {
                            "text": "Diminum setiap hari"
                        }
                    ],
                    "patientInstruction": "4 tablet perhari, diminum setiap hari tanpa jeda sampai prose pengobatan berakhir",
                    "timing": {
                        "repeat": {
                            "frequency": 1,
                            "period": 1,
                            "periodUnit": "d"
                        }
                    },
                    "route": {
                        "coding": [
                            {
                                "system": "http://www.whocc.no/atc",
                                "code": "O",
                                "display": "Oral"
                            }
                        ]
                    },
                    "doseAndRate": [
                        {
                            "type": {
                                "coding": [
                                    {
                                        "system": "http://terminology.hl7.org/CodeSystem/dose-rate-type",
                                        "code": "ordered",
                                        "display": "Ordered"
                                    }
                                ]
                            },
                            "doseQuantity": {
                                "value": 4,
                                "unit": "TAB",
                                "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                                "code": "TAB"
                            }
                        }
                    ]
                }
            ],
            "dispenseRequest": {
                "dispenseInterval": {
                    "value": 1,
                    "unit": "days",
                    "system": "http://unitsofmeasure.org",
                    "code": "d"
                },
                "validityPeriod": {
                    "start": "2022-01-01",
                    "end": "2022-01-30"
                },
                "numberOfRepeatsAllowed": 0,
                "quantity": {
                    "value": 120,
                    "unit": "TAB",
                    "system": "http://terminology.hl7.org/CodeSystem/v3-orderableDrugForm",
                    "code": "TAB"
                },
                "expectedSupplyDuration": {
                    "value": 30,
                    "unit": "days",
                    "system": "http://unitsofmeasure.org",
                    "code": "d"
                },
                "performer": {
                    "reference": "Organization/ed617f64-c335-458b-bcb3-7a8cb5ae6655"
                }
            }
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
