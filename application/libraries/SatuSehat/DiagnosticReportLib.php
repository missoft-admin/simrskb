<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class DiagnosticReportLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function searchByEncounter($name)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/DiagnosticReport?encounter=b0f26937-2340-43ca-a9ae-367d0160b47f",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getById($diagnosticReportId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/DiagnosticReport/ec63dc9a-738d-4f7b-8a4d-86ca9e621ef6",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($diagnosticReportId, $diagnosticReportData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/DiagnosticReport",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "DiagnosticReport",
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/diagnostic/ed617f64-c335-458b-bcb3-7a8cb5ae6655/lab",
                    "use": "official",
                    "value": "5234342"
                }
            ],
            "status": "final",
            "category": [
                {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/v2-0074",
                            "code": "MB",
                            "display": "Microbiology"
                        }
                    ]
                }
            ],
            "code": {
                "coding": [
                    {
                        "system": "http://loinc.org",
                        "code": "11477-7",
                        "display": "Microscopic observation [Identifier] in Sputum by Acid fast stain"
                    }
                ]
            },
            "subject": {
                "reference": "Patient/100000030009"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f"
            },
            "effectiveDateTime": "2012-12-01T12:00:00+01:00",
            "issued": "2012-12-01T12:00:00+01:00",
            "performer": [
                {
                    "reference": "Practitioner/N10000001"
                },
                {
                    "reference": "Organization/ed617f64-c335-458b-bcb3-7a8cb5ae6655"
                }
            ],
            "result": [
                {
                    "reference": "Observation/dc0b1b9c-d2c8-4830-b8bb-d73c68174f02"
                }
            ],
            "specimen": [
                {
                    "reference": "Specimen/3095e36e-1624-487e-9ee4-737387e7b55f"
                }
            ],
            "conclusionCode": [
                {
                    "coding": [
                        {
                            "system": "http://snomed.info/sct",
                            "code": "260347006",
                            "display": "+"
                        }
                    ]
                }
            ]
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($diagnosticReportId, $diagnosticReportData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/DiagnosticReport/ec63dc9a-738d-4f7b-8a4d-86ca9e621ef6",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "DiagnosticReport",
            "id": "ec63dc9a-738d-4f7b-8a4d-86ca9e621ef6",
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/diagnostic/ed617f64-c335-458b-bcb3-7a8cb5ae6655/lab",
                    "use": "official",
                    "value": "5234342"
                }
            ],
            "status": "final",
            "category": [
                {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/v2-0074",
                            "code": "MB",
                            "display": "Microbiology"
                        }
                    ]
                }
            ],
            "code": {
                "coding": [
                    {
                        "system": "http://loinc.org",
                        "code": "11477-7",
                        "display": "Microscopic observation [Identifier] in Sputum by Acid fast stain"
                    }
                ]
            },
            "subject": {
                "reference": "Patient/100000030009"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f"
            },
            "effectiveDateTime": "2012-12-01T12:00:00+01:00",
            "issued": "2012-12-01T12:00:00+01:00",
            "performer": [
                {
                    "reference": "Practitioner/N10000001"
                },
                {
                    "reference": "Organization/ed617f64-c335-458b-bcb3-7a8cb5ae6655"
                }
            ],
            "result": [
                {
                    "reference": "Observation/dc0b1b9c-d2c8-4830-b8bb-d73c68174f02"
                }
            ],
            "specimen": [
                {
                    "reference": "Specimen/3095e36e-1624-487e-9ee4-737387e7b55f"
                }
            ],
            "conclusionCode": [
                {
                    "coding": [
                        {
                            "system": "http://snomed.info/sct",
                            "code": "2667000",
                            "display": "Absent"
                        }
                    ]
                }
            ]
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
