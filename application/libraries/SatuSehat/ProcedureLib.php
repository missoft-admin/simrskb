<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class ProcedureLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function searchByEncounter($name)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Procedure?encounter=b0f26937-2340-43ca-a9ae-367d0160b47f",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getById($procedureId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Procedure/87859868-c35b-4f7b-86dd-da9830ae58c5",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($procedureId, $procedureData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Procedure",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "Procedure",
            "status": "completed",
            "category": {
                "coding": [
                    {
                        "system": "http://snomed.info/sct",
                        "code": "103693007",
                        "display": "Diagnostic procedure"
                    }
                ],
                "text": "Diagnostic procedure"
            },
            "code": {
                "coding": [
                    {
                        "system": "http://hl7.org/fhir/sid/icd-9-cm",
                        "code": "87.44",
                        "display": "Routine chest x-ray, so described"
                    }
                ]
            },
            "subject": {
                "reference": "Patient/100000030009",
                "display": "Budi Santoso"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f",
                "display": "Tindakan Rontgen Dada Budi Santoso pada Selasa tanggal 14 Juni 2022"
            },
            "performedPeriod": {
                "start": "2022-06-14T13:31:00+01:00",
                "end": "2022-06-14T14:27:00+01:00"
            },
            "performer": [
                {
                    "actor": {
                        "reference": "Practitioner/N10000001",
                        "display": "Dokter Bronsig"
                    }
                }
            ],
            "reasonCode": [
                {
                    "coding": [
                        {
                            "system": "http://hl7.org/fhir/sid/icd-10",
                            "code": "A15.0",
                            "display": "Tuberculosis of lung, confirmed by sputum microscopy with or without culture"
                        }
                    ]
                }
            ],
            "bodySite": [
                {
                    "coding": [
                        {
                            "system": "http://snomed.info/sct",
                            "code": "302551006",
                            "display": "Entire Thorax"
                        }
                    ]
                }
            ],
            "note": [
                {
                    "text": "Rontgen thorax melihat perluasan infiltrat dan kavitas."
                }
            ]
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($procedureId, $procedureData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Procedure/87859868-c35b-4f7b-86dd-da9830ae58c5",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "Procedure",
            "id": "87859868-c35b-4f7b-86dd-da9830ae58c5",
            "status": "completed",
            "category": {
                "coding": [
                    {
                        "system": "http://snomed.info/sct",
                        "code": "103693007",
                        "display": "Diagnostic procedure"
                    }
                ],
                "text": "Diagnostic procedure"
            },
            "code": {
                "coding": [
                    {
                        "system": "http://hl7.org/fhir/sid/icd-9-cm",
                        "code": "87.44",
                        "display": "Routine chest x-ray, so described"
                    }
                ]
            },
            "subject": {
                "reference": "Patient/P00030004",
                "display": "Budi Santoso"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f",
                "display": "Tindakan Rontgen Dada Budi Santoso pada Selasa tanggal 14 Juni 2022"
            },
            "performedPeriod": {
                "start": "2022-06-14T13:31:00+01:00",
                "end": "2022-06-14T14:27:00+01:00"
            },
            "performer": [
                {
                    "actor": {
                        "reference": "Practitioner/N10000001",
                        "display": "Dokter Bronsig"
                    }
                }
            ],
            "reasonCode": [
                {
                    "coding": [
                        {
                            "system": "http://hl7.org/fhir/sid/icd-10",
                            "code": "A15.0",
                            "display": "Tuberculosis of lung, confirmed by sputum microscopy with or without culture"
                        }
                    ]
                }
            ],
            "bodySite": [
                {
                    "coding": [
                        {
                            "system": "http://snomed.info/sct",
                            "code": "302551006",
                            "display": "Entire Thorax"
                        }
                    ]
                }
            ],
            "note": [
                {
                    "text": "Rontgen thorax melihat perluasan infiltrat dan kavitas."
                }
            ]
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
