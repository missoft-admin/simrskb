<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class ConfigLib
{
    protected $_CI;
    protected $_config;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->_CI->config->load('satusehat');
        if ($this->_CI->config->load('satusehat', TRUE, TRUE))
		{
            $this->_config = $this->_CI->config->config;
		}
        $this->loadConfig();
    }

    public function getConfig()
    {
        return $this->_config;
    }

    protected function loadConfig(): void
    {
        $mode = $this->_config['satusehat_mode'];
        if ('development' === $mode) {
            $this->_CI->config->load('satusehat_development');
        } elseif ('production' === $mode) {
            $this->_CI->config->load('satusehat_production');
        }

        $this->_config = $this->_CI->config->config;
    }
}
