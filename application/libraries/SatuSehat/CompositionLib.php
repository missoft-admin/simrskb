<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class CompositionLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function searchByEncounter($name)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Composition?encounter=b0f26937-2340-43ca-a9ae-367d0160b47f",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer v5TLrNv6E6iVpCGAaQGghr6e48Z3'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getById($compositionId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Composition/e511fb00-3641-4816-a9fd-db2a55d1897d",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function edukasiDiet($compositionId, $compositionData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Composition",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "Composition",
            "identifier": {
                "system": "http://sys-ids.kemkes.go.id/composition/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "value": "P20240001"
            },
            "status": "final",
            "type": {
                "coding": [
                    {
                        "system": "http://loinc.org",
                        "code": "18842-5",
                        "display": "Discharge summary"
                    }
                ]
            },
            "category": [
                {
                    "coding": [
                        {
                            "system": "http://loinc.org",
                            "code": "LP173421-1",
                            "display": "Report"
                        }
                    ]
                }
            ],
            "subject": {
                "reference": "Patient/100000030009",
                "display": "Budi Santoso"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f",
                "display": "Kunjungan Budi Santoso di hari Selasa, 14 Juni 2022"
            },
            "date": "2022-06-14",
            "author": [
                {
                    "reference": "Practitioner/N10000001",
                    "display": "Dokter Bronsig"
                }
            ],
            "title": "Resume Medis Rawat Jalan",
            "custodian": {
                "reference": "Organization/ed617f64-c335-458b-bcb3-7a8cb5ae6655"
            },
            "section": [
                {
                    "code": {
                        "coding": [
                            {
                                "system": "http://loinc.org",
                                "code": "42344-2",
                                "display": "Discharge diet (narrative)"
                            }
                        ]
                    },
                    "text": {
                        "status": "additional",
                        "div": "Rekomendasi diet rendah lemak, rendah kalori"
                    }
                }
            ]
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($compositionId, $compositionData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Composition/e511fb00-3641-4816-a9fd-db2a55d1897d",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "Composition",
            "id": "e511fb00-3641-4816-a9fd-db2a55d1897d",
            "identifier": {
                "system": "http://sys-ids.kemkes.go.id/composition/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                "value": "P20240001"
            },
            "status": "final",
            "type": {
                "coding": [
                    {
                        "system": "http://loinc.org",
                        "code": "18842-5",
                        "display": "Discharge summary"
                    }
                ]
            },
            "category": [
                {
                    "coding": [
                        {
                            "system": "http://loinc.org",
                            "code": "LP173421-1",
                            "display": "Report"
                        }
                    ]
                }
            ],
            "subject": {
                "reference": "Patient/100000030009",
                "display": "Budi Santoso"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f",
                "display": "Kunjungan Budi Santoso di hari Selasa, 14 Juni 2022"
            },
            "date": "2022-06-14",
            "author": [
                {
                    "reference": "Practitioner/N10000001",
                    "display": "Dokter Bronsig"
                }
            ],
            "title": "Resume Medis Rawat Jalan",
            "custodian": {
                "reference": "Organization/ed617f64-c335-458b-bcb3-7a8cb5ae6655"
            },
            "section": [
                {
                    "code": {
                        "coding": [
                            {
                                "system": "http://loinc.org",
                                "code": "42344-2",
                                "display": "Discharge diet (narrative)"
                            }
                        ]
                    },
                    "text": {
                        "status": "additional",
                        "div": "Rekomendasi diet rendah karbohidrat"
                    }
                }
            ]
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
