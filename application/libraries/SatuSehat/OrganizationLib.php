<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class OrganizationLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function searchByName($name)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl."/Organization?name={$name}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$accessToken,
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function searchByPartOf()
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];
        $organizationId = $this->_config['satusehat_organization_id'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl."/Organization?partof={$organizationId}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$accessToken,
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getData()
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];
        $organizationId = $this->_config['satusehat_organization_id'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl."/Organization/{$organizationId}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$accessToken,
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($organizationData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];
        $organizationId = $this->_config['satusehat_organization_id'];

        // FormData
        $organizationName = $organizationData['organization_name'];
        $resourceType = $organizationData['resource_type'];
        $typeCode = $organizationData['type_code'];
        $typeName = $organizationData['type_name'];
        $telecomPhone = $organizationData['telecom_phone'];
        $telecomEmail = $organizationData['telecom_email'];
        $telecomUrl = $organizationData['telecom_url'];
        $addressLine = $organizationData['address_line'];
        $addressCity = $organizationData['address_city'];
        $addressPostalCode = $organizationData['address_postal_code'];
        $addressCountry = $organizationData['address_country'];
        $addressProvinceId = $organizationData['address_province_id'];
        $addressCityId = $organizationData['address_city_id'];
        $addressDistrictId = $organizationData['address_district_id'];
        $addressVillageId = $organizationData['address_village_id'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Organization",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "Organization",
            "active": true,
            "identifier": [
                {
                    "use": "official",
                    "system": "http://sys-ids.kemkes.go.id/organization/1000079374",
                    "value": "Pos Imunisasi LUBUK BATANG"
                }
            ],
            "type": [
                {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/organization-type",
                            "code": "'.$typeCode.'",
                            "display": "'.$typeName.'"
                        }
                    ]
                }
            ],
            "name": "'.$organizationName.'",
            "telecom": [
                {
                    "system": "phone",
                    "value": "'.$telecomPhone.'",
                    "use": "work"
                },
                {
                    "system": "email",
                    "value": "'.$telecomEmail.'",
                    "use": "work"
                },
                {
                    "system": "url",
                    "value": "'.$telecomUrl.'",
                    "use": "work"
                }
            ],
            "address": [
                {
                    "use": "work",
                    "type": "both",
                    "line": [
                        "'.$addressLine.'"
                    ],
                    "city": "'.$addressCity.'",
                    "postalCode": "'.$addressPostalCode.'",
                    "country": "'.$addressCountry.'",
                    "extension": [
                        {
                            "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                            "extension": [
                                {
                                    "url": "province",
                                    "valueCode": "'.$addressProvinceId.'"
                                },
                                {
                                    "url": "city",
                                    "valueCode": "'.$addressCityId.'"
                                },
                                {
                                    "url": "district",
                                    "valueCode": "'.$addressDistrictId.'"
                                },
                                {
                                    "url": "village",
                                    "valueCode": "'.$addressVillageId.'"
                                }
                            ]
                        }
                    ]
                }
            ],
            "partOf": {
                "reference": "Organization/'.$organizationId.'"
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($organizationData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];
        $organizationId = $this->_config['satusehat_organization_id'];

        // FormData
        $organizationName = $organizationData['organization_name'];
        $typeCode = $organizationData['type_code'];
        $typeName = $organizationData['type_name'];
        $telecomPhone = $organizationData['telecom_phone'];
        $telecomEmail = $organizationData['telecom_email'];
        $telecomUrl = $organizationData['telecom_url'];
        $addressLine = $organizationData['address_line'];
        $addressCity = $organizationData['address_city'];
        $addressPostalCode = $organizationData['address_postal_code'];
        $addressCountry = $organizationData['address_country'];
        $addressProvinceId = $organizationData['address_province_id'];
        $addressCityId = $organizationData['address_city_id'];
        $addressDistrictId = $organizationData['address_district_id'];
        $addressVillageId = $organizationData['address_village_id'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Organization/$organizationId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => '{
            "resourceType": "Organization",
            "id":"'.$organizationId.'",
            "active": true,
            "identifier": [
                {
                    "use": "official",
                    "system": "http://sys-ids.kemkes.go.id/organization/'.$organizationId.'",
                    "value": "R220001"
                }
            ],
            "type": [
                {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/organization-type",
                            "code": "'.$typeCode.'",
                            "display": "'.$typeName.'"
                        }
                    ]
                }
            ],
            "name": "'.$organizationName.'",
            "telecom": [
                {
                    "system": "phone",
                    "value": "'.$telecomPhone.'",
                    "use": "work"
                },
                {
                    "system": "email",
                    "value": "'.$telecomEmail.'",
                    "use": "work"
                },
                {
                    "system": "url",
                    "value": "'.$telecomUrl.'",
                    "use": "work"
                }
            ],
            "address": [
                {
                    "use": "work",
                    "type": "both",
                    "line": [
                        "'.$addressLine.'"
                    ],
                    "city": "'.$addressCity.'",
                    "postalCode": "'.$addressPostalCode.'",
                    "country": "'.$addressCountry.'",
                    "extension": [
                        {
                            "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                            "extension": [
                                {
                                    "url": "province",
                                    "valueCode": "'.$addressProvinceId.'"
                                },
                                {
                                    "url": "city",
                                    "valueCode": "'.$addressCityId.'"
                                },
                                {
                                    "url": "district",
                                    "valueCode": "'.$addressDistrictId.'"
                                },
                                {
                                    "url": "village",
                                    "valueCode": "'.$addressVillageId.'"
                                }
                            ]
                        }
                    ]
                }
            ],
            "partOf": {
                "reference": "Organization/'.$organizationId.'"
            }
        }',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
