<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class AuthLib
{
    protected $_CI;
    protected $_config;
    protected $_accessToken;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
        $this->fetchAccessToken();
    }

    public function getAccessToken()
    {
        return $this->_accessToken;
    }

    protected function fetchAccessToken(): void
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->_config['satusehat_auth_url'].'/accesstoken?grant_type=client_credentials',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'client_id='.$this->_config['satusehat_client_id'].'&client_secret='.$this->_config['satusehat_client_secret'],
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/x-www-form-urlencoded',
            ],
        ]);

        $response = curl_exec($curl);
        curl_close($curl);
        
        if (false !== $response) {
            $tokenData = json_decode($response, true);
            if (isset($tokenData['access_token'])) {
                $this->_accessToken = $tokenData['access_token'];
            }
        }
    }

    protected function loadDependencies() {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
    }
}
