<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class LocationLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function searchByName($name)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Location?name={$name}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function searchByOrganization()
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];
        $organizationId = $this->_config['satusehat_organization_id'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl."/Location?organization={$organizationId}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$accessToken,
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function getById($locationId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl."/Location/$locationId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$accessToken,
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($locationData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];
        $organizationId = $this->_config['satusehat_organization_id'];

        // FormData
        $locationStatus = $locationData['location_status'];
        $locationIdentifier = $locationData['location_identifier'];
        $locationName = $locationData['location_name'];
        $locationDescription = $locationData['location_description'];
        $telecomPhone = $locationData['telecom_phone'];
        $telecomFax = $locationData['telecom_fax'];
        $telecomEmail = $locationData['telecom_email'];
        $telecomUrl = $locationData['telecom_url'];
        $addressLine = $locationData['address_line'];
        $addressCity = $locationData['address_city'];
        $addressPostalCode = $locationData['address_postal_code'];
        $addressCountry = $locationData['address_country'];
        $addressProvinceId = $locationData['address_province_id'];
        $addressCityId = $locationData['address_city_id'];
        $addressDistrictId = $locationData['address_district_id'];
        $addressVillageId = $locationData['address_village_id'];
        $addressRt = $locationData['address_rt'];
        $addressRw = $locationData['address_rw'];
        $physicalTypeCode = $locationData['physical_type_code'];
        $physicalTypeName = $locationData['physical_type_name'];
        $postionLongitude = $locationData['position_longitude'];
        $postionLatitude = $locationData['position_latitude'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Location",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "Location",
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/location/'.$organizationId.'",
                    "value": "'.$locationIdentifier.'"
                }
            ],
            "status": "'.$locationStatus.'",
            "name": "'.$locationName.'",
            "description": "'.$locationDescription.'",
            "mode": "instance",
            "telecom": [
                {
                    "system": "phone",
                    "value": "'.$telecomPhone.'",
                    "use": "work"
                },
                {
                    "system": "fax",
                    "value": "'.$telecomFax.'",
                    "use": "work"
                },
                {
                    "system": "email",
                    "value": "'.$telecomEmail.'"
                },
                {
                    "system": "url",
                    "value": "'.$telecomUrl.'",
                    "use": "work"
                }
            ],
            "address": {
                "use": "work",
                "line": [
                    "'.$addressLine.'"
                ],
                "city": "'.$addressCity.'",
                "postalCode": "'.$addressPostalCode.'",
                "country": "'.$addressCountry.'",
                "extension": [
                    {
                        "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                        "extension": [
                            {
                                "url": "province",
                                "valueCode": "'.$addressProvinceId.'"
                            },
                            {
                                "url": "city",
                                "valueCode": "'.$addressCityId.'"
                            },
                            {
                                "url": "district",
                                "valueCode": "'.$addressDistrictId.'"
                            },
                            {
                                "url": "village",
                                "valueCode": "'.$addressVillageId.'"
                            },
                            {
                                "url": "rt",
                                "valueCode": "'.$addressRt.'"
                            },
                            {
                                "url": "rw",
                                "valueCode": "'.$addressRw.'"
                            }
                        ]
                    }
                ]
            },
            "physicalType": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/location-physical-type",
                        "code": "'.$physicalTypeCode.'",
                        "display": "'.$physicalTypeName.'"
                    }
                ]
            },
            "position": {
                "longitude": '.$postionLongitude.',
                "latitude": '.$postionLatitude.',
                "altitude": 0
            },
            "managingOrganization": {
                "reference": "Organization/'.$organizationId.'"
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($locationId, $locationData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];
        $organizationId = $this->_config['satusehat_organization_id'];

        // FormData
        $locationStatus = $locationData['location_status'];
        $locationIdentifier = $locationData['location_identifier'];
        $locationName = $locationData['location_name'];
        $locationDescription = $locationData['location_description'];
        $telecomPhone = $locationData['telecom_phone'];
        $telecomFax = $locationData['telecom_fax'];
        $telecomEmail = $locationData['telecom_email'];
        $telecomUrl = $locationData['telecom_url'];
        $addressLine = $locationData['address_line'];
        $addressCity = $locationData['address_city'];
        $addressPostalCode = $locationData['address_postal_code'];
        $addressCountry = $locationData['address_country'];
        $addressProvinceId = $locationData['address_province_id'];
        $addressCityId = $locationData['address_city_id'];
        $addressDistrictId = $locationData['address_district_id'];
        $addressVillageId = $locationData['address_village_id'];
        $addressRt = $locationData['address_rt'];
        $addressRw = $locationData['address_rw'];
        $physicalTypeCode = $locationData['physical_type_code'];
        $physicalTypeName = $locationData['physical_type_name'];
        $postionLongitude = $locationData['position_longitude'];
        $postionLatitude = $locationData['position_latitude'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/Location/$locationId",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "Location",
            "id": "'.$locationId.'",
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/location/'.$organizationId.'",
                    "value": "'.$locationIdentifier.'"
                }
            ],
            "status": "'.$locationStatus.'",
            "name": "'.$locationName.'",
            "description": "'.$locationDescription.'",
            "mode": "instance",
            "telecom": [
                {
                    "system": "phone",
                    "value": "'.$telecomPhone.'",
                    "use": "work"
                },
                {
                    "system": "fax",
                    "value": "'.$telecomFax.'",
                    "use": "work"
                },
                {
                    "system": "email",
                    "value": "'.$telecomEmail.'"
                },
                {
                    "system": "url",
                    "value": "'.$telecomUrl.'",
                    "use": "work"
                }
            ],
            "address": {
                "use": "work",
                "line": [
                    "'.$addressLine.'"
                ],
                "city": "'.$addressCity.'",
                "postalCode": "'.$addressPostalCode.'",
                "country": "'.$addressCountry.'",
                "extension": [
                    {
                        "url": "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                        "extension": [
                            {
                                "url": "province",
                                "valueCode": "'.$addressProvinceId.'"
                            },
                            {
                                "url": "city",
                                "valueCode": "'.$addressCityId.'"
                            },
                            {
                                "url": "district",
                                "valueCode": "'.$addressDistrictId.'"
                            },
                            {
                                "url": "village",
                                "valueCode": "'.$addressVillageId.'"
                            },
                            {
                                "url": "rt",
                                "valueCode": "'.$addressRt.'"
                            },
                            {
                                "url": "rw",
                                "valueCode": "'.$addressRw.'"
                            }
                        ]
                    }
                ]
            },
            "physicalType": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/location-physical-type",
                        "code": "'.$physicalTypeCode.'",
                        "display": "'.$physicalTypeName.'"
                    }
                ]
            },
            "position": {
                "longitude": '.$postionLongitude.',
                "latitude": '.$postionLatitude.',
                "altitude": 0
            },
            "managingOrganization": {
                "reference": "Organization/'.$organizationId.'"
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
