<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class AllergyIntoleranceLib
{
    protected $_CI;
    protected $_config;
    protected $_auth;

    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->loadDependencies();
    }

    public function getById($allergyIntoleranceId)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/AllergyIntolerance/94b05c94-7429-4e98-bebe-d9cbda19d3d5",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function create($allergyIntoleranceId, $allergyIntoleranceData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/AllergyIntolerance",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "AllergyIntolerance",
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/allergy/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                    "use": "official",
                    "value": "98457729"
                }
            ],
            "clinicalStatus": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical",
                        "code": "active",
                        "display": "Active"
                    }
                ]
            },
            "verificationStatus": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/allergyintolerance-verification",
                        "code": "confirmed",
                        "display": "Confirmed"
                    }
                ]
            },
            "category": [
                "food"
            ],
            "code": {
                "coding": [
                    {
                        "system": "http://snomed.info/sct",
                        "code": "89811004",
                        "display": "Gluten"
                    }
                ],
                "text": "Alergi bahan gluten, khususnya ketika makan roti gandum"
            },
            "patient": {
                "reference": "Patient/100000030009",
                "display": "Budi Santoso"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f",
                "display": "Kunjungan Budi Santoso di hari Selasa, 14 Juni 2022"
            },
            "recordedDate": "2022-06-14T15:37:31+07:00",
            "recorder": {
                "reference": "Practitioner/N10000001"
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function update($allergyIntoleranceId, $allergyIntoleranceData)
    {
        $accessToken = $this->_auth->getAccessToken();
        $baseUrl = $this->_config['satusehat_base_url'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $baseUrl."/AllergyIntolerance/94b05c94-7429-4e98-bebe-d9cbda19d3d5",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_POSTFIELDS =>'{
            "resourceType": "AllergyIntolerance",
            "id" : "94b05c94-7429-4e98-bebe-d9cbda19d3d5",
            "identifier": [
                {
                    "system": "http://sys-ids.kemkes.go.id/allergy/ed617f64-c335-458b-bcb3-7a8cb5ae6655",
                    "use": "official",
                    "value": "98457729"
                }
            ],
            "clinicalStatus": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical",
                        "code": "resolved",
                        "display": "resolved"
                    }
                ]
            },
            "verificationStatus": {
                "coding": [
                    {
                        "system": "http://terminology.hl7.org/CodeSystem/allergyintolerance-verification",
                        "code": "confirmed",
                        "display": "Confirmed"
                    }
                ]
            },
            "category": [
                "food"
            ],
            "code": {
                "coding": [
                    {
                        "system": "http://snomed.info/sct",
                        "code": "89811004",
                        "display": "Gluten"
                    }
                ],
                "text": "Alergi bahan gluten, khususnya ketika makan roti gandum"
            },
            "patient": {
                "reference": "Patient/100000030009",
                "display": "Budi Santoso"
            },
            "encounter": {
                "reference": "Encounter/b0f26937-2340-43ca-a9ae-367d0160b47f",
                "display": "Kunjungan Budi Santoso di hari Selasa, 14 Juni 2022"
            },
            "recordedDate": "2022-06-14T15:37:31+07:00",
            "recorder": {
                "reference": "Practitioner/N10000001"
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    protected function loadDependencies(): void
    {
        $this->_CI->load->library('SatuSehat/ConfigLib', null, 'SatuSehatConfig');
        $this->_config = $this->_CI->SatuSehatConfig->getConfig();
        
        $this->_CI->load->library('SatuSehat/AuthLib', null, 'SatuSehatAuth');
        $this->_auth = $this->_CI->SatuSehatAuth;
    }
}
